' EventHandlers
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Option Explicit

Sub doRecalcAll()
Application.CalculateFull
Application.Calculation = xlCalculationAutomatic
Application.StatusBar = False
Application.ScreenUpdating = True
End Sub

