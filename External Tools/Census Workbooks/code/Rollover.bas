' Rollover
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI


Option Explicit
Option Compare Text
Sub doRollover()
Dim wkName As String
Dim src As ListObject
Dim selectedSchool As String

Dim dest As ListObject

Dim SelectedList As String

On Error GoTo ErrorHandler

' get rid of spaces - which are not in 'SchoolStaff'
SelectedList = Replace(Range("selectedListName"), " ", "")
wkName = Range("selectedWorkbookName")
If wkName > "" Then
Else
        MsgBox "No source workbook selected. Open the source workbook, then select it form the list", vbExclamation, "Merge"
        Exit Sub
End If
If Range("AllOrOneSchool") = 2 Then
    If Range("mergeSelectedSchool") > 0 Then
        selectedSchool = Range("mergeSelectedSchoolName")
    Else
        MsgBox "Select a school from the list, or else select to merge All Schools", vbExclamation, "Merge"
        Exit Sub
    End If
End If


' get the source school year in the form SY2016-2017
Dim srcYear As String
Dim dstYear As String
Dim calcYear As String
srcYear = getSheetObject(wkName, "Settings").Range("nm_SchoolYear")
dstYear = shtSettings.Range("nm_SchoolYear")
Dim yr As Integer

yr = Val(Right(srcYear, 4))
calcYear = "SY" & yr & "-" & (yr + 1)

If calcYear <> dstYear Then
    If MsgBox("School year will be updated to " & calcYear, vbInformation + vbOKCancel, "Rollover") = vbCancel Then
        Exit Sub
    End If
    shtSettings.Range("nm_SchoolYear").Value = calcYear
End If


' roll over the student list
shtStudents.Unprotect
Set dest = shtStudents.ListObjects(1)
Set src = getListObject(wkName, "Students")
If selectedSchool <> "" Then
    RollFilteredSchoolList dest, src, "School Name", selectedSchool
Else
    RollFilteredSchoolList dest, src
End If
doProtect shtStudents
        
' merge the other lists
shtStaff.Unprotect
Set dest = shtStaff.ListObjects(1)
Set src = getListObject(wkName, "SchoolStaff")
If selectedSchool <> "" Then
    MergeFilteredListObject dest, src, "School Name", selectedSchool
Else
    MergeFilteredListObject dest, src
End If
doProtect shtStaff

shtSchools.Unprotect
Set dest = shtSchools.ListObjects(1)
Set src = getListObject(wkName, "Schools")
If selectedSchool <> "" Then
    MergeFilteredListObject dest, src, "School Name", selectedSchool
Else
    MergeFilteredListObject dest, src
End If
doProtect shtSchools

shtWash.Unprotect
Set dest = shtWash.ListObjects(1)
Set src = getListObject(wkName, "Wash")
If selectedSchool <> "" Then
    MergeFilteredListObject dest, src, "School Name", selectedSchool
Else
    MergeFilteredListObject dest, src
End If
doProtect shtWash

Exit Sub

ErrorHandler:

    


End Sub

'' roll over a single row read from the src table, into the dest table

''
'' the rollover depends on these fields of the Src
'' - Completed
'' - Outcome
'' - Grade

'' COMPLETED - NO
'' -> Outcome = Repeat - Rollover Grade is current grade, school is current school
'' -> any other - no rollover

'' COMPLETED - YES
'' Grade = Last Grade -> no rollkover created
'' Otherwise - rollover grade is next grade, school is current school

'' TRANSFERS - transfers are then manually adjusted

Sub RollSchoolList(dest As ListObject, src As ListObject)

'' first get a dictionary of all the fileds  we want to copy
'' ie fields that exist in both dest and src, and are not calculated

Dim col As ListColumn
Dim colName As Variant

Dim matches As Collection
Set matches = New Collection
For Each col In dest.ListColumns
    ' is it calculated?
    Dim r As Range
    Set r = col.DataBodyRange(1, 1)     ' always is at least one row
    If Left(r.Formula & " ", 1) = "=" Then
    ' its calculated - don't copy it
    Else
        ' mark to copy if it is in the source as well
        Dim srcCol As ListColumn
        For Each srcCol In src.ListColumns
            If srcCol.name = col.name Then
                matches.Add col.name
                Exit For
            End If
        Next
    End If
Next


'' now step through the rows in src, moving the data into dest

Dim srcRow As ListRow
Dim destRow As ListRow
Dim isAdding As Boolean
Dim continue As String

Dim srcRowCount As Integer
srcRowCount = src.ListRows.Count

Dim destIdx As Integer

' aim for better performance by turning off screen updates and recalc
' be sure to turn on again in event of error

On Error GoTo ErrorHandler

'Application.ScreenUpdating = False
Application.Calculation = xlCalculationManual

For Each srcRow In src.ListRows
    ' are we adding or updating? to do...
    '' first, look at the source to see if this is a row that needs to be rolled
    If srcRow.Index Mod 100 = 0 Then
        Debug.Print Now
        Application.StatusBar = "Student Rollover " & srcRow.Index & " of " & srcRowCount
        If srcRow.Index Mod 1000 = 0 Then
           Application.Calculate
        End If
        Application.ScreenUpdating = True
        DoEvents
        Application.ScreenUpdating = False
    End If
    continue = isContinuing(src, srcRow.Index)
    If continue <> "" Then
    
        Set destRow = dest.ListRows.Add(, True)
    
        '' step through the matched columns, and move the value from srcRow to destRow
        For Each colName In matches
            Select Case colName
                Case "Grade Level"
                    Select Case continue
                        Case "P" ' promoted
                            ' the next grade
                            dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = _
                                nextGrade(src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1))
                        Case Else
                            ' copy the current grade, (for Repeaters
                            dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = _
                                src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1)
                    End Select
                Case "From"     ' where did the pupil come from?
                    Select Case continue
                        Case "R"        ' repeater
                            dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = "Repeater"
                        Case Else ' leave blank
                            dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = ""
                    End Select
                Case Else
                ''dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) =
                    destRow.Range.Cells(1, dest.ListColumns(colName).Index) = _
                        src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1)
            End Select
        Next
    End If
Next
    
Application.ScreenUpdating = True
Application.CalculateFull
Application.Calculation = xlCalculationAutomatic

Exit Sub

ErrorHandler:
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "An error occured rolling over student data: " & Err.Description, vbCritical, "Rollover"
End Sub

'' roll over a single row read from the src table, into the dest table

''
'' the rollover depends on these fields of the Src
'' - Completed
'' - Outcome
'' - Grade

'' COMPLETED - NO
'' -> Outcome = Repeat - Rollover Grade is current grade, school is current school
'' -> any other - no rollover

'' COMPLETED - YES
'' Grade = Last Grade -> no rollkover created
'' Otherwise - rollover grade is next grade, school is current school

'' TRANSFERS - transfers are then manually adjusted

Sub RollFilteredSchoolList(dest As ListObject, src As ListObject, Optional filterColumn As String = "", Optional filterValue As Variant)

'' first get a dictionary of all the fileds  we want to copy
'' ie fields that exist in both dest and src, and are not calculated

Dim col As ListColumn
Dim colName As Variant

Dim matches As Collection
Set matches = New Collection
For Each col In dest.ListColumns
    ' is it calculated?
    Dim r As Range
    Set r = col.DataBodyRange(1, 1)     ' always is at least one row
    If Left(r.Formula & " ", 1) = "=" Then
    ' its calculated - don't copy it
    Else
        ' mark to copy if it is in the source as well
        Dim srcCol As ListColumn
        For Each srcCol In src.ListColumns
            If srcCol.name = col.name Then
                matches.Add col.name
                Exit For
            End If
        Next
    End If
Next


'' now step through the rows in src, moving the data into dest

Dim srcRow As ListRow
Dim destRow As ListRow
Dim isAdding As Boolean
Dim continue As String

Dim srcRowCount As Integer
srcRowCount = src.ListRows.Count

Dim destIdx As Integer

' aim for better performance by turning off screen updates and recalc
' be sure to turn on again in event of error

On Error GoTo ErrorHandler

Dim idx As Integer
If filterColumn <> "" Then
    idx = src.ListColumns(filterColumn).Index
End If

'Application.ScreenUpdating = False
Application.Calculation = xlCalculationManual

For Each srcRow In src.ListRows
    ' are we adding or updating? to do...
    '' first, look at the source to see if this is a row that needs to be rolled
    
    If srcRow.Index Mod 100 = 0 Then
        Debug.Print Now
        Application.StatusBar = "Student Rollover " & srcRow.Index & " of " & srcRowCount
        If srcRow.Index Mod 1000 = 0 Then
           Application.Calculate
        End If
        Application.ScreenUpdating = True
        DoEvents
        Application.ScreenUpdating = False
    End If
    
    If filterColumn = "" Or srcRow.Range.Cells(1, idx).Value = filterValue Then
        continue = isContinuing(src, srcRow.Index)
        If continue <> "" Then
        
            Set destRow = dest.ListRows.Add(, True)
        
            '' step through the matched columns, and move the value from srcRow to destRow
            For Each colName In matches
                Select Case colName
                    Case "Grade Level"
                        Select Case continue
                            Case "P" ' promoted
                                ' the next grade
                                dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = _
                                    nextGrade(src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1))
                            Case Else
                                ' copy the current grade, (for Repeaters
                                dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = _
                                    src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1)
                        End Select
                    Case "From"     ' where did the pupil come from?
                        Select Case continue
                            Case "R"        ' repeater
                                dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = "Repeater"
                            Case Else ' leave blank
                                dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) = ""
                        End Select
                    Case Else
                    ''dest.ListColumns(colName).DataBodyRange.Cells(destRow.Index, 1) =
                        destRow.Range.Cells(1, dest.ListColumns(colName).Index) = _
                            src.ListColumns(colName).DataBodyRange.Cells(srcRow.Index, 1)
                End Select
            Next
        End If
    End If
Next
    
Application.ScreenUpdating = True
Application.CalculateFull
Application.Calculation = xlCalculationAutomatic

Exit Sub

ErrorHandler:
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "An error occured rolling over student data: " & Err.Description, vbCritical, "Rollover"
End Sub


Function gradeIndex(grade As String)

gradeIndex = Application.WorksheetFunction.Match(grade, ThisWorkbook.GradeList, False)

End Function

Function isLastGrade(grade As String)

isLastGrade = (gradeIndex(grade) = ThisWorkbook.GradeList.Rows.Count)

End Function

Function gradeyearOfEd(ByVal grade As String)
gradeyearOfEd = ThisWorkbook.ClassLevelsList.ListRows(gradeIndex(grade)).Range(1, 2)
End Function

' next grade must advance the YearOfEd - this accounts for RMI Grade 8 => GRade 9 but not to GRade Pre-9
Function nextGrade(ByVal grade As String) As String
Dim currentYearOfEd As Integer

currentYearOfEd = gradeyearOfEd(grade)
Do While currentYearOfEd = gradeyearOfEd(grade)
    If isLastGrade(grade) Then
        nextGrade = ""
        Exit Function
    Else
        grade = ThisWorkbook.GradeList.Cells(gradeIndex(grade) + 1, 1)
    End If
Loop
nextGrade = grade
End Function
'
' Returns -> P - promoted
'' R - > Repeat
' '' -> not continiuing
Function isContinuing(src As ListObject, srcRow As Integer) As String

Dim completed As String
Dim outcome As String

Dim grade As String

completed = listValue(src, srcRow, "Completed?")
outcome = listValue(src, srcRow, "Outcome")
grade = listValue(src, srcRow, "Grade Level")

Select Case completed
    Case "Yes"
        ' continuing if not in the last grade
        If (isLastGrade(grade)) Then
            isContinuing = ""
        Else
            isContinuing = "P"
        End If
    Case "No"
        If (outcome = "To Repeat") Then
            isContinuing = "R"
        Else
            isContinuing = ""
        End If
    Case Else
        isContinuing = ""
End Select
End Function


Function listValue(src As ListObject, srcRow As Integer, columnName As String)
listValue = src.ListColumns(columnName).DataBodyRange.Cells(srcRow, 1)
End Function
'' get the school year rnage from the source workbook
Function srcYear()

End Function
