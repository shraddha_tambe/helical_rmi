' ThisWorkbook
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI


Dim X As New AppEvents

Private Sub Workbook_BeforeClose(Cancel As Boolean)
If X Is Nothing Then
Else
    Set X.app = Nothing
    Set X = Nothing
End If
End Sub

Private Sub Workbook_Open()

doProtect shtStudents
doProtect shtStaff
doProtect shtWash

Set X.app = Application
End Sub

'----------------------------------------------
' globals
'----------------------------------------------

Property Get SurveyYear() As String
SurveyYear = Range("nm_SchoolYear")
End Property


'----------------------------------------------------------------------------
' Properties of the workbook to simplify access to listobjects and pivottables
'----------------------------------------------------------------------------

'
' principal data entry lists
'

Property Get SchoolList() As ListObject
Set SchoolList = shtSchools.ListObjects(1)
End Property
Property Get StudentList() As ListObject
Set StudentList = shtStudents.ListObjects(1)
End Property
Property Get StaffList() As ListObject
Set StaffList = shtStaff.ListObjects(1)
End Property
Property Get WashList() As ListObject
Set WashList = shtWash.ListObjects(1)
End Property

'
' Settings lists
'
Property Get ClassLevelsList() As ListObject
Set ClassLevelsList = shtLists.ListObjects("lstClassLevels")
End Property


Property Get GradeList() As Range
Set GradeList = Range("lstGrades")
End Property

'-------------------------------------------
' merge and rollover
'-------------------------------------------
Property Get MergeSchools() As Range
Set MergeSchools = Range("MergeSchools")
End Property

'
' list to merge ( or (all))'
Property Get SelectedListName() As String
SelectedListName = Replace(Range("selectedListName"), " ", "")
End Property
Property Let SelectedListName(selected As String)
SelectedList = Application.Match(selected, Range("lists"), False)
End Property


Property Get SelectedList() As Integer
SelectedList = Range("selectedList")
End Property

Property Let SelectedList(ByVal selected As Integer)
Range("selectedList") = selected
End Property

Property Get BulkExportPath()
BulkExportPath = Range("BulkExportPath")
End Property

Property Get BulkExportName()
Dim strPath As String
strPath = BulkExportPath
If Right(strPath, 1) <> "\" Then
    strPath = strPath & "\"
End If
BulkExportName = strPath & MergeSelectedSchoolNo & " " & SurveyYear & ".xlsm"
End Property
'-------------------------------------------------
' pointers, current values, list indexes
'-------------------------------------------------

'
' index to the selected school for merge and rollover
Property Get MergeSelectedSchool() As Integer
MergeSelectedSchool = Range("MergeSelectedSchool")
End Property
Property Let MergeSelectedSchool(selected As Integer)
Range("MergeSelectedSchool") = selected
End Property


' name of the selected school for merge and rollover
Property Get MergeSelectedSchoolName() As String
MergeSelectedSchoolName = Range("MergeSelectedSchoolName")
End Property
' name of the selected school for merge and rollover
Property Get MergeSelectedSchoolNo() As String
MergeSelectedSchoolNo = Range("MergeSelectedSchoolNo")
End Property




