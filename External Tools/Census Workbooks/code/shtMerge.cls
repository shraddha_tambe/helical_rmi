' shtMerge
' Version date: 20181019
' Version msg: Applied changes to Merge and Rollover from RMI

Option Explicit

Private Sub Worksheet_Activate()
' this is to make sure the list of workbooks is up to date
' this is better than having a volatile function
On Error GoTo ErrorHandler
Application.CalculateFull
setButtonVisibility
Exit Sub

ErrorHandler:
    Debug.Print "ERROR: shtMerge_Activate: " & Err.Description
End Sub

Property Get clearButton() As Shape
Set clearButton = Me.Shapes("cmdClearList")
End Property

Property Get gotoButton() As Shape
Set gotoButton = Me.Shapes.Item("cmdGotoList")
End Property


Sub setButtonVisibility()
Dim SelectedList As String
SelectedList = Replace(Range("selectedListName"), " ", "")

Dim f As Boolean

f = (SelectedList = "(all)")

clearButton.Visible = Not f
gotoButton.Visible = Not f
End Sub



