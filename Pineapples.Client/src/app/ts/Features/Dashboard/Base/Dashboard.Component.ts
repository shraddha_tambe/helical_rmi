﻿namespace Pineapples.Dashboards {
  export interface IKeyValuePair<K, V> {
    key: K;
    value: V;
  }
  export interface IDashboard {
    options: any;             // a core options object 
    hub: Sw.IHub;

    // child selection support

   /**
    * Make the specifed child the selectedChild
    * return the selected Child
    **/
    setSelected(childId: string): string;

   /**
    * if the specified child is the selected child, unselect it; otherwise, select it
    * return the selected Child
    **/
    toggleSelected(childId: string): string;

   /**
    * return true if the specified child is the selected child
    **/
    isSelected(childId: string): boolean;

    /**
     * return true if the specified child is Unselected; ie some other child is selected
     **/
    isUnselected(childId: string): boolean;

   /**
    * return the currently selected child, empty strng if no selection
    **/
    readonly selectedChild: string;

  }

  // a dashboard that exposes a crossfilter object for its children to display and manipulate
  export interface ICrossfilterDashboard extends IDashboard{
    // a data table managed by the dashboard
    table: any[];
    deflated: Sw.xFilter.IDeflatedTable;
    // crossfilter object and dimensions
    xf: CrossFilter.CrossFilter<any>;

   /**
    * when data is available, create the required crossfilter dimensions
    **/
    createDimensions(): void;
  }

  /**
   * Base implementation of Dashboard
   */
  export class Dashboard implements IDashboard {


    public options: any;
    public hub: Sw.IHub;

    // the dashboard reports > 0 when it has established its expected data and layout
    // thereafter, the dashboard increments this value if it neeeds to report data changes
    // this value is a binding on each child, so onChanges is triggered each time it changes
    public dashboardReady = 0;

    // utlity class for cross filters but made available here for access to reflate
    public xFilter: Sw.xFilter.XFilter;  

    //-----------------------------------------------------
    // child Selection Implementation
    //-----------------------------------------------------
    private _selectedChild: string = "";

    public isSelected(childId: string): boolean {
      return (childId === this._selectedChild);
    }

    public isUnselected(childId: string): boolean {
      return (this._selectedChild != childId && this._selectedChild !== "");
    }

    public setSelected(childId: string): string {
      this._selectedChild = childId;
      return this._selectedChild;
    }

    public boundToggleSelected: (child: string) => string; // not required - remove

    public toggleSelected = (childId: string) => {
      if (childId === this._selectedChild) {
        this._selectedChild = "";
      } else {
        this._selectedChild = childId;
      }
      return this._selectedChild;
    }

    public get selectedChild() {
      return this._selectedChild;
    }

    //-------------------------------------------------------------
    // Constructor and injections
    //------------------------------------------------------------

    static $inject = ["$rootScope", "$mdDialog", "$state", "$http", "$q", "Lookups"];
    constructor(rootScope: Sw.IRootScopeEx
      , public mdDialog: ng.material.IDialogService
      , protected $state: ng.ui.IStateService
      , public http: ng.IHttpService
      , public $q: ng.IQService
      , public lookups: Sw.Lookups.LookupService
        ) {
      this.hub = rootScope.hub;
      this.options = new coreOptions(this.hub);
      this.xFilter = new Sw.xFilter.XFilter();

      this.options.subscribe(this.onOptionChange.bind(this));
      this.boundToggleSelected = this.toggleSelected.bind(this);  // remove 
    }

    // optionChange is bound to a client to communicate any option change
    public optionChange: any;
    // this is the data relayed from the options object if something has changed
    public onOptionChange(data, sender) {
      this.optionChange = data;
    }

    /** Utility method to return a function that will "toggle" the value of a nominated option
     * @option the name of the option to manage e.g. selectedDistrict
     * In the generated function, newValue is the (scalar) value to assign to this property
     * earlier usages had a key/value object.
     * See the comments in DsChart on binding options for onChartClick 
    **/
    public optionToggler = _.memoize(
      (option: string) => (newValue) => {
        if (!newValue) { return; }
        this.options[option] = newValue === this.options[option] ? null : newValue;
      }
    );      
  }

  export class CrossfilterDashboard extends Dashboard implements ICrossfilterDashboard {
    public table: any[];      // the raw data 

    // allow for deflated data to be passed as a binding
    public deflated: Sw.xFilter.IDeflatedTable;

    // crossfilter object and dimensions
    public xf: CrossFilter.CrossFilter<any>;
     
    // the dashboard can supply some precreated dimensions from its crossfilter
    // these should be declared as public members, and constructed when data is available
    // e.g.
    //public dimSurveyYear: CrossFilter.Dimension<IxfData, number>;


    public $onChanges(changes) {
      if (changes.deflated && changes.deflated.currentValue) {
        // deflated data has been retirved by the ui-router as a Resolve
        // and pass to the dashboard as a binding - process it into a crossfilter
        this.makeCrossfilter(this.deflated);
      }
    }


    public $onInit() {
      // if we get to onInit, without having the deflated data injected, 
      // then assume we go fetch it
      if (!this.deflated) {
        this.fetchData().then((deflated) => {
          this.deflated = deflated;
          this.makeCrossfilter(this.deflated);
        })
          .catch((error) => {
          })
          .finally(() => {
          });
      }
    }

    // virtual Members
    // the Dashboard may get its own data, or expect that data to be injected by the routing
    // in either case it must call makeCrossfilter to generate the crossfilter object
    // from the deflated table
    // it must increment dashboardReady when the crossfilter is up-to-date
    protected makeCrossfilter(deflated: Sw.xFilter.IDeflatedTable) {

      this.table = <any[]>this.xFilter.reflate(this.deflated);
      // create the crossfilter from the warehouse table
      this.xf = crossfilter(this.table);
      this.createDimensions();
      this.dashboardReady += 1;
    }

    // virtual method for concrete instances to create the dimensions that their children
    // will expect
    public createDimensions() {
    }

    // crossfilter dashboards will need to implement this, so that the options are applied as filters to the 
    // cross tab dimensions 
    // any implementation should call super implementation
    onOptionChange(data, sender) {
      super.onOptionChange(data, sender);
      // the update of the current dc obects happens here
      dc.redrawAll();
    }


    public fetchData(): ng.IPromise<Sw.xFilter.IDeflatedTable> {
      return null;
    }
  }
}
