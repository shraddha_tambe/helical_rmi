﻿namespace Pineapples.Dashboards {

  class Controller extends DashboardChild implements IDashboardChild {

    // reference to the host page is from the 'require' on the componentOptions
    public dashboard: SchoolsDashboard;      

    // groups 
    public grpAuthorityCode: CrossFilter.Group<Enrolments.IxfData, string, number>;

    public onOptionsChanged(data, sender) {
    }
 
    public onDashboardReady() {
      this.grpAuthorityCode = this.dashboard.dimAuthorityCode.group().reduceSum(Enrolments.va);
    }
  }

  angular
    .module("pineapples")
    .component("enrolmentLevelByAuthority", new EnrolComponent(Controller, "EnrolmentLevelByAuthority"));
}
