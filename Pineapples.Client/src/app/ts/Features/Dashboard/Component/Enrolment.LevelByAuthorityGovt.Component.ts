﻿namespace Pineapples.Dashboards {

  class Controller extends DashboardChild implements IDashboardChild {

    public dashboard: SchoolsDashboard;      
    public grpAuthorityGovt: CrossFilter.Group<Enrolments.IxfData, string, number>;
                                                                           
    public onDashboardReady() {
      this.grpAuthorityGovt = this.dashboard.dimAuthorityGovt.group().reduceSum(d => d.EnrolM + d.EnrolF);
    }
  }

  angular
    .module("pineapples")
    .component("enrolmentLevelByAuthorityGovt", new EnrolComponent(Controller, "EnrolmentLevelByAuthorityGovt"));
}
