﻿namespace Pineapples.Dashboards {

  class Controller extends DashboardChild implements IDashboardChild {


    // reference to the host page is from the 'require' on the componentOptions
    public dashboard: SchoolsDashboard;      

    // groups 
    public grpDistrictCode: CrossFilter.Group<Enrolments.IxfData, string, number>;
    public tabYear_District: CrossFilter.Group<Enrolments.IxfData, string, any>;

    public onOptionsChanged(data, sender) {
    }
 
    public onDashboardReady() {
      let xFilter = this.dashboard.xFilter;

      // crossfilter objects requried by this component
      this.grpDistrictCode = this.dashboard.dimDistrictCode.group().reduceSum(Enrolments.va);
      // crosstab by distinct by year, with gender
      this.tabYear_District = xFilter.xReduce(this.dashboard.dimDistrictCode, xFilter.getPropAccessor("SurveyYear"), Enrolments.vaGendered);
    }
  }

  angular
    .module("pineapples")
    .component("enrolmentLevelByDistrict", new EnrolComponent(Controller, "EnrolmentLevelByDistrict"));
}
