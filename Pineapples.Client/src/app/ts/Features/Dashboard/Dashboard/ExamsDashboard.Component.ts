﻿namespace Pineapples.Dashboards {

  export class ExamsDashboard extends Dashboard {

    public xFilter: Sw.xFilter.XFilter;
    public deflatedExamsschoolresults: Sw.xFilter.IDeflatedTable;
    
    public examsschoolresults: any;

    public exams = (year) => _.uniq(
      this.examsschoolresults
        .filter(({ ExamYear }) => ExamYear === year)
        .map(({ Exam }) => Exam)
    );

    public standardsForExam = _.memoize(
      (exam, year) => _.uniq(
        this.examsschoolresults
          .filter(({ ExamYear }) => ExamYear === year)
          .filter(({ Exam }) => Exam === exam)
          .map(({ ExamStandard }) => ExamStandard)
      ),
      (year, examStandard) => [year, examStandard]);

    public dataExamResultsByStandardDistrict = _.memoize((year, exam) => {
      const mPercent = (level, sign) => x => ({
        ExamStandardState: x['ExamStandardState'] + ' - M',
        percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
      })
      const fPercent = (level, sign) => x => ({
        ExamStandardState: x['ExamStandardState'] + ' - F',
        percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
      })
      const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

      return _(this.examsschoolresults)
        .filter(({ ExamYear }) => ExamYear === year)
        .filter(_.matchesProperty('Exam', exam))
        .map(x => _.assign({}, x, { ExamStandardState: `${x.ExamStandard} - ${x.DistrictCode}` }))

        .groupBy(_.property('ExamStandardState'))
        .mapValues((x: any[]) => x.reduce((acc, val) => _.assign({}, val,
          {
            CompetentM: acc.CompetentM + val.CompetentM || 0,
            CompetentF: acc.CompetentF + val.CompetentF || 0,
            MinimallyCompetentF: acc.MinimallyCompetentF + val.MinimallyCompetentF || 0,
            MinimallyCompetentM: acc.MinimallyCompetentM + val.MinimallyCompetentM || 0,
            ApproachingCompetenceM: acc.ApproachingCompetenceM + val.ApproachingCompetenceM || 0,
            ApproachingCompetenceF: acc.ApproachingCompetenceF + val.ApproachingCompetenceF || 0,
            WellBelowCompetentM: acc.WellBelowCompetentM + val.WellBelowCompetentM || 0,
            WellBelowCompetentF: acc.WellBelowCompetentF + val.WellBelowCompetentF || 0,
            CandidatesM: acc.CandidatesM + val.CandidatesM || 0,
            CandidatesF: acc.CandidatesF + val.CandidatesF || 0
          }
        ), { CompetentM: 0, CompetentF: 0, MinimallyCompetentF: 0, MinimallyCompetentM: 0, ApproachingCompetenceM: 0, ApproachingCompetenceF: 0, WellBelowCompetentM: 0, WellBelowCompetentF: 0, CandidatesM: 0, CandidatesF: 0 }
        ))
        .values()
        .map(data =>
          [
            { meta: 'Approaching competence', color: "#FFC000", data: _.flatten(genderPercent('ApproachingCompetence', -1)(data)) },
            { meta: 'Well below competent', color: "#FF0000", data: _.flatten(genderPercent('WellBelowCompetent', -1)(data)) },
            { meta: 'Minimally competent', color: "#92D050", data: _.flatten(genderPercent('MinimallyCompetent', 1)(data)) },
            { meta: 'Competent', color: "#00B050", data: _.flatten(genderPercent('Competent', 1)(data)) },
          ])
        .flatten()
        .groupBy(_.property('meta'))
        .mapValues(x => _.assign({}, x[0], { data: _(x).map(_.property('data')).flatten().sortBy('ExamStandardState').value() })).values()
        .value()

    },
      (year, exam) => [year, exam]);


    public dataExamResultsByStandardThreeYears = _.memoize((year, exam) => {
      const startYear = year - 2
      const mPercent = (level, sign) => x => ({
        StandardYear: x['StandardYear'] + ' - M',
        percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
      })
      const fPercent = (level, sign) => x => ({
        StandardYear: x['StandardYear'] + ' - F',
        percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
      })
      const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

      return _(this.examsschoolresults)
        .filter(({ ExamYear }) => ExamYear <= year)
        .filter(({ ExamYear }) => ExamYear >= startYear)
        .filter(_.matchesProperty('Exam', exam))
        .map(x => _.assign({}, x, { StandardYear: `${x.ExamStandard} - ${x.ExamYear} ` }))

        .groupBy(_.property('StandardYear'))
        .mapValues((x: any[]) => x.reduce((acc, val) => _.assign({}, val,
          {
            CompetentM: acc.CompetentM + val.CompetentM || 0,
            CompetentF: acc.CompetentF + val.CompetentF || 0,
            MinimallyCompetentF: acc.MinimallyCompetentF + val.MinimallyCompetentF || 0,
            MinimallyCompetentM: acc.MinimallyCompetentM + val.MinimallyCompetentM || 0,
            ApproachingCompetenceM: acc.ApproachingCompetenceM + val.ApproachingCompetenceM || 0,
            ApproachingCompetenceF: acc.ApproachingCompetenceF + val.ApproachingCompetenceF || 0,
            WellBelowCompetentM: acc.WellBelowCompetentM + val.WellBelowCompetentM || 0,
            WellBelowCompetentF: acc.WellBelowCompetentF + val.WellBelowCompetentF || 0,
            CandidatesM: acc.CandidatesM + val.CandidatesM || 0,
            CandidatesF: acc.CandidatesF + val.CandidatesF || 0
          }
        ), { CompetentM: 0, CompetentF: 0, MinimallyCompetentF: 0, MinimallyCompetentM: 0, ApproachingCompetenceM: 0, ApproachingCompetenceF: 0, WellBelowCompetentM: 0, WellBelowCompetentF: 0, CandidatesM: 0, CandidatesF: 0 }
        ))
        .values()
        .map(data =>
          [
            { meta: 'Approaching competence', color: "#FFC000", data: _.flatten(genderPercent('ApproachingCompetence', -1)(data)) },
            { meta: 'Well below competent', color: "#FF0000", data: _.flatten(genderPercent('WellBelowCompetent', -1)(data)) },
            { meta: 'Minimally competent', color: "#92D050", data: _.flatten(genderPercent('MinimallyCompetent', 1)(data)) },
            { meta: 'Competent', color: "#00B050", data: _.flatten(genderPercent('Competent', 1)(data)) },
          ])
        .flatten()
        .groupBy(_.property('meta'))
        .mapValues(x => _.assign({}, x[0], { data: _(x).map(_.property('data')).flatten().sortBy('StandardYear').value() })).values()
        .value()

    },
      (year, exam) => [year, exam]);

    public dataExamResultsByStandardThreeYearsGenderColors = _.memoize((year, exam) => {
      const startYear = year - 2
      const mPercent = (level, sign) => x => ({
        StandardYear: x['StandardYear'] + ' - M',
        percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
      })
      const fPercent = (level, sign) => x => ({
        StandardYear: x['StandardYear'] + ' - F',
        percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
      })
      const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

      return _(this.examsschoolresults)
        .filter(({ ExamYear }) => ExamYear <= year)
        .filter(({ ExamYear }) => ExamYear >= startYear)
        .filter(_.matchesProperty('Exam', exam))
        .map(x => _.assign({}, x, { StandardYear: `${x.ExamStandard} - ${x.ExamYear} ` }))

        .groupBy(_.property('StandardYear'))
        .mapValues((x: any[]) => x.reduce((acc, val) => _.assign({}, val,
          {
            CompetentM: acc.CompetentM + val.CompetentM || 0,
            CompetentF: acc.CompetentF + val.CompetentF || 0,
            MinimallyCompetentF: acc.MinimallyCompetentF + val.MinimallyCompetentF || 0,
            MinimallyCompetentM: acc.MinimallyCompetentM + val.MinimallyCompetentM || 0,
            ApproachingCompetenceM: acc.ApproachingCompetenceM + val.ApproachingCompetenceM || 0,
            ApproachingCompetenceF: acc.ApproachingCompetenceF + val.ApproachingCompetenceF || 0,
            WellBelowCompetentM: acc.WellBelowCompetentM + val.WellBelowCompetentM || 0,
            WellBelowCompetentF: acc.WellBelowCompetentF + val.WellBelowCompetentF || 0,
            CandidatesM: acc.CandidatesM + val.CandidatesM || 0,
            CandidatesF: acc.CandidatesF + val.CandidatesF || 0
          }
        ), { CompetentM: 0, CompetentF: 0, MinimallyCompetentF: 0, MinimallyCompetentM: 0, ApproachingCompetenceM: 0, ApproachingCompetenceF: 0, WellBelowCompetentM: 0, WellBelowCompetentF: 0, CandidatesM: 0, CandidatesF: 0 }
        ))
        .values()
        .map(data =>
          [
            { meta: 'Approaching competence-F', color: "#FFC000", data: fPercent('ApproachingCompetence', -1)(data) },
            { meta: 'Well below competent-F', color: "#FF0000", data: fPercent('WellBelowCompetent', -1)(data) },
            { meta: 'Minimally competent-F', color: "#92D050", data: fPercent('MinimallyCompetent', 1)(data) },
            { meta: 'Competent-F', color: "#00B050", data: fPercent('Competent', 1)(data) },
            { meta: 'Approaching competence-M', color: "#8000ff", data: mPercent('ApproachingCompetence', -1)(data) },
            { meta: 'Well below competent-M', color: "#00ff00", data: mPercent('WellBelowCompetent', -1)(data) },
            { meta: 'Minimally competent-M', color: "#d05092", data: mPercent('MinimallyCompetent', 1)(data) },
            { meta: 'Competent-M', color: "#b01500", data: mPercent('Competent', 1)(data) },
          ])
        .flatten()
        .groupBy(_.property('meta'))
        .mapValues(x => _.assign({}, x[0], { data: _(x).map(_.property('data')).flatten().sortBy('StandardYear').value() })).values()
        .value()
    },
      (year, exam) => [year, exam]);

    public dataExamResultsByBenchmarkForYear = _.memoize((year, exam) => {

      const mPercent = (level, sign) => x => ({
        ExamBenchmark: x['ExamBenchmark'] + ' - M',
        percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
      })
      const fPercent = (level, sign) => x => ({
        ExamBenchmark: x['ExamBenchmark'] + ' - F',
        percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
      })
      const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

      return _(this.examsschoolresults)
        .filter(_.matchesProperty('ExamYear', year))
        .filter(_.matchesProperty('Exam', exam))
        .groupBy(_.property('ExamBenchmark'))
        .mapValues((x: any) => x.reduce((acc, val: any) => _.assign({}, val,
          {
            CompetentM: acc.CompetentM + val.CompetentM || 0,
            CompetentF: acc.CompetentF + val.CompetentF || 0,
            MinimallyCompetentF: acc.MinimallyCompetentF + val.MinimallyCompetentF || 0,
            MinimallyCompetentM: acc.MinimallyCompetentM + val.MinimallyCompetentM || 0,
            ApproachingCompetenceM: acc.ApproachingCompetenceM + val.ApproachingCompetenceM || 0,
            ApproachingCompetenceF: acc.ApproachingCompetenceF + val.ApproachingCompetenceF || 0,
            WellBelowCompetentM: acc.WellBelowCompetentM + val.WellBelowCompetentM || 0,
            WellBelowCompetentF: acc.WellBelowCompetentF + val.WellBelowCompetentF || 0,
            CandidatesM: acc.CandidatesM + val.CandidatesM || 0,
            CandidatesF: acc.CandidatesF + val.CandidatesF || 0
          }
        ), { CompetentM: 0, CompetentF: 0, MinimallyCompetentF: 0, MinimallyCompetentM: 0, ApproachingCompetenceM: 0, ApproachingCompetenceF: 0, WellBelowCompetentM: 0, WellBelowCompetentF: 0, CandidatesM: 0, CandidatesF: 0 }
        ))
        .values()
        .groupBy(_.property('ExamStandard'))
        .mapValues((d: any) => d.map(data =>
          [
            {
              data: _.flatten(genderPercent('ApproachingCompetence', -1)(data)),
              color: '#FFC000',
              meta: 'Approaching competence'
            },
            {
              data: _.flatten(genderPercent('WellBelowCompetent', -1)(data)),
              color: '#FF0000',
              meta: 'Well below competent'
            },
            {
              data: _.flatten(genderPercent('MinimallyCompetent', 1)(data)),
              color: '#92D050',
              meta: 'Minimally competent'
            },
            {
              data: _.flatten(genderPercent('Competent', 1)(data)),
              color: '#00B050',
              meta: 'Competent'
            }
          ]))
        .mapValues((x: object[]) => [ { data: _(x).flatten().filter(_.matchesProperty('meta', "Approaching competence")).map(_.property('data')).flatten().value(), color: "#FFC000", meta: "Approaching competence" },
                                 { data: _(x).flatten().filter(_.matchesProperty('meta', "Well below competent")).map(_.property('data')).flatten().value(), color: "#FF0000", meta: "Well below competent" },
                                 { data: _(x).flatten().filter(_.matchesProperty('meta', "Minimally competent")).map(_.property('data')).flatten().value(), color: "#92D050", meta: "Minimally competent" },
                                 { data: _(x).flatten().filter(_.matchesProperty('meta', "Competent")).map(_.property('data')).flatten().value(), color: "#00B050", meta: "Competent" }]).value()

    },
      (year, exam) => [year, exam]);


    public $onInit() {
     console.log('$onInit');
      this.examsschoolresults = <any[]>this.xFilter.reflate(this.deflatedExamsschoolresults);

      console.log('ExamsDashboard.$onInit');
      console.log(this);
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      deflatedExamsschoolresults: "<"
    };
    public controller: any = ExamsDashboard;
    public controllerAs: string = "vm";
    public templateUrl: string = `dashboard/dashboard/ExamsDashboard`;
  }

  angular
    .module("pineapples")
    .component("examsDashboard", new Component());
}
