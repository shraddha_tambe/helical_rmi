﻿namespace Pineapples.Dashboards {

	export class SchoolDashboard extends Dashboard {

		public xFilter: Sw.xFilter.XFilter;

		// bindings
		public rawEnrolment: any;           // bound to name 'enrolment'
		public districtEnrolment: any;
		public school: Pineapples.Schools.School;                 //
		public schoolTeacherCount: any;
		public examsSchoolResults: any;
		public schoolFlow: any;

		public enrolment: any;

		private colors: Array<string> = ['#FF6666', '#FFB266', '#FFFF66', '#B2FF66', '#66FF66', '#66FFB2', '#66FFFF', '#66B2FF', '#6666FF', '#B266FF', '#FF66FF', '#FF66B2', 'C00000', '#C06600', 'CCCC00', '66DD00'];

		public schoolNo() {
			return this.school._id();
		}


		public dataForFlowBarChart;

		public dataForBarChart;

		public dataForLineChart;
		public lastSurvey = () => _(this.school.Surveys).last()['svyYear'];

		public gradesForSchool;
		public dataForSchoolFlowLineChart;



		private national = (year) => x => x.filter(_.matchesProperty('surveyYear', year))
		private district = (district, year) => x => x.filter(_.matchesProperty('surveyYear', year))
			.filter(_.matchesProperty('District', district));

		private classLevels;

		public dataForFemalePercentBarChart; // to be initialised in $onChanges





		public latestEnrolPieChartData = _.memoize(
			() => [{ enrol: _(this.school.Surveys).last()['ssEnrolF'] }, { enrol: _(this.school.Surveys).last()['ssEnrolM'] }]
		);

		public latestEnrolPieChartMetadata = _.memoize(() => ['Female', 'Male']);
		public latestEnrolPieChartColors = _.memoize(() => ["#00b161", "#0964a5"]);

		public dataExamResultsByBenchmarkForYear = _.memoize((year, exam) => {

			const tPercent = (level, sign) => x => ({
				ExamBenchmark: x['ExamBenchmark'] + ' - T',
				percent: Math.round((x[`${level}F`] + x[`${level}M`]) * sign * 1000 / (x['CandidatesF'] + x['CandidatesM']) / 10)
			})

			const mPercent = (level, sign) => x => ({
				ExamBenchmark: x['ExamBenchmark'] + ' - M',
				percent: Math.round((x[`${level}M`]) * sign * 1000 / (x['CandidatesM']) / 10)
			})

			const fPercent = (level, sign) => x => ({
				ExamBenchmark: x['ExamBenchmark'] + ' - F',
				percent: Math.round((x[`${level}F`]) * sign * 1000 / (x['CandidatesF']) / 10)
			})

			const genderPercent = (level, sign) => x => [fPercent(level, sign)(x), mPercent(level, sign)(x)];

			const data = this.examsSchoolResults
				.filter(x => x.ExamYear === year)
				.filter(x => x.SchoolNo === this.school._id())
				.filter(x => x.Exam === exam);

			const groupedData = _.groupBy(data, ({ ExamStandard }) => ExamStandard)

			return _.mapValues(groupedData, data =>
				[
					{
						data: _.flatten(data.map(genderPercent('ApproachingCompetence', -1))),
						color: '#FFC000',
						meta: 'Approaching competence'
					},
					{
						data: _.flatten(data.map(genderPercent('WellBelowCompetent', -1))),
						color: '#FF0000',
						meta: 'Well below competent'
					},
					{
						data: _.flatten(data.map(genderPercent('MinimallyCompetent', 1))),
						color: '#92D050',
						meta: 'Minimally competent'
					},
					{
						data: _.flatten(data.map(genderPercent('Competent', 1))),
						color: '#00B050',
						meta: 'Competent'
					}
				]
			)
		},
			(year, examStandard) => [year, examStandard]);



		public dataForStackedGenderHistory;


		private _classSorter = x => x.ClassLevel && (x.ClassLevel === 'GK' ? -1 : parseInt(x.ClassLevel.slice(1)));

		public enrol: any; // = _.memoize((year: number) => _.sortBy(this.enrolment[year]), this._classSorter);

		public enrolmentYears;

		public exams;

		public standardsForExam;

		public grade = yearOfEd => {
			if (yearOfEd === 0) return 'GK';
			if (yearOfEd === -1) return 'GPK';
			return `G${yearOfEd}`;
		};

		public schoolFlowData;

		public dataForFemalePercentHistoryLineChart;

		public schoolFlowDataValue = (schNo, year, grade, indicator) => {
			const data = this.schoolFlowData(schNo)[`${year}:${grade}`];
			return data && data[indicator] && `${(Math.round(data[indicator] * 10) / 10.0)}`;
		}

		public $onChanges(changes) {
			// novalues are pushed until all values are pushed
			if (this.school) {
				// enrolment has been set or changed
				this._collectData(this.schoolNo());
			}
		}

		private _collectData(schNo) {
			const xFilter = new Sw.xFilter.XFilter();
			// this code is migrated from resolves in dashboard.routes, (line 23 ff) which was removed in commmit
			// fdd4739 (fdd4739fe58e2ac14f3a2e99a7c1b4f390d723d1) 22-03-2019

			// api/schools is not needed now - passed in from School Component
			// although enrolments are available in the school object, 
			// leave this if we want to get district or national averages

			let pEnrol = this.http.get(`api/warehouse/enrolbyschool/${schNo}`);
			let pDistrict = this.http.get(`api/warehouse/districtEnrol`);
			let pTeacher = this.http.get(`api/warehouse/schoolteachercount`);
			let pExam = this.http.get(`api/warehouse/examsschoolresults/${schNo}`);
			let pFlow = this.http.get(`api/warehouse/schoolflowrates`);

			// use $q.all so we only proceeed to construct the various data shapes 
			// when all the data is available
			this.$q.all({ pEnrol, pDistrict, pTeacher, pExam, pFlow })
				.then(response => {
					this.rawEnrolment = xFilter.reflate(<Sw.xFilter.IDeflatedTable>response.pEnrol.data);
					this.districtEnrolment = xFilter.reflate(<Sw.xFilter.IDeflatedTable>response.pDistrict.data);
					this.schoolTeacherCount = xFilter.reflate(<Sw.xFilter.IDeflatedTable>response.pTeacher.data);
					this.examsSchoolResults = xFilter.reflate(<Sw.xFilter.IDeflatedTable>response.pExam.data);
					this.schoolFlow = xFilter.reflate(<Sw.xFilter.IDeflatedTable>response.pFlow.data);
					this.rebuild();
				});
		}
		// sequencing issues mean that functions bound to child components are called before the enrolments are populated
		// so these initialisations have to be moved so that they are called when enrolment changes
		private rebuild() {
			this.enrolment = _(this.rawEnrolment).groupBy((x: any) => [x.surveyYear, x.ClassLevel])
				.map(val => _(val).reduce((acc: any, vv: any) => ({
					surveyYear: vv.surveyYear,
					ClassLevel: vv.ClassLevel,
					EnrolF: (acc.EnrolF || 0) + vv.EnrolF,
					EnrolM: (acc.EnrolM || 0) + vv.EnrolM
				})))
				.groupBy(x => x.surveyYear).value()
			this.enrol = _.memoize((year: number) => _.sortBy(this.enrolment[year], this._classSorter)); // fixes issue 556
			this.enrolmentYears = () => _.keys(this.enrolment);

			this.dataForBarChart = _.memoize((year) => [         // Will need to clear memoize cache for all memoized funcs in $onChange if .enrolment is updated
				this.enrol(year).map(({ ClassLevel, EnrolF: Enrol }) => ({ ClassLevel, Enrol })),
				this.enrol(year).map(({ ClassLevel, EnrolM: Enrol }) => ({ ClassLevel, Enrol }))
			]);


			this.dataForLineChart = _.memoize(() => {
				const colors = ["#34b24c", "#ffa500", "#551a8b"];
				const meta = ['Female', 'Male', 'Total'];

				const addObjs = (a, b) => (<any>_.assign)(
					...Object.keys(b).map(
						key => ({ [key]: (a[key] || 0) + (b[key] || 0) })
					)
				);

				const totals = _(this.enrolment)
					.mapValues(y => _(y)
						.map(({ EnrolF, EnrolM }) => ({ Female: EnrolF, Male: EnrolM, Total: EnrolF + EnrolM }))
						.reduce(addObjs, {}))
					.value()

				return _(meta)
					.map(g => _(totals).pairs()
						.map(x => ({ year: parseInt(x[0]), enrol: x[1][g] }))
						.value())
					.map((dataset, i) => ({ dataset, meta: meta[i], color: colors[i] }))
					.value();
			}, () => this.enrolment);


			this.dataForFemalePercentBarChart = _.memoize((year) => [
				// School Dataset
				_(this.enrol(year))
					.map(({ ClassLevel, EnrolF, EnrolM }) => ({ ClassLevel, Ratio: _.round(100 * EnrolF / (EnrolF + EnrolM), 1) }))
					.sortBy(({ ClassLevel }) => ClassLevel === 'GK' ? -1 : parseInt(ClassLevel.slice(1)))
					.value(),

				// District Dataset
				_(this.district(this.school._id().slice(0, 3), year)(this.districtEnrolment))
					.filter(({ ClassLevel }) => (this.classLevels(year).indexOf(ClassLevel) !== -1))
					.map(({ ClassLevel, EnrolF, EnrolM }) => ({ ClassLevel, Ratio: _.round(100 * EnrolF / (EnrolF + EnrolM), 1) }))
					.value(),

				// National Dataset
				_(this.national(year)(this.districtEnrolment))
					.filter(({ ClassLevel }) => (this.classLevels(year).indexOf(ClassLevel) !== -1))
					.groupBy((x: any) => x.ClassLevel)
					.mapValues((x: any) =>
						x.reduce((acc, val) =>
							_.assign({}, { EnrolF: val.EnrolF + acc.EnrolF, EnrolM: val.EnrolM + acc.EnrolM }), { EnrolF: 0, EnrolM: 0 }))
					.mapValues((x: any) => _.round(100 * x.EnrolF / (x.EnrolF + x.EnrolM), 1))
					.pairs()
					.map(x => ({ ClassLevel: x[0], Ratio: x[1] }))
					.value()
			]);

			this.schoolFlowData = _.memoize(
				(schNo) => _(this.schoolFlow)
					.filter(_.matchesProperty('schNo', schNo))
					.map(x => _.assign({}, x, { grade: this.grade(x.YearOfEd) }))
					.groupBy(({ surveyYear, grade }) => `${surveyYear}:${grade}`)
					.mapValues(_.first)
					.value()
			);

			this.exams = (year) => _.uniq(
				this.examsSchoolResults
					.filter(({ ExamYear }) => ExamYear === year)
					.filter(({ SchoolNo }) => SchoolNo === this.schoolNo())
					.map(({ Exam }) => Exam)
			);

			this.standardsForExam = _.memoize(
				(exam, year) => _.uniq(
					this.examsSchoolResults
						.filter(({ ExamYear }) => ExamYear === year)
						.filter(({ Exam }) => Exam === exam)
						.filter(({ SchoolNo }) => SchoolNo === this.schoolNo())
						.map(({ ExamStandard }) => ExamStandard)
				),
				(year, examStandard) => [year, examStandard]);

			this.gradesForSchool = _.memoize(
				(schNo) => ['GPK', 'GK', 'G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7', 'G8', 'G9', 'G10', 'G11', 'G12']
					.filter(x => _(this.schoolFlowData(schNo)).values().map(_.property('grade')).uniq().sort().value().indexOf(x) != -1)
			);

			this.dataForSchoolFlowLineChart = _.memoize(
				(indicator) =>
					_(this.gradesForSchool(this.school._id()))
						.map((grade, i) => (
							{
								meta: grade,
								color: this.colors[i],
								dataset: [2013, 2014, 2015, 2016, 2017].map(year => (
									{
										year,
										value: Number(this.schoolFlowDataValue(this.school._id(), year, grade, indicator)) || 0
									}
								))
							}
						)).value()
			);

			this.classLevels = (year) => _(this.enrol(year)).pluck('ClassLevel').value()

			this.dataForFemalePercentHistoryLineChart = _.memoize(() => {
				const schoolDataset = _(this.enrolment)
					.map(x => _(x).reduce((acc, { surveyYear, EnrolF, EnrolM }) => ({ year: surveyYear, F: acc.F + EnrolF, M: acc.M + EnrolM, }), { year: 0, F: 0, M: 0 }))
					.map(({ year, F, M }) => ({ year: year, Ratio: _.round(100 * F / (F + M), 1) }))
					.value();

				const years = schoolDataset.map(x => x.year);
				const keepYear = ({ surveyYear }) => _.contains(years, surveyYear);
				return [
					{
						meta: 'School',
						color: "#34b24c",
						dataset: schoolDataset
					},
					{
						meta: 'District',
						color: "#ffa500",
						dataset:
							_(this.districtEnrolment)
								.filter(_.matchesProperty('District', this.school._id().slice(0, 3)))
								.filter(keepYear)
								.groupBy(_.property('surveyYear'))
								.map(x => _(x).reduce((acc, { surveyYear, EnrolF, EnrolM }) => ({ year: surveyYear, F: acc.F + EnrolF, M: acc.M + EnrolM, }), { year: 0, F: 0, M: 0 }))
								.map(({ year, F, M }) => ({ year: year, Ratio: _.round(100 * F / (F + M), 1) }))
								.value()
					},
					{
						meta: 'National',
						color: "#551a8b",
						dataset:
							_(this.districtEnrolment)
								.filter(keepYear)
								.groupBy(_.property('surveyYear'))
								.map(x => _(x).reduce((acc, { surveyYear, EnrolF, EnrolM }) => ({ year: surveyYear, F: acc.F + EnrolF, M: acc.M + EnrolM, }), { year: 0, F: 0, M: 0 }))
								.map(({ year, F, M }) => ({ year: year, Ratio: _.round(100 * F / (F + M), 1) }))
								.value()
					}
				];
			});

			this.dataForStackedGenderHistory = _.memoize(() => {
				const colors = ["#34b24c", "#ffa500"];
				const meta = ['Female', 'Male'];

				const addObjs = (a, b) => (<any>_.assign)(
					...Object.keys(b).map(
						key => ({ [key]: (a[key] || 0) + (b[key] || 0) })
					)
				);

				const totals = _(this.enrolment)
					.mapValues(y => _(y)
						.map(({ EnrolF, EnrolM }) => ({ Female: EnrolF, Male: EnrolM }))
						.reduce(addObjs, {}))
					.value()

				return _(meta)
					.map(g => _(totals).pairs()
						.map(x => ({ year: parseInt(x[0]), enrol: x[1][g] }))
						.value())
					.map((dataset, i) => ({ dataset, meta: meta[i], color: colors[i] }))
					.value();
			}, () => this.enrolment);
			this.dataForFlowBarChart = _.memoize(
				(schNo, year, indicator) => [
					_.map(
						this.gradesForSchool(schNo),
						(grade) => ({
							ClassLevel: grade,
							Value: this.schoolFlowDataValue(schNo, year, grade, indicator)
						}
						)),
				],
				(schNo, year, indicator) => `${schNo} - ${year} - ${indicator}`
			);
		}
	}


	class Component implements ng.IComponentOptions {
		public bindings: any = {
			school: "<",
			////schoolInfo: "<",
			////rawEnrolment: "<enrolment",
			////districtEnrolment: "<",
			////schoolTeacherCount: "<",
			////examsSchoolResults: "<",
			////schoolFlow: "<",
		};
		public controller: any = SchoolDashboard;
		public controllerAs: string = "vm";
		public templateUrl: string = `dashboard/dashboard/SchoolDashboard`;
	}

	angular
		.module("pineapples")
		.component("schoolDashboard", new Component());


}
