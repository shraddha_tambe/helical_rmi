﻿namespace Pineapples.Dashboards {

  export namespace Enrolments {
    export interface IxfData {
      Age: number;
      AuthorityCode: string;
      AuthorityGovt: string;
      ClassLevel: string;
      DistrictCode: string;
      Enrol: number;
      EnrolM: number;
      EnrolF: number;
      GenderCode: string;
      SchoolTypeCode: string;
      SurveyYear: number
    }
    // simplest value accessor based on this data shape:
    // just returns the sum M + F
    export function va(d: IxfData) {
      return d.EnrolM + d.EnrolF;
    };

    // this is a value accessor for building a cross tabulation
    // where each 'value' is an object containing M F and T values
    export function vaGendered(d: IxfData) {
      return { Male: d.EnrolM, Female: d.EnrolF, Total: d.EnrolM + d.EnrolF };
    };
  }
  export class SchoolsDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {

    public va = Enrolments.va;

    private xReduce = _.memoize((tableName, ...inputs) => this.xFilter.xReduce.bind(this.xFilter)(...inputs))
    private groupReduceSum = _.memoize((groupName, dim, va) => dim.group().reduceSum(va));

    // override with strongly typed crossfilter
    public xf: CrossFilter.CrossFilter<Enrolments.IxfData>;

    public dimSurveyYear: CrossFilter.Dimension<Enrolments.IxfData, number>;
    public dimDistrictCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
    public dimAuthorityCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
    public dimAuthorityGovt: CrossFilter.Dimension<Enrolments.IxfData, string>;
    public dimSchoolTypeCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
    public dimEducationLevel: CrossFilter.Dimension<Enrolments.IxfData, string>;
    public dimGender: CrossFilter.Dimension<Enrolments.IxfData, string>;
    public dimAge: CrossFilter.Dimension<Enrolments.IxfData, number>;
    public dimAgeGroup: CrossFilter.Dimension<Enrolments.IxfData, number>;
    public labelerAgeGroup: any;

    public onOptionChange(data, sender) {
      if (data.selectedYear) {
        this.dimSurveyYear.filter(this.options.selectedYear);
      }
      if (data.selectedDistrict) {
        this.dimDistrictCode.filter(this.options.selectedDistrict);
      }
      if (data.selectedAuthorityGovt) {
        this.dimAuthorityGovt.filter(this.options.selectedAuthorityGovt);
      }
      if (data.selectedAuthority) {
        this.dimAuthorityCode.filter(this.options.selectedAuthority);
      }
      if (data.selectedSchoolType) {
        this.dimSchoolTypeCode.filter(this.options.selectedSchoolType);
      }
      // always call the super version so that the clients get to know about the option change as well
      super.onOptionChange(data, sender);
    }

    private elSelector = ({ ClassLevel }) => this.lookups.byCode("levels", ClassLevel)["L"];
    private classLevelSelector = this.xFilter.getPropAccessor("ClassLevel")

    public createDimensions() {
      // create the dimensions

      this.dimAuthorityGovt = this.xf.dimension(d => 
        this.lookups.cache["authorityGovt"].byCode(d.AuthorityGovt).N);

      this.dimSurveyYear = this.xf.dimension(d => d.SurveyYear);
      this.dimDistrictCode = this.xf.dimension(d => d.DistrictCode);
      //this.dimGender = this.xf.dimension(d => d.GenderCode);
      this.dimAuthorityCode = this.xf.dimension(d => d.AuthorityCode);
      this.dimSchoolTypeCode = this.xf.dimension(d => d.SchoolTypeCode);
      this.dimAge = this.xf.dimension(d => d.Age);

      this.dimEducationLevel = this.xf.dimension(this.elSelector);
      this.dimAgeGroup = this.xf.dimension(this.ageGrouper(5));

      this.labelerAgeGroup = ((i) => this.ageGroupLabel(i, 5)).bind(this);
      this.dashboardReady += 1;
    }

    private firstCheckX = fn => x => !x.Female || !x.Male || x.Female === 0 || x.Male === 0 ? '' : fn(x); 
    
    public indicatorCalculations = {
      GPI:        this.firstCheckX( x => (1.0 * x.Female / x.Male).toFixed(2) ),
      "Female %": this.firstCheckX( x => (100.0 * x.Female / (x.Female + x.Male)).toFixed(1)),
      "Male %":   this.firstCheckX( x =>  (100.0 * x.Male / (x.Female + x.Male)).toFixed(1)),
      All:        this.firstCheckX( x =>  x.Female + x.Male )
    }    

    public filterString = (...exclude) => `
       ${!_(exclude).contains('Year') && this.options.selectedYear || ''}
       ${!_(exclude).contains('District') && this.lookups.byCode('districts', this.options.selectedDistrict, 'N') || ''}
       ${!_(exclude).contains('Authority') && this.lookups.byCode('authorities', this.options.selectedAuthority, 'N') || ''}
       ${!_(exclude).contains('AuthorityGovt') && this.options.selectedAuthorityGovt || ''}
       ${!_(exclude).contains('SchoolType') && this.lookups.byCode('schoolTypes', this.options.selectedSchoolType, 'N') || ''}
      `


    public grpDistrictCode = () => this.groupReduceSum('grpDistrictCode va', this.dimDistrictCode, Enrolments.va)
    public grpAuthorityCode = () => this.groupReduceSum('grpAuthorityCode va', this.dimAuthorityCode, Enrolments.va)
    public grpAuthorityGovt = () => this.groupReduceSum('grpAuthorityGovt va', this.dimAuthorityGovt, Enrolments.va)
    public tabAge_EdLevel = () => this.xReduce('tabAge_EdLevel vaG', this.dimAgeGroup, this.elSelector, Enrolments.vaGendered);
    public tabAge_Level = () => this.xReduce('tabAge_Level vaG', this.dimAge, this.classLevelSelector, Enrolments.vaGendered);

    public tabSchoolTypeByDistrict = () => this.xReduce(
      'tabSchoolTypeByDistrict',
      this.dimSchoolTypeCode,
      x => x.DistrictCode,
      Enrolments.vaGendered);

    public toggle = _.memoize(
      (key) => ({ key: filterValue }) => {
        if (!filterValue) { return; }
        this.options[key] = filterValue === this.options[key] ? null : filterValue; 
      }
    );      
      
    
    /**
     * Group a numeric value into bands
     * 
     * @param n the size of the band
     */
    private ageGrouper = (n: number) =>
      ({ Age }) => Age ? (Math.floor(Age / n) * n + 1) : 0;
    /**
     * Display a label for an age band
     * where the client uses an age grouped dimension, this function can be used to produce the row label
     * Note that if the grouping is done on this value, rather than ageGrouper, the sorting wont be right.
     * @param n starting value
     * @param groupSize size of the band
     */
    public ageGroupLabel = (n: number, groupSize: number) => {
      if (!n || n === 0) { return '-'; }
      return n ? n.toString() + "-" + (n + groupSize - 1).toString() : 0;
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
       deflated: "<table"
      };
      this.controller = SchoolsDashboard;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/dashboard/SchoolsDashboard`;
    }
  }

  angular
    .module("pineapples")
    .component("schoolsDashboard", new Component());


  export class EnrolComponent implements ng.IComponentOptions {
    public bindings:any =  {
      dashboardReady: "<",
      selectedChild: "<",
      optionChange: "<",
      reportPath: "@"
    };
    public templateUrl: string;
    public controllerAs: string = 'vm';
    public require: any = { "dashboard": "^schoolsDashboard" };

    constructor(public controller: any, templateUrl) {
      this.templateUrl = "dashboard/component/" + templateUrl;
    }
  }

}
