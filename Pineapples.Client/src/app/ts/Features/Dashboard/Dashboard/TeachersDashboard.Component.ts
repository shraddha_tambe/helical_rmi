﻿
namespace Pineapples.Dashboards {

  export namespace Teachers {
    export interface IxfData {
      SurveyYear: number;
      AgeGroup: string;
      DistrictCode: string;
      AuthorityCode: string;
      AuthorityGovt: string;
      SchoolTypeCode: string;
      Sector: string;
      ISCEDSubClass: string;
      NumTeachersM: number;
      NumTeachersF: number;
      CertifiedM: number;
      CertifiedF: number;
      QualifiedM: number;
      QualifiedF: number;
      CertQualM: number;
      CertQualF: number;
    }
    
    export function vaNumTeachers(d: IxfData) {
      return d.NumTeachersF + d.NumTeachersM;
    };

    export function vaDetail(d: IxfData) {
      return {
        'NumTeachers@Male': d.NumTeachersM,
        'NumTeachers@Female': d.NumTeachersF,
        'NumTeachers@Total': d.NumTeachersF  + d.NumTeachersM,
        'Certified@Male': d.CertifiedM,
        'Certified@Female': d.CertifiedF,
        'Certified@Total': d.CertifiedF + d.CertifiedM,
        'Qualified@Male': d.QualifiedM,
        'Qualified@Female': d.QualifiedF,
        'Qualified@Total': d.QualifiedF + d.QualifiedM,
        'CertQual@Male': d.CertQualM,
        'CertQual@Female': d.CertQualF,
        'CertQual@Total': d.CertQualF + d.CertQualM,
      };
    };
  }

  export class TeachersDashboard extends CrossfilterDashboard implements ICrossfilterDashboard {

    // if a or b is 0 or null, return '' else return sum(a,b)
    private checkThenSum = (a, b) => x => !x[a] || !x[b] || x[a] === 0 || x[b] === 0 ? '' : x[a] + x[b];

    public totalAccessors = {
      Total: this.checkThenSum('NumTeachersM', 'NumTeachersF'),
      Certified: this.checkThenSum('CertifiedM', 'CertifiedF'),
      Qualified: this.checkThenSum('QualifiedM', 'QualifiedF'),
      CertQual: this.checkThenSum('CertQualM', 'CertQualF'),
    }   

    public accessors = {
      NumTeachersM: (d) => d.NumTeachersM,
      NumTeachersF: (d) => d.NumTeachersF,
      NumTeachers: this.checkThenSum('NumTeachersF', 'NumTeachersM'),
      CertifiedM: (d) => d.CertifiedM,
      CertifiedF: (d) => d.CertifiedF,
      Certified: this.checkThenSum('CertifiedF', 'CertifiedM'),
      QualifiedM: (d) => d.QualifiedM,
      QualifiedF: (d) => d.QualifiedF,
      Qualified: this.checkThenSum('QualifiedF', 'QualifiedM'),
      CertQualM: (d) => d.CertQualM,
      CertQualF: (d) => d.CertQualF,
      CertQual: this.checkThenSum('CertQualF', 'CertQualM'),
    }

    public tabSchoolTypeByDistrict = () => this.xReduce(
      'tabSchoolTypeByDistrict',
      this.dimSchoolTypeCode,
      x => x.DistrictCode,
      Teachers.vaDetail);

    public filterString = (...exclude) => `
       ${!_(exclude).contains('Year') && this.options.selectedYear || ''}
       ${!_(exclude).contains('District') && this.lookups.byCode('districts', this.options.selectedDistrict, 'N') || ''}
       ${!_(exclude).contains('Authority') && this.lookups.byCode('authorities', this.options.selectedAuthority, 'N') || ''}
       ${!_(exclude).contains('AuthorityGovt') && this.options.selectedAuthorityGovt || ''}
       ${!_(exclude).contains('SchoolType') && this.lookups.byCode('schoolTypes', this.options.selectedSchoolType, 'N') || ''}
      `

    private xReduce = _.memoize((tableName, ...inputs) => this.xFilter.xReduce.bind(this.xFilter)(...inputs))
    private groupReduceSum = _.memoize((groupName, dim, va) => dim.group().reduceSum(va));

    //// override with strongly typed crossfilter
    //public xf: CrossFilter.CrossFilter<Enrolments.IxfData>;

    public dimSurveyYear: CrossFilter.Dimension<Enrolments.IxfData, number>;
    public dimDistrictCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
    public dimAuthorityCode: CrossFilter.Dimension<Enrolments.IxfData, string>;
    public dimAuthorityGovt: CrossFilter.Dimension<Enrolments.IxfData, string>;
    public dimSchoolTypeCode: CrossFilter.Dimension<Enrolments.IxfData, string>;

    public dimAge: CrossFilter.Dimension<Enrolments.IxfData, number>;


    public onOptionChange(data, sender) {
      console.log('TeacherDashboard.onOptionChange', data, sender )

      if (data.selectedYear) {
        this.dimSurveyYear.filter(this.options.selectedYear);
      }
      if (data.selectedDistrict) {
        this.dimDistrictCode.filter(this.options.selectedDistrict);
      }
      if (data.selectedAuthorityGovt) {
        this.dimAuthorityGovt.filter(this.options.selectedAuthorityGovt);
      }
      if (data.selectedAuthority) {
        this.dimAuthorityCode.filter(this.options.selectedAuthority);
      }
      if (data.selectedSchoolType) {
        this.dimSchoolTypeCode.filter(this.options.selectedSchoolType);
      }
      // always call the super version so that the clients get to know about the option change as well
      super.onOptionChange(data, sender);
    }

    public createDimensions() {
    //  // create the dimensions

      this.dimAuthorityGovt = this.xf.dimension(d =>
        this.lookups.cache["authorityGovt"].byCode(d.AuthorityGovt).N);

      this.dimSurveyYear = this.xf.dimension(d => d.SurveyYear);
      this.dimDistrictCode = this.xf.dimension(d => d.DistrictCode);

      this.dimAuthorityCode = this.xf.dimension(d => d.AuthorityCode);
      this.dimSchoolTypeCode = this.xf.dimension(d => d.SchoolTypeCode);
      this.dimAge = this.xf.dimension(d => d.Age);

    }

    public grpDistrictCode  = () => this.groupReduceSum('grpDistrictCode vaNumTeachers', this.dimDistrictCode, Teachers.vaNumTeachers)
    public grpAuthorityCode = () => this.groupReduceSum('grpAuthorityCode vaNumTeachers', this.dimAuthorityCode, Teachers.vaNumTeachers)
    public grpAuthorityGovt = () => this.groupReduceSum('grpAuthorityGovt vaNumTeachers', this.dimAuthorityGovt, Teachers.vaNumTeachers)

    public toggle = _.memoize(
      (key) => ({ key: filterValue }) => {
        if (!filterValue) { return; }
        this.options[key] = filterValue === this.options[key] ? null : filterValue;
      }
    );      

    public vaDetail = Teachers.vaDetail;

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
       deflated: "<table"
      };
      this.controller = TeachersDashboard;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/dashboard/TeachersDashboard`;
    }
  }

  angular
    .module("pineapples")
    .component("teachersDashboard", new Component());

}
