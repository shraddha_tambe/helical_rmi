﻿namespace Pineapples.Dashboards {

  /**
   * CoreOptions provides selectors for the key "dimensions" of a dashboard:
   *
   *  Year - the survey year or reporting year
   *  District - aka state, province
   *  SchoolType - school type
   *  SchoolClass - adhoc code
   *  Authority - managing authority
   *  AuthorityGov - authorityGovNonGov aggregation
   *
   *  EducationLevel - level of education
   *  Sector         - education sector
   *
   *  Gender - of pupils, or teachers
   *
   * for 'at a glance' or indicator pages we may compare two years -
   *  BaseYear - the base year for comparison, the comparison year is Year

   * This class can be used by a dashboard system to manage filtering of data 
   */
  export class coreOptions {
    public eventName = ":option_change";
    constructor(private hub: Sw.IHub) {
      // to do: make a unique id for this instance of the class,
      // so we can in theory have two instances running and they will not conflict
      this.eventName = "coreoptions:option_change";
    }

    public resetAll = () => {
      this.selectedAuthority = undefined;
      this.selectedAuthorityGovt = undefined;
      this.selectedDistrict = undefined;
      this.selectedEducationLevel = undefined;
      this.selectedSchoolClass = undefined;
      this.selectedSchoolType = undefined;
    }

    // -----------------------------------------------------------------------------
    // selectedYear
    // -----------------------------------------------------------------------------

    // getter and setter for selectedYear
    private _selectedYear: number;
    public get selectedYear() {
      return this._selectedYear;
    };

    public set selectedYear(newValue) {
      if (newValue !== this._selectedYear) {
        let publishData = {
          selectedYear: {
            new: newValue,
            old: this._selectedYear
          }
        }
        this._selectedYear = newValue;
        // publish the change. Typically, a dashboard (the one that instantiated this object) is listening
        this.hub.publish(this.eventName, publishData, this);
      }
    }


    /**
     * helper function to check if a year value is the currenty selected value
     * user interface can use this to apply a style to a table row, or to a chart bar
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedYear ( ie 'all' are selected) return true
     * using trueWhenAll == true is useful if this expression controls an ng-show, or ng-hide
     * ng-show="vm.dashboard.options.isSelectedYear(r.key, true)"
     * leave out trueWhenAll (ie false) for a highlight:
     * ng-class="{'selected': vm.dashboard.options.isSelectedYear(r.key)}"
     */
    public isSelectedYear(testValue, trueWhenAll?: boolean) {
      if (trueWhenAll && !this.selectedYear) {
        return true;
      }
      return (this.selectedYear == testValue);
    }

    //selectedDistrict
    private _selectedDistrict: string;
    private _lockedDistrict: boolean = false;
    public get selectedDistrict() {
      return this._selectedDistrict;
    }; 

    public set selectedDistrict(newValue) {
      // treat all falsy values as null
      // crossfilter treats filter(null) as FilterAll() so this is convenient upstream
      if (this._lockedDistrict) {
        return;
      }
      if (!newValue) {
        newValue = null;
      }
      if (newValue !== this._selectedDistrict) {
        let publishData = {
          selectedDistrict: {
            new: newValue,
            old: this._selectedDistrict
          }
        }
        this._selectedDistrict = newValue;
        this.hub.publish(this.eventName, publishData, this);
      }
    }
   /**
    * helper function to check if the supplied value is the currently selectedDistrict
    * @param testValue value to test
    * @param trueWhenAll, if there is no selectedDistrict( ie 'all' are selected) return true
    */
    public isSelectedDistrict(testValue, trueWhenAll?: boolean) {
      if (trueWhenAll && !this.selectedDistrict) {
        return true;
      }
      return (this.selectedDistrict == testValue);
    }

    /**
     * Return true if there is no selected district
     * Useful for the client to show or hide totals, rather than a selected value
     */
    public noSelectedDistrict() {
      return !this.selectedDistrict;
    }
    
    /**
     * if district is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
    public toggleSelectedDistrict(newValue) {
      if (!newValue) {
        return;
      }
      if (newValue == this.selectedDistrict) {
        this.selectedDistrict = null;
      } else {
        this.selectedDistrict = newValue;
      }
    }

    /**
     * Locked the district so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed district value becomes the value of selectedDistrict
     */
    public lockDistrict(fixedValue) {
      this._selectedDistrict = fixedValue;
      this._lockedDistrict = true;
    }

    /**
     * Return true if district is locked
     * Useful for the UI to set disabled status on a control
     */
    public isLockedDistrict() {
      return this._lockedDistrict;
    }

    //-------------------------------------------------------------------------------------
    //selectedAuthority
    private _selectedAuthority: string;
    private _lockedAuthority: boolean = false;
    public get selectedAuthority() {
      return this._selectedAuthority;
    };

    public set selectedAuthority(newValue) {
      // treat all falsy values as null
      // crossfilter treats filter(null) as FilterAll() so this is convenient upstream
      if (this._lockedAuthority) {
        return;
      }
      if (!newValue) {
        newValue = null;
      }
      if (newValue !== this._selectedAuthority) {
        let publishData = {
          selectedAuthority: {
            new: newValue,
            old: this._selectedAuthority
          }
        }
        this._selectedAuthority = newValue;
        this.hub.publish(this.eventName, publishData, this);
      }
    }
    /**
     * helper function to check if the supplied value is the currently selectedAuthority
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedAuthority( ie 'all' are selected) return true
     */
    public isSelectedAuthority(testValue, trueWhenAll?: boolean) {
      if (trueWhenAll && !this.selectedAuthority) {
        return true;
      }
      return (this.selectedAuthority == testValue);
    }

    /**
     * Return true if there is no selected Authority
     * Useful for the client to show or hide totals, rather than a selected value
     */
    public noSelectedAuthority() {
      return !this.selectedAuthority;
    }

    /**
     * if Authority is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
    public toggleSelectedAuthority(newValue) {
      if (!newValue) {
        return;
      }
      if (newValue == this.selectedAuthority) {
        this.selectedAuthority = null;
      } else {
        this.selectedAuthority = newValue;
      }
    }

    /**
     * Locked the Authority so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed Authority value becomes the value of selectedAuthority
     */
    public lockAuthority(fixedValue) {
      this._selectedAuthority = fixedValue;
      this._lockedAuthority = true;
    }

    /**
     * Return true if Authority is locked
     * Useful for the UI to set disabled status on a control
     */
    public isLockedAuthority() {
      return this._lockedAuthority;
    }

    //-------------------------------------------------------------------------------------

    // SchoolType
    private _selectedSchoolType: string;
    private _lockedSchoolType: boolean = false;
    public get selectedSchoolType() {
      return this._selectedSchoolType;
    };

    public set selectedSchoolType(newValue) {
      // treat all falsy values as null
      // crossfilter treats filter(null) as FilterAll() so this is convenient upstream
      if (this._lockedSchoolType) {
        return;
      }
      if (!newValue) {
        newValue = null;
      }
      if (newValue !== this._selectedSchoolType) {
        let publishData = {
          selectedSchoolType: {
            new: newValue,
            old: this._selectedSchoolType
          }
        }
        this._selectedSchoolType = newValue;
        this.hub.publish(this.eventName, publishData, this);
      }
    }
    /**
     * helper function to check if the supplied value is the currently selectedSchoolType
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedSchoolType ( ie 'all' are selected) return true
     */
    public isSelectedSchoolType(testValue, trueWhenAll?: boolean) {
      if (trueWhenAll && !this.selectedSchoolType) {
        return true;
      }
      return (this.selectedSchoolType == testValue);
    }

    /**
     * Return true if there is no selected SchoolType
     * Useful for the client to show or hide totals, rather than a selected value
     */
    public noSelectedSchoolType() {
      return !this.selectedSchoolType;
    }

    /**
     * if SchoolType is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
    public toggleSelectedSchoolType(newValue) {
      if (!newValue) {
        return;
      }
      if (newValue == this.selectedSchoolType) {
        this.selectedSchoolType = null;
      } else {
        this.selectedSchoolType = newValue;
      }
    }

    /**
     * Locked the SchoolType so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed SchoolType value becomes the value of selectedSchoolType
     */
    public lockSchoolType(fixedValue) {
      this._selectedSchoolType = fixedValue;
      this._lockedSchoolType = true;
    }

    /**
     * Return true if district is locked
     * Useful for the UI to set disabled status on a control
     */
    public isLockedSchoolType() {
      return this._lockedSchoolType;
    }

    //-------------------------------------------------------------------------------------


    // AuthorityGovt
    // this is derived from Authority and is typically used to group authorities
    // - and by extension, schools and enrolments - into Govt/Non govt sectors.
    private _selectedAuthorityGovt: string;
    private _lockedAuthorityGovt: boolean = false;
    public get selectedAuthorityGovt() {
      return this._selectedAuthorityGovt;
    };

    public set selectedAuthorityGovt(newValue) {
      // treat all falsy values as null
      // crossfilter treats filter(null) as FilterAll() so this is convenient upstream
      if (this._lockedAuthorityGovt) {
        return;
      }
      if (!newValue) {
        newValue = null;
      }
      if (newValue !== this._selectedAuthorityGovt) {
        let publishData = {
          selectedAuthorityGovt: {
            new: newValue,
            old: this._selectedAuthorityGovt
          }
        }
        this._selectedAuthorityGovt = newValue;
        this.hub.publish(this.eventName, publishData, this);
      }
    }
    /**
     * helper function to check if the supplied value is the currently selectedAuthorityGovt
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedAuthorityGovt( ie 'all' are selected) return true
     */
    public isSelectedAuthorityGovt(testValue, trueWhenAll?: boolean) {
      if (trueWhenAll && !this.selectedAuthorityGovt) {
        return true;
      }
      return (this.selectedAuthorityGovt == testValue);
    }

    /**
     * Return true if there is no selected AuthorityGovt
     * Useful for the client to show or hide totals, rather than a selected value
     */
    public noSelectedAuthorityGovt() {
      return !this.selectedAuthorityGovt;
    }

    /**
     * if AuthorityGovt is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
    public toggleSelectedAuthorityGovt(newValue) {
      if (!newValue) {
        return;
      }
      if (newValue == this.selectedAuthorityGovt) {
        this.selectedAuthorityGovt = null;
      } else {
        this.selectedAuthorityGovt = newValue;
      }
    }

    /**
     * Locked the AuthorityGovt so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed AuthorityGovt value becomes the value of selectedAuthorityGovt
     */
    public lockAuthorityGovt(fixedValue) {
      this._selectedAuthorityGovt = fixedValue;
      this._lockedAuthorityGovt = true;
    }

    /**
     * Return true if AuthorityGovt is locked
     * Useful for the UI to set disabled status on a control
     */
    public isLockedAuthorityGovt() {
      return this._lockedAuthorityGovt;
    }

    // use this version of subscribe so that the coreOptions can register its unique message tag
    public subscribe(func: (data, sender) => void) {
      this.hub.subscribe(this.eventName, func);
    }
    //-------------------------------------------------------------------------------------
    //selectedEducationLevel
    private _selectedEducationLevel: string;
    private _lockedEducationLevel: boolean = false;
    public get selectedEducationLevel() {
      return this._selectedEducationLevel;
    };

    public set selectedEducationLevel(newValue) {
      // treat all falsy values as null
      // crossfilter treats filter(null) as FilterAll() so this is convenient upstream
      if (this._lockedEducationLevel) {
        return;
      }
      if (!newValue) {
        newValue = null;
      }
      if (newValue !== this._selectedEducationLevel) {
        let publishData = {
          selectedEducationLevel: {
            new: newValue,
            old: this._selectedEducationLevel
          }
        }
        this._selectedEducationLevel = newValue;
        this.hub.publish(this.eventName, publishData, this);
      }
    }
    /**
     * helper function to check if the supplied value is the currently selectedEducationLevel
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedEducationLevel( ie 'all' are selected) return true
     */
    public isSelectedEducationLevel(testValue, trueWhenAll?: boolean) {
      if (trueWhenAll && !this.selectedEducationLevel) {
        return true;
      }
      return (this.selectedEducationLevel == testValue);
    }

    /**
     * Return true if there is no selected EducationLevel
     * Useful for the client to show or hide totals, rather than a selected value
     */
    public noSelectedEducationLevel() {
      return !this.selectedEducationLevel;
    }

    /**
     * if EducationLevel is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
    public toggleSelectedEducationLevel(newValue) {
      if (!newValue) {
        return;
      }
      if (newValue == this.selectedEducationLevel) {
        this.selectedEducationLevel = null;
      } else {
        this.selectedEducationLevel = newValue;
      }
    }

    /**
     * Locked the EducationLevel so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed EducationLevel value becomes the value of selectedEducationLevel
     */
    public lockEducationLevel(fixedValue) {
      this._selectedEducationLevel = fixedValue;
      this._lockedEducationLevel = true;
    }

    /**
     * Return true if EducationLevel is locked
     * Useful for the UI to set disabled status on a control
     */
    public isLockedEducationLevel() {
      return this._lockedEducationLevel;
    }

    //-------------------------------------------------------------------------------------
    //selectedSchoolClass
    private _selectedSchoolClass: string;
    private _lockedSchoolClass: boolean = false;
    public get selectedSchoolClass() {
      return this._selectedSchoolClass;
    };

    public set selectedSchoolClass(newValue) {
      // treat all falsy values as null
      // crossfilter treats filter(null) as FilterAll() so this is convenient upstream
      if (this._lockedSchoolClass) {
        return;
      }
      if (!newValue) {
        newValue = null;
      }
      if (newValue !== this._selectedSchoolClass) {
        let publishData = {
          selectedSchoolClass: {
            new: newValue,
            old: this._selectedSchoolClass
          }
        }
        this._selectedSchoolClass = newValue;
        this.hub.publish(this.eventName, publishData, this);
      }
    }
    /**
     * helper function to check if the supplied value is the currently selectedSchoolClass
     * @param testValue value to test
     * @param trueWhenAll, if there is no selectedSchoolClass( ie 'all' are selected) return true
     */
    public isSelectedSchoolClass(testValue, trueWhenAll?: boolean) {
      if (trueWhenAll && !this.selectedSchoolClass) {
        return true;
      }
      return (this.selectedSchoolClass == testValue);
    }

    /**
     * Return true if there is no selected SchoolClass
     * Useful for the client to show or hide totals, rather than a selected value
     */
    public noSelectedSchoolClass() {
      return !this.selectedSchoolClass;
    }

    /**
     * if SchoolClass is selected, turn off; turn on
     * useful for responding to click events on table rows, chart elements in the Ui
     */
    public toggleSelectedSchoolClass(newValue) {
      if (!newValue) {
        return;
      }
      if (newValue == this.selectedSchoolClass) {
        this.selectedSchoolClass = null;
      } else {
        this.selectedSchoolClass = newValue;
      }
    }

    /**
     * Locked the SchoolClass so it cannot be changed
     * Useful for implementing some 'security' in the dashboard to restrict a view
     * @param fixedValue the fixed SchoolClass value becomes the value of selectedSchoolClass
     */
    public lockSchoolClass(fixedValue) {
      this._selectedSchoolClass = fixedValue;
      this._lockedSchoolClass = true;
    }

    /**
     * Return true if SchoolClass is locked
     * Useful for the UI to set disabled status on a control
     */
    public isLockedSchoolClass() {
      return this._lockedSchoolClass;
    }

  }
}