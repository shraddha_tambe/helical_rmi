﻿namespace Pineappples.Dashboards {

  class Controller {
    static $inject = ['Lookups'];
    constructor(public lookups) { }

    reduceIterator: any;
    group = _.memoize(() => this.dim.group().reduceSum(this.reduceIterator));
    valueAccessor = (x) => x.value;
    aspectRatio = 2;

    // Bindings
    dim: any;
    selectedKey: any;
    onClick: any;

    //$onChanges(changes) {
    //  console.log('BarChartBase.$onChanges', changes);
    //}
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        dim: "<",
        selectedKey: "<",
        onClick: "<",
        reduceIterator: "<?",
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/widget/BarChartBase`;
    }
  }

  angular
    .module("pineapples")
    .component("barChartBase", new Component());
}