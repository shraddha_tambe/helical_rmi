﻿namespace Pineappples.Dashboards {

  export const addObjs = (a, b) => (<any>_.assign)(
    ...Object.keys(b).map(
      key => ({ [key]: (a[key] || 0) + (b[key] || 0) })
    )
  );

  class Controller {

    columnTotals = (col) => {
      return this.selectedOption ?
        this.getValue(this.gridData.all().map(x => x.value[col.top]).filter(_.identity).reduce(addObjs, {}), col.sub) :
        this.getValue(this.gridData.all().map(x => x.value[col.top]).filter(_.identity).reduce((a, b) => a + b, 0), col.sub);
    }
    
    colLabels = () => this.lookups.cache[this.colLabelsLookup].map(x => x.N);
    colCodes = () => this.lookups.cache[this.colLabelsLookup].map(x => x.C);

    columnLabels = _.memoize(
      () => this.colLabels().map(x => this.selectedOption.map(y => ({ top: x, sub: y.split('@').pop() }))).reduce((acc, val) => [...acc, ...val]),
      () => this.selectedOption
    );

    totalColumnLabels = () => this.selectedOption.map(x => x.split('@').pop());

    columnCodes = _.memoize(
      () => this.colCodes().map(x => this.selectedOption.map(y => ({ top: x, sub: y }))).reduce((acc, val) => [...acc, ...val]),
      () => this.selectedOption
    );

    getValue = (colValue, indicator) => {
      if (typeof colValue === 'undefined') return '';
      if (typeof indicator === 'undefined') return colValue;
      if (_(this.indicatorCalculators).keys().includes(indicator)) return this.indicatorCalculators[indicator](colValue);
      else return colValue[indicator];
    }


    // Bindings
    firstDimName: string = 'You forgot to pass first-dim-name';
    gridData: any; // Pineapples.Dashboards.IKeyValuePair<any, any>[];
    colLabelsLookup: string;
    rowLabels: (rowNum: number) => any;
    hasColumnTotals: boolean = true;
    hasRowTotals: boolean = true;   
    onRowSelect: (key: any) => void;
    _rowLabels = (n) => this.rowLabels ? this.rowLabels(n) : n;
    indicatorCalculators: any;

    selectedOption: any;
    static $inject = ['Lookups'];
    constructor(public lookups) { }

  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        firstDimName: "<?",
        gridData: "<",
        colLabelsLookup: "<",
        rowLabels: "<?", 
        hasRowTotals: "<?",
        hasColumnTotals: "<?",
        selectedOption: "<?",
        indicatorCalculators: "<?",

        onRowSelect: "&"
       };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = `dashboard/widget/CrossTabBase`;
    }
  }

  angular
    .module("pineapples")
    .component("crossTabBase", new Component());
}