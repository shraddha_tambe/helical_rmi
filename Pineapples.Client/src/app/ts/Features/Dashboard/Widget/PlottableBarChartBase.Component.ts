﻿namespace Pineappples.Dashboards {
  
  class Controller {
    private chart: any;
    public isSelected: boolean;
    public chartTitle: string;
    public datasets: any;
    public datasetsMetadata: any;
    public datasetsX: any;
    public datasetsY: any;

    static $inject = ['$timeout', '$element', 'Lookups'];
    constructor(
      protected timeout: ng.ITimeoutService,
      public element) {
    }

    public $onChanges(changes) {
      this.timeout(() => {
        this.chart ? this.chart.redraw() : this.render();
      }, 50, true);
    }

		private render() {
			if (!this.datasets) {
				return;
			}

      const xScale = new Plottable.Scales.Category();
      const yScale = new Plottable.Scales.Linear();
      const colorScale = new Plottable.Scales.Color();
      const legend = this.datasetsMetadata && new Plottable.Components.Legend(colorScale);
      legend && legend.maxEntriesPerRow(1);

      const meta = this.datasetsMetadata || _.range(30).map(x => '');
      const plot = new Plottable.Plots.StackedBar();
      _.forEach(this.datasets, (v: any, i: number) => {
        plot.addDataset(new Plottable.Dataset(v).metadata(meta[i]));
      })

      plot.x(_.property(this.datasetsX), xScale)
      plot.y(_.property(this.datasetsY), yScale)
      plot.attr("fill", function (d, i, dataset) { return dataset.metadata(); }, colorScale);

      const xAxis = new Plottable.Axes.Category(xScale, "bottom");
      const yAxis = new Plottable.Axes.Numeric(yScale, "left");
      const title = new Plottable.Components.TitleLabel(this.chartTitle);

      this.chart = new Plottable.Components.Table([
        [null, title, null],
        [yAxis, plot, legend],
        [null, xAxis, null]
      ]);

      const svg = $(this.element).find("svg.plottableBarChart");
      const d3svg = d3.selectAll(svg.toArray());

      this.chart.renderTo(d3svg);
      svg.attr('width', '100%');
      svg.attr('height', '100%');
      this.chart.redraw();

      window.addEventListener("resize", () => {
        this.isSelected && this.chart.redraw();
      });
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      isSelected: "<",   // needed to trigger re-render
      chartTitle: '@',
      datasets: "<",
      datasetsMetadata: "<",
      datasetsX: "@",
      datasetsY: "@",
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `<svg class="plottableBarChart" style="height: 100%; width: 100%" ></svg>`;
  }

  angular
    .module("pineapples")
    .component("plottableBarChartBase", new Component());
}