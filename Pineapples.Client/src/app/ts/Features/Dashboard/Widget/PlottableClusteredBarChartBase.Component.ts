﻿namespace Pineappples.Dashboards {

  const memoizeKeys = _.memoize(_.keys)
  
  class Controller {
    private chart: any;
    public isSelected: boolean;
    public chartTitle: string;
    public datasets: any;
    public datasetsMetadata: any;
    public datasetsX: any;
    public datasetsY: any;
    

    static $inject = ['$timeout', '$element', 'Lookups'];
    constructor(
      protected timeout: ng.ITimeoutService,
      public element) {
    }

    public $onChanges(changes) {
      this.timeout(() => {
        this.chart ? this.chart.redraw() : this.render();
      }, 50, true);
    }

    private render() {
			if (!this.datasets) {
				return;
			}
      const xScale = new Plottable.Scales.Category();
      const yScale = new Plottable.Scales.Linear();

      const colorScale = new Plottable.Scales.Color();
      colorScale.range(["#BDCE00", "#527900"]);
      const legend = new Plottable.Components.Legend(colorScale);
      legend.maxEntriesPerRow(1);

      const plot = new Plottable.Plots.ClusteredBar();

      _.forEach(this.datasets, (v: any, i: number) => {
        plot.addDataset(new Plottable.Dataset(v).metadata(this.datasetsMetadata[i]));
      })

      plot.x(_.property(this.datasetsX), xScale)
      plot.y(_.property(this.datasetsY), yScale)
      plot.attr("fill", function (d, i, dataset) { return dataset.metadata(); }, colorScale)

      const xAxis = new Plottable.Axes.Category(xScale, "bottom");
      const yAxis = new Plottable.Axes.Numeric(yScale, "left");

      const title = new Plottable.Components.TitleLabel(this.chartTitle);

      this.chart = new Plottable.Components.Table([
        [null, title, null],
        [yAxis, plot, legend],
        [null, xAxis, null]
      ]);


      const svg = $(this.element).find("svg.plottableClusteredBarChart");
      const d3svg = d3.selectAll(svg.toArray());

      this.chart.renderTo(d3svg);
      svg.attr('width', '100%');
      svg.attr('height', '100%');
      this.chart.redraw();

      window.addEventListener("resize", () => {
        this.chart.redraw();
      });
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      isSelected: "<",   // needed to trigger re-render
      chartTitle: '@',
      datasets: "<",
      datasetsMetadata: "<",
      datasetsX: "@",
      datasetsY: "@",
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `<svg class="plottableClusteredBarChart" style="height: 100%; width: 100%" ></svg>`;
  }

  angular
    .module("pineapples")
    .component("plottableClusteredBarChartBase", new Component());
}