﻿namespace Pineappples.Dashboards {
  
  class Controller {
    private chart: any;
    private plot: any;
    public isSelected: boolean;
    public chartTitle: string;
    public datasets: any;
    public datasetsMetadata: any;
    public datasetsX: any;
    public datasetsY: any;
    public colors: any;

    static $inject = ['$timeout', '$element', 'Lookups'];
    constructor(
      protected timeout: ng.ITimeoutService,
      public element) {
    }

    public $onChanges(changes) {
      this.timeout(() => {
        !changes.datasets && this.chart ? this.chart.redraw() : this.render();
      }, 50, true);
    }

    private render() {
			if (!this.datasets) {
				return;
			}
      const xScale = new Plottable.Scales.Linear();
      const yScale = new Plottable.Scales.Category();
      const colorScale = new Plottable.Scales.Color();
      const legend = new Plottable.Components.Legend(colorScale);
      legend.maxEntriesPerRow(1);

      this.colors = _.zipObject(this.datasets.map(({ meta, color }) => [meta, color]))

      const plot = new Plottable.Plots.StackedBar('horizontal');
      _.forEach(this.datasets, ({ data, meta }) => {
        plot.addDataset(new Plottable.Dataset(data).metadata(meta));
      })

      plot.x(_.property(this.datasetsX), xScale)
      plot.y(_.property(this.datasetsY), yScale)

      plot.attr('height', 100);
      plot.attr("fill", (d, i, dataset) => this.colors[dataset.metadata()]);

      const xAxis = new Plottable.Axes.Numeric(xScale, "bottom");
      const yAxis = new Plottable.Axes.Category(yScale, "left");
      const title = new Plottable.Components.TitleLabel(this.chartTitle);

      this.chart = new Plottable.Components.Table([
        [null, title, null],
        [yAxis, plot, legend],
        [null, xAxis, null]
      ]);

      const svg = $(this.element).find("svg.plottableHorizontalBarChart");
      const d3svg = d3.selectAll(svg.toArray());
      svg.children().remove();

      this.plot = plot;

      this.chart.renderTo(d3svg);
      svg.attr('width', '100%');
      svg.attr('height', '100%');
      this.chart.redraw();

      window.addEventListener("resize", () => {
        this.isSelected && this.chart.redraw();
      });

//      //////////////////////

//      // Initializing tooltip anchor
//      var tooltipAnchorSelection = plot.foreground().append("circle").attr({
//        r: 3,
//        opacity: 0
//      });

//      var tooltipAnchor = $(tooltipAnchorSelection.node());
//      tooltipAnchor.tooltip({
//        animation: false,
//        container: "body",
//        placement: "auto",
//        title: "text",
//        trigger: "manual"
//      });

//      // Setup Interaction.Pointer
//      var pointer = new Plottable.Interactions.Pointer();
//      pointer.onPointerMove((p) => {
//        var closest = plot.entityNearest(p);
//        if (closest) {
//          tooltipAnchor.attr({
//            cx: closest.position.x,
//            cy: closest.position.y,
//            "data-original-title": `${Math.abs(closest.datum[this.datasetsX])}%`
//          });
//        }
////        console.log('tooltip')
////        console.log(closest.datum)
//        console.log(`tooltip:  ${Math.abs(closest.datum[this.datasetsX])}%`);
//      });

//      pointer.onPointerExit(function () {
//        tooltipAnchor.tooltip("hide");
//        console.log('tooltip: hide')
//      });

//      pointer.attachTo(plot);

//      //////////////////////

    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      isSelected: "<",   // needed to trigger re-render
      chartTitle: '@',
      datasets: "<",
      datasetsMetadata: "<",
      datasetsX: "@",
      datasetsY: "@",
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `<svg class="plottableHorizontalBarChart" style="height: 100%; width: 100%" ></svg>`;
  }

  angular
    .module("pineapples")
    .component("plottableHorizontalBarChartBase", new Component());
}