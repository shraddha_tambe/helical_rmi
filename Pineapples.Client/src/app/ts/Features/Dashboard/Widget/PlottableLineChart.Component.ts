﻿/* <PlottableLineChart />
 * Line Chart Component
 * 
 * Attibutes
 * =========
 * 
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 * chartTitle: Title to display on chart
 * datasets: data for chart - list of objects containing { dataset, meta, color }
        i.e. 
          [
            {
              "meta": "GPK",
              "color": "#FF6666",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 0
                }
              ]
            },
            {
              "meta": "GK",
              "color": "#FFB266",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 0
                }
              ]
            },
            {
              "meta": "G1",
              "color": "#FFFF66",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 137.5
                }
              ]
            },
            {
              "meta": "G2",
              "color": "#B2FF66",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 150
                }
              ]
            },
            {
              "meta": "G3",
              "color": "#66FF66",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 206.3
                }
              ]
            },
            {
              "meta": "G4",
              "color": "#66FFB2",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 114.3
                },
                {
                  "year": 2016,
                  "value": 76.9
                },
                {
                  "year": 2017,
                  "value": 255.6
                }
              ]
            },
            {
              "meta": "G5",
              "color": "#66FFFF",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 145.5
                },
                {
                  "year": 2016,
                  "value": 157.1
                },
                {
                  "year": 2017,
                  "value": 110
                }
              ]
            },
            {
              "meta": "G6",
              "color": "#66B2FF",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 78.6
                },
                {
                  "year": 2017,
                  "value": 440
                }
              ]
            },
            {
              "meta": "G7",
              "color": "#6666FF",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 1200
                }
              ]
            },
            {
              "meta": "G8",
              "color": "#B266FF",
              "dataset": [
                {
                  "year": 2013,
                  "value": 0
                },
                {
                  "year": 2014,
                  "value": 0
                },
                {
                  "year": 2015,
                  "value": 0
                },
                {
                  "year": 2016,
                  "value": 0
                },
                {
                  "year": 2017,
                  "value": 0
                }
              ]
            }
          ]

 * datasetsX: key for objects in dataset attribute for x axis.  i.e. "year"
 * datasetsY: key for objects in dataset attribute for y axis.  i.e. "value"
 */

namespace Pineappples.Dashboards {

  class Controller {
    public selectedChild: any;
    public toggleSelected: any;
    public dimensions: string;
    public reportPath: string;

    public headingFilters: string;
    public headingTitle: string;
    public chartTitle: string;
    public datasets: any;
    public datasetsMetadata: any;
    public datasetsX: any;
    public datasetsY: any;

    constructor() {
      this.componentId = uniqueId();
    }

    // This is ugly, but is required when components define their componentIds - Componenent Design recommends pushing state (like this) up. 
    componentId: string;
    isSelected = () => this.componentId == this.selectedChild;
    anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      // For <dashboard-child>
      dimensions: "@",
      selectedChild: "<",
      headingTitle: "@?",
      headingFilters: "<?",
      reportPath: "@?",
      toggleSelected: "<",

      chartTitle: '@',
      datasets: "<",
      datasetsMetadata: "<",
      datasetsX: "@",
      datasetsY: "@",
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `
        <dashboard-child class="dashboard-wrapper"
                         ng-class="vm.dimensions"
                         report-path="vm.reportPath"
                         toggle-selected="vm.toggleSelected"
                         is-selected="vm.isSelected()"
                         another-component-selected="vm.anotherComponentSelected()"
                         component-id="vm.componentId">

          <heading-title>{{vm.headingTitle}}</heading-title>
          <heading-filters>{{vm.headingFilters}}</heading-filters>
          <heading-options>
            <md-select ng-if="vm.headingOptions" ng-model="vm.selectedViewOption">
              <md-option ng-value="opt" ng-repeat="opt in vm.viewOptions()">{{ opt }}</md-option>
            </md-select>
          </heading-options>  

          <child-body>
            <plottable-line-chart-base 
                chart-title="{{vm.chartTitle}}"
                is-selected="vm.isSelected()"
                datasets="vm.datasets"
                datasets-metadata="vm.datasetsMetadata"
                datasets-x="{{vm.datasetsX}}"
                datasets-y="{{vm.datasetsY}}" />

          </child-body>
        </dashboard-child>
`;
  }

  angular
    .module("pineapples")
    .component("plottableLineChart", new Component());
}
