﻿namespace Pineappples.Dashboards {

  const memoizeKeys = _.memoize(_.keys)
  
  class Controller {

    private chart: any;
    public isSelected: boolean;

    public chartTitle: string;
    public datasets: any;
    public datasetsMetadata: any;
    public datasetsX: any;
    public datasetsY: any;
    

    static $inject = ['$timeout', '$element', 'Lookups'];
    constructor(
      protected timeout: ng.ITimeoutService,
      public element) {
    }

    public $onChanges(changes) {
      this.timeout(() => {
        this.chart ? this.chart.redraw() : this.render();
        //console.log('PlottableLineChart.$onChanges');
        //console.log(this);
      }, 50, true);
    }

    private render() {
			if (!this.datasets) {
				return;
			}

      const cs = new Plottable.Scales.Color();
      cs.range(_.map(this.datasets, x => x['color']));
      cs.domain(_.map(this.datasets, x => x['meta']));
      const legend = new Plottable.Components.Legend(cs);
      legend.maxEntriesPerRow(1);

      const title = new Plottable.Components.TitleLabel(this.chartTitle);
      const xScale = new Plottable.Scales.Category();
      const yScale = new Plottable.Scales.Linear();
      const xAxis = new Plottable.Axes.Category(xScale, "bottom");
      const yAxis = new Plottable.Axes.Numeric(yScale, "left");

      const plot = new Plottable.Plots.Line();
      plot.x(_.property(this.datasetsX), xScale)
      plot.y(_.property(this.datasetsY), yScale)
      plot.attr("stroke", (d, i, ds) => ds.metadata());
      plot.datasets(_.map(this.datasets, x => new Plottable.Dataset(x['dataset']).metadata(x['color'])));

      this.chart = new Plottable.Components.Table([
        [null, title, null],
        [yAxis, plot, legend],
        [null, xAxis, null],
      ]);

      const svg = $(this.element).find("svg.plottableLineChart");
      const d3svg = d3.selectAll(svg.toArray());

      this.chart.renderTo(d3svg);
      svg.attr('width', '100%');
      svg.attr('height', '100%');
      this.chart.redraw();

      window.addEventListener("resize", () => {
        this.chart.redraw();
        console.log('redraw')
      });
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      isSelected: "<",   // needed to trigger re-render
      chartTitle: '@',
      datasets: "<",
      datasetsMetadata: "<",
      datasetsX: "@",
      datasetsY: "@",
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `<svg class="plottableLineChart" style="height: 100%; width: 100%" ></svg>`;
  }

  angular
    .module("pineapples")
    .component("plottableLineChartBase", new Component());
}