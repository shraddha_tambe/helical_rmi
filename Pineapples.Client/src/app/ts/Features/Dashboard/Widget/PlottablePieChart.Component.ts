﻿/* <PlottablePieChart />
 * Pie Chart Component
 * 
 * Attibutes
 * =========
 * 
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 * chartTitle: Title to display on chart
 * datasetsMetadata: metadata for data i.e. ["Female", "Male"]
 * datasetsColors: colors for chart    i.e. ["#00b161", "#0964a5"]
 * datasetsVal: value from datasets    i.e. "enrol"
 * datasets: data for chart - list of objects containing { dataset, meta, color }
        i.e.[
              {
                "enrol": 117
              },
              {
                "enrol": 126
              }
            ]

 */


namespace Pineappples.Dashboards {

  class Controller {
    public selectedChild: any;
    public toggleSelected: any;
    public dimensions: string;
    public reportPath: string;

    public headingOptions: any;
    public selectedViewOption: string;

    public chartTitle: string;
    public datasets: any;
    public datasetsMetadata: any;
    public datasetsColors: any;
    public datasetsVal: string;

    $onInit() {
      this.selectedViewOption = this.headingOptions && this.headingOptions[0] || '';
    }

    constructor() {
      this.componentId = uniqueId();
    }

    // This is ugly, but is required when components define their componentIds - Componenent Design recommends pushing state (like this) up. 
    componentId: string;
    isSelected = () => this.componentId == this.selectedChild;
    anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      // For <dashboard-child>
      dimensions: "@",
      selectedChild: "<",
      headingTitle: "@?",
      headingFilters: "<?",
      reportPath: "@?",
      toggleSelected: "<",

      headingOptions: "<?",

      chartTitle: '@',
      datasets: "<",
      datasetsMetadata: "<",
      datasetsColors: "<",
      datasetsVal: "@",
      
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public template: string = `
        <dashboard-child class="dashboard-wrapper"
                         ng-class="vm.dimensions"
                         report-path="vm.reportPath"
                         toggle-selected="vm.toggleSelected"
                         is-selected="vm.isSelected()"
                         another-component-selected="vm.anotherComponentSelected()"
                         component-id="vm.componentId">

          <heading-title>{{vm.headingTitle}}</heading-title>
          <heading-filters>{{vm.headingFilters}}</heading-filters>
          <heading-options>
            <md-select ng-if="vm.headingOptions" ng-model="vm.selectedViewOption" aria-label="Select">
              <md-option ng-value="opt" ng-repeat="opt in vm.headingOptions">{{ opt }}</md-option>
            </md-select>
          </heading-options>  

          <child-body>
            <plottable-pie-chart-base
                chart-title="{{vm.chartTitle}}"
                is-selected="vm.isSelected()"
                datasets="vm.headingOptions ? vm.datasets[vm.selectedViewOption] : vm.datasets"
                datasets-metadata="vm.datasetsMetadata"
                datasets-colors="vm.datasetsColors"
                datasets-val="{{vm.datasetsVal}}" />

          </child-body>
        </dashboard-child>
        `;
  }

  angular
    .module("pineapples")
    .component("plottablePieChart", new Component());
}


