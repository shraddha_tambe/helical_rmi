﻿/* <SchoolDetails />
 * School Details Component
 *
 * Attibutes
 * =========
 *
 * dimensions: size on screen, i.e.  "height4 width4"
 * headingTitle: title to display on Component,
 * headingFilters: filters to display on component,
 * reportPath: JasperReport url
 * selectedChild: id of selected component
 * toggleSelected: callback function to toggle 'selected' status
 * schoolInfo: js object containing school info - as returned from /api/schools/<schNo>
 */

namespace Pineappples.Dashboards {

  const memoizeKeys = _.memoize(_.keys)
  
  class Controller {
    selectedChild: any;
    toggleSelected: any;
    dimensions: string;
    reportPath: string;

    public schoolInfo: any;

    static $inject = ['Lookups'];
    constructor() {
      this.componentId = uniqueId();
    }

    // This is ugly, but is required when components define their componentIds - Componenent Design recommends pushing state (like this) up. 
    componentId: string;
    isSelected = () => this.componentId == this.selectedChild;
    anotherComponentSelected = () => this.selectedChild != '' && this.componentId != this.selectedChild;
  }

  class Component implements ng.IComponentOptions {
    public bindings: any = {
      // For <dashboard-child>
      dimensions: "@",
      selectedChild: "<",
      headingTitle: "@?",
      headingFilters: "<?",
      reportPath: "@?",
      toggleSelected: "<",

      schoolInfo: '<',
    };

    public controller: any = Controller;
    public controllerAs: string = "vm";
    public templateUrl: string = `dashboard/widget/SchoolDetails`;
  }

  angular
    .module("pineapples")
    .component("schoolDetails", new Component());
}
