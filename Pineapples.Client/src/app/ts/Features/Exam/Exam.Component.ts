﻿namespace Pineapples.Exams {

  interface IBindings {
    model: Exam;
  }

  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: Exam;

    static $inject = ["ApiUi", "examsAPI"];
    constructor(apiui: Sw.Api.IApiUi, api: any) {
      super(apiui, api);
    }

    public $onChanges(changes) {
      super.$onChanges(changes);
    }

  }

  angular
    .module("pineapples")
    .component("componentExam", new Sw.Component.ItemComponentOptions("exam", Controller));
}