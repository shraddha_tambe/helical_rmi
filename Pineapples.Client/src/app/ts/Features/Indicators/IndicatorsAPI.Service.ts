﻿namespace Pineapples.Api {

  export interface IIndicatorsApi {
    vermdata(): ng.IPromise<any>;
    vermdataDistrict(districtCode: string): ng.IPromise<any>;
    refreshWarehouse(startFromYear): ng.IPromise<any>;
    refreshVermData(): ng.IPromise<any>;
    refreshVermDataDistrict(districtCode): ng.IPromise<any>;
  }

  let factory = (restAngular: restangular.IService
    , http: ng.IHttpService, errorService
    , cacheFactory: ng.ICacheFactoryService) => {
    let svc = <any>restAngular.all("indicators");

    svc.vermdata = () => {
      return http.get("api/indicators/vermdata", { cache: true }).then((response) => {
        return response.data;       // return the xml payload
      });
    };

    // get the vermdata for a specific district
    svc.vermdataDistrict = (districtCode) => {
      if (!districtCode) {
        return svc.vermdata()
      }
      let url ='api/indicators/vermdatadistrict/' + districtCode;
      return http.get(url, { cache: true }).then((response) => {
        return response.data;       // return the xml payload
      });
    };

    svc.refreshWarehouse = (startFromYear) => {
      return svc.customGET("makewarehouse", { year: startFromYear });
    }
    svc.refreshVermData = () => {
      return http.get("api/indicators/makevermdata")
        .then((response) => {
          // destroy the cached vermdata
          if (cacheFactory.get("$http")) {
            cacheFactory.get("$http").remove('api/indicators/vermdata');
          }
          return response.data;
        });

    }
    svc.refreshVermDataDistrict = (districtCode) => {

      let url = "api/indicators/makevermdatadistrict/" + districtCode;
      return http.get(url)
        .then((response) => {
          // destroy the cached vermdata
          if (cacheFactory.get("$http")) {
            cacheFactory.get("$http").remove("api/indicators/vermdatadistrict/" + districtCode);
          }
          return response.data;
        })
    };
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("IndicatorsAPI", ['Restangular', "$http", 'ErrorService', '$cacheFactory', factory]);
}
