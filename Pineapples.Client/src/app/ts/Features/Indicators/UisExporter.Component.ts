﻿namespace Pineapples.Admin {

  interface IBindings {

  }
  class Controller implements IBindings {

    static $inject = ['IndicatorsAPI', 'ApiUi'];
    constructor(public api: Pineapples.Api.IIndicatorsApi, public apiUi: Sw.Api.IApiUi) { }

    // refresh the warehouse.... this may take some time
    public refresh() {
      this.isWaiting = true;
      this.api.generateUISExport().then(() => {
        this.isWaiting = false;
      }, (e) => {
        this.isWaiting = false;
        this.apiUi.showErrorResponse(null, e)
      });
    };

    public isWaiting: boolean;

    public $onChanges(changes) {
    }
  }
  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "indicators/UisExporter";
    }
  }

  angular
    .module("pineapples")
    .component("uisExporter", new Component());
}