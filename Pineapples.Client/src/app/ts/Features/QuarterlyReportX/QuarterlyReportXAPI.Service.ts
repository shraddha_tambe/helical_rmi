﻿module Pineapples.Api {

  let factory = ($q: ng.IQService, restAngular: restangular.IService, errorService) => {

    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "quarterlyreportsx");
    svc.read = (id) => svc.get(id).catch(errorService.catch);

    svc.new = () => {

      let e = new Pineapples.QuarterlyReportsX.QuarterlyReportX({
        qrID: null
      });
      restAngular.restangularizeElement(null, e, "quarterlyreportsx");
      var d = $q.defer();
      d.resolve(e);
      return d.promise;
    }
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("quarterlyReportsXAPI", ['$q', 'Restangular', 'ErrorService', factory]);
}
