﻿namespace Pineapples.Dashboards {

    var routes = function ($stateProvider) {
        $stateProvider
          .state('site.xycharts', {
              url: '/xycharts',

              views: {
                  "navbar@": {
                      templateUrl: "schoolscatter/searcher",
                      controller: "FilterController",
                      controllerAs: "vm"
                  },
                  "@": "componentSchoolScatter"
              },
              resolve: {
                scatterData: ['$http', (http: ng.IHttpService) => {
                  let req = {
                    BaseYear: 2018,
                    XSeries: "Enrolment",
                    XSeriesOffset: 0,
                    YSeries: "Enrolment",
                    YSeriesOffset: -1
                  }
                  // return http.get("api/warehouse/tableEnrol").then(response => (<any>response.data).ResultSet);
                  return http.post("api/schoolscatter/getscatter", req)
                    .then(response => (response.data["ResultSet"]));
                }]
              }
          });

    }

    angular
        .module('pineapples')
        .config(['$stateProvider', routes])

}