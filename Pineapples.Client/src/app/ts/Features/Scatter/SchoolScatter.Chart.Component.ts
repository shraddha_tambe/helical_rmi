﻿namespace Pineapples.Dashboards {
  // utulities
  var fx = function (d) { return d.x; };
  var fy = function (d) { return d.y; };

  class Controller {

    // bindings
    public datasets: any;
    public colorBy: string;
    public colorByValue: string;
    public height = 800;
    public width = 800;

    static $inject = ['$compile', "$scope", "$element"];
    constructor(private $compile: ng.ICompileService
      , public scope: ng.IScope
      , public element) { }

    /**
     * set up the plottable chart
     * @param scope
     * @param element
     * @returns {}
     */

    // LifeCycle methods

    public $onChanges(changes) {
        this._drawChart(this.element);
    }

    //-----------------------------------------------------------------
    // persisted chart elements

    // CHARTS

    private chart: Plottable.Components.Table;

    // AXES
    private xAxisLabel: Plottable.Components.AxisLabel;
    private yAxisLabel: Plottable.Components.AxisLabel;

    private xScale: Plottable.Scales.Linear;
    private yScale: Plottable.Scales.Linear;

    // LEGENDS AND SCALES
    private typeScale: Plottable.Scales.Color;

    // INTERACTIONS
    private panZoom: Plottable.Interactions.PanZoom;

    // datasets
    private scatterds: Plottable.Dataset;     // the scatter data in plottable form
    private midlineds: Plottable.Dataset;
    // areas
    private a0ds: Plottable.Dataset;  
    private a1ds: Plottable.Dataset;  
    private a2ds: Plottable.Dataset;  
    private a3ds: Plottable.Dataset;  
    private a4ds: Plottable.Dataset;  

    // x-axis decorations
    private xextentds: Plottable.Dataset; // line covering extent of x values
    private xminds: Plottable.Dataset; // min x "tick mark"
    private xmaxds: Plottable.Dataset; // max x "tick mark"
    private xmidds: Plottable.Dataset; // mid x "tick mark"
    private xq1ds: Plottable.Dataset; // q1 x "tick mark"
    private xq3ds: Plottable.Dataset; // q3 x "tick mark"


    // y-axis decorations
    private yextentds: Plottable.Dataset; // line covering extent of x values
    private yminds: Plottable.Dataset; // min x "tick mark"
    private ymaxds: Plottable.Dataset; // max x "tick mark"
    private ymidds: Plottable.Dataset; // mid x "tick mark"
    private yq1ds: Plottable.Dataset; // q1 x "tick mark"
    private yq3ds: Plottable.Dataset; // q3 x "tick mark"


    private _makeChart(element) {
     
      // ------ datasets -----------------
      this.scatterds = new Plottable.Dataset()
        .keyFunction(Plottable.KeyFunctions.useProperty("schNo"));

      // centre line
      this.midlineds = new Plottable.Dataset();
      // areas
      this.a0ds = new Plottable.Dataset().metadata('0');
      this.a1ds = new Plottable.Dataset().metadata('1');
      this.a2ds = new Plottable.Dataset().metadata('2');
      this.a3ds = new Plottable.Dataset().metadata('3');
      this.a4ds = new Plottable.Dataset().metadata('4');

      // x-axis decorations
      this.xextentds = new Plottable.Dataset(); // line covering extent of x values
      this.xminds = new Plottable.Dataset(); // min x "tick mark"
      this.xmaxds = new Plottable.Dataset(); // max x "tick mark"
      this.xmidds = new Plottable.Dataset(); // mid x "tick mark"
      this.xq1ds = new Plottable.Dataset(); // q1 x "tick mark"
      this.xq3ds = new Plottable.Dataset(); // q3 x "tick mark"

      // y axis decorations
      this.yextentds = new Plottable.Dataset();
      this.yminds = new Plottable.Dataset();
      this.ymaxds = new Plottable.Dataset();
      this.ymidds = new Plottable.Dataset();
      this.yq1ds = new Plottable.Dataset(); // q1 y "tick mark"
      this.yq3ds = new Plottable.Dataset(); // q3 y "tick mark"

      /**
       * scales, axes, axislabels
       */
      this.xScale = new Plottable.Scales.Linear();
      this.yScale = new Plottable.Scales.Linear();

      var xAxis = new Plottable.Axes.Numeric(this.xScale, "bottom");
      var yAxis = new Plottable.Axes.Numeric(this.yScale, "left");

      // persist these in the controller so we can get to the text property
      this.xAxisLabel = new Plottable.Components.AxisLabel("Enrolment 2007")
        .padding(1);
      this.yAxisLabel = new Plottable.Components.AxisLabel("Enrolment 2008", -90)
        .padding(5);

      var colorScale = new Plottable.Scales.Color();
      var symbolScale = d3.scale.ordinal()
        .range([
          Plottable.SymbolFactories.circle(),
          Plottable.SymbolFactories.cross(),
          Plottable.SymbolFactories.square(),
          Plottable.SymbolFactories.triangleUp(),
          Plottable.SymbolFactories.triangleDown(),
          Plottable.SymbolFactories.diamond()
        ]);

      /**
       * Interactions
       */
      // do these need to be properties???
      this.panZoom = new Plottable.Interactions.PanZoom(this.xScale, this.yScale);
      let tooltipper = new Plottable.Interactions.Pointer();

      /**
       * Plots
       */

      // ---------------- Scatter ---------------------
      // this is the main scatter plot

      var colorFactory = function (d) {
        if (this.colorByValue) {
          return (d[this.colorBy] === this.colorByValue ? "red" : "gray");
        }
        return d[this.colorBy];
      };
      let scatter = new Plottable.Plots.Scatter()
        .symbol((d) => <Plottable.SymbolFactory>symbolScale(d.schType))
        .size(16)
        .x((d) => d.XValue, this.xScale)
        .y((d) => d.YValue, this.yScale)
        .addDataset(this.scatterds)
        .attr("fill", colorFactory, colorScale)
        .attr("opacity", .8)
        .attr("md-tooltip", function (d) { return '<div><h4>' + d.schName + '</h4><p>' + d.YValue + '</p></div>'; })
        //.attr("uib-popover-append-to-body", "true")
        .attr("color-by", "{{vm.colorBy}}")
      

      // set up the animator
      var startAttrs: Plottable.AttributeToAppliedProjector = { opacity: () => 0, fill: () => "pink" };

      startAttrs = {
        d: (d) => {
          let symbol = <(x: number) => string>symbolScale(d.schType);
          return symbol(0);
        }
      };
      var scatterAnimator = new Plottable.Animators.Attr()
        .startAttrs(startAttrs)
        .endAttrs(startAttrs)
        .exitEasingMode(Plottable.Animators.EasingFunctions.squEase("linear", 0, .5))
        .easingMode(<any>Plottable.Animators.EasingFunctions.squEase("linear", .5, 1))
        .stepDuration(500)
        .stepDelay(0);

      scatter
        .animated(true)
        .animator(Plottable.Plots.Animator.MAIN, scatterAnimator);

      // supporting grid
      var grid = new Plottable.Components.Gridlines(this.xScale, this.yScale);

      // ------------- centre line
      // centre line on the scatter grid
      // depending on view , this plots the slope of YonXmedian or YonX average
      var midline = new Plottable.Plots.Line()
        .x(fx, this.xScale)
        .y(fy, this.yScale)
        .addDataset(this.midlineds)
        .attr("stroke", '#404040')
        .attr("stroke-width", 2)
        .attr("opacity", .2);

      // ------area ranges
      // one blank range and four coloured ranges
      var area = new Plottable.Plots.StackedArea()
        .addDataset(this.a0ds)
        .addDataset(this.a1ds)
        .addDataset(this.a2ds)
        .addDataset(this.a3ds)
        .addDataset(this.a4ds)
        .x(fx, this.xScale)
        .y(fy, this.yScale)
        .attr("fill", '#5279c7')
        .attr("stroke-width", 0)
        .attr("opacity", function (d, i, dataset) {
          switch (dataset.metadata()) {
            case '0':
              return 0;
            case '1':
            case '4':
              return .05;
            case '2':
            case '3':
              return .1;
          }
          return 0;
        });

      var nullScale = new Plottable.Scales.Linear()
        .domain([0, 10])
        .range([0, 10]);

      let styleLinePlot = function (plot) {
        plot.x(fx, this.xScale).y(fy, nullScale)
          .attr("stroke", "#dddd00").attr("stroke-width", 12);
      };

      var ydecoScale = new Plottable.Scales.Linear()
        .domain([0, 10])
        .range([0, -10]);

      var styleYdeco = function (plot) {
        plot.x(fx, ydecoScale).y(fy, this.yScale)
          .attr("stroke", "#dddd00").attr("stroke-width", 12);
      };

      var xextentl = new Plottable.Plots.Line();
      xextentl.addDataset(this.xextentds);
      styleLinePlot(xextentl);

      var xminl = new Plottable.Plots.Line();
      xminl.addDataset(this.xminds);
      styleLinePlot(xminl);

      var xmaxl = new Plottable.Plots.Line()
        .addDataset(this.xmaxds)
        .attr("title", function () { return this.xmaxds.data()[0].x; });
      styleLinePlot(xmaxl);

      var xq1l = new Plottable.Plots.Line();
      xmaxl.addDataset(this.xq1ds);
      styleLinePlot(xq1l);

      var xq3l = new Plottable.Plots.Line();
      xmaxl.addDataset(this.xq3ds);
      styleLinePlot(xq3l);

      var xmidl = new Plottable.Plots.Scatter()
        .symbol(function (d, i) { return Plottable.SymbolFactories.triangleUp(); })
        .size(20)
        .x(fx, this.xScale)
        .y(fy, nullScale)
        .addDataset(this.xmidds);

      // y axis decoration
      var yextentl = new Plottable.Plots.Line()
        .addDataset(this.yextentds);
      styleYdeco(yextentl);

      var yminl = new Plottable.Plots.Line()
        .addDataset(this.yminds);
      styleYdeco(yminl);

      var ymaxl = new Plottable.Plots.Line()
        .addDataset(this.ymaxds);
      styleYdeco(ymaxl);

      var yq1l = new Plottable.Plots.Line()
        .addDataset(this.yq1ds);
      styleYdeco(yq1l);

      var yq3l = new Plottable.Plots.Line()
        .addDataset(this.yq3ds);
      styleYdeco(yq3l);

      var ymidpt = new Plottable.Plots.Scatter();
      ymidpt.addDataset(this.ymidds);
      ymidpt
        .symbol(function (d, i) { return Plottable.SymbolFactories.triangleUp(); })
        .size(20)
        .x(fx, ydecoScale).y(fy, this.yScale);

      // groups
      var mergedPlots = new Plottable.Components.Group([grid, area, midline, scatter]);

      var grpx: Plottable.Component = new Plottable.Components.Group([xextentl, xminl, xmaxl, xmidl, xq1l, xq3l, xAxis]);
      grpx = new Plottable.Components.Table([
        [grpx],
        [this.xAxisLabel]
      ])
        .rowWeight(1, .1);
      var grpy = new Plottable.Components.Group([yextentl, yminl, ymaxl, ymidpt, yq1l, yq3l, yAxis, this.yAxisLabel]);

      this.panZoom
        .attachTo(mergedPlots);
      this.panZoom
        .minDomainExtent(this.xScale, 0);

      /**
       * Legends
       */
      // colorLegend legend
      // based on the color-by attribute
      var colorLegend = new Plottable.Components.Legend(colorScale);
      colorLegend
        .maxEntriesPerRow(1)
        .xAlignment('left');

      // school type legend
      this.typeScale = new Plottable.Scales.Color();
      this.typeScale.scale = function (d) { return "#202020"; };

      let typeLegend = new Plottable.Components.Legend(this.typeScale);
      typeLegend
        .symbol(<any>((d) => symbolScale(d)))
        .maxEntriesPerRow(1)
        .xAlignment('left');

      var legends = new Plottable.Components.Table([
        [typeLegend],
        [colorLegend]
      ]);

      this.chart = new Plottable.Components.Table([
        [grpy, mergedPlots, legends],
        [null, grpx, null]
      ])
        .rowWeight(1, .1)
        .columnWeight(0, .05);

      var svg = $(element).find("svg.schoolScatter");
      var d3svg = d3.selectAll(svg.toArray());
      this.chart.renderTo(d3svg);

    }

    private _drawChart(element) {

      if (this.datasets === undefined) {
        return;
      }
      if (!this.chart) {
        this._makeChart(element);
      };

      // set up the data
      var scatterdata = this.datasets[0];
      // first and only row of the stats resultset
      var stats = this.datasets[1][0];

      // x axie decoration
      var midlinedata = [{ x: 0, y: 0 }, { x: stats.Xmax, y: stats.YonXmedian * stats.Xmax }];

      var xextentdata = [{ x: stats.Xmin, y: 10 }, { x: stats.Xmax, y: 10 }];
      var xmindata = [{ x: stats.Xmin, y: 10 }, { x: stats.Xmin, y: 7 }];

      var xmaxdata = [{ x: stats.Xmax, y: 10 }, { x: stats.Xmax, y: 7 }];
      var xmiddata = [{ x: stats.Xmedian, y: 7 }];

      var xq1data = [{ x: stats.XQ1, y: 10 }, { x: stats.XQ1, y: 7 }];
      var xq3data = [{ x: 10, y: stats.XQ3 }, { x: 9, y: stats.XQ3 }];

      // y axis decoration
      var yextentdata = [{ x: 10, y: stats.Ymin }, { x: 10, y: stats.Ymax }];
      var ymindata = [{ x: 10, y: stats.Ymin }, { x: 9, y: stats.Ymin }];

      var ymaxdata = [{ x: 10, y: stats.Ymax }, { x: 9, y: stats.Ymax }];
      var ymiddata = [{ x: 8, y: stats.Ymedian }];

      var yq1data = [{ x: 10, y: stats.YQ1 }, { x: 9, y: stats.YQ1 }];
      var yq3data = [{ x: 10, y: stats.YQ3 }, { x: 9, y: stats.YQ3 }];

      var intConfidence = 1.5;
      // area shading
      var cFactor = intConfidence * (stats.YonXQ3 - stats.YonXQ1);
      var a0data = [{ x: 0, y: 0 }, { x: stats.Xmax, y: stats.Xmax * (stats.YonXQ1 - cFactor) }];
      var a1data = [{ x: 0, y: 0 }, { x: stats.Xmax, y: stats.Xmax * (cFactor) }];
      var a2data = [{ x: 0, y: 0 }, { x: stats.Xmax, y: stats.Xmax * (stats.YonXmedian - stats.YonXQ1) }];

      var a3data = [{ x: 0, y: 0 }, { x: stats.Xmax, y: stats.Xmax * (stats.YonXQ3 - stats.YonXmedian) }];
      var a4data = [{ x: 0, y: 0 }, { x: stats.Xmax, y: stats.Xmax * (cFactor) }];

      this.xScale
        .domain([0, stats.Xmax + 30]);

      this.yScale
        .domainMin(0)
        .domainMax(stats.Ymax + 30);

      this.panZoom
        .maxDomainExtent(this.xScale, stats.Xmax + 30);

      // update the datasets with fresh data
      this.scatterds.data(scatterdata);
      this.midlineds.data(midlinedata);

      this.xextentds.data(xextentdata);
      this.xminds.data(xmindata);
      this.xmaxds.data(xmaxdata);
      this.xmidds.data(xmiddata);
      this.xq1ds.data(xq1data);
      this.xq3ds.data(xq3data);

      this.yextentds.data(yextentdata);
      this.yminds.data(ymindata);
      this.ymaxds.data(ymaxdata);
      this.ymidds.data(ymiddata);
      this.yq1ds.data(yq1data);
      this.yq3ds.data(yq3data);

      this.a0ds.data(a0data);
      this.a1ds.data(a1data);
      this.a2ds.data(a2data);
      this.a3ds.data(a3data);
      this.a4ds.data(a4data);

      // axis labels
      this.xAxisLabel.text('Foo bar');
      this.yAxisLabel.text('bar foo 2008');

      // type legend
      this.typeScale.domain(
        <string[]>_(this.datasets[0]).pluck("schType").unique().value());
      // compile

      var svg = $(element).find('svg.schoolScatter');
      this.$compile(svg);
      setTimeout(() => {
        this.$compile(svg)(this.scope);
      }, 5000);
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor() {
      this.bindings = {
        datasets: "<",      // the 3 datasets returned by the scatterdata API
        colorBy: "<",
        colorByValue: "<",
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.template = '<svg class="schoolScatter" height="{{vm.height}}" width="{{vm.width}}"></svg>';
    }
  }

  angular
    .module("pineapples")
    .component("schoolScatterChart", new Component());
}
