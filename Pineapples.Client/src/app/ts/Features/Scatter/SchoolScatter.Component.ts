﻿namespace Pineapples.Dashboards {

  /* Corresponds to the legacy Pineapples X Y Charts */

  class Controller extends Dashboard implements IDashboard {
    public theData: any;    // the data that is used to build the scatter chart
    public resultset: any;
    public isWaiting: boolean;
    public colorBy: string;

    // temporarily put a binding here
    public scatterData: any;

    public $onChanges(changes) {
      if (changes.scatterData) {

     
        // 3 recordsets come back - first is the scatter data

        this.theData = d3.nest()
          .key(function (d: any) { return d.District; })
          .entries(this.scatterData[0]);

      }
    }
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        scatterData: "<"
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "schoolscatter/chart";
    }
  }

  angular
    .module("pineapples")
    .component("componentSchoolScatter", new Component());

}