﻿namespace Pineapples.SurveyEntry {

  interface IBindings {
    resourceList: any     // a ResourceList object
  }
  class Controller implements IBindings {
    public resourceList: any

    static $inject = ["Lookups"];
    constructor(public lookups: Sw.Lookups.LookupService) {
    }

    public $onChanges(changes) {      
      if (changes.resourceList) {
        console.log("changes: ", this.resourceList);
      }
    }

    public get category() {
      if (this.resourceList) {
        return this.resourceList.resourceCategory;
      }
    }

    public columns = {
      Available: 0,
      Adequate: 1,
      Num: 2,
      Qty: 3,
      Condition: 4,
      Functioning: 5
    }
    private _maxcolumn = 5;

    // row/column tracking
    public selectedRow = -1;
    public selectedCol = -1;

    public gridFocus = (row: number, col: number) => {
      this.selectedRow = row;
      this.selectedCol = col;
    };

    public gridBlur = () => {
      this.selectedRow = -1;
      this.selectedCol = -1;
    };

    // keyboard navigation
    public selectedRowNew = -1;
    public selectedColNew = -1;

    public isEnabled(r: number, c: number) {
      var row = this.resourceList.rows[r];

      // Availale is always enabled if it is included in the layout
      if (c === this.columns.Available) {
        return row.promptAvailable;
      }
      if (this.unavailable(r)) {
        return false;
      }

      switch (c) {
        case this.columns.Available:
          return row.promptAvailable;
        case this.columns.Adequate:
          return row.promptAdequate;
        case this.columns.Num:
          return row.promptNum;
        case this.columns.Qty:
          return row.promptQty;
        case this.columns.Condition:
          return row.promptCondition;
        case this.columns.Functioning:
          return row.promptFunctioning;
      }
    }

    public unavailable(r) {
      var row = this.resourceList.rows[r];
      if (row.promptAvailable === false) {
        return false;
      }
      return (this.resourceList.data[r].available ? false : true);
    }

    /**
     * grid key down handler
     * @param $event
     * @param row row index
     * @param col column title
     * @returns {}
     */
    public gridkeydown = ($event, row, col) => {
      // allow any numbers wihtout special handling,

      // eithr from keyboard or number pad
      if ($event.keyCode >= 48 && $event.keyCode <= 57) {
        return;
      }
      // number pad numerics
      if ($event.keyCode >= 96 && $event.keyCode <= 105) {
        return;
      }
      // process anything with alt or ctrl
      if ($event.altKey || $event.ctrlKey) {
        return;
      }
      // process space bar
      if ($event.keyCode == 32) {
        return;
      }
      // process either G (good), F (fair) or P (poor)
      if ($event.keyCode == 71 || $event.keyCode == 70 || $event.keyCode == 80) {
        return;
      }

      var rl: any = this.resourceList;
      let r: number = row;
      let c: number = col;
      let maxr = rl.rows.length - 1;
      let maxc = 5;
      switch ($event.keyCode) {
        case 8:          // backspace
        case 9:          // tab
        case 13:         // enter
        case 45:         // insert
        case 46:         // delete
          return;      // don;t prevent default

        case 33:         // page up

          // find the highest row in which this column is editable
          for (r = 0; r <= this.selectedRow; r++) {
            if (this.isEnabled(r, c)) {
              break;
            }
          }
          break;
        case 34:         // page down
          // step up from the bottom row
          for (r = maxr; r >= this.selectedRow; r--) {
            if (this.isEnabled(r, c)) {
              break;
            }
          }
          break;
        case 35:         // end
          r = maxr;
          // step back along the bottom row until we find an editable column
          for (c = maxc; c >= col; c--) {
            if (this.isEnabled(r, c)) {
              break;
            }
          }

          break;

        case 36:         // home
          r = 0;
          // step forward along the bottom row until we find an editable column
          for (c = 0; c <= col; c++) {
            if (this.isEnabled(r, c)) {
              break;
            }
          }
          break;
        case 37:         // left
          console.log("LEFT");
          for (c = --c; !(r === row && c === col); c--) {
            if (c < 0) {

              // wrap to the end
              c = maxc;
              r = (r === 0 ? maxr : --r);
            }
            console.log(r + ":" + c);
            if (this.isEnabled(r, c)) {
              break;
            }
          }
          break;
        case 38:         // up
          console.log("UP");
          for (r = --r; !(r === row && c === col); r--) {
            if (r < 0) {
              r = maxr;
              c = (c === 0 ? maxc : --c);
            }
            console.log(r + ":" + c);
            if (this.isEnabled(r, c)) {
              break;
            }
          }
          break;

        case 39:  // right
          console.log("RIGHT")
          for (c = ++c; !(r === row && c === col); c++) {
            if (c > maxc) {

              // wrap to the start ot the next row
              c = 0;
              r = (r === maxr ? 0 : ++r);
            }
            console.log(r + ":" + c);
            if (this.isEnabled(r, c)) {
              break;
            }
          }
          break;

        case 40: //down

          console.log("DOWN");
          for (r = ++r; !(r === row && c === col); r++) {
            if (r > maxr) {
              r = 0;
              c = (c === maxc ? 0 : ++c);
            }
            console.log(r + ":" + c);
            if (this.isEnabled(r, c)) {
              break;
            }
          }
          break;
      }
      this.selectedColNew = c;
      this.selectedRowNew = r;
      $event.preventDefault();
    };

   /**
    *
    * @param row
    * @param col
    * @returns true if focus should move to the row/col cell
    *  based on
    * https://www.emberex.com/programmatically-setting-focus-angularjs-way/
    */
    public focusRequested = (row, col, gender) => {
      var result = (row === this.selectedRowNew && col === this.selectedColNew);
      return result;
    };

  }

  class ResourceListGrid implements ng.IComponentOptions {
    public bindings: any = {
      resourceList: "<list",
      ngDisabled: "="
    };
    public controller: any = Controller;
    public controllerAs: string = "vm";

    // angular doesn't watch ngOptions - so we have to make sure the right string is in the template when
    // it is first rendered
    public templateUrl = "survey/resourcelistgrid";
  }

  angular
    .module("pineapples")
    .component("resourceListGrid", new ResourceListGrid());
}
