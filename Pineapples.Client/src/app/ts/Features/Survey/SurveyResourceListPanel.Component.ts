﻿namespace Pineapples.SurveyEntry {

  interface IBindings {
    schoolNo: string;       // the school no
    year: number;     // the survey year
    category: string;      // table name as in metaResourceDefs
  }

  class Controller implements IBindings {

    public gridForm: ng.IFormController;   // angular matches this to the name of the form in the view

    public schoolNo: string;
    public year: number;
    public category: string;

    public isWaiting = false;
    public isEditing = false;

    public setEditing(editing: boolean) {
      this.isEditing = editing;
    }

    static $inject = ["schoolsAPI"];

    private _rl: Pineapples.Survey.ResourceList;
    private _rlInit: Pineapples.Survey.ResourceList;

    constructor(private _api) {
      console.log("gridpage");
    }

    public $onChanges(changes: any) {
      console.log(changes);
      this.getResourceList();
    }

    public get resourceList(): Pineapples.Survey.ResourceList {
      return this._rl;
    };


    public getResourceList() {
      this._api.resourceList(this.schoolNo, this.year, this.category)
        .then((data: Pineapples.Survey.ResourceList) => {
          this._rl = data;
          this.schoolNo = data.schoolNo;
        });
    }

    public save() {
      this.isWaiting = true;
      this._api.saveResourceList(this.resourceList)
        .then((data: Pineapples.Survey.ResourceList) => {
          this.isWaiting = false;
          this._rl = data;
          this.schoolNo = data.schoolNo;
          this.pristine();
        });
    }

    public undo() {
      // TO DO not quite going to work with New, becuase modelInit does not have all the properties that model has
      angular.extend(this._rl, this._rlInit);
      this.pristine();
    }

    public refresh() {
      this.getResourceList();
    }

    private pristine() {
      if (this.gridForm) {
        this.gridForm.$setPristine();
        this._rlInit = angular.copy(this.resourceList);      // if we want to implement Undo
        this.setEditing(false);
      }
    }
  }

  class ResourceListPanel implements ng.IComponentOptions {
    public bindings: any = {
      schoolNo: "@",
      year: "@",
      category: "@"

    };
    public controller: any = Controller;
    public controllerAs: string = "vm";

    public templateUrl = "survey/resourcelistpanel";
  }

  angular
    .module("pineapples")
    .component("resourceListPanel", new ResourceListPanel());
}