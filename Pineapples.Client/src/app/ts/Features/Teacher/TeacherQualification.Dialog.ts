﻿namespace Pineapples.Teachers {

  export interface IQualificationDialogLocals {
    model: TeacherQualification;
  }

  class DialogController extends Sw.Component.ComponentEditController {

    public model;

    static $inject = ["$mdDialog", "ApiUi", "Restangular",];
    constructor(public mdDialog: ng.material.IDialogService, apiUi: any
      , private restangular: restangular.IService) {
      super(apiUi, restangular);
      if (!this.model._id()) {
        this.isNew = true;
        this.isEditing = true;
      }
    }

    public closeDialog() {
      this.mdDialog.cancel();
    }

    public onModelUpdated(newData) {
      if (newData.plain) {
        // its a restangular object
        //        angular.extend(this.source, newData.plain());
        angular.extend(this.model, newData.plain());
      } else {
        //      angular.extend(this.source, newData);
        angular.extend(this.model, newData);
      }
    }

    public get minYear() {
      return new Date().getFullYear() - 70;
    }

    public get maxYear() {
      return new Date().getFullYear() + 3;
    }
  }

  export class QualificationDialog {

    public static options(locals: IQualificationDialogLocals): ng.material.IDialogOptions {
      let o = {
        clickOutsideToClose: true,
        templateUrl: "teacher/qualificationdialog",
        controller: DialogController,
        controllerAs: "vm",
        bindToController: true,
        locals: locals
      }
      return o;
    }

  }
}