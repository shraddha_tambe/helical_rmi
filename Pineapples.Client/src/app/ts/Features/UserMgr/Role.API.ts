﻿/*-----------------------------------------
  Role API
    ------------------------------------------
*/
namespace Sw.Auth {
  export interface IRole {
    Id: string;
    Name: string;
  }
  export interface IUmgr {
    roles(): ng.IPromise<IRole[]>;
    menuKeys(): ng.IPromise<any[]>;
  }

  var api = function ($q: ng.IQService, restAngular: restangular.IService, errorService) {
    //var restAngular = Restangular.withConfig(function(Configurer) {
    //     Configurer.setBaseUrl('/api/v2/messages');
    //  });

    var _svc = restAngular.all('umgr/roles');

    return {
      list: () => {
        return _svc.getList();
      },
      read: function (id) {
        return _svc.get(id).catch(errorService.catch);
      },
      //rowSave from grid
      save: function (rowData) {
        return _svc.post(rowData).catch(errorService.catch);
      },
      new: function () {
        let e = {
          instID: 0,
          Rowversion: null
        };
        restAngular.restangularizeElement(null, e, "role");
        var d = $q.defer();
        d.resolve(e);
        return d.promise;
      }
    }
  };



  angular.module('pineapplesAPI')
    .factory('rolesAPI', ['$q', 'Restangular', 'ErrorService', api])
}
