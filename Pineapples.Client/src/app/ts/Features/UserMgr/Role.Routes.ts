﻿// institution Routes
namespace Sw.Auth {

  let routes = function ($stateProvider) {
    let state: ng.ui.IState = {
      abstract: true
    }
    let statename = "site.roles";
    $stateProvider.state(statename, state);

    state = {
      url: "^/roles/list",
      views: {
        "@": "roleList"
      },
      resolve: {
        roles: ['rolesAPI',  (api) => {
          return api.list();
        }]
      }
    };
    statename = "site.roles.list";
    $stateProvider.state(statename, state);

    state = {
      url: "^/roles/{id}",
      params: { id: null },
      views: {
        "@": "componentRole"
      },
      resolve: {
        model: ['rolesAPI', "$stateParams", (api, stateParams) => {
          return api.read(stateParams.id);
        }]
      }
    };
    statename = "site.roles.list.item";
    $stateProvider.state(statename, state);
  }

  angular
    .module("sw.common")
    .config(['$stateProvider', routes])
}
