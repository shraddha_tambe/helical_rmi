﻿namespace Sw.Auth {
  export interface IRole {
    Id: string;
    Name: string;
  }
  export interface IUmgr {
    roles(): ng.IPromise<IRole[]>;
    menuKeys(): ng.IPromise<any[]>;
  }

  var api = function ($q: ng.IQService, restAngular: restangular.IService, errorService) {
    //var restAngular = Restangular.withConfig(function(Configurer) {
    //     Configurer.setBaseUrl('/api/v2/messages');
    //  });

    var _svc = restAngular.all('umgr/users');

    return {
      list: () => {
        return _svc.getList();
      },
      read: function (id) {
        return _svc.get(id).catch(errorService.catch);
      },
      new: function () {
        let e = {
          instID: 0,
          Rowversion: null
        };
        restAngular.restangularizeElement(null, e, "users");
        var d = $q.defer();
        d.resolve(e);
        return d.promise;
      }
    }
  };



  let umgr = ($q: ng.IQService, restAngular: restangular.IService, errorService) => {
    var _svc = restAngular.all("umgr");
    let r: IUmgr = {
      roles: () => {
        return _svc.customGET("roles");
      },
      menuKeys: () => {
        return _svc.customGET("menukeys");
      },
    };
    return r;
  };


  angular.module('pineapplesAPI')
    .factory('usersAPI', ['$q', 'Restangular', 'ErrorService', api])
    .factory('umgrAPI', ['$q', 'Restangular', 'ErrorService', umgr]);
}
