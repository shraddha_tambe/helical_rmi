﻿namespace Sw.Auth {
  interface IBindings {
    model: any;
    roles: any[];
    menuKeys: any[];
  }
  class Controller extends Sw.Component.ComponentEditController implements IBindings {
    public model: any;
    // list of availablle roles
    public roles: any[]; 
    public menuKeys: any[]; 

    static $inject = ['ApiUi', 'usersAPI', '$http', "$state", 'FileSaver', 'Lookups', "reportManager"];
    constructor(apiUi: Sw.Api.IApiUi, api: any, private _http: ng.IHttpService
      , public state: ng.ui.IStateService
      , private _filesaver: any
      , public lookups: Sw.Lookups.LookupService
      , public reportManager: Pineapples.Reporting.IReportManagerService) {
      super(apiUi, api);
    }

    // lifecycle hooks
    public $onChanges(changes) {
      super.$onChanges(changes);
    }

 
  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        model: '<',
        roles: '<',
        menuKeys: '<'
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "usermgr/Item";
    }
  }
  angular
    .module("sw.common")
    .component("componentUser", new ComponentOptions());
}
