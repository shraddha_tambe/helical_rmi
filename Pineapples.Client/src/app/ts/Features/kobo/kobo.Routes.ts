﻿// Teachers Routes
namespace Pineappples.Kobo {

  let routes = function ($stateProvider) {
    //var mapview = 'TeacherMapView';

    // root state for 'teachers' feature
    let state: ng.ui.IState;
    let statename: string;

    // base kobo state
    state = {
      url: "^/kobo",
      abstract: true
    };
    $stateProvider.state("site.kobo", state);

    // upload state
    state = {
      url: "^/kobo/upload",

      views: {
        "@": {
          component: "koboUploadComponent"
        }
      },
    };
    $stateProvider.state("site.kobo.upload", state);

    // reload state for testing
    state = {
      url: '^/kobo/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("kobo/upload");
        $state.go("site.kobo.upload");
      }]
    };
    statename = "site.kobo.reload";
    $stateProvider.state(statename, state);

  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}