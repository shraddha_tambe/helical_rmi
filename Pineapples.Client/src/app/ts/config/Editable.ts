﻿namespace Pineapples.Api {

  export class Editable implements Sw.Api.IEditable {
    public get _rowversion() {
      return (<any>this)[this._rowversionProperty()];
    }

    public set _rowversion(newValue) {
      (<any>this)[this._rowversionProperty()] = newValue;
    }

    /**
     * the property of the object that contains the Row version
     */
    public _rowversionProperty() {
      return "pRowversion";
    }

    public _id() {
      return (<any>this).id;
    }
    public _type(): string {
      throw "_type() not implemented";
    };
    public _name() {
      return (<any>this).name;
    }

    public remove(): ng.IPromise<any> {
      throw "element is not Restangularized";
    }
    // default for refresh is simply to extend the current object with the new data
    public _onUpdated(newData) {

      angular.extend(this, this._transform(newData));
    }

    // normally we don't send arrays properties back to be saved - because these are 
    // most often related records
    public _beforeSave() {
      return this._dropArrays();
    }

    // stub to allow type changes of the incoming data
    public _transform(newData) {
      return newData;
    }

    // helper methods
    public _transformDates(data: any, datefields: string[]) {
      datefields.forEach(df => {
        if (data[df]) {
          data[df] = new Date(data[df]);
        }
      });
    }

    // clear any array properties from a copy before sending to save
    public _dropArrays() {
      return _.mapValues(this, (p) => {
        if (angular.isArray(p)) {
          return null;
        }
        return p;
      });
    }
  }
}
