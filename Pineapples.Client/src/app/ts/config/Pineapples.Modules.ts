﻿namespace Pineapples {

    angular.module('pineapplesAPI', ['restangular']);

    var app = angular.module('pineapples',
        ['pineapplesAPI',
            'ui.bootstrap',
            'ngAnimate',
            'ngMaterial',                  // angular material - did the Queen Mary just pull up at the jetty?
            'ngMessages',                  // for error handling
            'ui.grid',
            'ui.grid.pagination',       // breaking change in ui.grid from pagigng to pagination
            'ui.grid.pinning',
            //'ui.grid.autoResize', // Still in Beta and not serving any useful purpose currently
            'ui.grid.resizeColumns',
            // support for editable grids
            'ui.grid.cellNav',
            'ui.grid.edit',
            'ui.grid.rowEdit',
            'ui.grid.selection',
            'ui.router',
            'ngMap',
            //'angularDc',
            'SignalR',
            'rzModule',               // the slider
            'ngFileSaver',            // for saving downloaded blobs - eg reports 
            'angularFileUpload',      // file uploader https://github.com/nervgh/angular-file-upload
            'ngThumb',                // in the demos for file uploader, but not part of the package
            // packaged for bower here: https://github.com/RoM4iK/ng-thumb
            'sw.common'

        ]);

    var consts = function ($rootScope) {
        $rootScope.appName = 'Pineapples';
    };

    angular
        .module('pineapples')
        .run(['$rootScope', consts]);
}