﻿namespace Pineapples {

  // $mdDateLocaleProvider configures date display in angularjs material
  // see https://material.angularjs.org/latest/api/service/$mdDateLocaleProvider
  // adapted from angularjs material documentation
  // note this introduces moment.ts for date handling logic
  let config = ($mdDateLocaleProvider) => {
    // get the current user locale from the operating system
    // https://www.w3schools.com/jsref/prop_nav_language.asp
    let locale = window.navigator.language;
    // set moment's locale 
    moment.locale(locale.toLowerCase());

    // Use moment.js to parse and format dates.
    $mdDateLocaleProvider.parseDate = function (dateString) {
      // parse incoming dates according to the client machine settings
      // moment defines this setting 'L' - try it at 
      // https://momentjs.com/
      // parse the date according to the locale
      let m = moment(dateString, "L");
      if (m.isValid()) {
        return m.toDate();
      }
      m = moment(dateString);
      return m.isValid() ? m.toDate() : new Date(NaN);
    };

    $mdDateLocaleProvider.formatDate = function (date) {
      var m = moment(date);
      // use this output format to minimize ambiguity
      return m.isValid() ? m.format('D-MMM-YYYY') : '';
    };

  
    // In addition to date display, date components also need localized messages
    // for aria-labels for screen-reader users.

    $mdDateLocaleProvider.weekNumberFormatter = function (weekNumber) {
      return 'Week ' + weekNumber;
    };

    $mdDateLocaleProvider.msgCalendar = 'Calendar';
    $mdDateLocaleProvider.msgOpenCalendar = 'Open the calendar';

    // You can also set when your calendar begins and ends.
    $mdDateLocaleProvider.firstRenderableDate = new Date(1756, 1, 27);
    $mdDateLocaleProvider.lastRenderableDate = new Date(2099, 12, 31);

    // this modifies the json emitted by date objects to drop the Time zone information
    // this prevents problems with mismatch of interpretation as described in issue #382
 	 Date.prototype.toJSON = function () {
        var m = moment(this);
        return m.format("YYYY-MM-DDTHH:mm:ss");
      }
  };

  angular
    .module('pineapples')
    .config(['$mdDateLocaleProvider', config])


}