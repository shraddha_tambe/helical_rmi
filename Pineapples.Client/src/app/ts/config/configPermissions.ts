﻿module Sw.Auth {


  function permissionFactory(identity: Sw.Auth.IIdentity, charIdx: number, rightMask: number) {
    return () => {
      let val = identity.permissionHash.charCodeAt(charIdx);
      return (((val - 48) & rightMask) > 0 ? true : false);
    };
  }

  // create a role for the ful set of server defined roles 
  function processRoles(roles: Sw.Auth.IRole[], roleStore: ng.permission.RoleStore, identity: Sw.Auth.IIdentity) {
    roles.forEach((r) => {
      let roleName = r.Name;
      roleStore.defineRole(roleName, (stateparams) => {
        return identity.inRole(roleName);
      });
    });
  }
  function permissions(roleStore: angular.permission.RoleStore
    , permissionStore: angular.permission.PermissionStore
    , identity: Sw.Auth.IIdentity
    , umgr: Sw.Auth.IUmgr) {

    umgr.roles().then((roles) => {
      processRoles(roles, roleStore, identity);
    });


    roleStore.defineRole("anonymous", (stateParams) => {
      return !identity.isAuthenticated;
    });
    roleStore.defineRole("authenticated", (stateParams) => {
      return identity.isAuthenticated;
    });

    let permissionHash: string;
    let areas: string[] = [
      "Enrolment",
      "Establishment",
      "Exam",
      "Finance",
      "Infrastructure",
      "Inspection",
      "IST",
      "PA",
      "School",
      "Survey",
      "Teacher"
    ];

    let rights: any[][] = [
      ["Admin", 32],
      ["Ops", 16],
      ["WriteX", 8],
      ["Write", 4],
      ["ReadX", 2],
      ["Read", 1]
    ]


    let areaIdx = 0;
    let rIdx = 0;
    for (areaIdx = 0; areaIdx < areas.length; areaIdx++) {
      for (rIdx = 0; rIdx < rights.length; rIdx++) {
        let area = areas[areaIdx];
        let right = rights[rIdx];
        let permission = area + right[0];
        var rMask = right[1];
        permissionStore.definePermission(permission, permissionFactory(identity, areaIdx, rMask));
      };
    };

    // append the areas as a cusomt property to permission store, gives us a way to decode them when needed
    // ( e.g. error reporting) permissionStore.areas[i];
    (<any>permissionStore).areas = areas;
  }

  permissions.$inject = ["PermRoleStore", "PermPermissionStore", "identity", "umgrAPI"];
  angular
    .module("pineapples")
    .run(permissions);
}
