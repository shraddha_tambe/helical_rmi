﻿namespace Pineapples.Books {

  export class Book extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(bookData) {
      super();
      angular.extend(this, bookData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let book = new Book(resultSet);
      return book;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).bkTitle;
    }
    public _type() {
      return "book";
    }
    public _id() {
      return (<any>this).bkCode
    }

    public Curriculum: any[];
    public Holdings: any[];
  }
}
