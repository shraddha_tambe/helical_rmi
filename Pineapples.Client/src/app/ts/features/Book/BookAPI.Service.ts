﻿module Pineapples.Api {

  let factory = ($q: ng.IQService, restAngular: restangular.IService, errorService) => {

    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "books");
    svc.read = (id) => svc.get(id).catch(errorService.catch);

    svc.new = () => {

      let e = new Pineapples.Books.Book({
        bkCode: null
      });
      restAngular.restangularizeElement(null, e, "books");
      var d = $q.defer();
      d.resolve(e);
      return d.promise;
    }
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("booksAPI", ['$q', 'Restangular', 'ErrorService', factory]);
}
