﻿namespace Pineapples.Documents {
  export class Document extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(documentData) {
      super();
      this._transform(documentData);
      angular.extend(this, documentData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let document = new Document(resultSet);
      return document;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).docTitle;
    }
    public _type() {
      return "document";
    }
    public _id() {
      return (<any>this).docID
    }

    public _transform(newData) {
      // convert these incoming data values
      // transformDates is deprecated: http interceptor now converts dates on load
      //this._transformDates(newData, ["docDate"]);
      return super._transform(newData);
    }
  }
}