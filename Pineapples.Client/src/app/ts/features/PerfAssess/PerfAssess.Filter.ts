﻿namespace Pineapples.PerfAssess {

  let viewDefaults = {
      columnSet: 1,
      columnDefs: [
        {
          field: 'paID',
          name: 'paID',
          displayName: 'PA ID',
          width: 90,
          cellClass: 'gdAlignRight'
        },
        {
          field: 'pafrmCode',
          name: 'Type',
          width: 60,
          cellClass: 'gdAlignLeft',
          enableSorting: false
        },
        {
          displayName: 'Name',
          name: 'ctlFull',
          field: 'tSurname',
          cellTemplate: '<div class="ui-grid-cell-contents"><span class="surname">{{row.entity.tSurname}}</span>, {{row.entity.tGiven}}</div>',
          pinnedLeft: true,
          width: 300,
          enableSorting: true,
          sortDirectionCycle: ["asc", "desc"]
        },
        {
          field: 'paSchNo',
          name: 'School No',
          width: 80,
          cellClass: 'gdAlignLeft',
          enableSorting: false
        },
        {
          field: 'schName',
          name: 'School Name',
          width: 200,
          cellClass: 'gdAlignLeft',
          enableSorting: false
        },
        {
          field: "paDate",
          name: 'paDate',
          displayName: "Date",
          cellFilter: "date",
          width: 120,
          cellClass: 'gdAlignLeft',
          enableSorting: false
        },
        {
          field: "paConductedBy",
          name: 'paConductedBy',
          displayName: "Conducted By",
          width: 200,
          cellClass: 'gdAlignLeft',
          enableSorting: false
        },
      ]  // columndefs
    };

  class PerfAssessParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {

    constructor(lookups: any) {
      super(lookups);
    };

    public toFlashString(params: any) {
      // TO DO
      var tt = ['SchoolType', 'District',
        'Authority', 'SchoolName'
      ];
      return this.flashStringBuilder(params, tt);
    }

    public fromFlashString(flashstring: string) {
      if (flashstring.trim().length === 0) {
        return;
      }
      let params: any = {};
      var parts = this.tokenise(flashstring);         // 8 3 2015 smarter Tokenise
      var parsed = [];
      for (var i = 0; i < parts.length; i++) {
        var s = parts[i].trim();
        var S = s.toUpperCase();

        if (this.cacheFind(S, params, "schoolTypes", "SchoolType")) {
          continue;
        }
        if (this.cacheFind(S + ' SCHOOL', params, "schoolTypes", "SchoolType")) {
          continue;
        }
        if (this.cacheFind(S, params, "districts", "District")) {
          continue;
        }
        if (this.cacheFind(S, params, "authorities", "Authority")) {
          continue;
        }
        if (s.indexOf('*') >= 0 || s.indexOf('?') >= 0 || s.indexOf('%') >= 0) {
          if (!params.SchoolName) {
            params.SchoolName = s;
            parsed.push(s);
            continue;
          }
        }
        if (parts.length === 1) {
          params.schNo = s;
          parsed.push(s);
        }
      }

      return params;
    }

    protected getParamString(name, value) {
      switch (name) {
        case 'AwardType':
          return this.lookups.findByID('awardTypes', value).C;
        default:
          return value.toString();
      }
    }
  }

  class Filter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "perfAssessAPI", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService,
      protected lookups: Sw.Lookups.LookupService, protected api: any, protected identity: Sw.Auth.IIdentity) {
      super();
      this.ViewDefaults = viewDefaults;
      this.entity = "performance assessment";
      this.ParamManager = new PerfAssessParamManager(lookups);
    }

    public createFindConfig() {
      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer<Sw.Filter.FindConfig>();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "paID";
      config.defaults.paging.sortDirection = "asc";
      config.defaults.table.row = "District";
      config.defaults.table.col = "School Type";
      config.defaults.viewMode = this.ViewModes[0].key;
      config.current = angular.copy(config.defaults);
      config.tableOptions = "paFields";
      config.dataOptions = "schoolDataOptions";

      config.identity.filter = this.identityFilter();
      config.reset();
      this.lookups.getList(config.tableOptions).then((list: Sw.Lookups.ILookupList) => {
        d.resolve(config);
      }, (reason: any) => {
        console.log("Unable to get tableOptions for PerfAssessFilter: " + reason);
        d.resolve(config);
      });
      return d.promise;
    }

  }
  angular
    .module("pineapples")
    .service("PerfAssessFilter", Filter);
}
