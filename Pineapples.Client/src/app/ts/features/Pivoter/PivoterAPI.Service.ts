﻿module Pineapples.Api {

  let factory = (restAngular, errorService) => {

    let svc = restAngular.all('pivoter');
    svc.addRestangularMethod("enrolment", "post", "collection/enrolment");

    return svc;
  };
  angular.module("pineapplesAPI")
    .factory("pivoterAPI", ['Restangular', 'ErrorService', factory]);
}

