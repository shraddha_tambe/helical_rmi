﻿namespace Pineapples.QuarterlyReports {

  export class QuarterlyReport extends Pineapples.Api.Editable implements Sw.Api.IEditable {

    constructor(qrData) {
      super();
      angular.extend(this, qrData);
    }

    // create static method returns the object constructed from the resultset object from the server
    public static create(resultSet: any) {
      let qr = new QuarterlyReport(resultSet);
      return qr;
    }

    // IEditable implementation
    public _name() {
      return (<any>this).schNo + '-' + (<any>this).InspQuarterlyReport;
    }

    public _type() {
      return "quarterlyreport";
    }

    public _id() {
      return (<any>this).qrID
    }

  }
}
