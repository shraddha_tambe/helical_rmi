﻿module Pineapples.Api {

  let factory = (restAngular, errorService) => {
    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "schoolScatter");
    svc.getScatter = (params: any) => {
      return svc.customPOST(params, "getScatter");
    };
    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("schoolScatterAPI", ['Restangular', 'ErrorService', factory]);
}
