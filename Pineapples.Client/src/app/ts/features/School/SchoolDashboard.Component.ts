﻿namespace Pineapples.Schools {

  class Controller {
    
    constructor() { }   

    private _currentSchoolsByTypeReport: Pineapples.Reporting.Report;

    /**
     * Create a report definition to feed to the reportdef component
     **/
    public get currentSchoolsByTypeReport() {
      if (!this._currentSchoolsByTypeReport) {
        let report = new Pineapples.Reporting.Report();
        report.provider = "jasper";
        report.path = "School_Reports/Current_Number_of_Schools_by_Type";
        report.name = "Current Number of Schools by Type";
        report.format = "PDF";        
        this._currentSchoolsByTypeReport = report;
      }
      return this._currentSchoolsByTypeReport;
    }

    /**
     * callback from reportdef component on report execution
     * @param report - the Report object
     * @param context - context as passed to the report component
     * @param instanceInfo - instanceInfo object, its properties can be
     * set in this call back, specifically:
     * parameters: these are the report parameters that will go to the report server
     * filename: if supplied can be a more meaningful filename for the downloaded file
     */
    public setupReport(report, context, instanceInfo) {
      instanceInfo.parameters = {}
      instanceInfo.filename = "Current_Number_of_Schools_by_Type";
    }

  }

  class ComponentOptions implements ng.IComponentOptions {
    public controller = Controller;
    public controllerAs = "vm";
    public templateUrl = "school/Dashboard";
  }

  angular
    .module("pineapples")
    .component("schoolsDashboardComponent", new ComponentOptions());
}