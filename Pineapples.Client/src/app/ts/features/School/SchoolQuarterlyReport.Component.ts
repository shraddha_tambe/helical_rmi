﻿namespace Pineapples.Schools {

  interface IBindings {
    quarterlyreports: any;
  }

  class Controller implements IBindings {
    public quarterlyreports: any;

    constructor() { }

    public $onChanges(changes) {
      if (changes.quarterlyreports) {
        console.log('$onChanges quarterlyreports: ', this.quarterlyreports);
      }
    }

  }

  class Component implements ng.IComponentOptions {
    public bindings = {
      quarterlyreports: '<'
    };
    public controller = Controller;
    public controllerAs = "vm";
    public templateUrl = "school/quarterlyreports";
  }

  angular
    .module("pineapples")
    .component("schoolQuarterlyReports", new Component());
}