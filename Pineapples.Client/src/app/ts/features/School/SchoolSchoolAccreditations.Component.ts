﻿namespace Pineapples.Schools {

  interface IBindings {
    schoolaccreditations: any;
  }

  class Controller implements IBindings {
    public schoolaccreditations: any;

    constructor() { }

    public $onChanges(changes) {
      if (changes.schoolaccreditations) {
        console.log('$onChanges: ', this.schoolaccreditations);
      }
    }

  }

  class Component implements ng.IComponentOptions {
      public bindings = {
        schoolaccreditations: '<'
      };
      public controller = Controller;
      public controllerAs = "vm";
      public templateUrl = "school/schoolaccreditations";
  }

  angular
    .module("pineapples")
    .component("schoolAccreditations", new Component());
}