﻿namespace Pineapples.SchoolAccreditations {

  class Controller {
    
    constructor() { }   

    private _nationalSchoolAccreditationProgressReport: Pineapples.Reporting.Report;

    /**
     * Create a report definition to feed to the reportdef component
     **/
    public get nationalSchoolAccreditationProgressReport() {
      if (!this._nationalSchoolAccreditationProgressReport) {
        let report = new Pineapples.Reporting.Report();
        report.provider = "jasper";
        report.path = "School_Accreditations_Reports/National_Standard_Performance_Accreditation_Progress";
        report.name = "National Standard Performance Accreditation Progress";
        report.format = "PDF";        
        this._nationalSchoolAccreditationProgressReport = report;
      }
      return this._nationalSchoolAccreditationProgressReport;
    }

    /**
     * callback from reportdef component on report execution
     * @param report - the Report object
     * @param context - context as passed to the report component
     * @param instanceInfo - instanceInfo object, its properties can be
     * set in this call back, specifically:
     * parameters: these are the report parameters that will go to the report server
     * filename: if supplied can be a more meaningful filename for the downloaded file
     */
    public setupReport(report, context, instanceInfo) {
      instanceInfo.parameters = {}
      instanceInfo.filename = "National_Standard_Performance_Accreditation_Progress";
    }

  }

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "schoolaccreditation/Dashboard";
    }
  }

  angular
    .module("pineapples")
    .component("schoolAccreditationDashboardComponent", new ComponentOptions());
}