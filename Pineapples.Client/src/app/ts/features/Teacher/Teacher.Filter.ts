﻿module Pineapples {

  let viewDefaults = {
    columnSet: 0,
    columnDefs: [
      //{
      //  field: 'tID',
      //  name: 'Teacher ID',
      //  displayName: 'Teacher ID',
      //  editable: false,
      //  pinnedLeft: true,
      //  width: 90,
      //  cellTemplate: '<div class="ngCellText ui-grid-cell-contents" ng-class="col.colIndex()"><a  ng-click="grid.appScope.listvm.action(\'item\',col.field, row.entity);">{{row.entity[col.field]}}</a></div>'
      //},
      {
        field: 'tSurname',
        name: 'ctlFull',
        displayName: 'Name',
        pinnedLeft: true,
        width: 160,
        cellTemplate: 
          `<div class="ui-grid-cell-contents">
            <a  ng-click="grid.appScope.listvm.action('item','tID', row.entity);">
            <span class="surname">{{row.entity.tSurname}}</span>, {{row.entity.tGiven}}
           </a></div>`,
        enableSorting: true,
        sortDirectionCycle: ["asc", "desc"]
      },
      {
        field: 'tSex',
        name: 'Gender',
        pinnedLeft: true,
        width: 70,
        enableSorting: true,
        sortDirectionCycle: ["asc", "desc"]
      },
      { // Feedback was to hide the DoB and I don't see any reason for this column to take space in the list grid (open teacher detail if really needed)
        // reason is perhaps to identify... replace ID with DoB
        field: 'tDOB',
        name: 'tDOB',
        pinnedLeft: true,
        displayName: 'DoB',
        cellFilter: "date:'d-MMM-yyyy'",
        width: 100,
        enableSorting: true,
        sortDirectionCycle: ["asc", "desc"]
      },
    ]
  };

  class TeacherParamManager extends Sw.Filter.FilterParamManager implements Sw.Filter.IFilterParamManager {

    constructor(lookups: any) {
      super(lookups);
    };

    public toFlashString(params: any) {
      var tt = ['ApplicationID', 'YearOfStudy', 'Gender', 'CountryApply',
        'AwardType', 'Status', 'PrioritySectorGroup', 'LevelOfStudyS1',
        'InService', 'ApplicantName'
      ];
      return this.flashStringBuilder(params, tt);
    }

    public fromFlashString(flashstring: string) {
      if (flashstring.trim().length === 0) {
        return;
      }
      let params: any = {};
      var parts = this.tokenise(flashstring);         // 8 3 2015 smarter Tokenise
      var parsed = [];
      for (var i = 0; i < parts.length; i++) {

        // test for numeric
        var s = parts[i].trim();
        if ($.isNumeric(s)) {
          var n = parseInt(s);
          if (n > 1990 && n < 2050) {
            if (!params.YearOfStudy) {
              params.YearOfStudy = n;
            }
          } else {
            // if there is only 1 part, assume its an ID number
            if (!params.ApplicationID) {
              params.ApplicationID = n;
            }
          }
          parsed.push(s);
        } else {
          // for non-numerics look explicitly for a gender, country code, Award Type, Priority Sector or Status
          var S = s.toUpperCase();
          var found = false;

          if (this.cacheFind(S, params, "schoolTypes", "SchoolType")) {
            continue;
          }
          if (this.cacheFind(S + ' SCHOOL', params, "schoolTypes", "SchoolType")) {
            continue;
          }
          if (this.cacheFind(S, params, "districts", "District")) {
            continue;
          }
          if (this.cacheFind(S, params, "authorities", "Authority")) {
            continue;
          }

          if (s.indexOf('*') >= 0 || s.indexOf('?') >= 0 || s.indexOf('%') >= 0) {
            if (!params.SchoolName) {
              params.SchoolName = s;
              parsed.push(s);
              found = true;
            }
          }
        }
      }
      return params;
    }

    protected getParamString(name, value) {
      switch (name) {
        case 'AwardType':
          return this.lookups.findByID('awardTypes', value).C;
        default:
          return value.toString();
      }

    }
  }
  class TeacherFilter extends Sw.Filter.Filter implements Sw.Filter.IFilter {

    static $inject = ["$rootScope", "$state", "$q", "Lookups", "teachersAPI", "identity"];
    constructor(protected $rootScope: ng.IRootScopeService, protected $state: ng.ui.IStateService, protected $q: ng.IQService,
      protected lookups: Sw.Lookups.LookupService, protected api: any, protected identity: Sw.Auth.IIdentity) {
      super();
      this.ViewDefaults = viewDefaults;
      this.entity = "teacher";
      this.ParamManager = new TeacherParamManager(lookups);
    }

    protected identityFilter() {
      let fltr: any = {};
      // cycle through any filters in the identity, use them
      // to construct the identity filter
      for (var propertyName in this.identity.filters) {
        switch (propertyName) {
          case "Authority":
          case "District":
          case "ElectorateN":
          case "ElectorateL":
            fltr[propertyName] = this.identity.filters[propertyName];
            break;
          case "SchoolNo":
            fltr["AtSchool"] = this.identity.filters[propertyName];
            break;
            
        }
      }
      return fltr;
    }
    public createFindConfig() {
      let config = new Sw.Filter.FindConfig();
      let d = this.$q.defer<Sw.Filter.FindConfig>();
      config.defaults.paging.pageNo = 1;
      config.defaults.paging.pageSize = 50;
      config.defaults.paging.sortColumn = "tSurname";
      config.defaults.paging.sortDirection = "asc";
      config.defaults.table.row = "District";
      config.defaults.table.col = "School Type";
      config.defaults.viewMode = this.ViewModes[0].key;
      config.current = angular.copy(config.defaults);
      config.tableOptions = "teacherFieldOptions";
      config.dataOptions = "teacherDataOptions";

      config.identity.filter = this.identityFilter();
      config.reset();
      this.lookups.getList(config.tableOptions).then((list: Sw.Lookups.ILookupList) => {
        // resolve it when it the lookup is available
        d.resolve(config);
      }, (reason: any) => {
        // swallow the error
        console.log("Unable to get tableOptions for TeacherFilter: " + reason);
        d.resolve(config);
      });
      return d.promise;
    }

  }
  angular
    .module("pineapples")
    .service("TeacherFilter", TeacherFilter);
}
