﻿// Teachers Routes
namespace Pineappples.Teachers {

  let routes = function ($stateProvider) {
    var featurename = 'Teachers';
    var filtername = 'TeacherFilter';
    var templatepath = "teacher";
    var tableOptions = "teacherFieldOptions";
    var url = "teachers";
    var usersettings = null;
    //var mapview = 'TeacherMapView';

    // root state for 'teachers' feature
    let state: ng.ui.IState = Sw.Utils.RouteHelper.featureState(featurename, filtername, templatepath, url, usersettings, tableOptions);

    // default 'api' in this feature is teachersAPI
    state.resolve = state.resolve || {};
    state.resolve["api"] = "teachersAPI";

    let statename = "site.teachers";
    $stateProvider.state(statename, state);

    // takes a list state
    state = Sw.Utils.RouteHelper.listState("Teacher");
    statename = "site.teachers.list";
    state.url = "^/teachers/list";
    $stateProvider.state(statename, state);
    console.log("state:" + statename);
    console.log(state);

    state = {
      url: '^/teachers/reload',
      onEnter: ["$state", "$templateCache", function ($state, $templateCache) {
        $templateCache.remove("teacher/item");
        $templateCache.remove("teacher/searcher");
        $state.go("site.teachers.list");
      }]
    };
    statename = "site.teachers.reload";
    $stateProvider.state(statename, state);

    // chart, table and map
    Sw.Utils.RouteHelper.addChartState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addTableState($stateProvider, featurename);
    Sw.Utils.RouteHelper.addMapState($stateProvider, featurename); // , mapview

    // new - state with a custom url route
    state = {
      url: "^/teachers/new",
      params: { id: null, columnField: null, rowData: {} },
      data: {
        permissions: {
          only: 'TeacherWriteX'
        }
      },
      views: {
        "actionpane@site.teachers.list": {
          component: "componentTeacher"
        }
      },
      resolve: {
        model: ['teachersAPI', '$stateParams', function (api, $stateParams) {
          return api.new();
        }]
      }
    };
    $stateProvider.state("site.teachers.list.new", state);
    
    state = {
      url: "^/teachers/reports",
      views: {
        "@": "reportPage"       // note this even more shorthand syntax for a component based view
      },
      resolve: {
        folder: () => "Teachers",           // not a promise, but to get the automatic binding to the component, make a resolve for folder
        promptForParams: () => "always"
      }
    }
    $stateProvider.state("site.teachers.reports", state);

    // ----------------------------------------------------------
    // dashboard
    state = {
      url: '^/teachers/dashboard',
      views: {
        "@": "teachersDashboard"     // to do - create this component
      },
      resolve: {
        // resolve the calculator
        table: ['$http', (http: ng.IHttpService) => {
          return http.get("api/warehouse/teachercount").then(response => (<any>response.data));
        }]
      }
    };
    $stateProvider.state("site.teachers.dashboard", <any>state);


    // item state
    //$resolve is intriduced in ui-route 0.3.1
    // it allows bindings from the resolve directly into the template
    // injections are replaced with bindings when using a component like this
    // the component definition takes care of its required injections - and its controller
    state = {
      url: "^/teachers/{id}",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "actionpane@site.teachers.list": {
          component: "componentTeacher"
        }
      },
      resolve: {
        model: ['teachersAPI', '$stateParams', function (api, $stateParams) {
          return api.read($stateParams.id);
        }]
      }
    };
    $stateProvider.state("site.teachers.list.item", state);

    // upload file / image state
    state = {
      url: "^/upload/teachers/{id}",
      params: { id: null, columnField: null, rowData: {} },
      views: {
        "@": {
          component: "teacherLinkUploadComponent"
        }
      },
      resolve: {
        teacher: ['teachersAPI', '$stateParams', function (api, $stateParams) {
          return api.read($stateParams.id);
        }]
      }
    };
    $stateProvider.state("site.teachers.upload", state);
  }

  angular
    .module('pineapples')
    .config(['$stateProvider', routes])

}