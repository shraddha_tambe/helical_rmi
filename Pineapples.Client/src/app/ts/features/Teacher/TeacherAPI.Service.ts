﻿module Pineapples.Api {

  let factory = ($q: ng.IQService, restAngular: restangular.IService, errorService) => {

    let svc: any = Sw.Api.ApiFactory.getApi(restAngular, errorService, "teachers");

    //svc.addRestangularMethod("read", "get");
    svc.read = (id) => svc.get(id).catch(errorService.catch);
    svc.new = () => {

      let e = new Pineapples.Teachers.Teacher({
        tID: null
      });
      restAngular.restangularizeElement(null, e, "teachers");
      var d = $q.defer();
      d.resolve(e);
      return d.promise;
    }

    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("teachersAPI", ['$q', 'Restangular', 'ErrorService', factory]);
}
