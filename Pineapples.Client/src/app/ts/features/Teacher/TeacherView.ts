﻿namespace Pineapples.Teachers {
  let modes = [
    {
      key: "Employment",
      gridOptions: {
        columnDefs: [
          {
            field: 'tPayroll',
            name: 'Payroll',
            displayName: 'EmployNo',
            headerCellFilter: 'vocab',
            cellClass: 'gdAlignRight',
            enableSorting: true,
            sortDirectionCycle: ["asc", "desc"],

          },
          {
            field: 'tProvident',
            name: 'Provident Fund',
            displayName: 'ProvidentNo',
            headerCellFilter: 'vocab',
            cellClass: 'gdAlignRight',
          },
          {
            field: 'tDatePSAppointed',
            name: 'tDatePSAppointed',
            displayName: 'PS Appointed',
            cellFilter: "date:'d-MMM-yyyy'",
          },
          {
            field: 'tDatePSClosed',
            name: 'tDatePSClosed',
            displayName: 'PS End',
            cellFilter: "date:'d-MMM-yyyy'",
          },
        
     
          {
            field: 'tYearStarted',
            name: 'tYearStarted',
            displayName: 'Started Teaching',
           
          },
          {
            field: 'tYearEnded',
            name: 'tYearEnded',
            displayName: 'Finished Teaching',
          },
        ]
      }
    },
    {
      key: "Latest Survey",
      columnSet: 2,
      gridOptions: {
        columnDefs: [
          {
            field: 'svyYear',
            name: 'Year',
            cellClass: 'gdAlignRight',
            enableSorting: false
          },
          {
            field: 'schNo',
            name: 'School ID',
            displayName: 'School ID',
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
          {
            field: 'schName',
            name: 'School Name',
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
          {
            field: 'tchStatus',
            name: 'Status',
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
          {
            field: 'tchRole',
            name: 'Role',
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
          {
            field: 'tchTAM',
            name: 'Duties',
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
          {
            field: 'tchHouse',
            name: 'House?',
            cellClass: 'gdAlignLeft',
            enableSorting: false
          }
        ]  // columndefs
      }       // gridoptions
    },           //mode
    {
      key: "Registration",
      columnSet: 1,
      gridOptions: {
        columnDefs: [
          {
            field: "tRegister",
            name: "tRegister",
            displayName: "Registration",
            cellClass: 'gdAlignLeft',
          },
          {
            field: "tRegisterStatus",
            name: "tRegisterStatus",
            displayName: "Status",
            cellClass: 'gdAlignLeft',
          },
          {
            field: "tDateRegister",
            name: "tDateRegister",
            displayName: "Date",
            cellClass: 'gdAlignLeft',
            cellFilter: "date:'d-MMM-yyyy'",
          },
          {
            field: "tDateRegisterEnd",
            name: "tDateRegisterEnd",
            displayName: "End Date",
            cellClass: 'gdAlignLeft',
            cellFilter: "date:'d-MMM-yyyy'",
          },
          {
            field: "tRegisterEndReason",
            name: "tRegisterEndReason",
            displayName: "End Reason",
            cellClass: 'gdAlignLeft',
          },
        ]
      }
    },
    {
      key: "Latest PA",
      columnSet: 20,
      gridOptions: {
        columnDefs: [
          {
            field: 'paID',
            name: 'paID',
            displayName: 'Performance Assessment ID',
            cellClass: 'gdAlignRight'
          },
          {
            field: 'pafrmCode',
            name: 'Type',
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
          {
            field: 'paSchNo',
            name: 'School ID',
            displayName: 'School ID',
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
          {
            field: 'schName',
            name: 'School Name',
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
          {
            field: "paDate",
            name: 'paDate',
            displayName: "Date",
            cellFilter: "date:'d-MMM-yyyy'",
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
          {
            field: "paConductedBy",
            name: 'paConductedBy',
            displayName: "Conducted By",
            cellClass: 'gdAlignLeft',
            enableSorting: false
          },
        ]  // columndefs
      }       // gridoptions
    }           //mode
  ];              // modes

  var pushModes = function (filter) {
    filter.PushViewModes(modes);
  };

  angular
    .module('pineapples')
    .run(['TeacherFilter', pushModes]);
}
