﻿namespace Pineapples.Teachers {

  interface IBindings {
    payslips: any;
  }

  class Controller implements IBindings {
    public payslips: any;
 
    static $inject = ["$mdDialog", "$location", "$state"];
    constructor(public mdDialog: ng.material.IDialogService, private $location: ng.ILocationService, public $state: ng.ui.IStateService) { }

    public $onChanges(changes) {
      if (changes.payslips) {
//        console.log(this.surveys);
      }
    }

    public showPayslip(payslip) {
      let options: any = {
        clickOutsideToClose: true,
        templateUrl: "teacher/payslipdialog",
        controller: DialogController,
        controllerAs: "dvm",
        bindToController: true,
        locals: {
          payslip: payslip
          //payslip: ["teacherApi", (api) => {
          //  return api.payslip(id);
          //}]
        }

      }
      this.mdDialog.show(options);
    }

 
  }

  class Component implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public templateUrl: string;

    constructor() {
      this.bindings = {
        payslips: '<',
      };
      this.controller = Controller;
      this.controllerAs = "vm";
      this.templateUrl = "teacher/paysliplist";
    }
  }

  // popup dialog for audit log
  class DialogController extends Sw.Component.MdDialogController {
    static $inject = ["$mdDialog", "payslip"];
    constructor(mdDialog: ng.material.IDialogService, public payslip) {
      super(mdDialog);
    }
  }

  angular
    .module("pineapples")
    .component("componentTeacherPayslipList", new Component());
}
