﻿module Pineapples.Api {

  let factory = (restAngular, errorService) => {
    let svc = Sw.Api.ApiFactory.getApi(restAngular, errorService, "applications");
    svc.addRestangularMethod("read", "get");
    svc.addRestangularMethod("updateLatLng", "customPOST", "item/updateLatLng");

    return svc;
  };

  angular.module("pineapplesAPI")
    .factory("workforceAPI", ['Restangular', 'ErrorService', factory]);
}
