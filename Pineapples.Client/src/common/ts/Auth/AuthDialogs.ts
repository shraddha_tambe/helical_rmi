﻿module Sw.Auth {

  export interface IAuthenticationDialogs {
    showLogin(): void;
    showNoConnection(): ng.IPromise<any>;
    cancelDialogs(): void;
  }


  class AuthenticationDialogs {
    static $inject = ["$mdDialog", "authenticationService", "$state"];

    constructor(private _mdDialog: ng.material.IDialogService, private _authService: IAuthenticationService,
      private _state: angular.ui.IStateService) { }

    private _getIEVersion() {
      let match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
      return match ? parseInt(match[1]) : undefined;
    }

    public showLogin() {
      let dlg: ng.material.IDialogOptions = {
        // Currently making use of same login markup as signin for consistency
        // IDialogOptions' defaults are use
        template:
        `
<div layout="row" layout-align="center center" style="min-height: 70%">
  <md-card flex="flex" flex-gt-xs="60" flex-gt-sm="33" ng-controller="LoginController as vm">
    <md-toolbar>
      <div class="md-toolbar-tools">
        <h2>Welcome! Please Login</h2>
      </div>
    </md-toolbar>
    <md-card-header layout="column">
      <img src="asset/logo" style="width:150px; margin-left:auto;margin-right:auto" aria-label="Logo" />
    </md-card-header>
    <md-card-content> 
      <form name="Form" ng-submit="vm.logIn(email, password, rememberMe)">
        <md-input-container class="md-block">
          <label>Account Name (user or email)</label>
          <input type="text" name="email" ng-model="email" required />
          <div ng-messages="Form.email.$error" role="alert">
            <div ng-message="required">You must supply an Account Name (user or email.)</div>
          </div>
        </md-input-container>
        <md-input-container class="md-block">
          <label>Password</label>
          <input type="password" name="password" ng-model="password" required />
          <div ng-messages="Form.password.$error" role="alert">
            <div class="my-message" ng-message="required">You must supply a password.</div>
          </div>
        </md-input-container>
        <md-input-container class="md-block">
          <md-checkbox name="rememberMe" ng-model="rememberMe">
            Stay logged in?
          </md-checkbox>
        </md-input-container>
        <i class="icon-spinner icon-spin icon-large"></i>
        <md-button class="md-raised md-primary" type="submit" ng-disabled="Form.$invalid || loginInProcess">
          {{loginState}} <i class="fa fa-spinner spinning" ng-show="loginInProcess" aria-hidden="true"></i>
        </md-button>
      </form>
      <md-card-actions layout="row" layout-align="start center">
        <md-button ng-click="vm.gotoForgotPassword()">Forgot password?</md-button>
      </md-card-actions>
      <div class="text-danger">{{messageText}}</div>
    </md-card-content>
  </md-card>
</div>
        `,
        controller: 'LoginPopupController',
        controllerAs: 'vm'
      };
      // if the mdDialog can't be displayed ( ie9?) then let's fall back to
      if (this._getIEVersion() < 10) {
        // just reroute to signon
        this._state.go("signin");
      } else {
        this._mdDialog.show(dlg);
      }

    }
    // this dialog is displayed if there is a 502 or -1 response from http
    public showNoConnection():ng.IPromise<any> {
      let dlg: ng.material.IDialogOptions = {
        template: `
          <div class="panel panel-primary">
              <div class="panel-body">
		          <div>No response from the server. Check your internet and VPN connection</div>
              <div><button ng-click="vm.closeDialog()">Retry</button></div>
              </div>  
          </div>`,

        controller: Sw.Component.MdDialogController,
        controllerAs: 'vm'
      }
      if (this._getIEVersion() < 10) {
        // just reroute to signon
        this._state.go("signin");
      } else {
        return this._mdDialog.show(dlg);
      }
    }
    public cancelDialogs() {
      this._mdDialog.cancel();
    }
  }
  angular
    .module("sw.common")
    .service("authenticationDialogs", AuthenticationDialogs);
}
