﻿module Sw {
  // shows a gender pyramid
  // as a column chart

  interface IMFT {
    M?: number;
    F?: number;
    T?: number;
  };
  // define the IsolateScope of the directive
  // this will include those members bound to the directive attributes
  // and any other values we may want to place on scope
  // we can use the generic name IIsolateScope becuase it is not exported outside this closure
  interface IIsolateScope extends ng.IScope {
    height: number;
    width: number;
    dataIn: IMFT[];
    data: any[];
    key: any;
    dataset: Plottable.Dataset;
    // keyField - the property in the data that represents the key
    // hiliteValue - key value for the highlighted point
    // yField - property for the y value
    // yLabel - label for the y values
    // hiliteClass - class for the hilite bar
  }

  // define the controller
  class ChartController {
    // always give the controller a reference to scope
    public scope: IIsolateScope;
    private _chart: Plottable.Components.Table;

    public transformData = (dataIn: IMFT[]) => {
      // first get teh M values
      let M = dataIn.map((d, i) => {
        return { Gender: "M", Key: this.scope.key ? d[this.scope.key] : i, Value: d.M * -1 };
      });
      let F = dataIn.map((d, i) => {
        return { Gender: "F", Key: this.scope.key ? d[this.scope.key] : i, Value: d.F };
      });
      return M.concat(F);
    };

    // function to establish the chart objects and render
    public makeChart = (scope: IIsolateScope, element) => {
      // use plottable to make the chart
      // yScale
      //  .domain([0, bands.length - 1]);  // y scale is shared
      // let ticks = function (scale) {
      //  return [41, 141, 241, 341];
      // }
      let yScale = new Plottable.Scales.Linear();
      let yAxis = new Plottable.Axes.Numeric(yScale, "left");
      let xScale = new Plottable.Scales.Linear();  // xScale is private
      let xAxis = new Plottable.Axes.Numeric(xScale, "bottom")
        .formatter((v) => Math.abs(v).toString());        // absoluate value, since we are showing M as negative

      scope.dataset = new Plottable.Dataset(scope.data);
      var cht = new Plottable.Plots.Bar('horizontal')
        .addDataset(scope.dataset)
        .y((d, i) => d.Key, yScale)
        .x((d) => d.Value, xScale)
        // .classed(scope.hiliteClass, function(d) { return (d[scope.keyField] == scope.hiliteValue);})
        .attr("index", (d, i) => i)
        .attr("fill", function (d, i) {
          return "skyblue";
        })
        ;
      // make gridlines
      var gl = new Plottable.Components.Gridlines(null, yScale);

      // group the gridlines and the chart
      var grp = new Plottable.Components.Group([gl, cht]);

      this._chart = new Plottable.Components.Table([
        [yAxis, grp],
        [null, xAxis]
      ]);

      //// Create
      // this is a bit of translation to turn a jQuery element to a d3 element
      let svg = $(element).find('svg.genderPyramid');
      let d3svg = d3.selectAll(svg.toArray());
      this._chart.renderTo(d3svg);
    };

    public drawChart = (scope, element) => {
      if (!this._chart) {
        this.makeChart(scope, element);
      }
      scope.dataset.data(scope.data);
    };
  };

  // declaration of the class that implements the directive
  // does not need to be exported
  class GenderPyramid implements ng.IDirective {
    // properties of the directive are public properties of this class

    // scope defines the bindings from the directive attributes to the isolate scope
    // it is not the scope itself
    public scope = {
      height: '@',
      width: '@',
      dataIn: "=dataset",
      key: "@"

    };
    public restrict = "EA";
    public template = "<svg class=\"genderPyramid\" height=\"{{height}}\" width=\"{{width}}\"></svg>";

    public link: ng.IDirectiveLinkFn;

    public controller = ChartController;
    public controllerAs = "vm";

    // the arguments to the constructor are the injected dependencies
    constructor() {
      // define the directive link function in the constructor
      // put it on the prototype
      GenderPyramid.prototype.link = (scope: IIsolateScope, element, attrs, ctrlr: ChartController, transclude) => {

        // give the controller a reference to scope
        ctrlr.scope = scope;
        // set any other instance variables on the controller

        // set up on the scope
        // this allows the chart to respond to changes in the controller
        scope.$watch("dataIn", (newValue, oldValue) => {
          if (newValue) {
            scope.data = ctrlr.transformData(<IMFT[]>newValue);
            ctrlr.drawChart(scope, element);
          }
        });
      };
    }

    // directive always get the factory - some plumbing
    public static factory() {
      let directive = () => {
        return new GenderPyramid();
      };
      return directive;
    }
  }

  angular
    .module("sw.common")
    // CALL the factory function to return the directive - ie has to have () after factory
    .directive("genderPyramid", GenderPyramid.factory());
}
