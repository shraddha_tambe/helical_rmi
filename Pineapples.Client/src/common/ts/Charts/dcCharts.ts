﻿
// Components wrapping dcjs charts, built on crossfilter
// 

namespace Sw.Charts {

  interface ILayer {
    group: any;
    name: string;
    accessor: (d) => number;
  }

  interface IBindings {

    dimension: CrossFilter.Dimension<any, any>;
    group: CrossFilter.Group<any, any, any>;
    valueAccessor: (d) => number;

    afterChartInit: any;
    onChartClick: any;
  }

  class SwDcController extends Sw.Charts.SizeableChartController implements IBindings {

    public dimension: CrossFilter.Dimension<any, any>;
    public group: CrossFilter.Group<any, any, any>;
    public valueAccessor: (d) => number;

    public layers: ILayer[];

    public aspectRatio: number;
    public afterChartInit: any;
    public onChartClick: any;
    public filters: any;
    public id: string



    // resize
    public setSize(newSize) {
      let h = 0;
      let w = 0

      if (!this.aspectRatio) {
        this.aspectRatio = 1;
      }

      if (newSize.height === 0) {
        h = newSize.width / this.aspectRatio;
        w = newSize.width;
      } else if (newSize.width === 0) {
        h = newSize.height;
        w = newSize.height * this.aspectRatio;
      } else if (newSize.width / newSize.height < this.aspectRatio) {
        h = newSize.width / this.aspectRatio;
        w = newSize.width;
      } else {
        h = newSize.height;
        w = newSize.height * this.aspectRatio;
      }
      // allow a margin for error - don;t change the size by small amounts
      let dh = this.size.height - h;
      let dw = this.size.width - w;

      if (Math.abs(dh) > 10 || Math.abs(dw) > 10) {
        this.size.height = h;
        this.size.width = w;
      }
    }

    public cht: any;

    public onResize(element) {
      if (!this.cht) {
        this.makeDcChart();
      } else {
        this.sizeDcChart();
        this.cht.render();
      }
    }

    // lifecycle hooks

    public $onInit() {

    }
    public $onChanges(changes) {
      //console.log('dcCharts.$onChanges', changes);
 
      if (changes.dimension) {
        this.makeDcChart();
      }
      if (this.cht && changes.filters) {
        this.cht.filterAll();
        this.cht.filter(changes.filters.currentValue);
        this.cht.redraw();
      }
    }

    static $inject = ["xFilter", "$compile", "$timeout", "$scope", "$element"];
    constructor(private _xfilter: Sw.xFilter.IXFilter,
      $compile: ng.ICompileService, $timeout: ng.ITimeoutService, scope: ng.IScope, private element: ng.IRootElementService) {
      super($compile, $timeout, scope, element);
      let randLetters = String.fromCharCode(65 + Math.floor(Math.random() * 26)) +
        String.fromCharCode(65 + Math.floor(Math.random() * 26));
      this.id = randLetters + Date.now();
      element.find("#target").attr("id", this.id);
    }


    public sizeDcChart() {
    }
    public makeDcChart = () => {
    }

    public setHandlers(cht: any) {
      if (this.valueAccessor) {
        cht
          .valueAccessor(this.valueAccessor);
      };
      // simple filter - ie 1 item at a time - is the default
      cht.filterHandler(() => { });        // noop
    }
  }


  class SwDcPie extends SwDcController implements IBindings {
    public cht: DC.PieChart;

    static $inject = ["xFilter", "$compile", "$timeout", "$scope", "$element"];
    constructor(_xfilter: Sw.xFilter.IXFilter,
      $compile: ng.ICompileService, $timeout: ng.ITimeoutService, scope: ng.IScope, element: ng.IRootElementService) {
      super(_xfilter, $compile, $timeout, scope, element);

    }
    public sizeDcChart() {
      this.cht.width(this.size.width)
        .height(this.size.height)
        // .margins({ top: 10, right: 10, bottom: 20, left: 40 })
        .innerRadius(30)
        .radius((this.size.width - 10) / 2)
        .transitionDuration(0);    // don;t animate these redraws
    }
    public makeDcChart = () => {
      this.cht = dc.pieChart("#" + this.id);
      this.cht
        .transitionDuration(500)
        .dimension(this.dimension)
        .group(this.group)

      this.setHandlers(this.cht);

      this.sizeDcChart();
      this.cht.render();
      // on the pie chart click deivers a different payload
      // it is just the key/value pair associated to the slice
      // we don;t get access to the d3datum
      this.cht.onClick = (data) => {
        this.timeout(() => {
          this.onChartClick({ d3datum: undefined, data: data, key: data.key });
        }, 0, true);
      };
      this.timeout(() => {
        this.cht.filterAll();
        this.cht.redraw();
      }, 0, true);
    }
  }

  class SwDcRow extends SwDcController implements IBindings {

    static $inject = ["xFilter", "$compile", "$timeout", "$scope", "$element"];
    constructor(_xfilter: Sw.xFilter.IXFilter,
      $compile: ng.ICompileService, $timeout: ng.ITimeoutService, scope: ng.IScope, element: ng.IRootElementService) {
      super(_xfilter, $compile, $timeout, scope, element);

    }
    public cht: DC.RowChart;

    public sizeDcChart() {
      this.cht.width(this.size.width)
        .height(this.size.height)
        // .margins({ top: 10, right: 10, bottom: 20, left: 40 })
        .transitionDuration(0);    // don;t animate these redraws
    }
    public makeDcChart = () => {
      this.cht = dc.rowChart("#" + this.id);
      this.cht
        .transitionDuration(500)
        .dimension(this.dimension)
        .group(this.group)
        .margins({ top: 10, right: 10, bottom: 20, left: 40 })
        .gap(1)                                            // bar width Keep increasing to get right then back off.
        .elasticX(true);

      this.setHandlers(this.cht);

      this.sizeDcChart();
      this.cht.render();
      this.cht.onClick = (datum) => {
        this.timeout(() => {
          this.onChartClick({ d3datum: datum, data: datum.data, key: datum.data.key });
        }, 0, true);
      };
      this.timeout(() => {
        this.cht.filterAll();
        this.cht.redraw();
      }, 0, true);
    }
  }

  

  class SwDcBar extends SwDcController implements IBindings {

    static $inject = ["xFilter", "$compile", "$timeout", "$scope", "$element"];
    constructor(_xfilter: Sw.xFilter.IXFilter,
      $compile: ng.ICompileService, $timeout: ng.ITimeoutService, scope: ng.IScope, element: ng.IRootElementService) {
      super(_xfilter, $compile, $timeout, scope, element);

    }
    public cht: DC.BarChart;

    public sizeDcChart() {
      this.cht.width(this.size.width)
        .height(this.size.height)
        // .margins({ top: 10, right: 10, bottom: 20, left: 40 })
        .transitionDuration(0);    // don;t animate these redraws
    }
    public makeDcChart = () => {
      // get the domain values from the passed in grp
      let domain: string[] = this.group.all().map( kv => kv.key);



      this.cht = dc.barChart("#" + this.id);
      this.cht
        .transitionDuration(250)
        .dimension(this.dimension)
        .x(d3.scale.ordinal().domain(domain))
        .xUnits(dc.units.ordinal)
        .margins({ top: 10, right: 10, bottom: 20, left: 40 })
        .gap(2)
        .elasticY(true);

      // we can create simple bar chart (no stacking) by passing the group and valueAccessor
      if (this.group) {
        this.cht
          .group(this.group);
      }

      this.setHandlers(this.cht);

      // if we pass in more layers, we can
      // add htese on to the existing valueAccessor OR
      // create the initial group and accessor from the first layer
      // 
      if (this.layers) {
        this.layers.forEach((v, idx) => {
          if (idx === 0 && !this.valueAccessor) {
            // if we did not explicitly supply a valueAccessor, create the first series using the first layer
            this.cht
              .group(v.group ? v.group : this.group, v.name)
              .valueAccessor(v.accessor)
          } else {
            this.cht.stack(v.group ? v.group : this.group, v.name, v.accessor);
          }
        });
      }

      this.sizeDcChart();
      this.cht.render();

      // on the bar chart click delivers the d3 object as the 'datum'
      // we can return this and/or the data and/or the key
      this.cht.onClick = (datum) => {
        this.timeout(() => {
          this.onChartClick({ d3datum: datum, data: datum.data, key: datum.data.key });
        }, 0, true);
      };

      // allow the consumer of this component to access the dc api if required
      this.afterChartInit({ chart: this.cht });

      this.timeout(() => {
        this.cht.filterAll();
        this.cht.redraw();
      }, 0, true);
    }
  } 

  class ComponentOptions implements ng.IComponentOptions {
    public bindings: any;
    public controller: any;
    public controllerAs: string;
    public template: string;

    constructor(controller: typeof SwDcController) {
      this.bindings = {
        // underlying crossfilter dimension
        dimension: "<",

        // a group built on the crossfilter dimension
        group: "<",
        //function to retireve the value from each key/value pair in group.all()
        valueAccessor: "<",

        // an array of ILayer to define row and bar stacks
        // applies to bar and row
        // when building a stackable bar or row this are best supplied as an alternative to 
        // group and valueAccessor; 
        //however:
        // if group supplied, it can be ommitted from each layer
        // if group, valueAccessor and layers are supplied, the group andvalueAccess define the 'base' layer.
        layers: "<",

        // a preferred aspect ratio for the chart
        aspectRatio: "<",
        // callback after the dcchart is initialised
        // passes back the chart to the consumer:
        // after-chart-init="vm.init(chart)"
        afterChartInit: "&", 

        // handler function for chart click
        // the client may request any of these arguments in the binding:
        // d3datum: the entire d3datum associated with the clicked data point
        //   note that data - representing the key / value pair in the group, is a member of this
        // data: the key value pair from the group
        // key: the key from the data
        //
        // e.g. binding may look like any of these:
        // on-chart-click="vm.onClick(d3datum, data, key)"
        // on-chart-click="vm.onClick(data)"
        // on-chart-click"vm.onClick(key)"
        // etc
        // note that d3datum is not avaliable on pie chart as of dc 2.2.1, and is returned as undefined
        onChartClick: "&",   

        // dc has built in logic to apply filters to crossfilter groups when a chart point is click
        // in the default case, this allows multiple selections
        // this default behaviour is turned off here by setting a noop filterHandler
        // filters are assumed to be handled by action on the click handler onChartClick
        // a filter is passed back in so that the chart may reflect the current state of the filter - set
        // either by the chart itself, some other chart, selecting a row in a table 
        // or some other UI (an options pane)
        filters: "<"
      };

      this.controller = controller;
      this.controllerAs = "vm";
      this.template = `<div style="width:100%; height:100%" ><div id="target" class="unfloat"></div></div>`;
    }
  }
  angular
    .module("sw.common")
    .component("dcPie", new ComponentOptions(SwDcPie))
    .component("dcRow", new ComponentOptions(SwDcRow))
    .component("dcBar", new ComponentOptions(SwDcBar));
}
