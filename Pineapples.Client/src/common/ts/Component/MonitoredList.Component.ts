﻿namespace Sw.Component {
  /* monitored list responds to notifications of data updates by updating the collection
   * when appropriate
   * for convenience, we provide SelectableList here
   */


  export abstract class ListMonitor extends SelectableList {
    // returns the collection of obect (ie any[]) currently being monitored

    abstract monitoredList(): any;

    public monitoredListUpdate(data: any) { };
    public monitoredListInsert(data: any) { };
    public monitoredListDelete(data: any) { };

    // matches the primary key of an object that is the parent of this collection
    // this is used on insertion
    public isListMember(newData: any): boolean {
      return true;
    };

    public monitor(scope: Sw.IScopeEx, relevant: string | ((d: IDataChangePublication) => boolean)
      , keyname: string) {
      let fcnRelevant: ((d: IDataChangePublication) => boolean) = null;
      if (_.isString(relevant)) {
        fcnRelevant = (data: IDataChangePublication) => { return (data.what == relevant); };
      } else if (_.isFunction(relevant)) {
        fcnRelevant = <any>relevant;
      };

      let onDataUpdated = (data) => {
        if (fcnRelevant(data)) {
          // data is data we can merge into this array
          let id = data.data[keyname];
          if (id) {
            let o = _.find(this.monitoredList(), o => { return o[keyname] === id; });
            if (o) {
              angular.extend(o, data.data);
              this.monitoredListUpdate(o);
            }
          }
        }
      }
      scope.hub.subscribe("dataUpdated", onDataUpdated);

      let onDataInserted = (data) => {
        if (fcnRelevant(data)) {
          // data is data we can merge into this array
          if (this.isListMember(data.data)) {
            let id = data.data[keyname];
            if (id) {
              let idx = _.findIndex(this.monitoredList(), o => { return o[keyname] === id; });
              if (idx < 0) {
                // dont insert twice if there re two monitors on the same collection
                let clone = angular.copy(data.data);
                this.monitoredList().push(clone);
                this.monitoredListInsert(clone);
              }
            }
          }
        }
      }
      scope.hub.subscribe("dataInserted", onDataInserted);

      let onDataDeleted = (data) => {
        if (fcnRelevant(data)) {
          // data is data we can merge into this array
          let id = data.data[keyname];
          if (id) {
            let idx = _.findIndex(this.monitoredList(), o => { return o[keyname] === id; });
            if (idx >= 0) {
              this.monitoredList().splice(idx, 1);
              this.monitoredListDelete(data.data);
            }

          }
        }
      }
      scope.hub.subscribe("dataDeleted", onDataDeleted);
    }
  }

  export class ConcreteListMonitor extends ListMonitor {

    public monitoredList(): any[] {
      return [];
    };

    constructor(monitorFcn: () => any[], isListMemberFcn: (data) => boolean) {
      super();
      this.monitoredList = monitorFcn;
      this.isListMember = isListMemberFcn;
    }
  }
}