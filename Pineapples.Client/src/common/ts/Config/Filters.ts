﻿namespace Sw {

  let checkmark = (value) => {
    if (value == null || value == '')
      return '';
    return value ? '\u2713' : '\u2718';
  };

  let block = (value) => {

    if (value == null || value == '')
      return '';
    return value ? '\u25A0' : '';
  };

  let z0 = (value) => {
    if (value) {
      return value;
    }
    return '';
  }

  let percentage = ($window) => {
    return function (input, decimals, suffix) {
      decimals = angular.isNumber(decimals) ? decimals : 3;
      suffix = suffix || '%';
      if ($window.isNaN(input)) {
        return '';
      }
      return Math.round(input * Math.pow(10, decimals + 2)) / Math.pow(10, decimals) + suffix
    };
  };

  let percif = ($window, $filter) => {
    return function (input, conditional, decimals, suffix) {
      decimals = angular.isNumber(decimals) ? decimals : 3;
      suffix = suffix || '%';
      if ($window.isNaN(input)) {
        return '';
      }
      if (conditional)
        return Math.round(input * Math.pow(10, decimals + 2)) / Math.pow(10, decimals) + suffix;
      else
        return $filter('number')(input, decimals);
    };
  };

  let html = ($sce) => {
    return function (val) {
      return $sce.trustAsHtml(val);
    };
  }

  angular
    .module('sw.common')
    .filter('checkmark', function () { return checkmark; })
    .filter('block', function () { return block; })
    .filter('z0', function () { return z0; })
    .filter('percentage', ['$window', percentage])
    .filter('percif', ['$window', '$filter', percif])
    .filter('html', ["$sce", html]);
}