﻿module Sw {
  /*
  --------------------------------------------------------------------
  FilterController

  / simple controller to provide access to a Filter object
  // used by headerbar items like searcher, chart params, table params

  // adds special support for chart and tables - row, col, chartType and swap
  // tableoptions is a string naming the member of lookups.cache to use for table and chart dropdowns
  //
  ?
  --------------------------------------------------------------------
  */
  interface IxScope extends ng.IScope {
    vm: FindConfigController;
  }
  class FindConfigController {

    public rowData: Sw.Lookups.ILookupEntry;
    public params: any;

    static $inject = ["$scope", 'theFilter', 'Lookups', 'findConfig'];
    constructor(scope: IxScope, public theFilter: Sw.Filter.IFilter, public lookups: Sw.Lookups.LookupService
      , public findConfig: Sw.Filter.FindConfig) {

      scope.vm = this;
      this.params = findConfig.current.filter;

    };

    public FindNow() {
      this.theFilter.FindNow(this.findConfig.current);
      this.findConfig.prepared = true;
    }
    public FlashFind() {
      this.theFilter.FlashFind(this.findConfig.current);
      this.findConfig.prepared = true;
    }
    public Reset() {
      this.findConfig.reset();
    }
    public get viewMode() {
      return this.findConfig.current.viewMode;
    }

    public set viewMode(newViewMode) {
      if (this.findConfig.current.viewMode !== newViewMode) {
        this.findConfig.current.viewMode = newViewMode;
        let vm = this.theFilter.GetViewMode(newViewMode);
        if (vm) {
          this.FindNow();
        }
      }
    }

    public supportedViewModes() {
      if (this.findConfig.allowedViews && this.findConfig.allowedViews.length > 0) {
        return this.findConfig.allowedViews;
      }
      return this.theFilter.AllViewModes();
    }
  };

  class FindConfigLocalController extends FindConfigController {
    public localparams: any;

    static $inject = ["$scope", 'theFilter', 'Lookups', 'findConfig'];
    constructor(scope: IxScope, public theFilter: Sw.Filter.IFilterCached, public lookups: Sw.Lookups.LookupService
      , public findConfig: Sw.Filter.FindConfig) {
      super(scope, theFilter, lookups, findConfig);
    }
    public localFind() {
      this.theFilter.findNowLocal(this.localparams);
    }
    public localReset() {
      this.localparams = {};
    }
  }

  class TableParamsController extends FindConfigController {
    public tableparams: Sw.Filter.TableParams;
    public tableOptions: Sw.Lookups.ILookupList;
    public dataOptions: Sw.Lookups.ILookupList;

    static $inject = ["$scope", 'theFilter', 'Lookups', 'findConfig'];
    constructor(scope: IxScope, public theFilter: Sw.Filter.IFilter, public lookups: Sw.Lookups.LookupService
      , public findConfig: Sw.Filter.FindConfig) {
      super(scope, theFilter, lookups, findConfig);

      this.tableparams = findConfig.current.table;
      this.lookups.getList(findConfig.tableOptions).then(list => {
        this.tableOptions = list;
      });
      this.lookups.getList(findConfig.dataOptions).then(list => {
        this.dataOptions = list;
      });
      scope.$watch("vm.tableparams.row", (newValue: string, oldValue) => {
        this.rowData = this.lookups.byCode(this.findConfig.tableOptions, newValue);
      });
    };
    private _chartType;
    public get chartType() {
      return this.tableparams.chartType;
    }
    public set chartType(newchartType) {
      this.tableparams.chartType = newchartType;
      // this.theFilter.FindNow(this.findConfig.current);
    }

    public get row() {
      return this.tableparams.row;
    }
    public set row(newrow) {
      if (this.tableparams.row !== newrow) {
        this.tableparams.row = newrow;
        this.FindNow();
      }
    }
    public hasRowGeoSet(): boolean {
      let rowData = this.lookups.byCode(this.findConfig.tableOptions, this.tableparams.row);
      if (rowData && rowData.g) {
        return true;
      }
      return false;
    }
    public get col() {
      return this.tableparams.col;
    }
    public set col(newcol) {
      if (this.tableparams.col !== newcol) {
        this.tableparams.col = newcol;
        this.FindNow();
      }
    }
    public get dataItem() {
      return this.tableparams.dataItem;
    }
    public set dataItem(newdataItem) {
      if (this.tableparams.dataItem !== newdataItem) {
        this.tableparams.dataItem = newdataItem;
     //   this.FindNow();
      }
    }
    public swap(): void {
      let tmp = this.col;
      this.tableparams.col = this.row;
      this.tableparams.row = tmp;
      this.FindNow();
    };
    public showDataOption() {
      return true;
    }
    public showColumnOption() {
      return true;
    }
    public showChartOptions() {
      return false;
    }

  }
  class ChartParamsController extends TableParamsController {
    static $inject = ["$scope", 'theFilter', 'Lookups', 'findConfig', 'palette', 'mapColorScale'];
    constructor(scope: IxScope, public theFilter: Sw.Filter.IFilter, public lookups: Sw.Lookups.LookupService
      , public findConfig: Sw.Filter.FindConfig
      , public palette: Sw.Maps.IColorBrewerPalette
      , public mapColorScale: Sw.Maps.IMapColorScale) {
      super(scope, theFilter, lookups, findConfig);
    }
    public showDataOption() {
      return this.chartType === "map";
    }
    public showColumnOption() {
      return this.chartType !== "map";
    }
    public showChartOptions() {
      return true;
    }

  }
  angular
    .module('sw.common')
    .controller('FilterController', FindConfigController)
    .controller('FindConfigLocalController', FindConfigLocalController)
    .controller('TableParamsController', TableParamsController)
    .controller('ChartParamsController', ChartParamsController);

}

