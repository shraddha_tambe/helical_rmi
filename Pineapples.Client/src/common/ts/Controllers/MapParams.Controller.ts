﻿module Sw.Maps {
  /*
  --------------------------------------------------------------------
  Map Params Controller

  manages the centre, zoom level etc working with the renderOptions object
  --------------------------------------------------------------------
  */
  class MapParamsController {

    public boxOptions = [];

    // the injection name 'theFilter' comes from the resolve on the ui-router state from which this controller is invoked
    static $inject = ["$scope", "$rootScope", "theFilter"];
    constructor($scope, private $rootScope: ng.IRootScopeService, public theFilter) {
      $scope.$on('SearchComplete', (event, resultpack) => {
        if (resultpack.entity === this.theFilter.entity && resultpack.method === "geo") {
          // the first recordset is the data - we don;t need that in the params
           // we grab the marker bounding box
          var markerBoundingBox = resultpack.resultset[1][0];
          markerBoundingBox.couName = "(all markers)";

          // the final array is the bounding boxes of each country
          var o = [];
          o.push(markerBoundingBox);
          o = o.concat(resultpack.resultset[2]);

          // KLUDGE ALERT
          // make sure that name and code are defined,in couCode and couName used by desktop implementation
          o.forEach(function (d) {
            d.name = d.name || d.couName;
            d.code = d.code || d.coucode;
          });
          // END KLUDGE

          this.boxOptions = o;
          // if the currently selectedbounding box is still valid, preserve it
          var i = -1;
          if (this.selectedBox) {
            var i = _.findIndex(this.boxOptions, { couName: this.selectedBox.couName });
          }
          if (i === -1) {
            i = 0;
          }
          this.selectedBox = this.boxOptions[i];
        }
      });
    }
    private _selectedBox;
    public get selectedBox() {
      return this._selectedBox;
    }
    public set selectedBox(newValue) {
      if (!(this._selectedBox === newValue)) {
        this._selectedBox = newValue;
        this.$rootScope.$broadcast("MapChangeBox", { entity: this.theFilter.entity, box: this._selectedBox });
      };
    }

    private _cluster: boolean = true;
    public get cluster() {
      return this._cluster;
    }
    public set cluster(newValue) {
      if (!(this._cluster === newValue)) {
        this._cluster = newValue;
        this.$rootScope.$broadcast("MapChangeCluster", { entity: this.theFilter.entity, cluster: this._cluster });
      };
    }

  };
  angular
    .module('sw.common')
    .controller('MapParamsController', MapParamsController);
};
