﻿module Sw {
  /*
  --------------------------------------------------------------------
  ModelController

  thin controller to allow access to an injected model
  -- as well as various context - lookups, state, and stateparams
  ?
  --------------------------------------------------------------------
  */
  class ModelController {

    public state: any;


    static $inject = ['$state', '$stateParams', 'theModel', 'Lookups'];
    constructor($state: ng.ui.IStateService, public stateParams: ng.ui.IStateParamsService, public model: any, public lookups: Sw.Lookups.LookupService) {
      this.state = $state.current;
    };
    public reLoad = (data) => {
      this.model.data = data.ResultSet[0];
    };
  }

  angular
    .module('sw.common')
    .controller('ModelController', ModelController);
}
