﻿module Sw.Maps {

  export interface IColorBrewerPalette {
    scheme: string;
    bands: number;
    invert: boolean;
  }
  export class ColorBrewerPalette implements IColorBrewerPalette {
    public scheme: string;
    public bands: number;
    public invert: boolean;
    constructor() {
      this.scheme = "Greens";
      this.bands = 9;
      this.invert = false;
    }
  }
  export interface IMapColorScale {
    scaletype: string;
    thresholds: number[];
    getBandLabel: (i: number) => string;
    buckets: number[];
    reserveZero: boolean;       // indicates that the first band is always for 0 only
    calcThresholds: (values: any[], numRanges: number) => number[];
    calcBuckets: (values: any[]) => number[];
    colorBrewerStyles: (bands: number, invert: boolean) => string[];
    getScale: (range: any[]) => d3.scale.Threshold<number, any>;
    getColorBrewerScale: (bands: number, invert: boolean) => d3.scale.Threshold<number, any>;

  }
  export class MapColorScale implements IMapColorScale {
    public scaletype: string;
    public thresholds: number[];
    public buckets: number[];
    public reserveZero: boolean;
    constructor() {
      this.scaletype = "quantize";
      this.thresholds = [];
      this.buckets = [];
      this.reserveZero = true;
    }

    public colorBrewerStyles = (bands: number, invert: boolean) => {
      let classNames: string[] = d3.range(bands).map((i) =>
        "q" + (invert ? bands - 1 - i : i).toString() + "-" + bands.toString());
      return classNames;
    };

    public getScale = (range: any[]) => {
      // the threshold scale requires one less element in the domain than the range
      // we remove the upper bound
      let d = angular.copy(this.thresholds);
      d.splice(d.length - 1);
      return d3.scale.threshold<any>()
        .domain(d)
        .range(range);
    };

    public getColorBrewerScale = (bands: number, invert: boolean) => {
      return this.getScale(this.colorBrewerStyles(bands, invert));
    };

    public getBandLabel = (i: number) => {
      // threshold[i] is the minimum value in band i+1
      if (i === 0) {
        return this._makeLabel(0, (this.thresholds[0] - 1));
      }
      if (i === this.thresholds.length - 1) {
        // this is a speciual case becuase the last threshold (not used in the scale)
        // is the maximum value
        return this._makeLabel(this.thresholds[i - 1], this.thresholds[i]);
      }
      // otherwise adjust the closing value of the range by 1
      return this._makeLabel(this.thresholds[i - 1], this.thresholds[i] - 1);
    };

    private _makeLabel = (min: number, max: number) => {
      if (min === max) {
        return max.toString();
      }
      return min.toString() + " : " + max.toString();
    };

    public calcThresholds = (values: any[], numRanges: number) => {

      switch (this.scaletype) {
        case "quantize":
          // quantize means the range of values is divided more or less equally. this will result
          // in buckets of varying sizes
          this._quantize(values, numRanges);
          break;
        case "quantile":
          this._quantile(values, numRanges);
          break;
      }
      this.calcBuckets(values);
      return this.thresholds;
    };

    public calcBuckets = (values: any[]) => {
      // initialise the buckets from the cardinality set of thresholds
      let bands = this.thresholds.length;
      this.buckets = new Array(bands);
      values.forEach((d) => {
        for (let i = 0; i < bands; i++) {
          if (i === (bands - 1) || d.value < this.thresholds[i]) {
            if (this.buckets[i] === undefined) {
              this.buckets[i] = 1;
            } else {
              this.buckets[i]++;
            }
            break;
          }
        }
      });
      return this.buckets;
    };

    private _quantize = (values: any[], numRanges: number) => {
      let max = <number>_.result(_.max(values, 'value'), 'value');
      // the minimum band is 1 if we are reserving the first threshold for 0
      let min = Math.min(<number>_.result(_.min(values, 'value'), 'value'), (this.reserveZero ? 1 : 0));

      // let d3 calculate the bands
      let numQuants = (this.reserveZero ? numRanges - 1 : numRanges);
      let scale = d3.scale.quantize<number>()
        .domain([min, max])
        .range(d3.range(numQuants));

      this.thresholds = [];        // careful with destriying this
      if (this.reserveZero) {
        // make the threshold 1 to isolate a band for 0
        //
        this.thresholds.push(1);
      }
      for (let i = 0; i < numQuants; i++) {
        // use invertExtent to get the domain segment associated to each range
        // the 1-element is the max - catch those and round
        this.thresholds.push(Math.round(scale.invertExtent(i)[1]));
      }
    };

    private _quantile = (values: any[], numRanges: number) => {
      let r: number[] = [];
      // get the values out of the data - if we are reserving a special place for 0 , ignore any 0
      values.forEach((d) => {
        if (d.value !== 0 || this.reserveZero === false) {
          r.push(d.value);
        }
      });
      let max = _.max(r);
      // let d3 calculate the bands
      let numQuantiles = (this.reserveZero ? numRanges - 1 : numRanges);
      let scale = d3.scale.quantile<number>()
        .domain(r)
        .range(d3.range(numQuantiles));

      this.thresholds = [];        // careful with destriying this
      if (this.reserveZero) {
        // make the threshold 1 to isolate a band for 0
        //
        this.thresholds.push(1);
      }
      let quantiles = scale.quantiles();
      for (let i = 0; i < quantiles.length; i++) {
        // use invertExtent to get the domain segment associated to each range
        // the 1-element is the max - catch those and round
        if (this.thresholds.length === 0) {       // we may have a single value of 1 in there if reserveZero
          this.thresholds.push(Math.round(quantiles[i]));
        } else {
          let v = Math.round(quantiles[i]);
          if (v <= this.thresholds[this.thresholds.length - 1]) {
            v = this.thresholds[this.thresholds.length - 1] + 1;
          }
          this.thresholds.push(v);
        }

      }
      // add a last value - this can be the max of the data, if we have not already gone past that
      if (max >= this.thresholds[this.thresholds.length - 1]) {
        this.thresholds.push(max);
      } else {
        this.thresholds.push(this.thresholds[this.thresholds.length - 1] + 1);
      }
    };
  }

  interface IIsolateScope extends ng.IScope {
    palette: IColorBrewerPalette;
    colorSchemes: string[];
    range: (start: number, end: number, step?: number) => any[];
    paletteToggled: (open: boolean) => void;
    setScheme: (scheme: string, close?: boolean) => void;
    status: any;
  }

  class ColorBrewerPicker implements ng.IDirective {
    public templateUrl = "./assets/maps/colorBrewerPicker.html";
    public link: any;
    public scope = {
      palette: "="
    };
    public restrict = "EA";

    private _scope: IIsolateScope;

    constructor($rootScope) { // constructor gets list of dependencies too
      ColorBrewerPicker.prototype.link = (scope: IIsolateScope) => {
        this._scope = scope;
        if (!scope.palette) {
          scope.palette = new ColorBrewerPalette();
        }
        scope.colorSchemes = [];
        for (let prop in colorbrewer) {
          scope.colorSchemes.push(prop);
        }
        scope.range = (start, end, step?) => {
          return _.range(start, end, step);
        };
        scope.paletteToggled = (): void => {
          $rootScope.$broadcast("rzSliderForceRender");
        };
        scope.setScheme = (scheme: string, close?: boolean): void => {
          if (scope.palette.scheme === scheme) {
            scope.status.isopen = false;
          } else {
            scope.palette.scheme = scheme;
          }
        };
        scope.status = { isopen: false };
      };
    }

    public static factory() {
      let directive = ($rootScope) => {
        return new ColorBrewerPicker($rootScope);
      };
      directive["$inject"] = ["$rootScope"];
      return directive;
    }
  }
  angular
    .module("sw.common")
    .directive("colorBrewerPicker", ColorBrewerPicker.factory());
}
