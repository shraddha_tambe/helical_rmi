﻿namespace Sw.Component {

  //standard options for an 'item' component
  export class ItemComponentOptions implements ng.IComponentOptions {
    public bindings: any = {
      model: "<",
      api: "<"        // api is only need when we allow 'new'
    };
    public controllerAs: string = "vm";
    public templateUrl: string;

    constructor(itemName: string, public controller) {
      this.templateUrl = itemName + "/Item";
    }
  }
  // base for Components that have no bindings - not so many of those
  export class UnboundComponentOptions implements ng.IComponentOptions {
    public controllerAs: string = "vm";
    constructor(public templateUrl, public controller) {
    }
  }

  export class ComponentOptions implements ng.IComponentOptions {
    public controllerAs: string = "vm";
    constructor(public templateUrl: string, public controller, public bindings: any) {
    }
  }
}
