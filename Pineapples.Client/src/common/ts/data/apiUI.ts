﻿
namespace Sw.Api {

  export interface IApiUi {
    getErrorType(errorData): string;
    showErrorResponse<T>(entity: T, errorInfo: any): ng.IPromise<string>;
    conflictReport<T>(oldVersion: T, newVersion: T): ng.IPromise<string>;
    validationReport<T>(entity: T, version: any): ng.IPromise<string>;
    errorReport<T>(entity: T, errorInfo: any): ng.IPromise<string>;
    notFoundReport<T>(entity: T, id: any): ng.IPromise<string>;
    duplicateReport<T>(entity: T, id: any, errorInfo: any): ng.IPromise<string>;
    addNewReport<T>(entity: T): ng.IPromise<string>;
    forbiddenReport(errorInfo: any): ng.IPromise<string>;
  }

  class BaseDlgCtrlr {
    static $inject = ["$mdDialog"];
    constructor(protected dlgInstance: ng.material.IDialogService) { }
    public close(returnstring: string) {
      this.dlgInstance.hide(returnstring);
    };

    public cancel(dismissstring?: string) {
      this.dlgInstance.cancel(dismissstring);
    }

  }

  class ConflictCtrlr extends BaseDlgCtrlr {
    static $inject = ["$mdDialog", "newVersion", "oldVersion"];
    constructor(dlgInstance: ng.material.IDialogService, public newVersion: any, public oldVersion: any) {
      super(dlgInstance);
    }
  }

  class ValidationCtrlr extends BaseDlgCtrlr {
    static $inject = ["$mdDialog", "entity", "validation"];
    constructor(dlgInstance: any, public entity: any, public validation: any) {
      super(dlgInstance);
    }
  }
  class ErrorCtrlr extends BaseDlgCtrlr {
    static $inject = ["$mdDialog", "entity", "errorInfo"];
    constructor(dlgInstance: any, public entity: any, public errorInfo: any) {
      super(dlgInstance);
    }
  }
  class ServerErrorCtrlr extends BaseDlgCtrlr {
    static $inject = ["$mdDialog", "errorInfo"];
    constructor(dlgInstance: any, public errorInfo: any) {
      super(dlgInstance);
    }
  }
  class PermissionCtrlr extends BaseDlgCtrlr {
    public Topic: string;
    static $inject = ["$mdDialog", "errorInfo","PermissionStore"];
    constructor(dlgInstance: any,  public errorInfo: any, public permissionStore: angular.permission.PermissionStore) {
      super(dlgInstance);
      this.Topic = (<any>permissionStore).areas[errorInfo.Topic];
    }
    
  }
  class NotFoundCtrlr extends BaseDlgCtrlr {
    static $inject = ["$mdDialog", "entity", "id"];
    constructor(dlgInstance: any, public entity: any, public id: any) {
      super(dlgInstance);
    }
  }

  class DuplicateCtrlr extends BaseDlgCtrlr {
    static $inject = ["$mdDialog", "entity", "id","errorInfo"];
    constructor(dlgInstance: any, public entity: any, public id: any, public errorInfo: any) {
      super(dlgInstance);
    }
    public get duplicateField() {
      return (this.errorInfo.field || "id");
    }
    public get duplicateValue() {
      return (this.errorInfo.value || this.id);
    }

  }
  class AddNewCtrlr extends BaseDlgCtrlr {
    static $inject = ["$mdDialog", "entity"];
    constructor(dlgInstance: any, public entity: IEditable) {
      super(dlgInstance);
    }
  }

  /**
   * @ngdoc service
   *
   * @name ApiUi
   * @description
   * Provides standard dialogs for handling Ui exceptions
  **/
  class ApiUi implements IApiUi {
    static $inject = ['Restangular','$mdDialog'];
    constructor(private restangular: any, private mdDialog: ng.material.IDialogService) {
    }

    public getErrorType(errorData) {
      switch (errorData.status) {
        case 409:     // its a concurrency conflict
          return "conflict";
        case 404:
          // not found - ie probably record not found but need to check
          if (errorData.data.type === "RecordNotFound") {
            return "notfound";
          }
          return "error";
        case 422:      // unprocessable entity is used for duplicate key
          if (errorData.data.type === "DuplicateKey") {
            return "duplicatekey";
          }
          return "error";

        case 400:     // a sql error - may be a hard error or a validation error
          if (errorData.data.type === "Validation") {
            return "invalid";
          } else {
            return "error";
          }
        case 403: // forbidden : a permission error , expect the permission info in the error message
          if (errorData.data.type === "Permission") {
            return "permission";
          } else {
            return "error"
          };
        case 500: // a server error (could be many things, add cases has they are encountered)
          if (errorData.data.type === "Error") {
            return "servererror";
          } else {
            return "error"
          };
      }
      return "error";
    }
    public showErrorResponse(entity, errorData) {
      let modalPromise: ng.IPromise<string>;
      switch (this.getErrorType(errorData)) {
        case "error":
          modalPromise = this.errorReport(entity, errorData);
          break;
        case "notfound":
          var id = this.restangular.configuration.getIdFromElem(entity);
          modalPromise = this.notFoundReport(entity, id);
          break;
        case "duplicatekey":
          var id = this.restangular.configuration.getIdFromElem(entity);
          modalPromise = this.duplicateReport(entity, id, errorData.data);
          break;
        case "invalid":
          modalPromise = this.validationReport(entity, errorData.data);
          break;
        case "conflict":
          modalPromise = this.conflictReport(entity, errorData.data);
          break;
        case "permission":
          modalPromise = this.forbiddenReport(errorData.data);
          break;
        case "servererror":
          modalPromise = this.serverErrorReport(errorData.data);
          break;
      }
      return modalPromise;
    }
    public conflictReport(oldVersion, newVersion ) {
      let modalPromise = this.mdDialog.show(this._conflictDlg(oldVersion, newVersion));
      return modalPromise;
    };
    public validationReport(entity, validation) {
      let modalPromise = this.mdDialog.show(this._validationDlg(entity, validation));
      return modalPromise;
    };
    public errorReport(entity, errorInfo) {
      let modalPromise = this.mdDialog.show(this._errorDlg(entity, errorInfo));
      return modalPromise;
    };
    public notFoundReport(entity, id) {
      let modalPromise = this.mdDialog.show(this._notFoundDlg(entity, id));
      return modalPromise;
    };
    // add errorInfo to this dialog
    // the 422 error of type DuplicateKey will return a 'field'and 'value'
    // being the field name and the key value that caused the duplicate
    // there are occasions (user management name, and email) where these duplicates are not on the
    // primary key field
    // allow the error to be displayed using these values if they are available
    public duplicateReport(entity, id, errorInfo) {
      let modalPromise = this.mdDialog.show(this._duplicateDlg(entity, id, errorInfo));
      return modalPromise;
    };
    public addNewReport(entity) {
      let modalPromise = this.mdDialog.show(this._addNewDlg(entity));
      return modalPromise;
    };
    public forbiddenReport(errorInfo) {
      let modalPromise = this.mdDialog.show(this._permissionDlg(errorInfo));
      return modalPromise;
    };
    public serverErrorReport(errorInfo) {
      let modalPromise = this.mdDialog.show(this._serverErrorDlg(errorInfo));
      return modalPromise;
    };

    private _conflictDlg(oldVersion, newVersion) {
      return {
        locals: {
          newVersion: newVersion,
          oldVersion: oldVersion
        },
        animation: true,
        templateUrl: 'dialog/conflict',
        controller: ConflictCtrlr,
        controllerAs: "vm",
        size: "lg",
        clickOutsideToClose: true,
        multiple: true

      };
    }
    private _validationDlg(entity, validation) {
      return {
        locals: {
          entity: entity,
          validation: validation
        },
        animation: true,
        templateUrl: 'dialog/validation',
        controller: ValidationCtrlr,
        controllerAs: "vm",
        size: "lg",
        clickOutsideToClose: true,
        multiple: true

      };
    }
    private _errorDlg(entity, errorInfo) {
      return {
        locals: {
          entity: entity,
          errorInfo: errorInfo
        },
        animation: true,
        templateUrl: 'dialog/dataError',
        controller: ErrorCtrlr,
        controllerAs: "vm",
        size: "lg",
        clickOutsideToClose: true,
        multiple: true
      };
    }
    private _addNewDlg(entity:IEditable) {
      return {
        locals: {
          entity: entity
        },
        animation: true,
        templateUrl: 'dialog/dataAddNew',
        controller: AddNewCtrlr,
        controllerAs: "vm",
        size: "lg",
        clickOutsideToClose: true,
        multiple: true

      };
    }
    private _notFoundDlg(entity, id) {
      return {
        locals: {
          entity: entity,
          id: id
        },
        animation: true,
        templateUrl: 'dialog/dataNotFound',
        controller: NotFoundCtrlr,
        controllerAs: "vm",
        size: "lg",
        clickOutsideToClose: true,
        multiple: true

      };
    }
    private _duplicateDlg(entity, id, errorInfo) {
      return {
        locals: {
          entity: entity,
          id: id,
          errorInfo: errorInfo
        },
        animation: true,
        templateUrl: 'dialog/dataDuplicate',
        controller: DuplicateCtrlr,      // can reuse this controller
        controllerAs: "vm",
        size: "lg",
        clickOutsideToClose: true,
        multiple: true

      };
    }
    private _permissionDlg(errorInfo) {
      return {
        locals: {
          errorInfo: errorInfo
        },
        animation: true,
        templateUrl: 'dialog/permissionError',
        controller: PermissionCtrlr,
        controllerAs: "vm",
        size: "lg",
        clickOutsideToClose: true,
        multiple: true

      };
    }
    private _serverErrorDlg(errorInfo) {
      return {
        locals: {
          errorInfo: errorInfo
        },
        animation: true,
        templateUrl: 'dialog/serverError',
        controller: ServerErrorCtrlr,
        controllerAs: "vm",
        size: "lg",
        clickOutsideToClose: true,
        multiple: true

      };
    }
  }

  angular
    .module("sw.common")
    .service("ApiUi", ApiUi);
}