﻿module Sw.Api {
  export interface IPagedData extends Array<any> {
    pageNo: number;
    pageSize: number;
    numResults: number;
    firstRec: number;
    lastRec: number;
    lastPage: number;
    isLastPage: boolean;
    hasPageInfo: boolean;
}

  export interface IRawPagedData {
    ResultSet: any[];
    NumResults: number;
    FirstRec: number;
    LastRec: number;
    PageNo: number;
    PageSize: number;
    LastPage: number;
    IsLastPage: boolean;
    HasPageInfo: boolean;
  }

  export class RawPagedData {
    private _rawData: IRawPagedData;
    constructor(rawData: IRawPagedData) {
      this._rawData = rawData;
    }
    public transform: () => IPagedData = () => {
      let data = this._rawData.ResultSet as IPagedData;
      data.numResults = this._rawData.NumResults;
      data.pageSize = this._rawData.PageSize;
      data.pageNo = this._rawData.PageNo;
      data.firstRec = this._rawData.FirstRec;
      data.lastRec = this._rawData.LastRec;
      data.lastPage = this._rawData.LastPage;
      data.isLastPage = this._rawData.IsLastPage;
      data.hasPageInfo = true;
      return data;
    };
  }
}
