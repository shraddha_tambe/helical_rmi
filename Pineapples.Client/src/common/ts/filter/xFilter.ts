﻿/* support class for CrossFilter
 *provides some standard methods for creating reductions, etc
*/


namespace Sw.xFilter {
  // token to use instead of null in dimensions
  var nullToken = '<>';
  export type keyAccessor = (d: any) => string;
  export type valueAccessor = (d: any) => any;
  export type grpValueAccessor = (g: any) => number;

  export interface IDeflatedTable {
    columns: string[];
    rows: any[][];
  }

/**
 * Interface for XFilter object - supplies utilities for generating crossfilter reducations in various ways
 */
  export interface IXFilter {
    crossfilter(data: any[]): CrossFilter.CrossFilter<any>;

    //--------------------------------------------------------------------------------------------------------
    // xReduce
    //--------------------------------------------------------------------------------------------------------
    // Create a crossfilter group that is a 'crosstab' or 'pivot' 
    // dimension is the underlying dimension to use - this will determine the 'row headings' in the crosstab
    // keyAccessor defines the 'column headings' in the crosstab - each distinct value that may be returned by this function
    // becomes a property of the crosstab accumulation. 
    // valueAccessor dermines the value to accumulate. This may be a scalar, but can be an object, in which case it is accumuluted
    // property-by-property
    // e.g, suppose we have data like [{Age: 10, Class: Grade1 , M: 5, F: 4},{Age: 10, Class: Grade2 , M: 2, F: 1} {Age: 11, Class: Grade2 , M: 3, F: 6}]
    // and we have a dimension 'dimAge', created by extracting the property 'Age'
    // the out-of-the-box crossfilter functionality gives:
    // dimAge.group()     -- cretes a record per age, Ccounting the number of records per age
    // More useful is:
    // dimAge.group().reduceSum(d => d.M + d.F); which is the total number of pupils per age
    // Using xReduce we go further:
    // define valueAccessor as above : d => d.M + d.F
    // and add keyAccess to make the Class the column headings: d => d.Class
    // then xReduce(dimAge, keyAccessor, valueAccessor) accumulates total like this:
    // age 10:[Grade1: 9, Grade2: 3, Tot: 12] Age 11:[Grade2: 9, Tot: 9]
    // in angularjs you'll use nested ng-repeats to extract these values into a crosstab
    //
    // In the above, the total that is accrued is a scalar value.
    // if valueAccess returns an object, we get the accumulation of that object by property:
    //e.g. supppose valueAccessor is d => { M:d.M, F:d.F, Tot: d.M + d.F}
    // then xReduce will prodcue this:
    // age 10:[Grade1: {M:4, F: 5, Tot: 9} , Grade2:{ M:2, F:1, Tot:3}, Tot:{M:6, F:6, Tot:12}] etc
    // This can be rendered in a table that shows M, F and Tot values within each class
    // 
    /*
            |-----------------------------------------------------|
            |        |    Grade1    |    Grade2    | Total        |
            |-----------------------------------------------------|
            | Age    |  M | F | Tot |  M | F | Tot |  M | F | Tot |
            |-----------------------------------------------------|
            |   10   |  4 | 5 |   9 |   2| 1 | 3   |  6 | 6 |  12 |
    */
    xReduce(dimension: CrossFilter.Dimension<any, any>,
      keyAccessor: keyAccessor, valueAccessor: valueAccessor): CrossFilter.Group<any, any, any>;

    // xReduceGrouped
    //-------------------
    // Provides the same functionality as xReduce, but also allows grouping on the dimension variable
    // for example, grouper could be set to 
    // d => { if (d.Age > OfficialAge(d.Class)) return 'Over'; return 'OK'}
    // so that the rows of the crosstabulation would now be 'Over' or 'OK'
    //
    xReduceGrouped(dimension: CrossFilter.Dimension<any, any>, keyAccessor: keyAccessor, valueAccessor: valueAccessor, grouper): CrossFilter.Group<any, any, any>;

    // xReduceTotal
    //--------------
    // same as xReduce, but collapses all rows to a single row "Total"
    // Note that each dimension may have different filters applied in a crossfilter, so the total produced 
    // is with respect to any cross-cutting filters
    //
    xReduceTotal(dimension: CrossFilter.Dimension<any, any>,
      keyAccessor: keyAccessor, valueAccessor: valueAccessor): CrossFilter.Group<any, any, any>;

    // Helper functions for generating keyAccessor and valueAccessor functions
    //------------------------------------------------------------------------
    // return a function that returns the value of a property
    // this value becomes a property in the reduction, this 'denormalises' or 'crosstabs' the field value
    // useful for tabulations
    getPropAccessor(propname: string): (d: any) => string;
    //
    // builds a reduction based on a property that has 1, 0 representing Yes or No
    // will create the property Y or N in the reduction
    //(the value assigned to this property
    getPropYnAccessor(propname: string): keyAccessor;
    // return a function that returns a constant ( usually 1)
    // can be used to supply the value when generating a reduction; ie when const is 1, this is a count of records.
    getConstAccessor(c: any): valueAccessor;

    // these are used for retrieving values from the constructed group
    // these operate on an array of key / value pairs
    // and are used in presentations - ie for charts or in tables ( ng-repeat="g in grp.all()" )

    // in the group reduction, return the value of a property in the group value object
    // this is for reporting from the group
    getGrpValueAccessor(propname: string): grpValueAccessor;
    // totAccessor is a special case where propname = 'Tot'
    totAccessor(): grpValueAccessor;
    // return the ratio of two values in the group value.
    // useful for returning a percentage
    getGrpRatioAccessor(numerator: string, divisor: string): grpValueAccessor;
    // Dimension type safety
    // crossfilter is unhappy with nulls in dimensions,
    // but doesn;t do any checking for perfomance reasons.


    getDimAccessor(propname: string): (d: any) => any;
    nullToken: string;


    toggle(grp: any, newValue): void;

    //simpleFilter enforces only one selection at a time
    simpleFilter(dimension: CrossFilter.Dimension<any, any>, filters: string[]);
    // externalFilter allows the chart to track changes in the dimensions filter
    externalFilter(dimension: CrossFilter.Dimension<any, any>, filters: string[]);
    // crosstabs may be based on very large recordsets
    // A number of techniques may be sued to reduce the data transmission in sending these to the client:
    // 1) ensure that json is gzipped by the server - far and away the most important
    // 2) consider some denomaralisatin of the data before sending; 
    // e.g.taking data that is normalised by gender, and converting to a form with fields M and F will halve the number of records
    // 3) deflate/reflate An I Deflated table is a condensed varison of the data, simiar to csv, in that the column names are
    // only supplied once. a Deflated table can be created by the DatasetSerializer in Softwords.Web
    // Deflating can result in further redcutions of packet size of around 20% 
    // Once you get the defalted data, use xFilter.reflate to return it to a regulr javascript collection (array of objects)
    reflate(table: IDeflatedTable);
  }


/**
 * 
 */
  export class XFilter implements IXFilter {

    /**
     * create a crossfilter object from a table
     *  assume that crossfilter is loaded, and therefore accessible via window.crossfilter
     * @param data - collection containing the table to crossfilter
     */
    public crossfilter(data: any[]) {
      return (<any>window).crossfilter(data);
    }

   /**
    * Function used to generate the function used as the Initial in a reduction
    * The generated function will create the empty accumulator, and store the
    * keyAccessor and valueAccessor in there so they are available to the Add and Remove functions
    * the Tot property will hold the accumulated value - which may be a scalar value, or an object
    */
    private xgetInitial = (keyAccessor, valueAccessor) => {

      var i = function () {
        var pc: any = {};
        // add the accessors as properties
        pc.keyAccessor = keyAccessor;
        pc.valueAccessor = valueAccessor;
        pc.Tot = null;
        return pc;

      }

      return i;

    };

    // reduceAdd
   /**
    * the function that willl used used as the Add in the reduction
    * p - is accumulated totals
    * d is the current data point
    */
    private xreduceAdd = (p, d) => {
      var pc = _.clone(p);

      // get the key for the current item
      var k = (pc.keyAccessor(d) || 'na');
      var v = pc.valueAccessor(d);
      pc.Tot = this.incrTotal(pc.Tot, v);
      pc[k] = this.incrTotal(pc[k], v);

      return pc;

    };

    private incrTotal(accum, value) {
      // 2 distinct cases - either a single value, or an object of values
      if (accum == null) {
        if (typeof (value) == "object") {
          return _.clone(value);
        } else {
          return value;
        }
      }
      if (typeof (value) == "object") {
        Object.keys(value).forEach((prop) => {
          accum[prop] += value[prop]; 
        });
      } else {
        accum += value;
      }
      return accum;
    }

    // reduceRemove
    private xreduceRemove = (p, d) => {
      var pc = _.clone(p);

      // get the key for the current item
      var k = (pc.keyAccessor(d) || 'na');
      var v = pc.valueAccessor(d);

      pc.Tot = this.decrTotal(pc.Tot, v);
      pc[k] = this.decrTotal(pc[k], v);
      return pc;
    };

    private decrTotal(accum, value) {
 
      // 2 distinct cases - either a single value, or an object of values
      if (typeof (value) == "object") {
        if (accum == null) {
          accum = {}
          Object.keys(value).forEach((prop) => {
            accum[prop] = value[prop] * -1;
          });
        } else {
          Object.keys(value).forEach((prop) => {
            accum[prop] -= value[prop];
          });
        }
      } else {
        if (accum == null) {
          accum = value * -1;
        } else {
          accum -= value;
        }
      }
      return accum;
    }


    public xReduce(dimension: CrossFilter.Dimension<any, any>, keyAccessor: keyAccessor, valueAccessor: valueAccessor) {

      // based  on the keyAccessor and valueAccessor generate the Initial Add and Remove functions
      var xreduceInitial = this.xgetInitial(keyAccessor, valueAccessor);
      return dimension.group().reduce(this.xreduceAdd, this.xreduceRemove, xreduceInitial);
    };


    public xReduceGrouped(dimension: CrossFilter.Dimension<any, any>, keyAccessor: keyAccessor, valueAccessor: valueAccessor, grouper) {

      // based  on the keyAccessor and valueAccessor generate the Initial Add and Remove functions
      var xreduceInitial = this.xgetInitial(keyAccessor, valueAccessor);
      return dimension.group(grouper).reduce(this.xreduceAdd, this.xreduceRemove, xreduceInitial);
    };

    public xReduceTotal(dimension: CrossFilter.Dimension<any, any>, keyAccessor: keyAccessor, valueAccessor: valueAccessor) {

      // based  on the keyAccessor and valueAccessor generate the Initial Add and Remove functions

      return this.xReduceGrouped(dimension, keyAccessor, valueAccessor, (d) => "Total");
    };


    /**
     * Simple helper to return a function that extracts a value by property name
     * @param propname
     */
    public getPropAccessor(propname: string) {
      return (d) => d[propname];
    }

    /**
     * Simple helper to translate a 1, 0 field into a Y, N accumulation
     * @param propname
     */
    public getPropYnAccessor(propname: string) {
      return (d) => {
        switch (d[propname]) {
          case 1:
            return 'Y';
          case 0:
            return 'N';
          default:
            return '?';
        }
      }
    };

    /**
     * When used as a value accessor, will return a constant. ie when c = 1 this is a count of records
     * @param c
     */
    public getConstAccessor(c: any) {
      return (d) => c;
    }
    
    /**
     * In a crosstab grouping created by xReduce, extract the 'column' value for propname
     * e.g. supppose the keyAccessor is d => d.Class
     * then getGrpValueAccessor('Grade1') return the accumulation in this row for Class=Grade1
     * @param propname - value of the column property to get the accumulation for
     */
    public getGrpValueAccessor(propname: string) {
      return (g) => g.value[propname] || null;
    }

    /**
     * shortcut to extract the 'column total' from a crosstab made with xReduce
     */
    public totAccessor() {
      return this.getGrpValueAccessor("Tot");
    }

    /**
     * extract a ratio from an accumulation
     * suppose keyAccessor is d 
     * @param numerator - property name of the numerator
     * @param divisor - property name of the denominator
     */
    public getGrpRatioAccessor(numerator: string, divisor: string) {
      return (g) => {
        var d = g.value[divisor];
        var n = g.value[numerator];
        if (d)
          return n / d;
        return null;
      };
    }

    public getDimAccessor(propname: string) {
      return (d) => d[propname] || nullToken;
    };
    public nullToken = nullToken;


    //Data Management

    /**
     * decode a compressed dataset
     * This object has two properties
     * columns: an array of the column names
     * rows: an array of arrays. Each array is the values of the data
     * this will turn this structure into a normal Json collection; ie array of objects
     * Objects in this form are created by Softwords.Web DatasetSerializer
     * @param table
     */
    public reflate(table: IDeflatedTable) {
      let reflateRow = (row) => {
        let out = {};
        row.forEach((cell, index) => {
          out[table.columns[index]] = cell;
        });
        return out;
      }
      return table.rows.map(reflateRow);
    }


    // utility functions

    // splitter is a helper function to simplify creating of selective groups; e.g. for Gender;
    // by totalling only those items where the specified property matches the specified value
    public splitter(field: string, value: any) {
      return function (d) {
        return (d[field] === value ? d.Num : 0);
      };
    };

    // toggle a filter on or off
    public toggle(cht, newValue) {
      var current = cht.filter();
      if (newValue === current) {
        return;     // otherwise we end up toggling it off
      }
      if (newValue) {
        cht.filter(newValue);
      } else {
        cht.filter(current);           // this will toggle the current off when newValue is empty
      }
      // setting the filter doesn;t redraw in dc
      ////////////dc.redrawAll(cht.chartGroup());
    }
    // simpleFilter is a filterHandler to apply to a dimension
    // it prevents multiple selections
    public simpleFilter(dimension, filters) {
      dimension.filter(null);

      if (filters.length === 0) {
        dimension.filter(null);
      } else {
        var f = filters[filters.length - 1];
        dimension.filter(f);
        return [f];
      };
    };

    /**
     * assign this funxction as the FilterHandler of a dc chart to prevent the default
     * behaviour to try to update the dimension
     * Use this when the filtering on the dimension is driven outside dc - e.g.
     * by selecting options in a dropdown or clicking on a table
     * the dc chart can still drive filtering by hadning its click event externally
     * to apply the filter
     * @param dimension
     * @param filters - assume this is a single value, 
     */
    public externalFilter(dimension, filters) {
      let cht: DC.BarChart = <any>this;
      if (filters) {
        cht.filters().splice(0, cht.filters().length, filters);
      } else {
        cht.filters().splice(0, cht.filters().length);
      }
    }
  };
  angular
    .module("sw.common")
    .service("xFilter", XFilter);

}
