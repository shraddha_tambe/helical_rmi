﻿/**
 * type declarations for objects that are returned by the jasper rest_v2 api
 * manually created
 */
declare module jasper {
  // a resource definition, as returned by  /jasperserver/rest_v2/resources

  interface IResource {
    creationDate: Date;
    description: string;
    label: string;
    permissionMask: number;
    resourceType: string;
    updateDate: Date;
    uri: string;
    version: number;

  }

  interface IResourceLookup {
    resourceLookup: IResource[];
  }

  interface IdataType {
    strictMin: boolean;
    strictMax: boolean;
    type: string;
  }
  interface IinputControlState {
    id: string;
    uri: string;
    value: any;
  }


  interface IinputControl {
    dataType: IdataType;
    id: string;
    label: string;
    mandatory: boolean;
    readOnly: boolean;
    state: IinputControlState;
    type: string;
    uri: string;
    visible: boolean;


  }

  interface IinputControlResponse {
    inputControl: IinputControl[];
  }
}