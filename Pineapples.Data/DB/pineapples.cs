using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.ComponentModel;
using System;
using System.Xml.Linq;
using System.Threading.Tasks;


namespace Pineapples.Data.DB
{
    partial class pineapplesDataContext
    {
        public static pineapplesDataContext CurrentContext()
        {
            string server = System.Web.Configuration.WebConfigurationManager.AppSettings["server"]; // "10.0.50.69";
            string database = System.Web.Configuration.WebConfigurationManager.AppSettings["database"]; // "10.0.50.69";
            string appname = System.Web.Configuration.WebConfigurationManager.AppSettings["appname"] ?? "Pineapples.Data"; // "10.0.50.69";
            return CurrentContext(server, database, appname);
        }
        public static pineapplesDataContext CurrentContext(string server, string database, string appname)
        {
            string conn = String.Format("Data Source={0};Initial Catalog={1};Application Name={2};Integrated Security=True", server, database, appname);
            return new pineapplesDataContext(conn);
        }
        public static pineapplesDataContext CurrentContext(string server, string database)
        {
            return CurrentContext(server, database, "pineapples.Injected");
        }

        public static pineapplesDataContext CurrentContext(string connectionstring)
        {

            return new pineapplesDataContext(connectionstring);
        }

        public DataSet execCommand(SqlCommand cmd)
        {
            cmd.Connection = this.Connection as SqlConnection;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            // SqDataAdatper will open the connection 
            //http://stackoverflow.com/questions/10153881/dataadapter-does-not-need-to-make-db-connection-open
            da.Fill(ds);
            return ds;
        }

        public async Task<DataSet> execCommandAsync(SqlCommand cmd)
        {
            if (this.Connection.State == ConnectionState.Closed)
            {
                await this.Connection.OpenAsync();
            }
            cmd.Connection = this.Connection as SqlConnection;
            SqlDataReader reader = await cmd.ExecuteReaderAsync();
            DataSet ds = new DataSet();
            bool go = true;
            while (go)
            {
                DataTable dt = new DataTable();
                dt.Load(reader);
                ds.Tables.Add(dt);
                if (reader.IsClosed)
                {
                    go = false;
                }
                else
                {
                    go = await reader.NextResultAsync();
                }
            }
            return ds;
        }

    }
}
