﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

using System.Data;
using System.Data.SqlClient;

namespace Pineapples.Data.DataLayer
{
    public class DSWarehouse : BaseRepository, IDSWarehouse
    {
        public DSWarehouse(DB.PineapplesEfContext cxt) : base(cxt) { }

        // For Schools dashboards
        public IDataResult TableEnrol()
        {
            SqlCommand cmd = tableEnrolCmd();
            return sqlExec(cmd);
        }

        // For individual School dashboards
        public IDataResult TableEnrolBySchool(string schNo)
        {
            SqlCommand cmd = tableEnrolBySchoolCmd(schNo);

            SqlParameter parameterSchNo = new SqlParameter("@SchNo", SqlDbType.VarChar);
            parameterSchNo.Value = schNo;
            cmd.Parameters.Add(parameterSchNo);
            return sqlExec(cmd);
        }

        public IDataResult TableDistrictEnrol()
        {
          SqlCommand cmd = tableDistrictEnrolCmd();
          return sqlExec(cmd);
        }


        public IDataResult SchoolFlowRates()
        {
            SqlCommand cmd = schoolFlowRatesCmd();
            return sqlExec(cmd);
        }

        public IDataResult SchoolTeacherCount()
        {
            SqlCommand cmd = schoolTeacherCountCmd();
            return sqlExec(cmd);
        }

        public IDataResult SchoolTeacherPupilRatio()
        {
            SqlCommand cmd = schoolTeacherPupilRatioCmd();
            return sqlExec(cmd);
        }

        #region Exam endpoints
        public IDataResult ExamSchoolResults(string schoolNo)
        {
            SqlCommand cmd = examSchoolResultsCmd(schoolNo);
            return sqlExec(cmd);
        }
        #endregion

        // For Teachers Dashboards
        public IDataResult TeacherCount()
        {
            SqlCommand cmd = teacherCountCmd();
            return sqlExec(cmd);
        }

        // Can also be used for individual Teacher dashboard
        public IDataResult TeacherQual()
        {
            SqlCommand cmd = teacherQualCmd();
            return sqlExec(cmd);
        }

        public IDataResult TeacherPupilRatio()
        {
            SqlCommand cmd = teacherPupilRatioCmd();
            return sqlExec(cmd);
        }
        
        // For Indicators Dashboards
        public IDataResult ClassLevelER()
        {
            SqlCommand cmd = classLevelERCmd();
            return sqlExec(cmd);
        }

        public IDataResult EdLevelER()
        {
            SqlCommand cmd = edLevelERCmd();
            return sqlExec(cmd);
        }

        public IDataResult FlowRates()
        {
            SqlCommand cmd = flowRatesCmd();
            return sqlExec(cmd);
        }

        // For Students dashboards
        // contains all of DistrictEdLevelAge and NationEdLevelAge
        public IDataResult EdLevelAge()
        {
            SqlCommand cmd = edLevelAgeCmd();
            return sqlExec(cmd);
        }

        // For Exams dashboards
        public IDataResult ExamResults()
        {
            SqlCommand cmd = examResultsCmd();
            return sqlExec(cmd);
        }

    #region Commands

    private SqlCommand tableEnrolBySchoolCmd(string schNo)
    {
      SqlCommand cmd = new SqlCommand();
      cmd.CommandType = CommandType.Text;
      cmd.CommandText = @"
          select 
            surveyYear,
            ClassLevel,
            Age,
            sum(CASE GenderCode WHEN 'F' THEN Enrol END) as EnrolF,
            sum(CASE GenderCode WHEN 'M' THEN Enrol END) as EnrolM
          from warehouse.enrol
          where schNo = @SchNo
          group by 
            schNo,
            surveyYear,
            ClassLevel,
            Age";
      cmd.CommandTimeout = 300;            // 5 mins
      return cmd;
    }

    private SqlCommand tableDistrictEnrolCmd()
    {
      SqlCommand cmd = new SqlCommand();
      cmd.CommandType = CommandType.Text;
      cmd.CommandText = @"
          Select 
              surveyYear,
              substring(schNo, 0, 4) as 'District',
		          ClassLevel,
		          sum(CASE GenderCode WHEN 'F' THEN Enrol END) as EnrolF,
		          sum(CASE GenderCode WHEN 'M' THEN Enrol END) as EnrolM
          from warehouse.enrol
          group by surveyYear, substring(schNo, 0, 4), ClassLevel";
      cmd.CommandTimeout = 300;            // 5 mins
      return cmd;
    }





    private SqlCommand tableEnrolCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
                    SELECT [SurveyYear]
      ,[ClassLevel]
      ,[Age]
      ,[DistrictCode]
      ,TE.[AuthorityCode]
	  , AU.AuthorityGroupCode AuthorityGovt
      ,[SchoolTypeCode]
      , sum(case Gendercode when 'M' then enrol else null end) EnrolM
	  , sum(case Gendercode when 'F' then enrol else null end) EnrolF
  FROM [warehouse].[tableEnrol] TE
	LEFT JOIN DimensionAuthority AU
		ON TE.AuthorityCode = AU.AuthorityCode
  GROUP BY 
  SurveyYear
      ,[ClassLevel]
      ,[Age]
      ,[DistrictCode]
      ,TE.[AuthorityCode]
      ,[SchoolTypeCode]
	  ,  AU.AuthorityGroupCode";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand schoolFlowRatesCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
                    SELECT [schNo]
      ,[surveyYear]
      ,[YearOfEd]
      ,100.0 * [RepeatRate] as RepeatRate /* from here all percentages to be rounded to single decimal */
      ,100.0 * [PromoteRate] as PromoteRate
      ,100.0 * [DropoutRate] as DropoutRate
      ,100.0 * [SurvivalRate] as SurvivalRate
  FROM [warehouse].[SchoolFlow]";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand schoolTeacherCountCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
                    SELECT [SchNo]
      ,SurveyYear
      ,AgeGroup
      ,DistrictCode
      ,T.AuthorityCode
	  , AU.AuthorityGroupCode AuthorityGovt
      ,SchoolTypeCode
      ,Sector
      ,ISCEDSubClass
	  , sum(case GenderCode when 'M' then NumTeachers else null end) NumTeachersM
	  , sum(case GenderCode when 'F' then NumTeachers else null end) NumTeachersF
	  , sum(case GenderCode when 'M' then Certified else null end) CertifiedM
	  , sum(case GenderCode when 'F' then Certified else null end) CertifiedF
	  , sum(case GenderCode when 'M' then Qualified else null end) QualifiedM
	  , sum(case GenderCode when 'F' then Qualified else null end) QualifiedF
	  , sum(case GenderCode when 'M' then CertQual else null end) CertQualM
	  , sum(case GenderCode when 'F' then CertQual else null end) CertQualF
  FROM warehouse.schoolTeacherCount T
        LEFT JOIN DimensionAuthority AU
		            ON T.AuthorityCode = AU.AuthorityCode
WHERE Support is null
GROUP BY 
      SchNo
      ,SurveyYear
      ,AgeGroup
      ,DistrictCode
      ,T.AuthorityCode
	  , AU.AuthorityGroupCode 
      ,SchoolTypeCode
      ,Sector
      ,ISCEDSubClass";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand schoolTeacherPupilRatioCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
                      SELECT [SchNo]
      ,SurveyYear
      ,DistrictCode
      ,STPR.AuthorityCode
	  , AU.AuthorityGroupCode AuthorityGovt
      ,SchoolTypeCode
      ,Sector      
      ,[NumTeachers]
      ,[Certified]
      ,[Qualified]
      ,[CertQual]
      ,[Enrol]
  FROM warehouse.[SchoolTeacherPupilRatio] STPR
        LEFT JOIN DimensionAuthority AU
		            ON STPR.AuthorityCode = AU.AuthorityCode";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand examSchoolResultsCmd(string SchoolNo)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
                      SELECT[examCode] + ': ' + [examName]
        AS Exam
     ,[examYear] AS ExamYear
     ,[StateID] AS DistrictCode
     ,[schNo] As SchoolNo
     ,[standardCode] + ': ' + [standardDesc]
        AS ExamStandard
     ,[benchmarkCode] + ': ' + [exbnchDescription]
        AS ExamBenchmark
     , sum(case Gender when 'M' then[Candidates] else null end) AS[CandidatesM]
	  ,sum(case Gender when 'M' then[1] else null end) AS[WellBelowCompetentM]
	  ,sum(case Gender when 'M' then[2] else null end) [ApproachingCompetenceM]
	  ,sum(case Gender when 'M' then[3] else null end) [MinimallyCompetentM]
	  ,sum(case Gender when 'M' then[4] else null end) [CompetentM]
	  ,sum(case Gender when 'F' then[Candidates] else null end) AS[CandidatesF]
	  ,sum(case Gender when 'F' then[1] else null end) AS[WellBelowCompetentF]
	  ,sum(case Gender when 'F' then[2] else null end) [ApproachingCompetenceF]
	  ,sum(case Gender when 'F' then[3] else null end) [MinimallyCompetentF]
	  ,sum(case Gender when 'F' then[4] else null end) [CompetentF]
        FROM [warehouse].[ExamSchoolResultsX]
        EX
INNER JOIN[dbo].[ExamBenchmarks] EB ON EX.benchmarkCode = EB.exbnchCode
WHERE schNo = @schoolNo 
GROUP BY
[examCode] + ': ' + [examName]
      ,[examYear]
      ,[StateID]
      ,[schNo]
      ,[standardCode] + ': ' + [standardDesc]
      ,[benchmarkCode] + ': ' + [exbnchDescription]";
            cmd.CommandTimeout = 300;            // 5 mins
            cmd.Parameters.Add("@schoolNo", SqlDbType.NVarChar, 50)
                    .Value = SchoolNo;

            return cmd;
        }        

        private SqlCommand teacherPupilRatioCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
                      SELECT SurveyYear
      ,DistrictCode
      ,STPR.AuthorityCode
	  , AU.AuthorityGroupCode AuthorityGovt
      ,SchoolTypeCode
      ,Sector      
      ,sum([NumTeachers]) AS NumTeachers
      ,sum([Certified]) AS Certified
      ,sum([Qualified]) AS Qualified
      ,sum([CertQual]) AS CertQual
	  ,sum([Enrol]) AS Enrol
  FROM warehouse.[SchoolTeacherPupilRatio] STPR
        LEFT JOIN DimensionAuthority AU
		            ON STPR.AuthorityCode = AU.AuthorityCode
GROUP BY
	  SurveyYear
      ,DistrictCode
      ,STPR.AuthorityCode
	  , AU.AuthorityGroupCode
      ,SchoolTypeCode
      ,Sector";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand classLevelERCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
             SELECT [SurveyYEar] AS SurveyYear
      ,[ClassLevel] AS Grade
      ,[OfficialAge]
      ,[districtCode] AS DistrictCode
      ,[enrolM] AS EnrolmentM
      ,[enrolF] AS EnrolmentF
      ,[enrol] AS EnrolmentT
      ,[repM] AS RepeatersM
      ,[repF] AS RepeatersF
      ,[rep] AS RepeatersT
      ,[psaM] AS PreSchoolAttendersM
      ,[psaF] AS PreSchoolAttendersF
      ,[psa] AS PreSchoolAttendersT
      ,[intakeM] AS IntakeM
      ,[intakeF] AS IntakeF
      ,[intake] AS IntakeMT
      ,[nEnrolM] AS NetEnrolmentM
      ,[nEnrolF] AS NetEnrolmentF
      ,[nEnrol] AS NetEnrolmentT
      ,[nRepM] AS NetRepeatersM
      ,[nRepF] AS NetRepeatersF
      ,[nRep] AS NetRepeatersT
      ,[nIntakeM] AS NetIntakeM
      ,[nIntakeF] AS NetIntakeF
      ,[nIntake] AS NetIntakeT
      ,[popM] AS PopulationM
      ,[popF] AS PopulationF
      ,[pop] AS PopulationT
	  ,100.0 * [enrolM] / [popM] AS GERM /* from here all percentages to be rounded to single decimal */
	  ,100.0 * [enrolF] / [popF] AS GERF
	  ,100.0 * [enrol] / [pop] AS GER
	  ,100.0 * [nEnrolM] / [popM] AS NERM
	  ,100.0 * [nEnrolF] / [popF] AS NERF
	  ,100.0 * [nEnrol] / [pop] AS NER
	  ,100.0 * [intakeM] / [popM] AS GIRM
	  ,100.0 * [intakeF] / [popF] AS GIRF
	  ,100.0 * [intake] / [pop] AS GIR
	  ,100.0 * [nIntakeM] / [popM] AS NIRM
	  ,100.0 * [nIntakeF] / [popF] AS NIRF
	  ,100.0 * [nIntake] / [pop] AS NIR
  FROM [warehouse].[classLevelERDistrict]";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand edLevelERCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
             SELECT [SurveyYear]
      ,[edLevelCode] AS EducationLevel
      ,[districtCode] AS DistrictCode
      ,[enrolM] AS EnrolmentM
      ,[enrolF] AS EnrolmentF
      ,[enrol] AS EnrolmentT
      ,[repM] AS RepeatersM
      ,[repF] AS RepeatersF
      ,[rep] AS RepeatersT
      ,[intakeM] AS IntakeM
      ,[intakeF] AS IntakeF
      ,[intake] AS IntakeMT
      ,[nEnrolM] AS NetEnrolmentM
      ,[nEnrolF] AS NetEnrolmentF
      ,[nEnrol] AS NetEnrolmentT
      ,[nRepM] AS NetRepeatersM
      ,[nRepF] AS NetRepeatersF
      ,[nRep] AS NetRepeatersT
      ,[nIntakeM] AS NetIntakeM
      ,[nIntakeF] AS NetIntakeF
      ,[nIntake] AS NetIntakeT
      ,[popM] AS PopulationM
      ,[popF] AS PopulationF
      ,[pop] AS PopulationT
      ,[firstYear] AS FirstYearOfEducationLevel
      ,[lastYear] AS LastYearOfEducationLevel
      ,[numYears] AS NumberOFYearsInEducationLevel
	  ,100.0 * [enrolM] / [popM] AS GERM /* from here all percentages to be rounded to single decimal */
	  ,100.0 * [enrolF] / [popF] AS GERF
	  ,100.0 * [enrol] / [pop] AS GER
	  ,100.0 * [nEnrolM] / [popM] AS NERM
	  ,100.0 * [nEnrolF] / [popF] AS NERF
	  ,100.0 * [nEnrol] / [pop] AS NER
	  ,100.0 * [intakeM] / [popM] AS GIRM
	  ,100.0 * [intakeF] / [popF] AS GIRF
	  ,100.0 * [intake] / [pop] AS GIR
	  ,100.0 * [nIntakeM] / [popM] AS NIRM
	  ,100.0 * [nIntakeF] / [popF] AS NIRF
	  ,100.0 * [nIntake] / [pop] AS NIR
  FROM [warehouse].[EdLevelERDistrict]";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand flowRatesCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
             SELECT [SurveyYear]
      ,[YearOfEd]
      ,[DistrictCode]
	  ,sum(case GenderCode when 'M' then [RepeatRate] else null end) [RepeatRateM] /* from here all percentages to be rounded to single decimal */
	  ,sum(case GenderCode when 'F' then [RepeatRate] else null end) [RepeatRateF]
	  ,sum(case GenderCode when 'M' then [PromoteRate] else null end) [PromoteRateM] 
	  ,sum(case GenderCode when 'F' then [PromoteRate] else null end) [PromoteRateF]
	  ,sum(case GenderCode when 'M' then [DropoutRate] else null end) [DropoutRateM] 
	  ,sum(case GenderCode when 'F' then [DropoutRate] else null end) [DropoutRateF]
	  ,sum(case GenderCode when 'M' then [SurvivalRate] else null end) [SurvivalRateM]
	  ,sum(case GenderCode when 'F' then [SurvivalRate] else null end) [SurvivalRateF]
  FROM [warehouse].[DistrictFlow]
  GROUP BY
   [SurveyYear]
   ,[YearOfEd]
   ,[DistrictCode]";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand edLevelAgeCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
                    SELECT[surveyYear]
      ,[ClassLevel]
      ,[districtCode]
      ,ELA.[AuthorityCode]
	  , AU.AuthorityGroupCode AuthorityGovt
      ,[SchoolTypecode]
      ,[yearOfEd]
      ,[EdLevel]
      , sum(case Gendercode when 'M' then UnderAge else null end) UnderAgeM
      ,sum(case Gendercode when 'M' then OfficialAge else null end) OfficialAgeM
      ,sum(case Gendercode when 'M' then OverAge else null end) OverAgeM
      ,sum(case Gendercode when 'M' then Enrol else null end) EnrolM
      ,sum(case Gendercode when 'M' then EstimatedEnrol else null end) EstimatedEnrolM
	  ,sum(case Gendercode when 'F' then UnderAge else null end) UnderAgeF
      ,sum(case Gendercode when 'F' then OfficialAge else null end) OfficialAgeF
      ,sum(case Gendercode when 'F' then OverAge else null end) OverAgeF
      ,sum(case Gendercode when 'F' then Enrol else null end) EnrolF
      ,sum(case Gendercode when 'F' then EstimatedEnrol else null end) EstimatedEnrolF
  FROM[warehouse].[EdLevelAge]
        ELA
   LEFT JOIN DimensionAuthority AU
       ON ELA.AuthorityCode = AU.AuthorityCode
 GROUP BY
      [surveyYear]
      ,[ClassLevel]
      ,[districtCode]
      ,ELA.[AuthorityCode]
	  , AU.AuthorityGroupCode
      ,[SchoolTypecode]
      ,[yearOfEd]
      ,[EdLevel];";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand examResultsCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;

            cmd.CommandText = @"
                    SELECT [examCode] + ': ' + [examName] AS Exam
      ,[examYear] AS ExamYear
      ,[StateID] AS DistrictCode
      ,[standardCode] + ': ' + [standardDesc] AS ExamStandard
      ,[benchmarkCode] + ': ' + [exbnchDescription] AS ExamBenchmark
	  ,sum(case Gender when 'M' then [Candidates] else null end) AS [CandidatesM]
	  ,sum(case Gender when 'M' then [1] else null end) AS [WellBelowCompetentM]
	  ,sum(case Gender when 'M' then [2] else null end) [ApproachingCompetenceM]
	  ,sum(case Gender when 'M' then [3] else null end) [MinimallyCompetentM]
	  ,sum(case Gender when 'M' then [4] else null end) [CompetentM]
	  ,sum(case Gender when 'F' then [Candidates] else null end) AS [CandidatesF]
	  ,sum(case Gender when 'F' then [1] else null end) AS [WellBelowCompetentF]
	  ,sum(case Gender when 'F' then [2] else null end) [ApproachingCompetenceF]
	  ,sum(case Gender when 'F' then [3] else null end) [MinimallyCompetentF]
	  ,sum(case Gender when 'F' then [4] else null end) [CompetentF]
  FROM [warehouse].[ExamStateResultsX] ESX
  INNER JOIN [dbo].[ExamBenchmarks] EB ON ESX.benchmarkCode = EB.exbnchCode
  GROUP BY
  [examCode] + ': ' + [examName]
      ,[examYear]
      ,[StateID]
      ,[standardCode] + ': ' + [standardDesc]
      ,[benchmarkCode] + ': ' + [exbnchDescription]";
            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand teacherCountCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            // denormalise on gender
            cmd.CommandText = @"
SELECT SurveyYear
      ,AgeGroup
      ,DistrictCode
      ,T.AuthorityCode
	  , AU.AuthorityGroupCode AuthorityGovt
      ,SchoolTypeCode
      ,Sector
      ,ISCEDSubClass
	  , sum(case GenderCode when 'M' then NumTeachers else null end) NumTeachersM
	  , sum(case GenderCode when 'F' then NumTeachers else null end) NumTeachersF
	  , sum(case GenderCode when 'M' then Certified else null end) CertifiedM
	  , sum(case GenderCode when 'F' then Certified else null end) CertifiedF
	  , sum(case GenderCode when 'M' then Qualified else null end) QualifiedM
	  , sum(case GenderCode when 'F' then Qualified else null end) QualifiedF
	  , sum(case GenderCode when 'M' then CertQual else null end) CertQualM
	  , sum(case GenderCode when 'F' then CertQual else null end) CertQualF
  FROM warehouse.schoolTeacherCount T
        LEFT JOIN DimensionAuthority AU
		            ON T.AuthorityCode = AU.AuthorityCode
WHERE Support is null
GROUP BY 
SurveyYear
      ,AgeGroup
      ,DistrictCode
      ,T.AuthorityCode
	  , AU.AuthorityGroupCode 
      ,SchoolTypeCode
      ,Sector
      ,ISCEDSubClass";

            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        private SqlCommand teacherQualCmd()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            // denormalise on gender
            cmd.CommandText = @"
SELECT TI.[tID]
	  ,TI.tGiven AS FirstName
	  ,TI.tSurname AS LastName
      ,[yr] As Year
      ,[tchQual] AS QualificationCode
	  ,[codeDescription] AS Qualification
	  ,[codeGroup] AS QualificationType
  FROM [warehouse].[TeacherQual] AS TQ
  INNER JOIN [dbo].[lkpTeacherQual] AS TQL ON TQ.tchQual = TQL.codeCode
  INNER JOIN [dbo].TeacherIdentity AS TI ON TQ.tID = TI.tID";

            cmd.CommandTimeout = 300;            // 5 mins

            return cmd;
        }

        #endregion
    }



}
