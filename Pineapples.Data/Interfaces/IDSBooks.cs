﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

namespace Pineapples.Data
{
    public interface IDSBooks : IDSCrud<BookBinder, string>
    {
        IDataResult Filter(BookFilter fltr);
    }
}
