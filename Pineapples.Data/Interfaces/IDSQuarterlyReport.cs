﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Security.Claims;

namespace Pineapples.Data
{
    public interface IDSQuarterlyReport : IDSCrud<QuarterlyReportBinder, int>
    {
        void AccessControl(int ID, ClaimsIdentity identity);
        IDataResult Filter(QuarterlyReportFilter fltr);
    }
}
