﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;

namespace Pineapples.Data
{
    public interface IDSQuarterlyReportX : IDSCrud<QuarterlyReportXBinder, int>
    {
        IDataResult Filter(QuarterlyReportXFilter fltr);
    }
}
