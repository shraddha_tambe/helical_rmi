﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Softwords.DataTools;
using System.Xml.Linq;

namespace Pineapples.Data
{
    public interface IDSWarehouse
    {
        IDataResult TableEnrol();
        IDataResult SchoolFlowRates();
        IDataResult SchoolTeacherCount();
        IDataResult SchoolTeacherPupilRatio();

        IDataResult TeacherCount();
        IDataResult TeacherQual();
        IDataResult TeacherPupilRatio();
        IDataResult ClassLevelER();
        IDataResult EdLevelER();
        IDataResult FlowRates();
        IDataResult EdLevelAge();
        IDataResult ExamResults();
        IDataResult TableDistrictEnrol();
        IDataResult TableEnrolBySchool(string schoolNo);

        // exam methods
        /// <summary>
        /// return all data from Exam School Results - for a single school
        /// </summary>
        /// <returns></returns>
        IDataResult ExamSchoolResults(string schoolNo);

    }
}
