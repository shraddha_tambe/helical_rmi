﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.Book>))]
    public class BookBinder : Softwords.Web.Models.Entity.ModelBinder<Models.Book>
    {
        public string bkCode
        {
            get
            {
                return getProp("bkCode") as string;
            }
            set
            {
                definedProps.Add("bkCode", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.Book b = null;

            IQueryable<Models.Book> qry = from book in cxt.Books
                                        where book.bkCode == bkCode
                                        select book;

            if (qry.Count() == 0)
            {
                throw RecordNotFoundException.Make(bkCode);
            }

            b = qry.First();
            if (Convert.ToBase64String(b.pRowversion.ToArray()) != this.RowVersion)
            {
                // optimistic concurrency error
                // we return the most recent version of the record
                FromDb(b);
                throw ConcurrencyException.Make(this.rowVersionProp, this.definedProps);
            }
            ToDb(cxt, identity, b);


        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.Book b = null;

            IQueryable<Models.Book> qry = from book in cxt.Books
                                      where book.bkCode == bkCode
                                      select book;

            if (qry.Count() != 0)
            {
                throw DuplicateKeyException.Make(keyProp, bkCode);
            }

            b = new Models.Book();
            cxt.Books.Add(b);
            ToDb(cxt, identity, b);
        }
    }
}
