﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;

namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.QuarterlyReport>))]
    public class QuarterlyReportBinder : Softwords.Web.Models.Entity.ModelBinder<Models.QuarterlyReport>
    {
        public int? qrID
        {
            get
            {
                return (int?)getProp("qrID");
            }
            set
            {
                definedProps.Add("qrID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.QuarterlyReport qr = null;

            IQueryable<Models.QuarterlyReport> qry = from quarterlyreport in cxt.QuarterlyReports
                                                         where quarterlyreport.qrID == qrID
                                                         select quarterlyreport;

            if (qry.Count() == 0)
            {
                throw RecordNotFoundException.Make(qrID);
            }

            qr = qry.First();
            if (Convert.ToBase64String(qr.pRowversion.ToArray()) != this.RowVersion)
            {
                // optimistic concurrency error
                // we return the most recent version of the record
                FromDb(qr);
                throw ConcurrencyException.Make(this.rowVersionProp, this.definedProps);
            }
            ToDb(cxt, identity, qr);

        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.QuarterlyReport qr = null;

            IQueryable<Models.QuarterlyReport> qry = from quarterlyreport in cxt.QuarterlyReports
                                      where quarterlyreport.qrID == qrID
                                      select quarterlyreport;

            if (qry.Count() != 0)
            {
                throw DuplicateKeyException.Make(this.keyProp, qrID);
            }

            qr = new Models.QuarterlyReport();
            cxt.QuarterlyReports.Add(qr);
            ToDb(cxt, identity, qr);
        }
    }
}
