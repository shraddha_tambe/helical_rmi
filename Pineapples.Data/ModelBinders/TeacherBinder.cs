﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Softwords.Web;
using System.Security.Claims;



namespace Pineapples.Data
{
    [JsonConverter(typeof(Softwords.Web.Models.ModelBinderConverter<Models.TeacherIdentity>))]
    public class TeacherBinder : Softwords.Web.Models.Entity.ModelBinder<Models.TeacherIdentity>
    {
        // allow nullable to handle new record
        public int? tID
        {
            get
            {
                return (int?)getProp("tID");
            }
            set
            {
                definedProps.Add("tID", value);
            }
        }

        public void Update(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.TeacherIdentity ti = null;

            IQueryable<Models.TeacherIdentity> qry = from teacher in cxt.TeacherIdentities
                                        where teacher.tID == tID
                                        select teacher;

            if (qry.Count() == 0)
            {
                throw RecordNotFoundException.Make(tID);
            }

            ti = qry.First();
            if (Convert.ToBase64String(ti.pRowversion.ToArray()) != RowVersion)
            {
                // optimistic concurrency error
                // we return the most recent version of the record
                FromDb(ti);
                throw ConcurrencyException.Make(this.rowVersionProp, definedProps);
            }
            ToDb(cxt, identity, ti);
        }

        public void Create(Data.DB.PineapplesEfContext cxt, ClaimsIdentity identity)
        {
            Models.TeacherIdentity ti = null;

            if  (getProp("tID") != null)
            {
                throw DuplicateKeyException.Make(keyProp, getProp("tID"));
            }

            ti = new Models.TeacherIdentity();
            cxt.TeacherIdentities.Add(ti);
            ToDb(cxt, identity, ti);
        }
    }
}

