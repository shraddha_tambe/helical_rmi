namespace Pineapples.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Authority
    {
        [Key]
        [StringLength(10)]
        public string authCode { get; set; }

        [StringLength(100)]
        public string authName { get; set; }

        [StringLength(1)]
        public string authType { get; set; }

        [StringLength(50)]
        public string authContact { get; set; }

        [StringLength(50)]
        public string authAddr1 { get; set; }

        [StringLength(50)]
        public string authAddr2 { get; set; }

        [StringLength(50)]
        public string authAddr3 { get; set; }

        [StringLength(50)]
        public string authAddr4 { get; set; }

        [StringLength(30)]
        public string authPh1 { get; set; }

        [StringLength(30)]
        public string authPh2 { get; set; }

        [StringLength(30)]
        public string authFax { get; set; }

        [StringLength(30)]
        public string authEmail { get; set; }

        [StringLength(50)]
        public string authNameL1 { get; set; }

        [StringLength(50)]
        public string authNameL2 { get; set; }

        [StringLength(10)]
        public string authBank { get; set; }

        [StringLength(10)]
        public string authBSB { get; set; }

        [StringLength(20)]
        public string authAccountNo { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int authID { get; set; }

        public int? authOrgUnitNumber { get; set; }

        public int? authSuperOrgUnitNumber { get; set; }
    }
}
