﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Pineapples.Data.Models
{
    [Table("Books")]
    public partial class Book
    {
        [Key]
        [MaxLength(20)]
        public string bkCode { get; set; }
        [MaxLength(10)]
        public string bkPublisher { get; set; }
        [MaxLength(20)]
        public string bkISBN { get; set; }
        [MaxLength(10)]
        public string bkSubject { get; set; }
        [MaxLength(100)]
        public string bkTitle { get; set; }
        [MaxLength(10)]
        public string bkVol { get; set; }
        [MaxLength(10)]
        public string bkTG { get; set; }
        [MaxLength(10)]
        public string bkEdition { get; set; }
        public int? bkYear { get; set; }
        [MaxLength(50)]
        public string pCreateUser { get; set; }
        public DateTime? pCreateDateTime { get; set; }
        [MaxLength(50)]
        public string pEditUser { get; set; }
        public DateTime? pEditDateTime { get; set; }
        [Timestamp]
        public byte[] pRowversion { get; set; }
    }

}
