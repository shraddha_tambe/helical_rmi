﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{

    [Table("Documents_")]
    public partial class Document:ChangeTracked
    {
 
        [Key]
        [Required(ErrorMessage = "Document ID is required")]
        [Display(Name = "Doc ID")]
        public Guid docID { get; set; }


        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "Document Title")]
        public string docTitle { get; set; }

        [MaxLength(400)]
        [StringLength(400)]
        [Display(Name = "Document Description")]
        public string docDescription { get; set; }

        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "Document Source")]
        public string docSource { get; set; }

        [Display(Name = "Document Date")]
        public DateTime? docDate { get; set; }

        [Required(ErrorMessage = "Rotate is required")]
        [Display(Name = "Rotate")]
        public int docRotate { get; set; }

        [MaxLength(200)]
        [StringLength(200)]
        [Display(Name = "Tags")]
        public string docTags { get; set; }

        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "File Type")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string docType { get; set; }
    }

}
