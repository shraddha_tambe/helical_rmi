﻿
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Pineapples.Data.Models
{
    [Table("Exams")]
    [Description("Each record in Exams represents an instance of a particular examination held in one year.")]
    public partial class Exam
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int exID { get; set; }

        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "Code")]
        public string exCode { get; set; }

        [Display(Name = "Year")]
        public int? exYear { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "User")]
        public string exUser { get; set; }

        [Display(Name = "Date")]
        public DateTime? exDate { get; set; }

        [Display(Name = "Candidates")]
        public int? exCandidates { get; set; }

        [Display(Name = "Marks")]
        public int? exMarks { get; set; }

        [Display(Name = "Subject Score")]
        public double? exSubjectScore { get; set; }

        [Display(Name = "Candidate Score")]
        public double? exCandidateScore { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "Scoring Model")]
        public string exScoringModel { get; set; }

        [Display(Name = "Num Scores")]
        public int? exsmNumScores { get; set; }

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Mandatory")]
        public string exsmMandatory { get; set; }

        [MaxLength(255)]
        [StringLength(255)]
        [Display(Name = "Excluded")]
        public string exsmExcluded { get; set; }

        [MaxLength(20)]
        [StringLength(20)]
        [Display(Name = "Exporter Version")]
        public string exExporterVersion { get; set; }

        [Display(Name = "Export Date")]
        public DateTime? exExportDate { get; set; }

        [Display(Name = "File ID")]
        public Guid? docID { get; set; }
    }
}