using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpBuildingSubType")]
    [Description(@"Subtypes of buildings within Building Type. Particularly used for teacher houses, where the subtype corresponds to the earlier resource type.")]
    public partial class BuildingSubType : SimpleCodeTable
    {
        [Key]

        [Column("bsubCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "bsub Code is required")]
        [Display(Name = "bsub Code")]
        public override string Code { get; set; }

        [Column("bdlgCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "bdlg Code is required")]
        [Display(Name = "bdlg Code")]
        public string bdlgCode { get; set; }

        [Column("bsubDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "bsub Description is required")]
        [Display(Name = "bsub Description")]
        public override string Description { get; set; }

        [Column("bsubDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bsub Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("bsubDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "bsub Description L2")]
        public override string DescriptionL2 { get; set; }

        [Column("bsubRoomsClass", TypeName = "int")]
        [Display(Name = "bsub Rooms Class")]
        public int? bsubRoomsClass { get; set; }

        [Column("bsubRoomsOHT", TypeName = "int")]
        [Display(Name = "bsub Rooms OHT")]
        public int? RoomsOHT { get; set; }

        [Column("bsubRoomsStaff", TypeName = "int")]
        [Display(Name = "bsub Rooms Staff")]
        public int? RoomsStaff { get; set; }

        [Column("bsubRoomsAdmin", TypeName = "int")]
        [Display(Name = "bsub Rooms Admin")]
        public int? RoomsAdmin { get; set; }

        [Column("bsubRoomsStorage", TypeName = "int")]
        [Display(Name = "bsub Rooms Storage")]
        public int? RoomsStorage { get; set; }

        [Column("bsubRoomsDorm", TypeName = "int")]
        [Display(Name = "bsub Rooms Dorm")]
        public int? RoomsDorm { get; set; }

        [Column("bsubRoomsKitchen", TypeName = "int")]
        [Display(Name = "bsub Rooms Kitchen")]
        public int? RoomsKitchen { get; set; }

        [Column("bsubRoomsDining", TypeName = "int")]
        [Display(Name = "bsub Rooms Dining")]
        public int? RoomsDining { get; set; }

        [Column("bsubRoomsLibrary", TypeName = "int")]
        [Display(Name = "bsub Rooms Library")]
        public int? RoomsLibrary { get; set; }

        [Column("bsubRoomsSpecialTuition", TypeName = "int")]
        [Display(Name = "bsub Rooms Special Tuition")]
        public int? RoomsSpecialTuition { get; set; }

        [Column("bsubRoomsOther", TypeName = "int")]
        [Display(Name = "bsub Rooms Other")]
        public int? RoomsOther { get; set; }
    }
}
