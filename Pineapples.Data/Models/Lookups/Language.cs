using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpLanguage")]
    [Description(@"Languages spoken. May categorise teachers, classes, and schools.
Users may also be assigned a language, which can determine the system language for display.")]
    public partial class Language : SimpleCodeTable
    {
        [Key]

        [Column("langCode", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Required(ErrorMessage = "lang Code is required")]
        [Display(Name = "lang Code")]
        public override string Code { get; set; }

        [Column("langName", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "lang Name")]
        public override string Description { get; set; }

        [Column("langGrp", TypeName = "nvarchar")]
        [MaxLength(5)]
        [StringLength(5)]
        [Display(Name = "lang Grp")]
        public string LangGroup { get; set; }

        [Column("langSystem", TypeName = "bit")]
        [Display(Name = "lang System")]
        public bool? langSystem { get; set; }

        [Column("langNameL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "lang Name L1")]
        public override string DescriptionL1 { get; set; }

        [Column("langNameL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "lang Name L2")]
        public override string DescriptionL2 { get; set; }
    }
}
