﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("Navigation")]
    public class Navigation: ChangeTracked
    {
        [Key]
        [Column("id", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Required(ErrorMessage = "id is required")]
        [Display(Name = "id")]
        public string id { get; set; }

        [Column("icon", TypeName = "nvarchar")]
        [MaxLength(30)]
        [StringLength(30)]
        [Display(Name = "icon")]
        public string icon { get; set; }
        [Column("label", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "label")]
        public string label { get; set; }
        [Column("state", TypeName = "nvarchar")]
        [MaxLength(80)]
        [StringLength(80)]
        [Display(Name = "state")]
        public string state { get; set; }
        [Column("children", TypeName = "nvarchar")]
        [MaxLength(1000)]
        [StringLength(1000)]
        [Display(Name = "children")]
        public string children { get; set; }
        [Column("note", TypeName = "nvarchar")]
        [MaxLength(100)]
        [StringLength(100)]
        [Display(Name = "note")]
        public string note { get; set; }
        [Column("homestate", TypeName = "nvarchar")]
        [MaxLength(80)]
        [StringLength(80)]
        [Display(Name = "homestate")]
        public string homestate { get; set; }
        [Column("menuPriority", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Display(Name = "menu Priority")]
        public string menuPriority { get; set; }
    }

}
