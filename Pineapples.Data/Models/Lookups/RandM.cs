using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpRandM")]
    public partial class RandM : SequencedCodeTable
    {
        [Key]

        [Column("randmCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "randm Code is required")]
        [Display(Name = "randm Code")]
        public override string Code { get; set; }

        [Column("randmDescription", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "randm Description is required")]
        [Display(Name = "randm Description")]
        public override string Description { get; set; }

        [Column("randmSeq", TypeName = "int")]
        [Display(Name = "randm Seq")]
        public override int? Seq { get; set; }

        [Column("randmDescriptionL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "randm Description L1")]
        public override string DescriptionL1 { get; set; }

        [Column("randmDescriptionL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "randm Description L2")]
        public override string DescriptionL2 { get; set; }
    }
}
