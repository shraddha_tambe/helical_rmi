using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpRegion")]
    [Description(@"Category of island. Generally used as an indicator of remoteness. Stored in iOuter.")]
    public partial class Region : SimpleCodeTable {

        [Key]
        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(1)]
        [StringLength(1)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

        [Column("codeGroup", TypeName = "nvarchar")]
        [MaxLength(1)]
        [StringLength(1)]
        [Display(Name = "code Group")]
        public string Group { get; set; }

    }
}
