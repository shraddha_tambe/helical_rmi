using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpRoomFunction")]
    [Description(@"Function of rooms. These are system defined and correspond to the explicit fields on Building and BuildingReview for room counts. This mapping back to room type enabes reconciliation of the room counts on Buildings, with room counts in the survey.")]
    public partial class RoomFunction : SimpleCodeTable
    {
        [Key]

        [Column("rfcnCode", TypeName = "nvarchar")]
        [MaxLength(10)]
        [StringLength(10)]
        [Required(ErrorMessage = "rfcn Code is required")]
        [Display(Name = "rfcn Code")]
        public override string Code { get; set; }

        [Column("rfcnDesc", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "rfcn Desc")]
        public override string Description { get; set; }

        [Column("rfcnDescL1", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "rfcn DescL1")]
        public override string DescriptionL1 { get; set; }

        [Column("rfcnDescL2", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "rfcn DescL2")]
        public override string DescriptionL2 { get; set; }

        [Column("rfcnNotes", TypeName = "ntext")]
        [Display(Name = "rfcn Notes")]
        public string Notes { get; set; }
    }
}
