using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpSchoolBuildingTypes")]
    [Description(@"Used only for building types on ECE survey. May be removed in future revisions")]
    public partial class SchoolBuildingType : SimpleCodeTable
    {
        [Column("codeSort", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Sort")]
        public string Seq { get; set; }
    }
}
