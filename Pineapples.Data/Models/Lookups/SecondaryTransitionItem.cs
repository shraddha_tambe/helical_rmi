using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpSecondaryTransitionItem")]
    [Description(@"Secondary transaitions grid is made of these items, and related Secondary Transisitions code. These appear on PupilTables for the Transitions.")]
    public partial class SecondaryTransitionItem : SequencedCodeTable { }
}
