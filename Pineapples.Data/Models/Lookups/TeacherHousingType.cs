using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Softwords.Web.Models;

namespace Pineapples.Data.Models
{
    [Table("lkpTeacherHousingTypes")]
    [Description(@"OBSELETE - teacher houising is defined by a resource type, and even more recently, by Cuilding Subtype.")]
    public partial class TeacherHousingType : SequencedCodeTable
    {
        [Key]

        [Column("codeCode", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Required(ErrorMessage = "code Code is required")]
        [Display(Name = "code Code")]
        public override string Code { get; set; }

 

        [Column("codeGroup", TypeName = "nvarchar")]
        [MaxLength(50)]
        [StringLength(50)]
        [Display(Name = "code Group")]
        public string codeGroup { get; set; }

        [Column("codeSort", TypeName = "int")]
        [Display(Name = "code Sort")]
        public override int? Seq { get; set; }
    }
}
