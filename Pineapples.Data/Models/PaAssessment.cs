﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{

    [Table("paAssessment_")]
    public partial class PaAssessment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int paID { get; set; }
        public int tID { get; set; }
        [MaxLength(50)]
        public string paSchNo { get; set; }
        public DateTime? paDate { get; set; }
        [MaxLength(100)]
        public string paConductedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int? paYear { get; set; }
        [MaxLength(10)]
        public string pafrmCode { get; set; }
        [Timestamp]
        public byte[] pRowversion { get; set; }
        [MaxLength(50)]
        public string pCreateUser { get; set; }
        public DateTime? pCreateDateTime { get; set; }
        [MaxLength(50)]
        public string pEditUser { get; set; }
        public DateTime? pEditDateTime { get; set; }

        public ICollection<PaAssessmentLine> Lines { get; set; }
    }

}
