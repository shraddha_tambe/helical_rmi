﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class QuarterlyReportFilter : Filter
    {
        public string QRID { get; set; }
        public string School { get; set; }
        public string SchoolNo { get; set; }
        public string InspQuarterlyReport { get; set; }
        public string District { get; set; }
        public string Authority { get; set; }

        public void ApplyUserFilter(ClaimsIdentity identity)
        {
            Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
            if (c != null)
            {
                District = c.Value;
            }
            c = identity.FindFirst(x => x.Type == "filterAuthority");
            if (c != null)
            {
                Authority = c.Value;
            }
            c = identity.FindFirst(x => x.Type == "filterSchoolNo");
            if (c != null)
            {
                SchoolNo = c.Value;
            }

        }

    }

    public class QuarterlyReportTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public QuarterlyReportFilter filter { get; set; }
    }
}
