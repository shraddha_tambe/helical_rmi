﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pineapples.Data.Models
{
    [Table("pInspectionRead.SchoolAccreditations")]
    public partial class SchoolAccreditation : CreateTagged
    {

        [Display(Name = "Inspection ID")]
        public int inspID { get; set; }

        [Display(Name = "School ID")]
        public string schNo { get; set; }

        [Display(Name = "School Name")]
        public string schName { get; set; }

        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }

        [Display(Name = "Inspected By")]
        public string InspectedBy { get; set; }

        // Since this is actually an updatable view the ID handling strategy 
        // is different. Se CreateTagged base class
        //[Key]
        //[Required(ErrorMessage = "School Accreditation ID is required")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "School Accreditation ID")]
        public int? saID { get; set; }

        [Display(Name = "L1")]
        public int? L1 { get; set; }

        [Display(Name = "L2")]
        public int? L2 { get; set; }

        [Display(Name = "L3")]
        public int? L3 { get; set; }

        [Display(Name = "L4")]
        public int? L4 { get; set; }

        [Display(Name = "T1")]
        public int? T1 { get; set; }

        [Display(Name = "T2")]
        public int? T2 { get; set; }

        [Display(Name = "T3")]
        public int? T3 { get; set; }

        [Display(Name = "T4")]
        public int? T4 { get; set; }

        [Display(Name = "D1")]
        public int? D1 { get; set; }

        [Display(Name = "D2")]
        public int? D2 { get; set; }

        [Display(Name = "D3")]
        public int? D3 { get; set; }

        [Display(Name = "D4")]
        public int? D4 { get; set; }

        [Display(Name = "N1")]
        public int? N1 { get; set; }

        [Display(Name = "N2")]
        public int? N2 { get; set; }

        [Display(Name = "N3")]
        public int? N3 { get; set; }

        [Display(Name = "N4")]
        public int? N4 { get; set; }

        [Display(Name = "F1")]
        public int? F1 { get; set; }

        [Display(Name = "F2")]
        public int? F2 { get; set; }

        [Display(Name = "F3")]
        public int? F3 { get; set; }

        [Display(Name = "F4")]
        public int? F4 { get; set; }

        [Display(Name = "S1")]
        public int? S1 { get; set; }

        [Display(Name = "S2")]
        public int? S2 { get; set; }

        [Display(Name = "LS3")]
        public int? S3 { get; set; }

        [Display(Name = "S4")]
        public int? S4 { get; set; }

        [Display(Name = "CO1")]
        public int? CO1 { get; set; }

        [Display(Name = "CO2")]
        public int? CO2 { get; set; }

        [Display(Name = "LT1")]
        public int? LT1 { get; set; }

        [Display(Name = "LT2")]
        public int? LT2 { get; set; }

        [Display(Name = "LT3")]
        public int? LT3 { get; set; }

        [Display(Name = "LT4")]
        public int? LT4 { get; set; }

        [Display(Name = "Total")]
        public int? T { get; set; }

        [Display(Name = "School Level")]
        public string SchLevel { get; set; }

        [Display(Name = "Inspection Year")]
        public string InspYear { get; set; }

        [Display(Name = "Inspection Type")]
        public string inspsetType { get; set; }

    }

}