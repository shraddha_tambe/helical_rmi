﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using Softwords.DataTools;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace Pineapples.Data
{
    public class SchoolAccreditationFilter : Filter
    {
        public string SAID { get; set; }
        public string School { get; set; }
        public string SchoolNo { get; set; }
        public string InspYear { get; set; }
        public string District { get; set; }
        public string Authority { get; set; }

        public void ApplyUserFilter(ClaimsIdentity identity)
        {
            Claim c = identity.FindFirst(x => x.Type == "filterDistrict");
            if (c != null)
            {
                District = c.Value;
            }
            c = identity.FindFirst(x => x.Type == "filterAuthority");
            if (c != null)
            {
                Authority = c.Value;
            }
            c = identity.FindFirst(x => x.Type == "filterSchoolNo");
            if (c != null)
            {
                SchoolNo = c.Value;
            }

        }
    }
    
    public class SchoolAccreditationTableFilter
    {
        public string row { get; set; }
        public string col { get; set; }
        public SchoolAccreditationFilter filter { get; set; }
    }
}
