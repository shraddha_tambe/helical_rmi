﻿using System.Web;
using System.Collections.Generic;
using System.Web.Optimization;

namespace Pineapples
{
    // by convention this is placed here - a static method on a public class
    // this is usually invoked from global.asax
    // but

    // Softwords.Web.CommonBundleConfig handles the usual things
    public class BundleConfig
    {

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862

        public static void RegisterBundles(BundleCollection bundles)
        {
            // put back into the application level,
            // pending redesign of CommonBundleConfig

            StyleBundle s = new StyleBundle("~/_styles/remotable");
                s.IncludeDirectory("~/styles/remotable", "*.css", true);
            bundles.Add(s);

            bundles.Add(new StyleBundle("~/_styles/vendor")
               .IncludeDirectory("~/styles/vendor", "*.css", true)
              );

            bundles.Add(new StyleBundle("~/_styles/app")
              .IncludeDirectory("~/styles/app", "*.css", true)
              );

            // add a custom orderer to this script bundle to ensure that any copile time dependencies are satisfied
            ScriptBundle b = new ScriptBundle("~/_scripts/remotable");
            b.IncludeDirectory("~/scripts/remotable","*.js", true)
               .Orderer = new ScriptOrderer();
            bundles.Add(b);

            b = new ScriptBundle("~/_scripts/vendor");
            b.IncludeDirectory("~/scripts/vendor", "*.js", true)
               .Orderer = new ScriptOrderer();
            bundles.Add(b);

            bundles.Add(new ScriptBundle("~/_scripts/app")
            .IncludeDirectory("~/scripts/app", "*.js", true)
            );


            //  BundleTable.EnableOptimizations = false;
        }


    }

    public class ScriptOrderer : IBundleOrderer
    {
        // provides a crude sort of the bundled vendor scripts,
        // to ensure that any compile time dependencies are satisfied
        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            List<BundleFile> outlist = new List<BundleFile>();
            List<BundleFile> list0 = new List<BundleFile>();
            List<BundleFile> list1 = new List<BundleFile>();
            List<BundleFile> list2 = new List<BundleFile>();
            List<BundleFile> list3 = new List<BundleFile>();
            List<BundleFile> list4 = new List<BundleFile>();
            foreach (BundleFile b in files)
            {
                switch (b.VirtualFile.Name)
                {
                    case "lodash.compat.js":
                    case "lodash.js":
                    case "jquery.js":
                    case "d3.js":

                        outlist.Add(b);
                        break;
                    case "jquery.signalR.js":
                    case "bootstrap.js":
                    case "crossfilter.js":
                    case "dc.js":
                    case "angular.js":
                        list0.Add(b);
                        break;
                    case "angular-ui-router.js":
                    case "restangular.js":
                        list1.Add(b);
                        break;
                    case "angular-permission.js":
                        // angular-permission has a compile-time dependency on ui-router
                        // angular-permission-ui has a compile time dependency on angular-permission (3.0.x)
                        list2.Add(b);
                        break;

                    default:
                        if (b.VirtualFile.Name.StartsWith("angular") ||
                            b.VirtualFile.Name.StartsWith("ng") ||
                            b.VirtualFile.Name.StartsWith("ui"))
                        {
                            list3.Add(b);
                        }
                        else
                        {
                            list4.Add(b);
                        }
                        break;
                }
            }
            foreach (BundleFile b in list0)
            {
                outlist.Add(b);
            }
            foreach (BundleFile b in list1)
            {
                outlist.Add(b);
            }
            foreach (BundleFile b in list2)
            {
                outlist.Add(b);
            }
            foreach (BundleFile b in list3)
            {
                outlist.Add(b);
            }
            foreach (BundleFile b in list4)
            {
                outlist.Add(b);
            }
            return outlist;
        }
    }
}
