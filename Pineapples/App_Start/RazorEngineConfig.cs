﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using System.Net.Http.Headers;
using System.Net.Http.Formatting;

using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using System.Web.Http.Routing;
using RazorEngine;
using RazorEngine.Templating; // For extension methods.
using RazorEngine.Configuration;


namespace Pineapples
{
    public class RazorEngineConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context">context is the application context in web.config
        /// As with the view engine, we allow searching in the context folder first
        /// Note that the ResolvePathTemplateManager doesn;t check subfolders
        /// </param>
        public static void Config(string context)
        {
            // set up the singleton instance of Engine.Razor
            // use the resolve path template manager
            // this means
            // a) we can find templates in a set of paths
            // b) we cache according to the file name, (and the requested model)
            const string emailRoot = @"~/emailTemplates";

            string emailRootBase = System.Web.Hosting.HostingEnvironment.MapPath(emailRoot);  // the base pat
            var paths = new[] { emailRootBase };
            if (context != string.Empty)
            {
                string contextPath = emailRootBase + @"\" + context;
                paths = new[] { contextPath, emailRootBase };
            }
            var templateManager = new ResolvePathTemplateManager(paths);
            var config = new TemplateServiceConfiguration()
            {
                TemplateManager = templateManager
            };
            // configure the singleton like this
            Engine.Razor = RazorEngineService.Create(config);
        }
    }
}