﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
    public class BookController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        [Authorize]
        public ActionResult Searcher(string version)
        {
            return View();
        }

        [LayoutInjector("EditPageLayout")]
        public ActionResult Item()
        {
            // pass through the data needed for the model
            ViewBag.EditPermission = PineapplesPermissionAttribute.Name(PermissionTopicEnum.School, PermissionAccess.Write);
            return View();
        }

        public ActionResult PagedList()
        {
            return View();
        }

        public ActionResult PagedListEditable()
        {
            return View();
        }

        #region Book-centric components
        public ActionResult SearcherComponent()
        {
            return View();
        }
        #endregion
    }
}