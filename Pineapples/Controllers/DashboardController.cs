﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
    public class DashboardController : Softwords.Web.mvcControllers.mvcControllerBase
    {


        // loosely couple the names of dashboards and components so new ones can be added
        // without changng the controller
        // dashboards go in the 'Dashboard' subfolder
        [Route("dashboard/dashboard/{dashboardName}")]
        public ActionResult Dashboard(string dashboardName)
        {
            return View(string.Format("dashboard/{0}", dashboardName), "Dashboard");
        }

        // children go in the 'component' subfolder
        [Route("dashboard/component/{componentName}")]
        public ActionResult Component(string componentName)
        {
            return View(string.Format("component/{0}", componentName), "DashboardComponent");
        }

        // render the options editor
        [Route("dashboard/options/{optionsFormat}")]
        public ActionResult Options(string optionsFormat)
        {
            return View(string.Format("options/{0}", optionsFormat));
        }

        // render a widget
        [Route("dashboard/widget/{widgetName}")]
        public ActionResult Widget(string widgetName)
        {
            return View(string.Format("widget/{0}", widgetName));
        }
    }
}