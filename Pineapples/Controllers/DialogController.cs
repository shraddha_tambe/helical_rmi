﻿using System.Web.Mvc;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
    public class DialogController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        // GET: Data

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult Conflict()
        {
            return View("conflictResponse");
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult Validation()
        {
            return View("validationResponse");
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult DataError()
        {
            return View("dataErrorResponse");
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult DataAddNew()
        {
            return View("dataAddNewResponse");
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult DataNotFound()
        {
            return View("dataNotFoundResponse");
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult DataDuplicate()
        {
            return View("dataDuplicateResponse");
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult PermissionError()
        {
            return View("dataNoPermissionResponse");
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult ServerError()
        {
            return View("serverErrorResponse");
        }

        [LayoutInjector("MaterialDialogLayout")]
        public ActionResult CodePicker()
        {
          return View("codePicker");
        }
  }
}