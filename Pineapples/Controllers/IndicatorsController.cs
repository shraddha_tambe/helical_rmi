﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Pineapples.Models;
using Softwords.Web.mvcControllers;

namespace Pineapples.mvcControllers
{
    public class IndicatorsController : Softwords.Web.mvcControllers.mvcControllerBase
    {
        // GET: Indicators

        [Route("indicators/{format}")]
        public ActionResult Indicators(string format)
        {
            string viewname = "Indicators_" + format;
            return View(viewname);
        }

        [LayoutInjector("MaterialDialogLayout")]
        [Route("indicators/info/{indicator}")]
        public ActionResult IndicatorsInfo(string indicator)
        {
            return View(indicator);
        }

        // various drill downs into Indicators data
        [Route("indicators/drill/{dataitem}")]
        public ActionResult Drill(string dataitem)
        {
          string viewname = "drill_" + dataitem;
          return View(viewname);
        }
        [Route("indicators/admin")]
        public ActionResult Admin()
        {
            
            return View();
        }
    }
}