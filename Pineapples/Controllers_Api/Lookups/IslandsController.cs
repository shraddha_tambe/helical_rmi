﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Security.Claims;
using Pineapples.Data.Models;
using System.Data.Entity;


namespace Pineapples.Controllers
{
    [RoutePrefix("api/islands")]
    public class IslandsController : TableMaintenanceController<Island, string>
    {
        public IslandsController(DataLayer.IDSFactory factory) : base(factory) { }

    }
}
