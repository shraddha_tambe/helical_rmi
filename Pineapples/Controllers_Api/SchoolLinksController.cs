﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using System.Threading.Tasks;
using Softwords.Web;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/schoollinks")]
    public class SchoolLinksController : PineapplesApiController
    {
        public SchoolLinksController(DataLayer.IDSFactory factory) : base(factory) { }

        #region SchoolLink methods


        [HttpPost]
        [Route(@"")]
        [Route(@"{linkID:int}")]
        //[PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
        public HttpResponseMessage Create(SchoolLinkBinder binder)
        {
            try
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created,
                    Ds.Create(binder, (ClaimsIdentity)User.Identity));
                return response;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpPut]
        [Route(@"{linkID:int}")]
        //[PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
        public object Update(SchoolLinkBinder binder)
        {
            try
            {
                return Ds.Update(binder, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }


        }

        [HttpGet]
        [Route(@"{linkID:int}")]
        //[PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
        public object Read(int linkID)
        {
            return Ds.Read(linkID);
        }

        [HttpDelete]
        [Route(@"{linkID:int}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
        public object Delete(int linkID)
        {
            // its CreateTagged (because its a view) so we can't just Find here...
            Pineapples.Data.Models.SchoolLink tl = Factory.Context.SchoolLinks.FirstOrDefault( e => e.lnkID == linkID);
            if (tl == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            string clientVersion = null;
            if (Request.Headers.IfMatch != null)
            {
                clientVersion = Request.Headers.IfMatch.ToString();
                {
                    if (Convert.ToBase64String(tl.pRowversion.ToArray()) != clientVersion)
                    {
                        // optimistic concurrency error
                        // we return the most recent version of the record

                        throw ConcurrencyException.Make("pRowversion", tl );
                    }
                }
            }
                Providers.FileDB.Remove(tl.docID.ToString());
                Factory.Context.SchoolLinks.Remove(tl);
                Factory.Context.SaveChanges();
                // TO DO
                // Ds.Delete(linkID, (ClaimsIdentity)User.Identity);
                return tl;
            }
        #endregion

            #region SchoolLink Collection methods
        [HttpPost]
        [ActionName("filter")]
        public object Filter(SchoolLinkFilter fltr)
        {
            return Ds.Filter(fltr);
        }

        /// <summary>
        /// Upload a file, and link to a school. The file can be anything - a photo, image, document, pdf spreadsheet etc
        /// If it is an image type, it will be rendered as such in the web client.
        /// The descriptive info about the file is passed as Json, in the form field called model.
        /// </summary>
        /// <returns>the document record</returns>
        [HttpPost]
        [Route(@"upload")]
        public async Task<Softwords.DataTools.IDataResult> Upload()
        {
            Softwords.DataTools.IDataResult result = null;

            if (!Request.Content.IsMimeMultipartContent())
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartFormDataStreamProvider(AppDataPath);

            var files = await Request.Content.ReadAsMultipartAsync(provider);

            foreach (MultipartFileData fd in files.FileData)
            {
                Guid g;

                string filename = fd.Headers.ContentDisposition.FileName;
                filename = filename.Replace("\"", "");          // tends to com back with quotes around
                string ext = System.IO.Path.GetExtension(filename);

                using (System.IO.FileStream fstrm = new System.IO.FileStream(fd.LocalFileName, System.IO.FileMode.Open))
                {
                    g = Providers.FileDB.Store(fstrm, ext);
                }
                // delete the local file, or else app_data is strewn with BodyParts! 'using' ensures the stream is disposed
                System.IO.File.Delete(fd.LocalFileName);

                // add the School Link record in Pineapples
                //
                // rather than passing the supporting data in other form fields, a single field is populated with the Json version
                // of these values. The client makes this using Json.Stringify(). Thus we don't need any new techniques to get at the 
                // url-form-encoded data - just deserialize the one piece of Json
                SchoolLinkBinder binder = Newtonsoft.Json.JsonConvert.DeserializeObject<SchoolLinkBinder>(provider.FormData["model"]);

                string SchoolNo = (string)binder.definedProps["schNo"];
                if (SchoolNo == null)
                {
                    throw RecordNotFoundException.Make(SchoolNo);
                }

                // the ID fields needs to be there, so it will be populated from the binder fromDB call
                binder.definedProps.Add("docID", g);
                binder.ID = null;
                try
                {
                    result = Ds.Create(binder, (ClaimsIdentity)User.Identity);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    // return the object as the body
                    var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                    throw new HttpResponseException(resp);
                }
            }
            return result;      // only the last one? but we expect only one at a time.....?
        }
        #endregion

        private IDSSchoolLink Ds
        {
            get { return Factory.SchoolLink(); }
        }
    }
}
