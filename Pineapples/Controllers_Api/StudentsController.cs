﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;

using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Data;
using Pineapples.Models;
using Softwords.Web;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/students")]
    public class StudentsController : PineapplesApiController
    {
        public StudentsController(DataLayer.IDSFactory factory) : base(factory) { }

        #region Collection methods
        [HttpPost]
        [ActionName("filter")]
        public object Filter(StudentFilter fltr)
        {
            return Ds.Filter(fltr);
        }

        //[HttpPost]
        //[ActionName("table")]
        //public object Table(StudentFilter fltr)
        //{
        //    return Factory.Student().Table(fltr);
        //}

        [HttpPost]
        [ActionName("table")]
        public object Table(Models.StudentTableBindingModel model)
        {
            StudentFilter fltr = model.filter;
            // fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Table(model.row, model.col, fltr);
        }

        [HttpPost]
        [ActionName("geo")]
        public object GeoData(StudentFilter fltr)
        {
            return Factory.Student().Geo("POINT", fltr); //.ResultSet;
        }
        #endregion

        #region Student CRUD methods
        [HttpPost]
        [Route(@"")]
        // [Route(@"{studentID}")] this route can never make sense because studentID is an identity and so
        // cannot be specified by the client
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.WriteX)]
        public object Create(StudentBinder student)
        {
            try
            {
                return Ds.Create(student, (ClaimsIdentity)User.Identity);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpGet]
        [Route(@"{stuID:guid}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
        public object Read(Guid? stuID)
        {
            return Ds.Read(stuID); //, ((ClaimsIdentity)User.Identity).hasPermission((int)PermissionTopicEnum.School, PermissionAccess.ReadX));
        }        

        [HttpPut]
        [Route(@"{stuID:guid}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
        public object Update(StudentBinder student)
        {
            try
            {
                return Ds.Update(student, (ClaimsIdentity)User.Identity).definedProps;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpDelete]
        [Route(@"{stuID:guid}")]
        [PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Write)]
        public object Delete([FromBody] StudentBinder student)
        {
            // TO DO
            return student.definedProps;
        }
        #endregion

        private IDSStudent Ds
        {
            get { return Factory.Student(); }
        }
    }
}
