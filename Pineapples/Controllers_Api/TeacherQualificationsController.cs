﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Pineapples.Data;
using Pineapples.Data.Models;
using DataLayer = Pineapples.Data.DataLayer;
using System.Security.Claims;
using Pineapples.Models;
using System.Threading.Tasks;
using Softwords.Web;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/teacherqualifications")]
    public class TeacherQualificationsController : TableMaintenanceController<TeacherQualification, int>
    {
        public TeacherQualificationsController(DataLayer.IDSFactory factory) : base(factory) { }
    }
}
