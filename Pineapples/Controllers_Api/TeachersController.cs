﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Security.Claims;

using DataLayer = Pineapples.Data.DataLayer;
using Pineapples.Models;
using Pineapples.Data;

using Softwords.Web;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/teachers")]
    public class TeachersController : PineapplesApiController
    {
        public TeachersController(DataLayer.IDSFactory factory) : base(factory) { }

        [HttpPost]
        [ActionName("filter")]
        public object Filter(TeacherFilter fltr)
        {
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Filter(fltr);
        }

        //[HttpPost]
        //[ActionName("table")]
        //public object Table(TeacherFilter fltr)
        //{
        //    return Factory.Teacher().Table(fltr);
        //}

        [HttpPost]
        [ActionName("table")]
        public object Table(Models.TeacherTableBindingModel model)
        {
            TeacherFilter fltr = model.filter;
            fltr.ApplyUserFilter(User.Identity as ClaimsIdentity);
            return Ds.Table(model.row, model.col, fltr);
        }

        [HttpPost]
        [ActionName("geo")]
        public object GeoData(TeacherFilter fltr)
        {
            return Factory.Teacher().Geo("POINT", fltr); //.ResultSet;
        }

        #region Teacher CRUD
        [HttpPost]
        [Route(@"")]
        // [Route(@"{teacherID}")] this route can never make sense becuase teacherID is an identity and so
        // cannot be specified by the client
        [PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.WriteX)]
        public object Create(TeacherBinder teacher)
        {
            try
            {
                return Ds.Create(teacher, (ClaimsIdentity)User.Identity);
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpGet]
        [Route(@"{teacherID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.Read)]
        public object Read(int teacherID)
        {
            AccessControl(teacherID);
            return Ds.Read(teacherID, ((ClaimsIdentity)User.Identity).hasPermission((int)PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.ReadX));
        }

        [HttpPut]
        [Route(@"{teacherID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.Write)]
        public object Update(TeacherBinder teacher)
        {
            AccessControl((int)teacher.ID);
            try
            {
                return Ds.Update(teacher, (ClaimsIdentity)User.Identity).definedProps;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // return the object as the body
                var resp = Request.CreateResponse(HttpStatusCode.InternalServerError, ex);
                throw new HttpResponseException(resp);
            }
        }

        [HttpDelete]
        [Route(@"{teacherID:int}")]
        [PineapplesPermission(PermissionTopicEnum.Teacher, Softwords.Web.PermissionAccess.Write)]
        public object Delete([FromBody] TeacherBinder teacher)
        {
            // TO DO
            AccessControl((int)teacher.ID);
            return teacher.definedProps;
        }
        #endregion

        #region AccessControl

        private void AccessControl(int teacherID)
        {
            Ds.AccessControl(teacherID, (ClaimsIdentity)User.Identity);
        }
        #endregion
        private IDSTeacher Ds
        {
            get { return Factory.Teacher(); }
        }
    }
}
