﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http;
using DataLayer = Pineapples.Data.DataLayer;
using Softwords.DataTools;
using System.Xml.Linq;
using System.Xml.XPath;
using System.IO;
using Pineapples.Providers;
using System.Security.Claims;

using Pineapples.Models;
using Softwords.Web;
using Pineapples.Data;

namespace Pineapples.Controllers
{
    [RoutePrefix("api/warehouse")]
    public class WarehouseController : PineapplesApiController
    {
        public WarehouseController(DataLayer.IDSFactory factory) : base(factory) { }


        [HttpGet]
        [Route(@"enrolbyschool/{schoolNo}")]
        //[PineapplesPermission(PermissionTopicEnum.School, Softwords.Web.PermissionAccess.Read)]
        public object GetSchool(string schoolNo)
        {
          //AccessControl(schoolNo);
          IDataResult ds = Ds.TableEnrolBySchool(schoolNo);
          return Deflate((System.Data.DataTable)ds.ResultSet);

        }

        [HttpGet]
        [Route("tableenrol")]
        public HttpResponseMessage TableEnrol()
        {
            IDataResult ds = Ds.TableEnrol();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("districtenrol")]
        public HttpResponseMessage DistrictEnrol()
        {
          IDataResult ds = Ds.TableDistrictEnrol();
          return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("schoolflowrates")]
        public HttpResponseMessage SchoolFlowRates()
        {
            IDataResult ds = Ds.SchoolFlowRates();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("schoolteachercount")]
        public HttpResponseMessage SchoolTeacherCount()
        {
            IDataResult ds = Ds.SchoolTeacherCount();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("schoolteacherpupilratio")]
        public HttpResponseMessage SchoolTeacherPupilRatio()
        {
            IDataResult ds = Ds.SchoolTeacherPupilRatio();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        //[HttpGet]
        //[Route("examsschoolresults")]
        //public HttpResponseMessage ExamsAllResults()
        //{
        //    IDataResult ds = Ds.ExamSchoolResults();
        //    return Deflate((System.Data.DataTable)ds.ResultSet);
        //}

        [HttpGet]
        [Route("examsschoolresults/{schoolNo}")]
        public HttpResponseMessage ExamsSchoolResults(string schoolNo)
        {
            IDataResult ds = Ds.ExamSchoolResults(schoolNo);
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("teachercount")]
        public HttpResponseMessage TeacherCount()
        {
            IDataResult ds = Ds.TeacherCount();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("teacherqual")]
        public HttpResponseMessage TeacherQual()
        {
            IDataResult ds = Ds.TeacherQual();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("teacherpupilratio")]
        public HttpResponseMessage TeacherPupilRatio()
        {
            IDataResult ds = Ds.TeacherPupilRatio();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("classleveler")]
        public HttpResponseMessage ClassLevelER()
        {
            IDataResult ds = Ds.ClassLevelER();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("edleveler")]
        public HttpResponseMessage EdLevelER()
        {
            IDataResult ds = Ds.EdLevelER();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("flowrates")]
        public HttpResponseMessage FlowRates()
        {
            IDataResult ds = Ds.FlowRates();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("edlevelage")]
        public HttpResponseMessage EdLevelAge()
        {
            IDataResult ds = Ds.EdLevelAge();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }

        [HttpGet]
        [Route("examresults")]
        public HttpResponseMessage ExamResults()
        {
            IDataResult ds = Ds.ExamResults();
            return Deflate((System.Data.DataTable)ds.ResultSet);
        }
        /// <summary>
        /// In this controller, use the 'deflating' json serializer since these arae likely to be big
        /// </summary>
        /// <param name="dt">the datatable to serialize</param>
        /// <returns></returns>
        private HttpResponseMessage Deflate( System.Data.DataTable dt)
        {
            var formatter = new System.Net.Http.Formatting.JsonMediaTypeFormatter();

            var json = formatter.SerializerSettings;

            json.DateFormatHandling = Newtonsoft.Json.DateFormatHandling.MicrosoftDateFormat;
            json.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Utc;
            json.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            json.Formatting = Newtonsoft.Json.Formatting.None;
            json.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
           
            var stream = Softwords.Web.Json.DatasetSerializer.toStream(dt);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentType =
                new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            return response;

        }
        private IDSWarehouse Ds
        {
            get { return Factory.Warehouse(); }
        }
    }
}
