﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using ImageProcessor.Web.Services;
using ImageProcessor.Web.Helpers;
using System.IO;

namespace Pineapples.Providers
{
    public class ImageService: IImageService
    {
        public bool IsFileLocalService => false;
        public Dictionary<string, string> Settings { get; set; }

        /// <summary>
        /// Gets or sets the white list of <see cref="System.Uri"/>.
        /// </summary>
        public Uri[] WhiteList { get; set; }

        public string Prefix { get; set; }
        public bool IsValidRequest(string path)
        {
            string[] parts = path.Split('/', '\\');
            path = parts[0];
            // path must either be a guid, or a guid with an extension <guid>.
            if (path.IndexOf(".") == 36 )
            {
                // if there is an extension specified, be sure it is an image type
                if (!ImageHelpers.IsValidImageExtension(path))
                {
                    return false;
                }
                path = path.Substring(0, 36);
            }
            Guid gOut = new Guid();
            return Guid.TryParse(path, out gOut);    // base.IsValidRequest(trimPath(path));
        }

        public async Task<byte[]> GetImage(object id)
        {
            byte[] buffer;
            string[] parts = ((string)id).Split('/', '\\');
            id = parts[0];
            string path = FileDB.GetFilePath(id.ToString());
            // Check to see if the file exists.
            if (!File.Exists(path))
            {
                throw new HttpException((int)System.Net.HttpStatusCode.NotFound, $"No image exists at {path}");
            }

            using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, true))
            {
                buffer = new byte[file.Length];
                await file.ReadAsync(buffer, 0, (int)file.Length);
            }

            return buffer;
        }

    }
}