﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExamSearcher.aspx.cs" Inherits="Pineapples.Views.ExamSearcher" %>

<div class="container searcher">
    <%--<div class="col-md-3">
        <div class="form-group">
            <label for="srchSchoolID">School ID</label>
            <input id="srchSchoolID" type="text" class="form-control" ng-model="vm.params.SchoolID" />
        </div>
    </div>--%>
    <div class="col-md-3">
        <div class="form-group">
            <label for="srchExamID">Exam ID</label>
            <input id="srchExamID" type="text" class="form-control" ng-model="vm.params.ExamID" />
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="srchExamName">Exam Name</label>
            <lookup-selector name="exName" lkp="examTypes" ng-model="vm.params.ExamCode" 
                             ng-disabled="vm.findConfig.isLocked('ExamName')"/>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label for="srchExamYear">Exam Year</label>
            <input id="srchExamYear" type="text" class="form-control" ng-model="vm.params.ExamYear" />
        </div>
    </div>
    <%--<div class="col-md-2">
        <div class="form-group">
            <label for="srchExamDate">Exam Date</label>
            <md-datepicker ng-model="vm.params.ExamDate" md-placeholder="Enter date"></md-datepicker>
        </div>
    </div>--%>

    <div class="col-xs-1">

        <div class="form-group">
            <button class="btn btn-default findNow" id="findNow" ng-click="vm.FindNow()">
                Find</button>
        </div>
        <div class="form-group">
            <button id="Reset" class="btn clearSearch" ng-click="vm.theFilter.Reset()">
                Clear</button>

        </div>
    </div>


</div>

