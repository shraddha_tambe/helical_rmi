﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeacherSearcher.aspx.cs" Inherits="Pineapples.Views.TeacherSearcher" %>
<div class="container searcher" >

        <div class="col-md-3">
            <div class="form-group">

                <label for="srchSurname">Family Name</label>
                <input id="srchSurname" type="text" class="form-control" ng-model="vm.params.Surname" />
            </div>

        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="srchPayroll">Payroll</label>
                <input id="srchPayroll" type="text" class="form-control" ng-model="vm.params.PayrollNo" />
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="srchAtSchool">At School</label>
                <%--<input id="srchAtSchool" type="text" class="form-control" ng-model="vm.theFilter.params.AtSchool" />--%>
                <select class="form-control" id="srchAtSchool"
                      ng-model="vm.params.AtSchool"
                      ng-options="r.C as r.N for r in vm.lookups.cache.schoolNames">
                  <option value=""></option>
              </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="srchInYear">In Year</label>
                <input id="srchInYear" type="text" class="form-control" ng-model="vm.params.InYear" />
            </div>
        </div>

        <div class="col-xs-1">

            <div class="form-group">
                <button class="btn btn-default findNow" id="findNow" ng-click="vm.FindNow()" >
                    Find</button>
            </div>
            <div class="form-group">
                <button id="Reset" class="btn clearSearch" ng-click="vm.theFilter.Reset()" >
                    Clear</button>

            </div>
        </div>


</div>

