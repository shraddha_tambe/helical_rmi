SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 06 2010
-- Description:	Convert decimal lat/long into degree/min/sec,
-- =============================================
CREATE FUNCTION [common].[LatLongfromDecimal]
(
	-- Add the parameters for the function here
	@latlongD as decimal(20,16)
	, @LatOrLong int = null		-- if 0, Lat and format with NS, if 1 Long format EW
)
RETURNS nvarchar(40)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result nvarchar(40)

	declare @degrees int

	declare @minutes int

	declare @seconds decimal(8,6)

	declare @Orientation nvarchar(2)

	declare @Negative bit

	Select @Negative =case when @LatLongD < 0 then 1 else 0 end
		, @LatLongD = case when @LatLongD < 0 then @LatLongD * -1  else @LatLongD end
	select @Orientation = ''


	Select @degrees = round(@latLongD ,0,1)


	select @LatLongD = @LatLongD-@degrees

	select @minutes = round(@LatLongD*60,0,1)

	Select @LatLongD = @LAtLongD * 60 - @minutes

	select @seconds = round(@LatLongD * 60 ,5)
	if @LatOrLong = 0  begin
		Select @Orientation = case when @Negative = 1 then ' S' else ' N' end

	end

	if @LatOrLong = 1  begin
		Select @Orientation = case when  @Negative = 1  then ' E' else ' W' end

	end

	iF @LatOrLong is null
		Select @degrees = case when  @Negative = 1  then @degrees * -1  else @degrees end
	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = cast(@Degrees as nvarchar(4)) + '° '
						+ cast(@Minutes as nvarchar(2)) + ''' '
						+ cast(@seconds as nvarchar(10)) + '"'
						+ @Orientation

	-- Return the result of the function
	RETURN @Result

END
GO

