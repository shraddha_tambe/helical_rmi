SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2009
-- Description:	wrapper for name functions to use in calculated fields on TeacherIdentity
-- =============================================
CREATE FUNCTION [dbo].[TeacherName]
(
	-- Add the parameters for the function here
	@NameType int
	, @NAmePrefix nvarchar(20)
	, @FirstName nvarchar(50)
	, @MiddleNames nvarchar(50)
	, @LastName nvarchar(50)
	, @NAmeSuffix nvarchar(50)

)
RETURNS nvarchar(400)
AS
BEGIN

	If (@NameType = 1)	-- short name
		return dbo.ShortName(@FirstName, @LastName)

	If (@NameType = 2)	-- full name
		return dbo.fullName(@NamePrefix, @FirstName, @LastName, @NAmeSuffix)


	If (@NameType = 3)	-- long name
		return dbo.longName(@NamePrefix, @FirstName, @MiddleNAmes, @LastName, @NAmeSuffix)

return null
END
GO
GRANT EXECUTE ON [dbo].[TeacherName] TO [public] AS [dbo]
GO

