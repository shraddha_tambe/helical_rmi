SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 4 2010
-- Description:	Returns the set of teacher roles applicable to a school type
-- =============================================
CREATE FUNCTION [common].[SchoolTypeTeacherRoles]
(
	-- Add the parameters for the function here
	@SchoolType nvarchar(10)= null
)
RETURNS TABLE
AS
RETURN
(
	Select T.*
FROM TRTeacherRole T

WHERE T.codeCode in
	(
		SELECT DISTINCT R.codeCode
		FROM metaSchoolTypeLevelMap SLM
			inner join lkpLevels L
				ON SLM.tlmLevel = L.codeCode
			INNER JOIN lkpTeacherRole R
				ON L.secCode = R.secCode
		WHERE (stCode = @SchoolType or @SchoolType is null)

	)
OR T.secCode is null
)
GO

