SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 10 2007
-- Description:	Calculate best survey for enrolment data for a school in a year
-- =============================================
CREATE FUNCTION [dbo].[tfnESTIMATE_BestSurveyEnrolments]
(

)
RETURNS
 TABLE

AS
RETURN

(

--	Select @FillForward = dbo.sysParamInt(N'EST_ENROL_FILL_FORWARD')
--	select @FillBack = dbo.sysParamInt(N'EST_ENROL_FILL_BACKWARD')

with RankedYears
as
(
select L.*
, D.svyYear as enrolYear
, D.ssqLevel as enrolssqLevel
, L.svyYear - D.svyYear as Offset
, D.enrolssID
, D.ssEnrol ssEnrol
, D.ssSchType ssSchType
, case when d.svyYear > L.svyYear then 1000 else 0 end + abs(d.svyYear - L.svyYear) ForwardBack
, row_number() OVER(PARTITION BY L.schNo, L.svyYear ORDER BY
case when d.svyYear > L.svyYear then 1000 else 0 end + abs(d.svyYear - L.svyYear) )
yearRank

 from
(Select paramInt from sysParams WHERE paramName = 'EST_ENROL_FILL_FORWARD' ) as  F
CROSS JOIN
(Select paramInt from sysParams WHERE paramName = 'EST_ENROL_FILL_BACKWARD' ) as B
CROSS JOIN
 dbo.SchoolLifeYears AS L
LEFT OUTER JOIN
dbo.schoolYearHasDataEnrolments AS D


	ON
		L.schNo = D.schNo
	AND		D.svyYear BETWEEN
							L.svyYear - F.ParamInt
								AND L.svyYear + B.ParamInt

)
Select schNo as SchNo
, svyYear as LifeYear
, actualssID
, enrolssID as bestssID
, enrolYear as bestYear
, [Offset]
, enrolssqLevel as bestssqLevel
, QI.ssqLevel as ActualssqLevel
, isnull(actualssID, enrolssID) SurveyDimensionssID
, case when Offset = 0 then 0 else 1 end Estimate
, ssEnrol as bestEnrol
, ssSchType as bestSchType
from RankedYears
LEFT OUTER JOIN dbo.tfnQualityIssues('Pupils','Enrolment') QI			-- very ancient bug enrol=>enrokment fixed 13 06 2010
		ON RankedYears.ActualssID = QI.ssID
WHERE yearRank = 1

)
GO
GRANT SELECT ON [dbo].[tfnESTIMATE_BestSurveyEnrolments] TO [public] AS [dbo]
GO

