SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- =============================================
CREATE FUNCTION [dbo].[tfnQualityBlock]
(
	-- Add the parameters for the function here
	@Item nvarchar(50),
	@SubItem nvarchar(50)
)
RETURNS TABLE
AS
RETURN
(
	-- Add the SELECT statement with parameter references here
	SELECT ssID from SchoolSurveyQuality WHERE ssqLevel = 2
AND ssqDataItem = @Item
AND (ssqSubItem = @subItem OR @SubItem is null)
)
GO
GRANT SELECT ON [dbo].[tfnQualityBlock] TO [public] AS [dbo]
GO

