SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 4 2010
-- Description:	Return the last survey for the specified teacher
-- =============================================
CREATE FUNCTION [pTeacherRead].[TeacherLastSurvey]
(
	-- Add the parameters for the function here
	@TeacherID int
)
RETURNS TABLE
AS
RETURN
(
	Select TOP 1
	SS.SchNo, svyYear, ssSchType, schName
	, TS.*
	from SchoolSurvey SS
	INNER JOIN Schools S
		ON SS.schNo = S.schNo
	INNER JOIN TEacherSurvey TS
		ON SS.ssID = TS.ssID
	WHERE TS.tID = @TeacherID
	ORDER BY svyYear DESC

)
GO

