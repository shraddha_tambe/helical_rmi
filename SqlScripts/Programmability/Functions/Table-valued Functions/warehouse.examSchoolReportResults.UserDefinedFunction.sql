SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 19 10 2017
-- Description:	For a single benchmark in a given exam, compare school, state and nation
-- =============================================
CREATE FUNCTION [warehouse].[examSchoolReportResults]
(
	@examCode nvarchar(10)
	, @examYear int
	, @SchoolNo nvarchar(50)
	, @BenchmarkCode nvarchar(30)
)
RETURNS
@result TABLE
(
	row nvarchar(50)
	, Gender nvarchar(1)
	, benchmarkCode nvarchar(20)
	, Candidates int
	, [0] int
	, [1] int
	, [2] int
	, [3] int
	, [4] int
	, [5] int
	, [6] int
	, [7] int
	, [8] int
	, [9] int
	, [0P] float
	, [1P] float
	, [2P] float
	, [3P] float
	, [4P] float
	, [5P] float
	, [6P] float
	, [7P] float
	, [8P] float
	, [9P] float

)
AS
BEGIN
	INSERT INTO @result
	Select schNo
	, Gender
	, benchmarkCode
	, Candidates
	, [0]
	, [1]
	, [2]
	, [3]
	, [4]
	, [5]
	, [6]
	, [7]
	, [8]
	, [9]
	, convert(float, [0]) / Candidates
	, convert(float, [1]) / Candidates
	, convert(float, [2]) / Candidates
	, convert(float, [3]) / Candidates
	, convert(float, [4]) / Candidates
	, convert(float, [5]) / Candidates
	, convert(float, [6]) / Candidates
	, convert(float, [7]) / Candidates
	, convert(float, [8]) / Candidates
	, convert(float, [9]) / Candidates
	From warehouse.ExamSchoolResultsX
	WHERE examCode = @examCode
	AND examYear = @examYear
	AND schNo = @schoolNo
	AND benchmarkCode = @benchmarkCode

	-- insert the state row
		INSERT INTO @result
	Select [State]
	, Gender
	, benchmarkCode
	, Candidates
	, [0]
	, [1]
	, [2]
	, [3]
	, [4]
	, [5]
	, [6]
	, [7]
	, [8]
	, [9]
	, convert(float, [0]) / Candidates
	, convert(float, [1]) / Candidates
	, convert(float, [2]) / Candidates
	, convert(float, [3]) / Candidates
	, convert(float, [4]) / Candidates
	, convert(float, [5]) / Candidates
	, convert(float, [6]) / Candidates
	, convert(float, [7]) / Candidates
	, convert(float, [8]) / Candidates
	, convert(float, [9]) / Candidates
	From warehouse.ExamStateResultsX
		INNER JOIN Islands I
			on stateID = I.iGroup
		INNER JOIN Schools S
			ON S.iCode = I.iCode
	WHERE examCode = @examCode
	AND examYear = @examYear
	AND S.schNo = @schoolNo
	AND benchmarkCode = @benchmarkCode

	-- finally national total
	INSERT INTO @result
	Select 'Nation'
	, Gender
	, benchmarkCode
	, Candidates
	, [0]
	, [1]
	, [2]
	, [3]
	, [4]
	, [5]
	, [6]
	, [7]
	, [8]
	, [9]
	, convert(float, [0]) / Candidates
	, convert(float, [1]) / Candidates
	, convert(float, [2]) / Candidates
	, convert(float, [3]) / Candidates
	, convert(float, [4]) / Candidates
	, convert(float, [5]) / Candidates
	, convert(float, [6]) / Candidates
	, convert(float, [7]) / Candidates
	, convert(float, [8]) / Candidates
	, convert(float, [9]) / Candidates
	From warehouse.ExamNationResultsX
	WHERE examCode = @examCode
	AND examYear = @examYear
	AND benchmarkCode = @benchmarkCode
	RETURN
END
GO

