SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 28 10 2009
-- Description:	Send positions to Aurion, with some batching controls
-- =============================================
CREATE PROCEDURE [Aurion].[sendPositions]
	-- Add the parameters for the stored procedure here
	@SendAll int = 0
	, @BatchID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- if not SendAll, export only those not yet sent

	SET NOCOUNT ON;

	begin try
		if (@SendAll = 0 and @BatchID = 0) begin
			-- we have to allocate a counter
			begin transaction
				exec @BatchID = dbo.getCounter 'BATCH_ID',1,1

				 --stamp the positions to export with this batch ID
----
				UPDATE Establishment
				SET
					estpSendBatch = @BatchID
					, estpSendUser = suser_sname()
					, estpSendDate = getDate()
				WHERE
					estpSendBatch is null

			commit transaction


		end

		Select *
		INTO #tmpTopPos
		FROM
			( Select
			schNo
			, E.estpRoleGRade
			, R. secCode
			, RG.rgSort
			, estpNo
			, RG.rgSalaryPointMedian
			, row_number() OVER (PARTITION BY schNo, secCode ORDER BY rgSort, rgSalaryLevelMax DESC) rowNum
			FROM
				Establishment E
				INNER JOIN RoleGRades RG
					ON E.estpRoleGRade = RG.rgCode
				INNER JOIN lkpTeacherRole R
					ON RG.roleCode = R.codeCode
			) subOrderedPos
		WHERE rowNum = 1


		-- now we can send

		SELECT EX.*
		, #tmpTopPOS.estpNo ReportsTo
		FROM Aurion.ExportPosition EX
			LEFT JOIN #tmpTopPos		-- left join becuase won;t apply to supernums
				ON EX.schNo = #tmpTopPos.schNo
				AND EX.secCode = #tmpTopPos.secCode
		WHERE
			(@SendAll = 1 or estpSendBatch = @BatchID)

		-- for a second recordset, send any supporting org units that may be required

		-- 1 6 2010 -- this depends on the Aurion Model sys param

		declare @AurionModel int	-- we'll preserve support for the deep model (Auth Type/Auth/SchoolType/School/Sector)
								-- while adding support for the shallow model (Auth Type/Auth/Sector)

		declare @OrgLevel int		-- the level in the hierarchy for these units . Determined by the model.
									-- In fact, these units are only in the Hierarchy if Model = 0 (thin)
		declare @TopOrgUnit int
		declare @TopOrgLevel int

		Select @TopOrgUnit = common.SysParamInt('Aurion_TopLevelOrgNumber',0)
		Select @TopOrgLevel = common.SysParamInt('Aurion_TopLevelOrgLevel',4)
		Select @AurionModel = common.SysParamInt('Aurion_Model',0)

		If (@AurionModel = 0)
			-- thin model
			exec Aurion.createAuthoritySectorCodeOrgUnits @SendAll

		If (@AurionModel = 1)
			exec Aurion.createSchoolOrgUnits @SendAll


	end try


--- catch block
	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch

END
GO

