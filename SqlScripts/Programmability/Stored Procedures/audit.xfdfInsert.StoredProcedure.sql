SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [audit].[xfdfInsert]
	-- Add the parameters for the stored procedure here
	@ssID int
	, @desc nvarchar(100)
	, @section nvarchar(100)
	, @num	int = null
	, @data nvarchar(50)= null

	, @row nvarchar(20)= null
	, @warning int = 0
	with execute as 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO audit.xfdfAudit
	(ssID, svyYear, schNo, sxaDate, sxaUser, sxaDesc, sxaSection,
		sxaNum, sxaData, sxaRow,
		sxaErrorFlag
	)
	 Select ssID, svyYear, schNo,
		getdate(), original_login(),
		@desc, @section,
		@num, @data, @row,
		@warning

	FROM SchoolSurvey
	WHERE ssID = @ssID

END
GO
GRANT EXECUTE ON [audit].[xfdfInsert] TO [pSurveyWrite] AS [pineapples]
GO

