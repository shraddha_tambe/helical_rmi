SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 03 2011
-- Description:	raise a structured validation error
-- =============================================
CREATE PROCEDURE [common].[RaiseValidationError]
(
	-- Add the parameters for the function here
	@text nvarchar(1000)
	, @field nvarchar(50) = null
	, @param nvarchar(50) = null
)
AS
BEGIN
	-- Declare the return variable here


	SELECT @text = common.ValidationError(@text, @field, @param)
	-- Return the result of the function
	RAISERROR (@text, 16, 1)

END
GO

