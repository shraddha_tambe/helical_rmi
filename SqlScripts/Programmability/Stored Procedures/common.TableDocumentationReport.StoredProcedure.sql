SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brain Lewis
-- Create date: 11 11 2009
-- Description:	Read cusomtised table documentation
-- =============================================
CREATE PROCEDURE [common].[TableDocumentationReport]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

declare @xprops table
(
tableID int,
tableName sysname,
schemaName sysname,
propName sysname NULL,
propValue nvarchar(4000) NULL
)


	-- make a little temp table

	INSERT INTO @xprops
	Select
		T.object_id
		, T.name
		, S.name
		, P.name
		, convert(nvarchar(4000),P.value)
	from
		sys.tables T
		INNER JOIN sys.schemas S
		ON S.schema_ID = T.schema_ID
		LEFT JOIN sys.extended_properties P
			on P.major_Id = T.object_id
			AND P.Minor_ID = 0  	-- table props, not column props


Select
	tableID, tableName, schemaName
	, MS_Description
	, pSystemTopic
	, pDataAccessUI
	, pDataAccessDB
	, pDiagramName
FROM (
	sELECT TABLEid, TABLEnAME, schemaName, propName, propValue from @xprops XPROPS) U
PIVOT
(
 min(propValue)
FOR propName in ([MS_Description],[pSystemTopic],[pDataAccessUI],[pDataAccessDB],[pDiagramName])
) as PivotTable


END
GO

