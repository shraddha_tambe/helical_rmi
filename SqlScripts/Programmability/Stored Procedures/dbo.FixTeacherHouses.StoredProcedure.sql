SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FixTeacherHouses]
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50)
	, @StartYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @ssID int
	declare @SurveyYEar int
	declare @type nvarchar(100)

	Select @type = 'Staff Housing Permanent'

	DECLARE ssCursor CURSOR FOR
	Select SchoolSurvey.ssID, svyYear
	FROM SchoolSurvey
		INNER JOIN Resources
			ON SchoolSurvey.ssID = Resources.ssID
	WHERE schNo = @SchoolNo
		AND svyYear >= @StartYear
		AND Resources.resName = @type
	ORDER BY svyYear

	OPEN ssCursor

	FETCH NEXT from ssCursor
	INTO @ssID, @SurveyYear

	begin transaction
		WHILE @@FETCH_STATUS = 0 begin
			print @SurveyYear

			exec pSurveyWrite.applyTeacherHouseResourceToBuildings @ssID, @type


			FETCH NEXT from ssCursor
			INTO @ssID, @SurveyYear

		end


	commit transaction

	CLOSE ssCursor
	DEALLOCATE ssCursor
END
GO

