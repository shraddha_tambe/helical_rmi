SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[InsertVocab]
(
@vxTerm nvarchar(255),
@vxObjType nvarchar(10),
@vxObjName nvarchar(200) = null
)
as

begin
	set nocount on;
INSERT INTO trVocab(vxEng,vxObjType,vxObjName)
VALUES (@vxTerm,@vxObjType,@vxObjName)
end
GO
GRANT EXECUTE ON [dbo].[InsertVocab] TO [pAdminAdmin] AS [dbo]
GO

