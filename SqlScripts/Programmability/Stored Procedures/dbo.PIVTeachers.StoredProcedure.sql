SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 10 2013
-- Description:	Teachers pivot table data
-- 28 01 2015: implement filter on survey year and schno
-- =============================================
CREATE PROCEDURE [dbo].[PIVTeachers]
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT *
INTO #ebt
from
dbo.tfnESTIMATE_BestSurveyTeachers(@SurveyYear,@schNo)


SELECT ESTIMATE.LifeYear AS [Survey Year],
ESTIMATE.Estimate,
ESTIMATE.bestYear AS [Teacher Data Year],
ESTIMATE.Offset AS [Teacher Data Age],
ESTIMATE.bestssID AS [Teacher Data Survey ID],
ESTIMATE.ActualssqLevel AS [Survey Year Quality Level],
ESTIMATE.bestssqLevel AS [Data Quality Level],
ESTIMATE.surveyDimensionssID,
GEN.*,
TS.tchsID AS TeacherSurveyID,
1 AS NumTeachers,
------------case when tchSalaryScale is null then
------------	else
------------		case when

--IIf(Not IsNull([tchSalaryScale]),IIf(InStr([tchSalaryScale],"-"),Left([tchSalaryScale],InStr([tchSalaryScale],"-")-1),[tchSalaryScale])) AS SalaryLevel,
null as SalaryLevel,			-- for now
TS.tchSalaryScale AS SalaryPoint,
TS.tchSalary AS Salary,
TS.tchSponsor AS SalaryPaidByCode,
AUTH.Authority AS SalaryPaidBy,
AUTH.AuthorityGroupCode AS SalaryGovCode,
AUTH.AuthorityGroup AS SalaryGov,
TS.tchCitizenship AS Citizenship,
TS.tchYears AS YearsTeaching,
TS.tchYearsSchool AS YearsAtSchool,
TS.tchQual AS HighestQualificationCode,
Q.codeDescription AS HighestQualification,
TS.tchEdQual AS HighestEdQualificationCode,
QE.codeDescription AS HighestEdQualification,
TS.tchSubjectMajor AS MajorSubject,
TS.tchSubjectMinor AS MinorSubject,
TS.tchSubjectMinor2 AS MinorSubject2,
TS.tchSubjectTrained AS SubjectTrainedToTeach,
trTeacherStatus.statusDesc AS Status,
trTeacherRole.codeDescription AS Role,
(ESTIMATE.LifeYear-Year([tDOB])) AS Age,
IsNull(qc.Qualified,'N') AS Qualified,
IsNull(qc.Certified,'N') AS Certified,
Q.codeGroup AS QualGroup,
case Qualified when 'Y' then 1 else 0 end AS NumQualified,
case Certified when 'Y' then 1 else 0 end AS NumCertified,
SECTOR.secDesc AS TeacherSector,
TS.tchSector AS TeacherSectorCode,
ISCED.TSISCED AS TeacherISCEDSub,
ISCEDLevelSub.ilCode AS TeacherISCED,
ISCED.TSGV AS TeacherGV,
TS.tchClass AS LevelTaught,
DimensionLevel.*,
TS.tchInserviceYear AS [Last Inservice Year],
TS.tchInservice AS [Last Inservice],
[LifeYear]-[tchInserviceYear] AS [Years Since Inservice],
--IIf([Years cSince Inservice]<=5,1,0) AS Inservice5Years,
case when [LifeYear]-[tchInserviceYear]<=5 then 1 else 0 end as Inservice5Years,
Estimate.bestYear-[tchYears] AS YearFirstTaught,
--IIf([NumFullTime],1,[tchFTE]) AS FTE,
case tchFullPart when 'P' then tchFTE else 1 end as FTE,

--IIf([NumFullTime],'Y','N') AS FullTime,
case tchFullPart when 'P'  then 'N' else 'Y' end as FullTime,

--IIf([NumFulltime],0,1) AS NumPartTime,
case tchFullPart when 'P' then 1 else 0 end as NumPartTime,
--IIf([tchFullPart]='P',0,1) AS NumFullTime,
case tchFullPart when 'P' then 0 else 1 end as NumFullTime,

TS.tchTAM AS Duties,
TI.tID AS TeacherIdentityID


INTO #tmpPIVTeachers
FROM #ebt ESTIMATE
	INNER JOIN TeacherSurvey TS on ESTIMATE.bestssID = TS.ssID
	INNER JOIN TeacherIdentity TI on TI.tID = TS.tID
	LEFT JOIN DimensionGender GEN on TI.tSex = GEN.GenderCode
	LEFT JOIN DimensionAuthority AUTH on Ts.tchSponsor = AUTH.authorityCode
	LEFT JOIN EducationSectors SECTOR ON TS.tchSector = SECTOR.secCode
	LEFT JOIN trTeacherRole ON TS.tchRole = trTeacherRole.codeCode
	LEFT JOIN TRTeacherQual AS Q ON TS.tchQual = Q.codeCode
	LEFT JOIN TRTeacherQual AS QE ON TS.tchEdQual = QE.codeCode
	LEFT JOIN tchsIDQualifiedCertified AS qc ON TS.tchsID = qc.tchsID
	LEFT JOIN DimensionLevel ON TS.tchClass = DimensionLevel.LevelCode
	LEFT JOIN trTeacherStatus ON TS.tchStatus = trTeacherStatus.statusCode
	LEFT JOIN TeacherSurveyISCED ISCED ON TS.tchsID = ISCED.tchsID
	LEFT JOIN ISCEDLevelSub ON ISCED.TSISCED = ISCEDLevelSub.ilsCode
	LEFT JOIN DimensionRank R on
		ESTIMATE.lifeYear = R.svyYEar and
		ESTIMATE.schNo = R.schNo

-----------------------------
-- output based n dimension columns
-----------------------------

if (@DimensionColumns is null) begin
	SELECT T.*
	FROM #tmpPIVTeachers T
end


if (@DimensionColumns= 'ALL') begin
	SELECT T.*
	, DSS.*
	FROM #tmpPIVTeachers T
	INNER JOIN DimensionSchoolSurvey DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end

if (@DimensionColumns= 'CORE') begin

	SELECT T.*
	, DSS.*
	FROM #tmpPIVTeachers T
	INNER JOIN DimensionSchoolSurveyCore DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'CORESUMM') begin
	SELECT
	sum(NumTeachers) NumTeachers,
	sum(NumQualified) NumQualified,
	sum(NumCertified) NumCertified,
	sum(NumFullTime) NumFullTime,
	sum(NumPartTime) NumPartTime,
	[Survey Year]
	, Estimate
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]

-- specific groupings in this dataset
		, TeacherSector
		, [Status]
		, [Role]
		, Gender
		, Duties
	FROM #tmpPIVTeachers T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	GROUP BY
	[Survey Year]
	, Estimate
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
-- specific groupings in this dataset
		, TeacherSector
		, [Status]
		, [Role]
		, Gender
		, Duties

end

-- variant on core including electorates, region and popgis ids, lat/long/elev
--
if (@DimensionColumns= 'GEO') begin

	SELECT T.*
	, DSS.*
	FROM #tmpPIVTeachers T
	INNER JOIN DimensionSchoolSurveyGeo DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'RANK') begin
-- rebuild the rank table
	exec buildRank


	SELECT T.*
	, DSS.*

      -- from DimensionRank
      ,	DRANK.[School Enrol],
		DRANK.[District Rank],
		DRANK.[District Decile],
		DRANK.[District Quartile],
		DRANK.[Rank],
		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmpPIVTeachers T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear


end


if (@DimensionColumns= 'RANKSUMM') begin
-- rebuild the rank table
	exec buildRank


	SELECT
	sum(NumTeachers) NumTeachers,
	sum(NumQualified) NumQualified,
	sum(NumCertified) NumCertified,
	sum(NumFullTime) NumFullTime,
	sum(NumPartTime) NumPartTime,

	[Survey Year]
	, Estimate
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
-- specific groupings in this dataset
		, TeacherSector
		, [Status]
		, [Role]
		, Gender
		, Duties
-- from DimensionRank
		, sum(DRANK.[School Enrol]) [School Enrol]
		, DRANK.[District Decile],
		DRANK.[District Quartile],

		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmpPIVTeachers T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear

	GROUP BY
	[Survey Year]
		, Estimate
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
-- specific groupings in this dataset
		, TeacherSector
		, [Status]
		, [Role]
		, Gender
		, Duties
-- from DimensionRank

	, DRANK.[District Decile],
	DRANK.[District Quartile],
	--DRANK.[Rank],			don;t use rank when grouping
	DRANK.[Decile],
	DRANK.[Quartile]

end


DROP TABLE #ebt

END
GO

