SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	data for toilets pivot
-- =============================================
CREATE PROCEDURE [dbo].[PIVToiletsSanitation]
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) = 'TOILETS',
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


if (@DataColumns is null) begin
	Select @DataColumns = case when paramText in ('SLEMIS','CSEMIS','PLEMIS') then 'SOMALIA'
							else null end
	from SysParams
	WHERE paramName = 'APP_NAME'
end
print @DataColumns

if (@DataColumns = 'SOMALIA') begin
	print 'SOMALIA'
	if (@Group = 'SANITATION')
		EXEC dbo.PIVToiletsSanitation_San_SO
			@DimensionColumns ,
			@DataColumns,
			@Group,
			@SchNo ,
			@SurveyYear

	else
		EXEC  dbo.PIVToiletsSanitation_Toi_SO
			@DimensionColumns ,
			@DataColumns,
			@Group,
			@SchNo ,
			@SurveyYear

end

if (@DataColumns <> 'SOMALIA') begin
	if (@Group = 'SANITATION')
		EXEC dbo.PIVToiletsSanitation_San
			@DimensionColumns ,
			@DataColumns,
			@Group,
			@SchNo ,
			@SurveyYear

	else
		EXEC  dbo.PIVToiletsSanitation_Toi
			@DimensionColumns ,
			@DataColumns,
			@Group,
			@SchNo ,
			@SurveyYear

end


END
GO

