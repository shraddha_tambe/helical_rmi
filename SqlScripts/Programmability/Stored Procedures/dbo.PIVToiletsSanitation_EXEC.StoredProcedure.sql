SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	Execute toilets pivot - this assumes we have already created
-- #tmpPivColsToilets in the calling procedure
-- the model is:
----- PIV 9 e.g.ToiletsSanitation
		--> calls PIV <data columns> <group>
		--> this family all create the temp table, then call PIV EXec
---- this oversomes the problem that
	-- you cannot have two SELECT INTO #tmp in the same proc
	-- tmp table are in scope downstram but not upstream. This is becuase
	-- they are deleted when the SP that created them exits

-- =============================================
CREATE PROCEDURE [dbo].[PIVToiletsSanitation_EXEC]
	@DimensionColumns nvarchar(20) = null,
	@DataColumns nvarchar(20) = null,
	@Group nvarchar(30) = null,
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Select * into
#ebst
from dbo.tfnESTIMATE_BestSurveyToilets() E
WHERE (E.schNo = @SchNo or @SchNo is null)
	and (E.LifeYear = @SurveyYear or @SurveyYear is null)


SELECT
EBT.LifeYear AS [Survey Year],
EBT.Estimate,
EBT.bestYear AS [Year of Data],
EBT.Offset AS [Age of Data],
EBT.bestssqLevel AS [Data Quality Level],
EBT.ActualssqLevel AS [Survey Year Quality Level],
EBT.surveyDimensionssID,
Toilets.*
INTO #TmpPivToilets
FROM #ebst AS EBT
	INNER JOIN #tmpPivColsToilets Toilets
		ON EBT.bestssID = Toilets.ssID


if (@DimensionColumns is null) begin
	SELECT T.*
	FROM #tmpPivToilets T
end


if (@DimensionColumns= 'ALL') begin
	SELECT T.*
	, DSS.*
	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurvey DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end

if (@DimensionColumns= 'CORE') begin

	SELECT T.*
	, DSS.*
	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyCore DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'CORESUMM') begin
	SELECT

	sum([Number]) Number,

	[Survey Year]
	, Estimate
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]

      -- groupings specific to this dataset
      , [Type]
      , UsedBy

	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	GROUP BY
	[Survey Year]
	, Estimate
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]

      , [Type]
      , UsedBy

end

-- variant on core including electorates, region and popgis ids, lat/long/elev
--
if (@DimensionColumns= 'GEO') begin

	SELECT T.*
	, DSS.*
	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyGeo DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
end


if (@DimensionColumns= 'RANK') begin
-- rebuild the rank table
	exec buildRank


	SELECT T.*
	, DSS.*

      -- from DimensionRank
      ,	DRANK.[School Enrol],
		DRANK.[District Rank],
		DRANK.[District Decile],
		DRANK.[District Quartile],
		DRANK.[Rank],
		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear


end


if (@DimensionColumns= 'RANKSUMM') begin
-- rebuild the rank table
	exec buildRank


	SELECT
	sum([Number]) Number,

	[Survey Year]
	, Estimate
	 ,[District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]
-- specific to this dataset
      , [Type]
      , UsedBy
      -- from DimensionRank
		, sum(DRANK.[School Enrol]) [School Enrol]
		, DRANK.[District Decile],
		DRANK.[District Quartile],

		DRANK.[Decile],
		DRANK.[Quartile]

	FROM #tmpPivToilets T
	INNER JOIN DimensionSchoolSurveyCORE DSS
		ON T.surveyDimensionssID = Dss.[survey Id]
	LEFT JOIN DimensionRank DRANK
		on DSS.[School No] = DRANK.schNo and T.[Survey Year] = DRANK.svyYear

	GROUP BY
	[Survey Year]
	, Estimate
	 , [District Code]
      ,[District]
      ,[Island]
      ,[AuthorityCode]
      ,[Authority]
      ,[AuthorityType]
      ,[AuthorityGroup]
      ,[LanguageCode]
      ,[Language]
      ,[LanguageGroup]
      ,[SchoolTypeCode]
      ,[SchoolType]

-- specific to this dataset
      , [Type]
      , UsedBy

-- from DimensionRank

	, DRANK.[District Decile],
	DRANK.[District Quartile],
	--DRANK.[Rank],			don;t use rank when grouping
	DRANK.[Decile],
	DRANK.[Quartile]

END

drop table #ebst
drop table #tmpPIVToilets


END
GO

