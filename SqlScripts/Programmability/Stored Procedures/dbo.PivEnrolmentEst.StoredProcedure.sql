SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[PivEnrolmentEst]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #vtblEnrolmentEst
(schNo [nvarchar] (255)  NULL ,
[Survey Year] [INT]  NULL ,
enAge [INT]  NULL ,
enLevel [nvarchar] (255)  NULL ,
enM [INT]  NULL ,
enF [INT]  NULL ,
bestssID [INT]  NULL ,
surveyDimensionssID [INT]  NULL ,
Estimate [INT]  NULL ,
[Age of Data] [INT]  NULL ,
[Year of Data] [INT]  NULL ,
DataQualityLevel [INT]  NULL ,
SurveyYearQualityLevel [INT]  NULL ,
AgeOffset [INT]  NULL ,
YearOfBirth [INT]  NULL
)


	INSERT INTO #vtblEnrolmentEst
		exec dbo.sp_vtblEnrolmentEst


Select *
	INTO #DSS
 from DimensionSchoolSurveyNoYear
create unique index ssID on #DSS
([Survey ID] ASC)


Select *
	INTO #DL
 from DimensionLevel
create unique index code on #DL
(levelCode ASC)


Select *
	INTO #DG
 from DimensionGender
create unique index code on #DG
(genderCode ASC)

	SELECT vtblEnrolmentEst.[Survey Year],
		vtblEnrolmentEst.Estimate,
	vtblEnrolmentEst.[Year of Data] AS [Year of Enrolment Data],
	vtblEnrolmentEst.[Age of Data],
	vtblEnrolmentEst.DataQualityLevel,
	vtblEnrolmentEst.SurveyYearQualityLevel,
	vtblEnrolmentEst.enAge AS Age,
	vtblEnrolmentEst.AgeOffset,
	vtblEnrolmentEst.YearOfBirth,
	case when [genderCode]='M' then [enM] else[enF] end AS Enrol,
	DimensionLevel.*,
	DimensionGender.*,
	DimensionSchoolSurveyNoYear.*,
	case
		when [AgeOffset]>=2 then
			case when [genderCode]='M' then [enM] else[enF] end
		else null
	end AS Overage
FROM #DG DimensionGender,
((#vtblEnrolmentEst vtblEnrolmentEst INNER JOIN
		#DL DimensionLevel ON vtblEnrolmentEst.enLevel = DimensionLevel.LevelCode)
	INNER JOIN
#DSS DimensionSchoolSurveyNoYear ON vtblEnrolmentEst.surveyDimensionssID = DimensionSchoolSurveyNoYear.[Survey ID])


END
GO
GRANT EXECUTE ON [dbo].[PivEnrolmentEst] TO [pEnrolmentRead] AS [dbo]
GO

