SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	data for pupil classrom ratio
-- =============================================
CREATE PROCEDURE [dbo].[PupilClassroomRatio]
	@SendASXML int = 0,
	@xmlOut xml = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- need the cahced enrolment classroom estimate

SELECT *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

SELECT *
INTO #ebr
FROM dbo.tfnESTIMATE_BestSurveyRoomType('CLASS')


-- create the shape of the temp data
DECLARE @data TABLE
(
LifeYear int,
schNo nvarchar(50),
[Room Data Year] int,
[Room Estimate] int,
[Age of Room Data] int,
RoomsurveyDimensionssID int,

[Room Data Quality Level] int,
[Enrol Estimate] int,
[Enrol Data Year] int,
[Age of Enrol Data] int,
[Enrol Data Quality Level] int,
surveyDimensionssID int,
Sector nvarchar(10),
Enrol int,
NumRooms int,
TotSize int,
minSize int,
[MaxSize] int,
SizeSupplied int,
sizeOK int,
RoomDataSupplied int
)


-- insert the enrolment data
INSERT INTO @data
(
   LifeYear
, SchNo
, surveyDimensionssID
, Enrol
, [Enrol Estimate]
, [Enrol Data Year]
, [Age of Enrol Data]
, [Enrol Data Quality Level]

)
SELECT
	E.LifeYear,
	E.schNo,
	E.surveyDimensionssID,
	SS.ssEnrol
, Estimate
, bestYear
, Offset
, bestssqLevel
FROM #ebse E
	INNER JOIN SchoolSurvey SS
		ON E.bestssID = ss.ssID

-- classrooms
INSERT INTO @data
(
LifeYear, schNo,
NumRooms,
TotSize,
minSize,
[MaxSize],
SizeSupplied,
sizeOK,
RoomDataSupplied
,[Room Data Year]
, [Room Estimate]
, [Age of Room Data]
, [Room Data Quality Level]
, RoomSurveyDimensionssID
)
Select
E.LifeYear, E.schNo,
NumRooms,
TotSize,
minSize,
[MaxSize],
SizeSupplied,
sizeOK,
1 as RoomDataSupplied
, bestYear
, Estimate
, offSet
, bestssqLevel
, surveyDimensionssID

FROM #ebr E
	INNER JOIN ssIDClassrooms C
		ON E.bestssID = C.ssID


if @SendAsXML <> 0 begin

	declare @XML xml
	declare @XML2 xml

	SELECT @xml =
	(
		Select LifeYear		[@year],
			systCode			[@schoolType],
			sum(NumRooms) NumRooms,
			sum(TotSize) TotSize,
			min(minSize) minSize,
			max([MaxSize]) [MaxSize],
			sum(SizeSupplied) SizeSupplied,			-- only one roww

			sum(Enrol) Enrol,
			case when isnull(sum(numRooms),0) = 0 then null else  sum(Enrol) / sum(NumRooms) end PCR
			from @data D
				INNER JOIN SchoolYEarHistory SYH
					ON D.LifeYear = SYH.syYEar
					AND D.schNo = SYH.schNo
			GROUP BY LifeYear,systCode
			FOR XML PATH('PupilClassroom')
	)
	SELECT @xmlOut =
	(
		SELECT @XML
		FOR XML PATH('PupilClassrooms')
	)

	If @SendAsXML = 1
		SELECT @xmlOut


END
drop table #ebse
drop table #ebr
END
GO

