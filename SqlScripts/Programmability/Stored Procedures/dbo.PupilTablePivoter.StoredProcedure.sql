SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PupilTablePivoter]
	@code nvarchar(10)
	, @columnset nvarchar(50) = 'CORE'

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @E TABLE
(
	schNo nvarchar(50)
	, lifeYear int
	, bestssID int NULL
	, bestYear int NULL
	, SurveyDimensionssID int NULL
	, Estimate int NULL
)


Declare @surveydata TABLE
(
	 ssID int
	, levelCode nvarchar(10)
	, code nvarchar(10)
	, description nvarchar(50)
	, age int
	, M int
	, F int
	, Value int
	, enM int
	, enF int
	, enSum int
)

INSERT INTO @surveyData
SELECT ssID
, ptLevel
, ptRow
, null
, ptAge
, ptM
, ptF
, ptSum
, null
, null
, null
FROM pupilTables
WHERE ptCode = @code

INSERT INTO @surveyData
SELECT ssID
, enLevel
, null
, null
, enAge
, null
, null
, null
, enM
, enF
, enSum
FROM Enrollments


INSERT INTO @E
Select schNo
, LifeYear
, bestssID
, bestYear
, SurveyDimensionssID
, Estimate

FROM dbo.tfnESTIMATE_BestSurveyEnrolments() E

DECLARE @SPLIT TABLE
(
	ssID int
	, r nvarchar(100)
	, c nvarchar(100)
)
INSERT INTO @Split
Select
	[Survey ID]
	, [school Name]
	, Authority
from DimensionSchoolSurveyNoYear


Select LifeYear
, E.Estimate
, DSS.SchoolID_Name
, DSS.Authority
, Value
, enSum Enrol
FROM DimensionSchoolSurveyNoYear DSS
	INNER JOIN @E E
	ON Dss.[Survey ID] = E.surveyDimensionssID
	INNER JOIN @SurveyData SD
		ON SD.ssID = E.bestssID
----GROUP BY LifeYear, r , c
--Select
--LifeYear
--, r
--, c
--, Value

--FROM @Split S
--	INNER JOIN @E E
--		ON S.ssID = E.surveyDimensionssID
--	INNER JOIN @SurveyData SD
--		ON SD.ssID = E.bestssID
----GROUP BY LifeYear, r , c
--END
END
GO

