SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 21 12 2010
-- Description:	pupil text book ratio indicators
--
-- This SP is cloned from dbo.sp_INDPupilTextBookRatioData
-- and will utlimately replace that
-- =============================================
CREATE PROCEDURE [dbo].[PupilTextBookRatio]
	@Summary int = 0
	, @SendAsXML int = 0
	, @xmlOut xml = null OUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- begin by creating a union query to denormalise the following:
-- enrolments
-- number of text books
-- number of readers

-- we estimate enrolments and resources separately
DECLARE @data TABLE
(
	LifeYear int,
	[SchNo] nvarchar(50),
	[levelCode] nvarchar(10),
	[Subject] nvarchar(50),
	Enrol int,
	NumTextBooks int,
	NumTextBooksGood int,
	NumTextBooksFair int,
	NumTextBooksPoor int,
	NumReaders int,
	NumReadersGood int,
	NumReadersFair int,
	NumReadersPoor int

)

Select *
INTO #ebse
FROM dbo.tfnESTIMATE_BestSurveyenrolments()

Select *
INTO #ebt
FROM dbo.tfnESTIMATE_BestSurveyResourceCategory('Text Books')

Select *
INTO #ebr
FROM dbo.tfnESTIMATE_BestSurveyResourceCategory('Readers')

INSERT INTO @data
(LifeYear, schNo, levelCode, Enrol)
SELECT
	E.LifeYear,
	E.SchNo,
	S.levelCode,
	S.Enrol
FROM
	#ebse E
	INNER JOIN pEnrolmentRead.ssIDEnrolmentLevel S
		ON E.bestssID = S.ssID

-- now textbooks

INSERT INTO @data
(LifeYear, schNo, levelCode,[Subject],
NumTextBooks,
NumTextBooksGood,NumTextBooksFair,NumTextBooksPoor)
SELECT
	E.LifeYear,
	E.SchNo,
	T.reslevel,
	T.subject,
	T.NumTextbooks,
	T.NumTextbooksGood,
	T.NumTextbooksFair,
	T.NumTextbooksPoor

FROM
	#ebt E
	INNER JOIN ssIDTextbooksLevelSubject T
		ON E.bestssID = T.ssID
	WHERE numTextbooks > 0

-- and readers
INSERT INTO @data
(LifeYear, schNo, levelCode,[Subject],
NumReaders,
NumReadersGood,NumReadersFair,NumReadersPoor)
SELECT
	E.LifeYear,
	E.SchNo,
	T.reslevel,
	T.subject,
	T.NumReaders,
	T.NumReadersGood,
	T.NumReadersFair,
	T.NumReadersPoor

FROM
	#ebr E
	INNER JOIN ssIDReadersLevelSubject T
		ON E.bestssID = T.ssID
	WHERE numReaders > 0

IF @Summary = 0 begin

	Select
		Q.LifeYear as [Survey Year],

		E.surveyDimensionssID,
		E.Estimate [Estimate Enrol],
		E.bestyear [Year of Enrol Data],
		E.offset [Age of Enrol Data],
		T.Estimate [Estimate Textbooks],
		T.bestyear [Year of Textbook Data],
		T.offset [Age of Textbook Data],

		Q.levelCode,
		Q.subject,
		Enrol,
		NumTextbooks,
		NumTextbooksGood,
		NumTextbooksFair,
		NumTextbooksPoor,

		NumReaders,
		NumReadersGood,
		NumReadersFair,
		NumReadersPoor

	from
	(
		Select
			LifeYear,
			schno,
			D.levelCode,
			D.subject,
			sum(Enrol) as Enrol,
			sum(NumTextbooks) NumTextbooks,
			sum(NumTextbooksGood) NumTextbooksGood,
			sum(NumTextbooksFair) NumTextbooksFair,
			sum(NumTextbooksPoor) NumTextbooksPoor,

			sum(NumReaders) NumReaders,
			sum(NumReadersGood) NumReadersGood,
			sum(NumReadersFair) NumReadersFair,
			sum(NumReadersPoor) NumReadersPoor
		FROM @data D
		GROUP BY LifeYear, schNo, levelCode, D.[Subject]
	) Q
	LEFT JOIN #ebse E
		ON Q.LifeYear = E.LifeYear and Q.schNo = E.schNo
	LEFT JOIN #ebt T
		ON Q.LifeYear = T.LifeYear and Q.schNo = T.schNo
end

if @Summary = 1 begin

	Select
		Q.LifeYear as [Survey Year],
		Q.levelCode,
		Enrol,
		NumTextbooks,
		NumTextbooksGood,
		NumTextbooksFair,
		NumTextbooksPoor,

		NumReaders,
		NumReadersGood,
		NumReadersFair,
		NumReadersPoor
	INTO #levelTotals
	from
	(
		Select
			LifeYear,
			D.levelCode,
			sum(Enrol) as Enrol,
			sum(NumTextbooks) NumTextbooks,
			sum(NumTextbooksGood) NumTextbooksGood,
			sum(NumTextbooksFair) NumTextbooksFair,
			sum(NumTextbooksPoor) NumTextbooksPoor,

			sum(NumReaders) NumReaders,
			sum(NumReadersGood) NumReadersGood,
			sum(NumReadersFair) NumReadersFair,
			sum(NumReadersPoor) NumReadersPoor
		FROM @data D
		GROUP BY LifeYear, levelCode
	) Q

-- now we'll process these level totals back up to edLevels
SELECT [Survey Year],
num edLevelSet,
 edLevel,
 sum(enrol) Enrol,
			sum(NumTextbooks) NumTextbooks,
			sum(NumTextbooksGood) NumTextbooksGood,
			sum(NumTextbooksFair) NumTextbooksFair,
			sum(NumTextbooksPoor) NumTextbooksPoor,

			sum(NumReaders) NumReaders,
			sum(NumReadersGood) NumReadersGood,
			sum(NumReadersFair) NumReadersFair,
			sum(NumReadersPoor) NumReadersPoor,

			cast(sum(NumTextbooks) as float)/sum(Enrol) TextbooksPerPupil,
			cast(sum(NumReaders) as float)/sum(Enrol) ReadersPerPupil

INTO #edLevelTotals
FROM
(
SELECT
	[Survey Year],
	num,
	case num
		when 0 then edLevelCode
		when 1 then edLevelAltCode
		when 2 then edLevelAlt2Code
	end edLevel,
	Enrol,
		NumTextbooks,
		NumTextbooksGood,
		NumTextbooksFair,
		NumTextbooksPoor,

		NumReaders,
		NumReadersGood,
		NumReadersFair,
		NumReadersPoor

	FROM #levelTotals LT
		INNER JOIN DimensionLevel L
			ON LT.levelCode = L.levelCode
		INNER JOIN metaNumbers
			ON num between 0 and 2
) sub
GROUP BY  [Survey Year], edLevel, num

	if @SEndAsXMl = 0
		SELECT * from #edLevelTotals

	if @SendASXML <> 0 begin
		declare @XML xml

		SELECT @xml =
		(
		SElect

			[Survey Year]	[@year],
			edLevelSet	[@edLevelSet],
			edLevel		[@edLevel],
			Enrol,
				NumTextbooks,
				NumTextbooksGood,
				NumTextbooksFair,
				NumTextbooksPoor,

				NumReaders,
				NumReadersGood,
				NumReadersFair,
				NumReadersPoor,
				TextbooksPerPupil,
				ReadersPerPupil

		from #edLEvelTotals
		FOR XML PATH('TextbookPupil')
		)

		SELECT @XmlOut =
		(
		SELECT
			@xml
		FOR XML PATH('TextbookPupils')
		)

		if @SendAsXML = 1
			SELECT @xmlOut
	end

	drop Table #levelTotals
	drop table #edLevelTotals


end

drop table #ebse
drop table #ebt
END
GO

