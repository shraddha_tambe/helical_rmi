SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 11 2009
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[TablePrivilegesAudit]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @p TABLE
	(
		tableSchema sysname
		, tableName sysname
		, pSelect sysname null
		, pInsert sysname null
		, pUpdate sysname null
		, pDelete sysname null
	)

INSERT INTO @p
Select	  P.TABLE_SCHEMA
		, P.TABLE_NAME
		, case privilege_type when 'SELECT' then GRANTEE end pSelect
		, case privilege_type when 'INSERT' then GRANTEE end pInsert
		, case privilege_type when 'UPDATE' then GRANTEE end pUpdate
		, case privilege_type when 'DELETE' then GRANTEE end pDelete
From INFORMATION_SCHEMA.TABLE_PRIVILEGES P
INNER JOIN INFORMATION_SCHEMA.TABLES T
on P.TABLE_NAME = T.TABLE_NAME
WHERE TABLE_TYPE = 'BASE TABLE'

Select TableSchema, TableName
, max(pSelect) pSelect
, max(pInsert) pInsert
, max(pUpdate) pUpdate
, max(pDelete) pDelete
from @p P
group by
	TableSchema,  TableName
END
GO

