SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-11
-- Description:	Return an incremented value for the next available value of a counter
-- =============================================
CREATE PROCEDURE [dbo].[getCounterCurrent]
(
	-- Add the parameters for the function here
	@counterName nvarchar(20)
	, @Result int OUTPUT
)

AS
BEGIN
	-- Declare the return variable here

	begin try
			INSERT INTO sysCounters (counterName, counterValue)
			SELECT @counterName, 0
			WHERE not exists (Select counterName from sysCounters WHERE counterName = @counterName)

-- for current, just omit the increment
--			UPDATE sysCounters
--				SET counterValue = counterValue + 1
--				WHERE counterName = @counterName

			Select @Result = counterValue
			from sysCounters
				WHERE counterName = @counterName

		Select @Result Counter
		RETURN @Result
	end try

	begin catch
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


	-- Return the result of the function

END
GO

