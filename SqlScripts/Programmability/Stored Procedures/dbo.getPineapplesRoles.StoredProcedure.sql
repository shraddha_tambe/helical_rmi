SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 10 2009
-- Description:	Return a list of all the organisation unit Roles defined in the current Pineapples system
-- =============================================
CREATE PROCEDURE [dbo].[getPineapplesRoles]
	-- Add the parameters for the stored procedure here
	@PineapplesRoleType nvarchar(20) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		Select u.Name
			, RN.*
			from sys.sysusers u
			inner join sys.extended_properties P
				on u.uid = P.Major_ID and P.class = 4
		cross apply common.ParsePineapplesRoleName(U.name) RN
		WHERE P.name = 'PineapplesUser'
		 and ( P.value = @PineapplesRoleType or @PineapplesRoleType is null)

		and u.issqlrole = 1
    -- Insert statements for procedure here
END
GO
GRANT EXECUTE ON [dbo].[getPineapplesRoles] TO [public] AS [dbo]
GO

