SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 11 2009
-- Description:
-- =============================================
CREATE PROCEDURE [dbo].[getRoleMembers]
	-- Add the parameters for the stored procedure here
	@RoleName sysname
	, @DirectMembersOnly bit = 1
	, @MemberPrincipalTypes nvarchar(5) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	Select *
	from RecursiveRoleMembership M
	WHERE
		(M.RoleName = @RoleName or @RoleName is null)
		AND
		(@MemberPrincipalTypes is null
			or charIndex(M.Type,@MemberPrincipalTypes) > 0
		)
		AND (depth = 1 or @DirectMembersOnly = 0)


END
GO
GRANT EXECUTE ON [dbo].[getRoleMembers] TO [public] AS [dbo]
GO

