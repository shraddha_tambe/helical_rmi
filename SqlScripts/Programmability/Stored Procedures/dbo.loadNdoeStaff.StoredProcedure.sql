SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 10 2017
-- Description:	Upload table of teacher data from ndoe (fsm) excel workbook
-- cf https://stackoverflow.com/questions/13850605/t-sql-to-convert-excel-date-serial-number-to-regular-date
-- =============================================
CREATE PROCEDURE [dbo].[loadNdoeStaff]
@xml xml
, @filereference uniqueidentifier
, @user nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	-- totals by education level

declare @ndoeStaff TABLE (
	rowIndex int
    , SchoolYear                 nvarchar(100) NULL
    , State                      nvarchar(100) NULL
    , School_Name                nvarchar(100) NULL
    , School_No                  nvarchar(100) NULL
    , School_Type                nvarchar(100) NULL
    , Office                     nvarchar(100) NULL
    , First_Name                 nvarchar(100) NULL
    , Middle_Name                nvarchar(100) NULL
    , Last_Name                  nvarchar(100) NULL
    , Full_Name                  nvarchar(100) NULL
    , Gender                     nvarchar(100) NULL
    , inDate_of_Birth            nvarchar(100) NULL
	, Age                        nvarchar(100) NULL
    , Citizenship                nvarchar(100) NULL		-- => tchCitizenship
    , Ethnicity                  nvarchar(100) NULL		--
    , FSM_SSN                    nvarchar(100) NULL		-- => tPayroll
    , OTHER_SSN                  nvarchar(100) NULL		-- => tchProvident ?
    , Highest_Qualification      nvarchar(100) NULL
    , Field_of_Study                           nvarchar(100) NULL
    , Year_of_Completion                       nvarchar(100) NULL
    , Highest_Ed_Qualification                 nvarchar(100) NULL
    , Year_Of_Completion2                      nvarchar(100) NULL
    , Employment_Status                        nvarchar(100) NULL
    , Reason                     nvarchar(100) NULL
    , Job_Title                  nvarchar(100) NULL
    , Organization               nvarchar(100) NULL					-- to tchSponsor, used for organisation that employs/pays the teacher
    , Staff_Type                 nvarchar(100) NULL
    , Teacher_Type               nvarchar(100) NULL			--- send to tchStatus, which has tradionally been used for e.g. 'Probationary' TRainee, etc
    , inDate_of_Hire             nvarchar(100) NULL
    , inDate_Of_Exit             nvarchar(100) NULL
    , Annual_Salary              nvarchar(100) NULL
    , Funding_Source             nvarchar(100) NULL
    , ECE                        nvarchar(1) NULL
    , Grade_1                    nvarchar(1) NULL
    , Grade_2                    nvarchar(1) NULL
    , Grade_3                    nvarchar(1) NULL
    , Grade_4                    nvarchar(1) NULL
    , Grade_5                    nvarchar(1) NULL
    , Grade_6                    nvarchar(1) NULL
    , Grade_7                    nvarchar(1) NULL
    , Grade_8                    nvarchar(1) NULL
    , Grade_9                    nvarchar(1) NULL
    , Grade_10                   nvarchar(1) NULL
    , Grade_11                   nvarchar(1) NULL
    , Grade_12                   nvarchar(1) NULL
    , Admin                      nvarchar(1) NULL
    , Other                      nvarchar(1) NULL
    , Total_Days_Absence         int NULL
    , Maths                      int NULL
    , Science                    int NULL
    , Language                   int NULL
    , Competency                 int NULL
    , fileReference				 uniqueidentifier
	, rowXml					 xml					-- this stores the entire raw xml of the row for posterity
	--
	, ssID						 int
	, tID						 int
	--
    , Date_of_Birth              date NULL
    , Date_of_Hire               date NULL
    , Date_Of_Exit               date NULL
	, minYearTaught			     int
	, maxYearTaught				 int
	, classMin					 nvarchar(10)
	, classMax					 nvarchar(10)
	, TAM						 nvarchar(1)		-- teacher / admin / mixed => tchTAM
	, teacherRole				 nvarchar(50)		-- from job title, via lkpTeacherRole => tchRole
	, sector					 nvarchar(10)		-- calculated from the school type, and the levels taught => tchSector
)

declare @validations TABLE
(
	field nvarchar(50),
	rowIndex int,
	errorValue nvarchar(100),
	message nvarchar(200),
	severity nvarchar(20)
)
-- use try / catch
begin try

declare @counter int
declare @debug int = 0

declare @SurveyYear int
declare @districtName nvarchar(50)

declare @districtID nvarchar(10)

-- derive the integer value of the survey year survey year as passed in looks like SY2017-2018
-- note the convention is that the year recorded is the FINAL year of the range ie 2018 in the above
declare @YearStartPos int = 8

-- startpos = 8
Select @DistrictName = v.value('@state', 'nvarchar(50)')
, @SurveyYear = cast(substring(v.value('@schoolYear','nvarchar(50)'),@YearStartPos,4) as int)
From @xml.nodes('ListObject') as V(v)

/*-----------------------------------------------------------
VALIDATIONS

validation errors will be reported back to the client, and no processing will take place
*/-----------------------------------------------------------

--- first off, check that there is a survey control record for year @SurveyYear
Select @SurveyYear = svyYear
FROM Survey
WHERE svyYEar = @SurveyYear

if (@@RowCount = 0) begin
	print 'No survey record created for survey year ' + cast(@SurveyYear as nvarchar(10))
	return
end

Select @districtID = dID
from Districts WHERE dName = @districtName
if (@@RowCount = 0) begin
	print 'District name not available ' + cast(@SurveyYear as nvarchar(10))
	return
end

print @SurveyYear
print @DistrictID

INSERT INTO @ndoeStaff
(
rowIndex
, SchoolYear
, State
, School_Name
, School_No
, School_Type
, Office
, First_Name
, Middle_Name
, Last_Name
, Full_Name
, Gender
, inDate_of_Birth
, Age
, Citizenship
, Ethnicity
, FSM_SSN
, OTHER_SSN
, Highest_Qualification
, Field_of_Study
, Year_of_Completion
, Highest_Ed_Qualification
, Year_Of_Completion2
, Employment_Status
, Reason
, Job_Title
, Organization
, Staff_Type
, Teacher_Type
, inDate_of_Hire
, inDate_Of_Exit
, Annual_Salary
, Funding_Source
, ECE
, Grade_1
, Grade_2
, Grade_3
, Grade_4
, Grade_5
, Grade_6
, Grade_7
, Grade_8
, Grade_9
, Grade_10
, Grade_11
, Grade_12
, Admin
, Other
, Total_Days_Absence
, Maths
, Science
, Language
, Competency
, filereference
, rowXml
)
SELECT
nullif(ltrim(v.value('@Index', 'int')),'')                           [Index]

, nullif(ltrim(v.value('@SchoolYear', 'nvarchar(100)')),'')                           [SchoolYear]
, nullif(ltrim(v.value('@State', 'nvarchar(100)')),'')                              [State]
, nullif(ltrim(v.value('@School_Name', 'nvarchar(100)')),'')                        [School_Name]
, nullif(ltrim(v.value('@School_No', 'nvarchar(100)')),'')                          [School_No]
, nullif(ltrim(v.value('@School_Type', 'nvarchar(100)')),'')                        [School_Type]
, nullif(ltrim(v.value('@Office', 'nvarchar(100)')),'')                             [Office]
, nullif(ltrim(v.value('@First_Name', 'nvarchar(100)')),'')                         [First_Name]
, nullif(ltrim(v.value('@Middle_Name', 'nvarchar(100)')),'')                        [Middle_Name]
, nullif(ltrim(v.value('@Last_Name', 'nvarchar(100)')),'')                          [Last_Name]
, nullif(ltrim(v.value('@Full_Name', 'nvarchar(100)')),'')                          [Full_Name]
, nullif(ltrim(v.value('@Gender', 'nvarchar(100)')),'')                             [Gender]
, nullif(ltrim(v.value('@Date_of_Birth', 'nvarchar(100)')),'')                      [Date_of_Birth]
, nullif(ltrim(v.value('@Age', 'nvarchar(100)')),'')                                [Age]
, nullif(ltrim(v.value('@Citizenship', 'nvarchar(100)')),'')                        [Citizenship]
, nullif(ltrim(v.value('@Ethnicity', 'nvarchar(100)')),'')                          [Ethnicity]
, nullif(ltrim(v.value('@FSM_SSN', 'nvarchar(100)')),'')                            [FSM_SSN]
, nullif(ltrim(v.value('@OTHER_SSN', 'nvarchar(100)')),'')                          [OTHER_SSN]
, nullif(ltrim(v.value('@Highest_Qualification', 'nvarchar(100)')),'')              [Highest_Qualification]
, nullif(ltrim(v.value('@Field_of_Study', 'nvarchar(100)')),'')                     [Field_of_Study]
, nullif(ltrim(v.value('@Year_of_Completion', 'nvarchar(100)')),'')                 [Year_of_Completion]
, nullif(ltrim(v.value('@Highest_Ed_Qualification', 'nvarchar(100)')),'')           [Highest_Ed_Qualification]
, nullif(ltrim(v.value('@Year_Of_Completion2', 'nvarchar(100)')),'')                [Year_Of_Completion2]
, nullif(ltrim(v.value('@Employment_Status', 'nvarchar(100)')),'')                  [Employment_Status]
, nullif(ltrim(v.value('@Reason', 'nvarchar(100)')),'')                             [Reason]
, nullif(ltrim(v.value('@Job_Title', 'nvarchar(100)')),'')                          [Job_Title]
, nullif(ltrim(v.value('@Organization', 'nvarchar(100)')),'')                       [Organization]
, nullif(ltrim(v.value('@Staff_Type', 'nvarchar(100)')),'')                         [Staff_Type]
, nullif(ltrim(v.value('@Teacher_Type', 'nvarchar(100)')),'')                       [Teacher_Type]
, nullif(ltrim(v.value('@Date_of_Hire', 'nvarchar(100)')),'')                       [Date_of_Hire]
, nullif(ltrim(v.value('@Date_of_Exit', 'nvarchar(100)')),'')                       [Date_Of_Exit]
, nullif(ltrim(v.value('@Annual_Salary', 'nvarchar(100)')),'')                      [Annual_Salary]
, nullif(ltrim(v.value('@Funding_Source', 'nvarchar(100)')),'')                     [Funding_Source]
, nullif(ltrim(v.value('@ECE', 'nvarchar(100)')),'')                                [ECE]
, nullif(ltrim(v.value('@Grade_1', 'nvarchar(1)')),'')                                          [Grade_1]
, nullif(ltrim(v.value('@Grade_2', 'nvarchar(1)')),'')                                          [Grade_2]
, nullif(ltrim(v.value('@Grade_3', 'nvarchar(1)')),'')                                          [Grade_3]
, nullif(ltrim(v.value('@Grade_4', 'nvarchar(1)')),'')                                          [Grade_4]
, nullif(ltrim(v.value('@Grade_5', 'nvarchar(1)')),'')                                          [Grade_5]
, nullif(ltrim(v.value('@Grade_6', 'nvarchar(1)')),'')                                          [Grade_6]
, nullif(ltrim(v.value('@Grade_7', 'nvarchar(1)')),'')                                          [Grade_7]
, nullif(ltrim(v.value('@Grade_8', 'nvarchar(1)')),'')                                          [Grade_8]
, nullif(ltrim(v.value('@Grade_9', 'nvarchar(1)')),'')                                          [Grade_9]
, nullif(ltrim(v.value('@Grade_10', 'nvarchar(1)')),'')                                         [Grade_10]
, nullif(ltrim(v.value('@Grade_11', 'nvarchar(1)')),'')                                         [Grade_11]
, nullif(ltrim(v.value('@Grade_12', 'nvarchar(1)')),'')                                         [Grade_12]
, nullif(ltrim(v.value('@Admin', 'nvarchar(1)')),'')									[Admin]
, nullif(ltrim(v.value('@Other', 'nvarchar(1)')),'')									[Other]
, nullif(ltrim(v.value('@Total_Days_Absence', 'int')),'')                               [Total_Days_Absence]
, nullif(ltrim(v.value('@Maths', 'int')),'')											[Maths]
, nullif(ltrim(v.value('@Science', 'int')),'')											[Science]
, nullif(ltrim(v.value('@Language', 'int')),'')                                         [Language]
, nullif(ltrim(v.value('@Competency', 'int')),'')                                       [Competency]
, @filereference
, v.query('.')																			[rowXml]
from @xml.nodes('ListObject/row') as V(v)

----- DEal with the DOC staff up front
----- Delete them!
----- to do - further discussion about handling these

-- find a better way to do this...
-- the teacher location on this page is a 'pay point', more general than a school

DELETE
FROM @ndoeStaff
WHERE School_No like '%%%DOE'

--- check any teacher qualification values
-- look for the qualification in both the Code and Description for flexibility in future
INSERT INTO @validations
Select
'Highest Qualification'
, rowIndex
, highest_qualification
, ' undefined code'
, 'Critical'

FROM @ndoeStaff ndoe
	LEFT JOIN lkpTeacherQual Q
		ON NDOE.Highest_Qualification in (codeCode, codeDescription)
WHERE highest_qualification is not null
AND Highest_Qualification <> 'None'			-- none is a special value
AND Q.codeCode is null

INSERT INTO @validations
Select
'Highest Qualification'
, rowIndex
, highest_qualification
, ' Classified as an Education Qualifiation (codeTeach=1)'
, 'Critical'
FROM @ndoeStaff ndoe
	INNER JOIN lkpTeacherQual Q
		ON NDOE.Highest_Qualification in (codeCode, codeDescription)
WHERE highest_qualification is not null
AND Highest_Qualification <> 'None'
AND Q.codeTeach = 1

--- check any teacher qualification values
INSERT INTO @validations
Select
'Highest Ed Qualification'
, rowIndex
, highest_ed_qualification
, ' undefined code'
, 'Critical'
FROM @ndoeStaff ndoe
	LEFT JOIN lkpTeacherQual Q
		ON NDOE.Highest_ed_Qualification in (codeCode, codeDescription)
WHERE highest_ed_qualification is not null
AND Highest_Ed_Qualification <> 'None'
AND Q.codeCode is null

INSERT INTO @validations
Select
'Highest Ed Qualification'
, rowIndex
, highest_ed_qualification
, ' not classified as an Education Qualifiation (codeTeach=0)'
, 'Critical'

FROM @ndoeStaff ndoe
	INNER JOIN lkpTeacherQual Q
		ON NDOE.Highest_ed_Qualification in (codeCode, codeDescription)
WHERE highest_ed_qualification is not null
AND Highest_Ed_Qualification <> 'None'
AND Q.codeTeach = 0

-- citizenship, ethinicity and gender, role should get validated

INSERT INTO @validations
Select
'Teacher Role'
, rowIndex
, Job_Title
, ' role not found in teacher roles'
, 'Information'

FROM @ndoeStaff ndoe
	INNER JOIN lkpTeacherRole R
		ON NDOE.Job_Title in (R.codeCode, R.codeDescription)
WHERE job_title is not null
AND job_title <> 'None'
AND R.codeCode is null


--- if there were validation errors abort here
---

-- clean up the names, defining the midle name if the name includes the surname

-- clean up the dob
UPDATE @ndoeStaff
SET Date_Of_Birth = convert(date, inDate_Of_Birth)
WHERE ISNUMERIC(inDate_Of_Birth) = 0

UPDATE @ndoeStaff
SET Date_Of_Birth = cast( convert(int, inDate_Of_Birth) - 2 as datetime)
WHERE ISNUMERIC(inDate_Of_Birth) = 1


UPDATE @ndoeStaff
SET Date_Of_Hire = convert(date, inDate_Of_Hire)
WHERE ISNUMERIC(inDate_Of_Hire) = 0

UPDATE @ndoeStaff
SET Date_Of_Hire = cast( convert(int, inDate_Of_Hire) - 2 as datetime)
WHERE ISNUMERIC(inDate_Of_Hire) = 1


UPDATE @ndoeStaff
SET Date_Of_Exit = convert(date, inDate_Of_Exit)
WHERE ISNUMERIC(inDate_Of_Exit) = 0

UPDATE @ndoeStaff
SET Date_Of_Exit = cast( convert(int, inDate_Of_Exit) - 2 as datetime)
WHERE ISNUMERIC(inDate_Of_Exit) = 1

UPDATE @ndoeStaff
SET Gender = case Gender when 'Male' then 'M' when 'Female' then 'F' else Gender end

-- apply the ssId to @ndeoStaff

UPDATE @ndoeStaff
SET ssID = SS.ssID

from @ndoeStaff
	LEFT JOIN SchoolSurvey SS
		ON [@ndoeStaff].School_No = SS.schNo
		AND SS.svyYear = @SurveyYear

-- get the minimum and maximum levels taught
UPDATE @ndoeStaff
 set minYearTaught =
	case
	 when ECE = 'X' then 0
	 when Grade_1 = 'X' then 1
	 when Grade_2 = 'X' then 2
	 when Grade_3 = 'X' then 3
	 when Grade_4 = 'X' then 4
	 when Grade_5 = 'X' then 5
	 when Grade_6 = 'X' then 6
	 when Grade_7 = 'X' then 7
	 when Grade_8 = 'X' then 8
	 when Grade_9 = 'X' then 9
	 when Grade_10 = 'X' then 10
	 when Grade_11 = 'X' then 11
	 when Grade_12 = 'X' then 12
	 else null
	end,
	maxYearTaught =
	case
	 when Grade_12 = 'X' then 12
	 when Grade_11 = 'X' then 11
	 when Grade_10 = 'X' then 10
	 when Grade_9 = 'X' then 9
	 when Grade_8 = 'X' then 8
	 when Grade_7 = 'X' then 7
	 when Grade_6 = 'X' then 6
	 when Grade_5 = 'X' then 5
	 when Grade_4 = 'X' then 4
	 when Grade_3 = 'X' then 3
	 when Grade_2 = 'X' then 2
	 when Grade_1 = 'X' then 1
	 when ECE = 'X' then 0
	 else null
	end

-- now get classMin and classMax
UPDATE @ndoeStaff
SET ClassMin = tlmLevel
FROM @ndoeStaff
	INNER JOIN Schools S
		on [@ndoeStaff].School_No = S.schNo
	INNER JOIN metaSchoolTypeLevelMap TLM
		ON S.schType = TLM.stCode
		AND [@ndoeStaff].minYearTaught = TLM.tlmOffset


UPDATE @ndoeStaff

SET ClassMax = tlmLevel
FROM @ndoeStaff
	INNER JOIN Schools S
		on [@ndoeStaff].School_No = S.schNo
	INNER JOIN metaSchoolTypeLevelMap TLM
		ON S.schType = TLM.stCode
		AND [@ndoeStaff].maxYearTaught = TLM.tlmOffset
WHERE [@ndoeStaff].maxYearTaught <> [@ndoeStaff].minYearTaught			-- histroically this is null if teaching only one level

--- sector --
-- take the sector from the highest grade taught ( coalesce classMax, classMin)
-- if not defined get the sector of the highest level in the school


UPDATE @ndoeStaff
SET sector = coalesce(L.secCode, TYPESEC.secCode)
FROM @ndoeStaff
	LEFT JOIN lkpLevels L
		ON coalesce(ClassMax, classMin) = codeCode
	INNER JOIN Schools S
		on [@ndoeStaff].School_No = S.schNo
	LEFT JOIN
	(
	Select stCode, tlmLevel, L.secCode
	from metaSchoolTypeLevelMap
		INNER JOIN
		-- the subquery returns the sector code of the level with the highest year of ed in the school type
		(
			select stCode xtlmCode,
			max(tlmOffset) MaxYoE
			from metaSchoolTypeLevelMap
			GROUP BY stCode
		) XTLM
			ON stCode = xtlmCode
			AND tlmOffset = MaxYoE
		INNER JOIN lkpLEvels L
			on codeCode = metaSchooltypeLevelMap.tlmLevel
	) TYPESEC
	ON S.schType = TYPESEC.stCode

-- role and duties
UPDATE @ndoeStaff
	SET TAM = case Staff_Type
				when 'Teaching Staff' then 'T'				-- teacher
				when 'Non-Teaching Staff' then 'A'			-- admin
				else null
				end

-- teacher role is encoded via lkpTeacherRole
UPDATE @ndoeStaff
	Set TeacherRole = codeCode
FROM @ndoeStaff
	INNER JOIN lkpTeacherRole TR
	ON [@ndoeStaff].Job_Title in (TR.codeCode, TR.codeDescription)


	---- debug -----
if @debug = 1 begin
	Select rowIndex
	, minYearTaught
	, maxYearTaught
	, classMin
	, classMax
	, ECE
	, GRade_1
	, Grade_2
	, Grade_3
	, Grade_4
	, Grade_5
	, Grade_6
	, Grade_7
	, Grade_8
	, Grade_9
	, Grade_10
	, Grade_11
	, Grade_12
	from @ndoeStaff
end

-- before updating teacher identity, try to delete obvious duplicates from the file
--
INSERT INTO @validations
SELECT
'Duplicate Row'
, rowIndex
, first_name + ' ' + last_name
, 'duplicate row was removed before loading'
, 'Information'
FROM @ndoeStaff ndoe
WHERE rowIndex in
(
Select min(rowIndex) RI
From @ndoeStaff ndoe
WHERE employment_status = 'Active'
GROUP BY school_No
, first_name
, last_name
, Date_of_Birth
, Job_Title
HAVING count(*) > 1
)

DELETE from @ndoeStaff
WHERE rowIndex in
(Select rowIndex
	FROM @validations
	WHERE field = 'Duplicate Row'
)


-- IDENTIFY Teacher Identity

UPDATE @ndoeStaff
Set tID = TI.tID
FROM @ndoeStaff
, TeacherIdentity TI

WHERE
-- a match is:
-- FSM_SSN = tPayroll + surname + given name + dob
-- surname + given name + dob + Gender fsm_ssn not known

(
	FSM_SSN = TI.tPayroll AND
	Last_Name = TI.tSurname AND
	First_Name = TI.tGiven AND
	Date_of_Birth = TI.tDOB AND
	Gender = tSex

)
OR
(
	TI.tPayroll is null AND
	Last_Name = TI.tSurname AND
	First_Name = TI.tGiven AND
	Date_of_Birth = TI.tDOB AND
	Gender = tSex
)

print 'Teacher identities found on 4 matches: ' + cast (@@Rowcount as nvarchar(5))

-- second pass at tagging tIDs - pick up match 3 of 4 of on payroll, surname, given, dob  and appointment/survey
-- at school - tId not otherwise assigned


UPDATE @ndoeStaff
Set tID = TI.tID
FROM @ndoeStaff
, TeacherIdentity TI
WHERE
	[@ndoeStaff].tID is null AND
	(
		(
		  FSM_SSN = TI.tPayroll AND
		  Last_Name = TI.tSurname AND
		  Date_of_Birth = TI.tDOB
		)
		OR
		(
		  Last_Name = TI.tSurname AND
		  First_Name = TI.tGiven AND
		  Date_of_Birth = TI.tDOB
		)
		OR
		(
		  FSM_SSN = TI.tPayroll AND
		  Last_Name = TI.tSurname AND
		  First_Name = TI.tGiven
		)
		OR
		(
 		 FSM_SSN = TI.tPayroll AND
		 First_Name = TI.tGiven AND
	     Date_of_Birth = TI.tDOB
		)
	)
	and TI.tID in
	(
		-- probably should specify a time period for this appointment,
		-- but for the prupose of matching, should be ok
		select tID from pTeacherRead.TeacherListByAppointment
		WHERE schNo = [@ndoeStaff].School_No
		UNION
		-- look for matching teachersurvey
		select tID from pTeacherRead.TeacherSurveyV
		WHERE schNo = [@ndoeStaff].School_No
		AND svyYear between @SurveyYear - 3 and @SurveyYear - 1
	)
	and TI.tID not in (Select tID from @ndoeStaff WHERE tID is not null)

print 'Teacher identities found from appointments/surveys: ' + cast (@@Rowcount as nvarchar(5))

-- 3rd pass - 3 demographic matches

UPDATE @ndoeStaff
Set tID = TI.tID
FROM @ndoeStaff
, TeacherIdentity TI
WHERE
	[@ndoeStaff].tID is null AND
	(
		(
		  FSM_SSN = TI.tPayroll AND
		  Last_Name = TI.tSurname AND
		  Date_of_Birth = TI.tDOB
		)
		OR
		(
		  Last_Name = TI.tSurname AND
		  First_Name = TI.tGiven AND
		  Date_of_Birth = TI.tDOB
		)
		OR
		(
		  FSM_SSN = TI.tPayroll AND
		  Last_Name = TI.tSurname AND
		  First_Name = TI.tGiven
		)
		OR
		(
 		  FSM_SSN = TI.tPayroll AND
		  First_Name = TI.tGiven AND
	      Date_of_Birth = TI.tDOB
		)
	)
	and TI.tID not in (Select tID from @ndoeStaff WHERE tID is not null)

print 'Teacher identities found from 3 matches: ' + cast (@@Rowcount as nvarchar(5))
-- we have tagged teachers with tID - we don;t want duplicates in different schools
-- duplicate in the same school we'll warn and ignore


INSERT INTO @validations
Select 'Teacher Identity'
, rowIndex
, isnull(first_name,'') + ' ' + isnull(last_name,'')
, 'Teacher listed twice'
, 'Critical'
FROM @ndoeStaff NDOE
	INNER JOIN
	(
	Select tID
	from @ndoeStaff
	WHERE tID is not null
	AND employment_Status = 'Active'
	GROUP BY tID
	HAVING count(*) > 1
	) S
	ON S.tID = NDOE.tID


Select @counter = count(*)
	from @validations
	where severity = 'Critical'

if (@counter > 0) begin
	Select * from @validations
	WHERE Severity = 'Critical'

	print 'Critical validations'
	return
end

-- create any required SchoolSurvey records
-- but they should not be needed if we have already processed Schools
print ' before insert school survey'
INSERT INTO SchoolSurvey
(
svyYear
, schNo
, ssSchType
, ssAuth
, ssElectN
, ssElectL
, ssSource
)
Select DISTINCT @SurveyYear
, NDOE.School_No
, schType
, schAuth
, schElectN
, schElectL
, @filereference

FROM @ndoeStaff NDOE
INNER JOIN Schools S
	ON NDOE.School_No = S.schNo
LEFT JOIN SchoolSurvey SS
	ON NDOE.School_No = SS.schNo
	AND @SurveyYear = SS.svyYear
WHERE SS.ssID is null


-- update an aggregate total for teachers at the school - we may not get further than this....
UPDATE SchoolSurvey
SET ssNumTeachers = NumTeachers
, ssSchType = schType -- make sure that these are in place, they get propogated back to Schools via the trigger
, ssAuth = schAuth
, ssElectN = schElectN
, ssElectL = schElectL
FROM SchoolSurvey
INNER JOIN (
	Select ssID
	, count(*) NumTeachers
	FROM @NdoeStaff
	WHERE Employment_status not in ('Inactive', 'In-Active')
	GROUP BY ssID) S
	ON SchoolSurvey.ssID = S.ssID
INNER JOIN Schools SCH
	ON SchoolSurvey.schNo = SCH.schNo

if @debug = 1
	Select rowIndex, tID from @ndoeStaff
-- for those teachers identified, update their TeacherIdentity record

UPDATE TeacherIdentity
SET tGiven = First_Name
, tSurname = Last_Name
, tDoB = Date_of_Birth
, tDatePSAppointed = Date_of_Hire
, tDatePSClosed = Date_of_Exit

, tSrc = @filereference
, tSrcID = rowIndex
FROM TeacherIdentity
INNER JOIN @ndoeStaff
ON TeacherIdentity.tId = [@ndoeStaff].tID

if @debug = 1 begin
	SELECT
		tID
		, FSM_SSN
		, First_Name
		, Middle_Name
		, Last_Name
		, Date_of_Birth
		, Gender
		, Date_of_Hire
		, Date_Of_Exit
		, @filereference
		, rowIndex

	FROM @ndoeStaff
	WHERE tID is null
end

-- CREATE the missing Teachers
INSERT INTO TeacherIdentity
(
	tPayroll
	, tGiven
	, tMiddleNames
	, tSurname
	, tDoB
	, tSex
	, tDatePSAppointed
	, tDatePSClosed
	, tSrc
	, tSrcID
)
SELECT
	ltrim(FSM_SSN)
	, First_Name
	, Middle_Name
	, Last_Name
	, Date_of_Birth
	, Gender
	, Date_of_Hire
	, Date_Of_Exit
	, @filereference
	, rowIndex

FROM @ndoeStaff
WHERE tID is null

-- get back these tIDs
-- filereference and rowIndex define uniquely

UPDATE @ndoeStaff
Set tID = TI.tID
FROM @ndoeStaff N
, TeacherIdentity TI
WHERE N.rowIndex = TI.tSrcID
AND TI.tSrc = @filereference

-- write the teacher survey record
-- by DELETing and reinserting
-- delete everything from any represented school

DELETE from TeacherSurvey
From TeacherSurvey
	INNER JOIN SchoolSurvey SS
		ON TeacherSurvey.ssID = SS.ssID
WHERE SS.svyYEar = @SurveyYear
AND schNo in (Select School_No from @ndoeStaff)


print 'before teacher survey'

-- REINSERT
INSERT INTO TeacherSurvey
(
tID
, ssID
, tchFirstName
, tchMiddleNames
, tchFamilyName
, tchDOB
, tchGender
, tchProvident
, tchCitizenship
, tchQual
, tchEdQual
, tchClass
, tchClassMax
, tchSector
, tchTAM
, tchRole
--, tchSubjectTrained
, tchSponsor
, tcheSource
, tcheData
)
SELECT
tID
, ssID
, First_Name
, Middle_Name
, Last_Name
, Date_of_Birth
, Gender
, Other_SSN
, Citizenship
, Q.codeCode
, EQ.codeCode
, classMin
, classMax
, sector
, TAM
, teacherRole
--, Field_of_Study
, Organization
, @filereference
, rowXml
FROM @ndoeStaff
	LEFT JOIN lkpTeacherQual Q
		ON highest_qualification in (Q.codeCode, Q.codeDescription)		-- allow the match on code or description
	LEFT JOIN lkpTeacherQual EQ
		ON highest_ed_qualification in (EQ.codeCode, EQ.codeDescription)

WHERE Employment_status not in ('Inactive', 'In-Active')
print @@Rowcount

-- finally, relay the breakdown by highest_qualification and school
Select schNo
, codeDescription
, count(*)
from pTeacherRead.TeacherSurveyV TS
	LEFT JOIN lkpTeacherQual Q
	ON  TS.tchQual = Q.codeCode
-- convert @filereference to string, since tcheSource may contain values that have not originated from guids
WHERE tcheSource = convert(nvarchar(36),@filereference)
GROUP BY SchNo, codeDescription
ORDER BY schNo, codeDescription


end try

begin catch
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

