SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 12 2007
-- Description:	REbuild all redundant data - to be called after a data upsize
-- =============================================
CREATE PROCEDURE [dbo].[rebuildDataTotals]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- most importantly the ssEnrolM and ssEnrolF on SchoolSurvey
-- the the trigger on enrolments
	UPDATE SchoolSurvey
	SET ssEnrolM = EnrolM ,
	ssEnrolF = EnrolF,
	ssEnrol = case
				when EnrolM is null and EnrolF is null then null
				else isnull(EnrolM,0) + isnull(enrolF,0)
				end
	from
(	Select
	ssID,
	sum(EnM) EnrolM,
	sum(enF) EnrolF
	from
	Enrollments E
	-- in the trigger, only the affected
	--WHERE E.ssID = any(select ssID from inserted)
		--or e.ssID = any(select ssID from deleted)
	group by ssID
) XX
where SchoolSurvey.ssID = xx.ssID

-- make the list xtab
exec MakeListXTb

END
GO

