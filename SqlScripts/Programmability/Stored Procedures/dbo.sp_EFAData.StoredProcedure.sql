SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Underlying data for calculation of efa12
-- =============================================
CREATE PROCEDURE [dbo].[sp_EFAData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- build estimate best survey enrolments as a temp table

Select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyenrolments()

-- current years enrolments by level and gender


-----------------------------------------------------------
-- Enrolments
-- 3 groups- This Year, NextYear, Next Year Next Level
-----------------------------------------------------------

declare @efa table
(
	schNo nvarchar(50),
	svyYear int,
	LevelCode nvarchar(20),
	YearOfEd int,
	EnrolM int,
	EnrolF int,
	RepM int,
	RepF int,
	EnrolNYM int,
	EnrolNYF int,
	RepNYM int,
	RepNYF int,
	EnrolNYNextLevelM int,
	EnrolNYNextLevelF int,
	RepNYNextLevelM int,
	RepNYNextLevelF int,
	Estimate smallint,
	[Year Of Data] int,
	[Age of Data] int,
	[Estimate NY] smallint,
	[Year Of Data NY] int,
	[Age of Data NY] int
)
-- enrolments this year
INSERT INTO @efa
(
	schNo, svyYear, levelCode,
	enrolM, enrolF,
	Estimate, [Year of Data],[Age of Data]
)
SELECT
	EBSE.schNo,
	EBSE.LifeYear svyYear,
	EL.LevelCode,
	EL.EnrolM,
	EL.enrolF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset
FROM #ebse EBSE
inner join pEnrolmentRead.ssidEnrolmentLevel EL
on EBSE.bestssID = El.ssID

-- next year's enrolment at the same level
INSERT INTO @efa
(
	schNo, svyYear, levelCode,
	enrolNYM, enrolNYF,
	[Estimate NY], [Year of Data NY],[Age of Data NY]
)
SELECT
	EBSE.schNo,
	EBSE.LifeYear-1 svyYear,
	EL.LevelCode,
	EL.enrolM AS EnrolNYM,
	EL.EnrolF AS EnrolNYF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset
FROM #ebse EBSE
inner join pEnrolmentRead.ssidEnrolmentLevel EL
on EBSE.bestssID = El.ssID
--INNER JOIN SchoolLifeYears SLY
--	ON EBSE.schNo = SLY.schNo AND EBSE.LifeYear = SLY.svyYear


-- enrolments next year next level
INSERT INTO @efa
(
	schNo, svyYear, levelCode,
	enrolNYNextLevelM, enrolNYNextLevelF,
	[Estimate NY], [Year of Data NY],[Age of Data NY]
)

SELECT
	EBSE.schNo,
	EBSE.LifeYear-1 svyYear,
	L2.codeCode as LevelCode,
	EL.enrolM AS EnrolNYM,
	EL.EnrolF AS EnrolNYF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset
FROM #ebse EBSE
	inner join pEnrolmentRead.ssidEnrolmentLevel EL
		on EBSE.bestssID = El.ssID
	inner join lkpLevels L1
		on L1.codeCode = EL.LevelCode,
-- find the 'next level' based on lvlYear, the year of education
-- default path levels is a way to get a unique level
-- needs to be considered how this would work in a school type not using the default path level levels

	common.LISTDefaultPathLevels LDPL
		inner join lkpLevels L2
		on L2.codeCode = LDPL.levelCode
WHERE L2.lvlYear = L1.lvlYear-1

-----------------------------------------------
-- Repeaters
-- this year, next year, next year next level
-----------------------------------------------

INSERT INTO @efa
(
	schNo, svyYear, levelCode,
	repM, repF,
	Estimate, [Year of Data],[Age of Data]
)
SELECT
	EBSE.schNo,
	EBSE.LifeYear-1 svyYear,
	RL.ptLevel,
	RL.repM,
	RL.repF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset
FROM #ebse EBSE
inner join pEnrolmentRead.ssidRepeatersLevel RL
on EBSE.bestssID = RL.ssID

INSERT INTO @efa
(
	schNo, svyYear, levelCode,
	repNYM, repNYF,
	[Estimate NY], [Year of Data NY],[Age of Data NY]
)
SELECT
	EBSE.schNo,
	EBSE.LifeYear-1 svyYear,
	RL.ptLevel,
	RL.repM,
	RL.repF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset
FROM #ebse EBSE
inner join pEnrolmentRead.ssidRepeatersLevel RL
on EBSE.bestssID = RL.ssID

-- Repeaters next year next level
INSERT INTO @efa
(
	schNo, svyYear, levelCode,
	repNYNextLevelM, repNYNextLevelF,
	[Estimate NY], [Year of Data NY],[Age of Data NY]
)
SELECT
	EBSE.schNo,
	EBSE.LifeYear-1 svyYear,
	L2.codeCode as LevelCode,
	RL.repM,
	RL.repF,
	EBSE.Estimate,
	EBSE.[bestYear],
	EBSE.offset
FROM #ebse EBSE
	inner join pEnrolmentRead.ssidRepeatersLevel RL
		on EBSE.bestssID = RL.ssID
	inner join lkpLevels L1
		on L1.codeCode = RL.ptLevel,
-- find the 'next level' based on lvlYear, the year of education
-- default path levels is a way to get a unique level
-- needs to be considered how this would work in a school type not using the default path level levels

	common.LISTDefaultPathLevels LDPL
		inner join lkpLevels L2
		on L2.codeCode = LDPL.levelCode
WHERE L2.lvlYear = L1.lvlYear-1

-- now return the sum across these
Select
	D.schNo,
	D.svyYear [Survey Year],
	LevelCode,
	sum(enrolM) as EnrolM,
	sum(enrolF) as enrolF,
	sum(EnrolNYM) AS EnrolNYM,
	sum(EnrolNYF) AS EnrolNYF,
	sum(EnrolNYNextLevelM) AS EnrolNYNextLevelM,
	sum(EnrolNYNextLevelF) AS EnrolNYNextLevelF,
	sum(RepM) AS RepM,
	sum(RepF)  AS RepF,
	sum(RepNYM) AS RepNYM,
	sum(RepNYF)  AS RepNYF,
	sum(RepNYNextLevelM)  AS RepNYNextLevelM,
	sum(RepNYNextLevelF)  AS RepNYNextLevelF,
	surveyDimensionssID
from @efa D
	INNER JOIN #ebse  E
		on D.schNo = E.SchNo
		and D.svyYear = E.LifeYear
group by D.schNo, D.svyYear, D.LevelCode,surveydimensionssID

DROP TABLE #ebse

END
GO

