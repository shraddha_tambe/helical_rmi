SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 11 2007
-- Description:	data for disability pivot
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVDisabiityData]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

create table #data
(
ssID int,		-- because both estimates are by the same table, we can do the estimation at the end
levelCode nvarchar(10),
disCode nvarchar(10),
Disability nvarchar(50),
disM int,
disF int,
EnrolM int,
EnrolF int
)

-- enrolments use a dummy code for disabiity
INSERT INTO #data
Select
ssID,
levelCode,
'<>',
'<enrolment>',
null,
null,
EnrolM,
EnrolF
FROM pEnrolmentRead.ssIDEnrolmentLevel

-- disabiity data
INSERT INTO #data
Select
ssID,
ptLevel,
disCode,
TR.codeDescription,
ptM,
ptF,
null,
null

FROM vtblDisabilities V
	INNER JOIN TRDisabilities TR
		ON V.disCode = TR.codeCode

Select
E.LifeYear [Survey Year],
E.Estimate,
E.bestYear [Year of Data],
E.offset [Age of Data],
E.surveyDimensionssID,
E.actualssqLevel [Survey Year Quality Level],
E.bestssqLevel [Data Quality Level],
levelCode,
disCode,
Disability,
disM,
disF,
EnrolM,
EnrolF
FROM dbo.tfnESTIMATE_BestSurveyEnrolments() E
	INNER JOIN #data D
		ON E.bestssID = D.ssID


END
GO
GRANT EXECUTE ON [dbo].[sp_PIVDisabiityData] TO [pEnrolmentReadX] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[sp_PIVDisabiityData] TO [pSchoolReadX] AS [dbo]
GO

