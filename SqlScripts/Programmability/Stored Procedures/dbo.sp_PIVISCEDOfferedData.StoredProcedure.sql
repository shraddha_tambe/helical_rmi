SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 18 11 2007
-- Description:	ISCED Levels offered at a school in a year
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVISCEDOfferedData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- cahce estimate
SELECT *
INTO #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

-- select distinct here, becuase we onlyneed to see what values are represented, not totals
SELECT DISTINCT
	EBSE.LifeYear AS [Survey Year],
	EBSE.SchNo,
	ISCEDLevel.ilCode,
	ISCEDLevelSub.ilsCode,
	EBSE.bestssID,
	EBSE.Estimate,
    EBSE.Offset AS [Age of Data],
	EBSE.bestYear AS [Year of Data],
    EBSE.bestssqLevel AS [Data Quality Level],
	EBSE.ActualssqLevel AS [Survey Year Quality Level],
	EBSE.surveyDimensionssID
FROM #ebse EBSE
	INNER JOIN SchoolSurvey
		ON EBSE.surveyDimensionssID = SchoolSurvey.ssID
	INNER JOIN metaSchoolTypeLevelMap
		ON SchoolSurvey.ssSchType = metaSchoolTypeLevelMap.stCode
	INNER JOIN lkpLevels
		ON lkpLevels.codeCode = metaSchoolTypeLevelMap.tlmLevel
	INNER JOIN ISCEDLevelSub
		ON ISCEDLevelSub.ilsCode = lkpLevels.ilsCode
	INNER JOIN ISCEDLevel
		ON ISCEDLevel.ilCode = ISCEDLevelSub.ilCode

DROP TABLE #ebse
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVISCEDOfferedData] TO [pSchoolRead] AS [dbo]
GO

