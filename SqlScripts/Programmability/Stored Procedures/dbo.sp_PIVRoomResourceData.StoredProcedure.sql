SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 6 12 2007
-- Description:	Pivot room/resource data. Used for RoomsLAB and RoomsLIB
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVRoomResourceData]
	-- Add the parameters for the stored procedure here
	@RoomType nvarchar(10) ,
	@ResourceCategory nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- the room estimate data
SELECT *
INTO #ebroom
FROM dbo.tfnESTIMATE_BestSurveyRoomType(@RoomType)


SELECT *
INTO #ebres
FROM dbo.tfnESTIMATE_BestSurveyResourceCategory(@ResourceCategory)

declare @ViewName nvarchar(50)
select @ViewName = 'vw' + @RoomType + 'ResourcesXTab'
select @ViewName = 'pSchoolRead.vwResourcesXTab'

exec dbo.MakeResourceXTabView @ResourceCategory,@ViewName
-- 22 03 2009 not sure if this is really the best way/place for this - what about changing context?
Select *
INTO #xtb
FROM pSchoolRead.vwResourcesXTab
drop view pSchoolRead.vwResourcesXTab


-- select * from #xtb

SELECT
E.LifeYear [Survey Year],
E.Estimate [Room Data Estimate],
E.bestyear [Year of Room Data],
E.offset [Age of Room Data],
E.Estimate [Resource Data Estimate],
E.bestyear [Year of Resource Data],
E.offset [Age of Resource Data],
-- E.schNo, will just cause confusion later, becuase of the cross tab inclusion we have to use all columns of this query
E.SurveyDimensionssID,
R.rmType RoomTypeCode,
lkpRoomTypes.codeDescription AS RoomType,
R.rmID,
R.rmYear AS YearBuilt,
R.rmNo AS RoomNo,
R.rmSize AS RoomSize,
case when (rmSize > 0 ) then 1 else 0 end AS SizeSupplied,
R.rmLevel AS ClassLevel,
R.rmMaterials Material,
R.rmMaterialRoof MaterialRoof,
R.rmMaterialFloor MaterialFloor,
R.rmCondition AS Condition,
1 AS NumRooms,
Partitions.ptName AS YearBracket,
case when [rmShareType] Is Null then 0 else 1 end AS SharedRoom,
R.rmShareType AS ShareRoomTypeCode,
X.*
FROM #ebroom E
	INNER JOIN #ebres ER
		on E.LifeYear = ER.LifeYear
				AND E.schno = ER.schno
	INNER JOIN Rooms as R
		ON E.rmType = R.rmType and E.bestssID = R.ssID
	INNER JOIN lkpRoomTypes
		ON lkpRoomTypes.codeCode = R.rmType
	LEFT JOIN #xtb X
		ON Er.bestssID = X.ssID,
	Partitions
WHERE
	Partitions.ptSet='YearBuilt' AND
	isnull(R.rmYear,0) between ptMin and ptMax
	AND R.rmType = @RoomType
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVRoomResourceData] TO [pSchoolRead] AS [dbo]
GO

