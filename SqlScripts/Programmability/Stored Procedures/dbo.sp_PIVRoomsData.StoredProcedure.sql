SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 12 2007
-- Description:	data for rooms pivot
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVRoomsData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select *
INTO #ebsr
FROM dbo.tfnESTIMATE_BestSurveyRoomType(null)
    -- Insert statements for procedure here

-- 14 11 2009 SRVU0017
declare @minYEar int

Select @MinYear = min(ptMin) -- shoul dne zero
from Partitions
WHERE
	ptSet='YearBuilt'


SELECT
E.LifeYear [Survey Year],
E.Estimate,
E.bestyear [Year of Data],
E.offset [Age of Data],
E.schNo,
E.SurveyDimensionssID,
R.rmType RoomTypeCode,
lkpRoomTypes.codeDescription AS RoomType,
R.rmID,
R.rmYear AS YearBuilt,
R.rmNo AS RoomNo,
R.rmSize AS RoomSize,
case when (rmSize > 0 ) then 1 else 0 end AS SizeSupplied,
R.rmLevel AS ClassLevel,
R.rmMaterials Material,
R.rmMaterialRoof MaterialRoof,
R.rmMaterialFloor MaterialFloor,
R.rmCondition AS Condition,
1 AS NumRooms,
case when rmYear is null then null else Partitions.ptName end AS YearBracket,
case when [rmShareType] Is Null then 0 else 1 end AS SharedRoom,
R.rmShareType AS ShareRoomTypeCode
FROM #ebsr E
	INNER JOIN Rooms as R
		ON E.rmType = R.rmType and E.bestssID = R.ssID
	INNER JOIN lkpRoomTypes
		ON lkpRoomTypes.codeCode = R.rmType,
	Partitions
WHERE
	--- 14 11 2009 SRVU0017 schools with rmYear is null were counted for every partition
	Partitions.ptSet='YearBuilt' AND
	((R.rmYear is null and ptMin = @MinYear	)	-- just to get us one row out of partitions
	or R.rmYear between ptMin and ptMax
	)

drop table #ebsr
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVRoomsData] TO [pSchoolRead] AS [dbo]
GO

