SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13/11/2007
-- Description:	Data for schools pivot table
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVSchoolsData]
	-- Add the parameters for the stored procedure here
	@SchNo nvarchar(50) = null,
	@SurveyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- collect the estimate temp table
Select * into
#ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments() E
WHERE (E.schNo = @SchNo or @SchNo is null)
	and (E.LifeYear = @SurveyYear or @SurveyYear is null)

-- rebuild the rank table
	exec buildRank
-- now the output
SELECT
	EE.LifeYear [Survey Year],
	EE.Estimate,
    EE.Offset AS [Age of Data],
	EE.bestYear AS [Year of Data],
    EE.bestssqLevel AS [Data Quality Level],
	EE.ActualssqLevel AS [Survey Year Quality Level],
	EE.SurveyDimensionssID,
	SS.ssEnrol as Enrol,

-- from pEnrolmentRead.ssIDEnrolmentLevelsummary
	ELS.NumLevels,
	ELS.RangeYearOfEd,
	ELS.HighestLevel,
	ELS.LowestLevel,
	ELS.HighestYearOfEd,
	ELS.LowestYearOfEd,
	ELS.MaxLevelEnrol,
	ELS.LevelOfMaxEnrol,
	ELS.FullRangeYN,
	ELS.FullRange,

-- from vtblSizeEst
	VSIZE.ssSizeSite as [Site Size],
	VSIZE.ssSizeFarm as [Farm Size],
	VSIZE.ssSizePlayground as [Playground Size],
	case when VSIZE.ssSizeSite > 0 then 1 else 0 end SiteSizeSupplied,
	case when VSIZE.ssSizeFarm > 0 then 1 else 0 end FarmSizeSupplied,
	case when VSIZE.ssSizePlayground > 0 then 1 else 0 end PlaygroundSizeSupplied,
	VSIZE.[Year of Data] as [Year of School Size Data],
	VSIZE.Estimate as [School Size Estimate],
	VSIZE.[Age Of Data] as [School Size Age of Data],

-- from DimensionRank
	DRANK.[School Enrol],
	DRANK.[District Rank],
	DRANK.[District Decile],
	DRANK.[District Quartile],
	DRANK.[Rank],
	DRANK.[Decile],
	DRANK.[Quartile]


FROM
	#ebse EE INNER JOIN SchoolSurvey SS
		on EE.bestssID = SS.ssID
	INNER JOIN pEnrolmentRead.ssIDEnrolmentLevelSummary ELS
		on EE.bestssID = ELS.ssID
	LEFT JOIN vtblSizeEst VSIZE
		on EE.schNo = VSIZE.schNo and EE.LifeYear = VSIZE.[Survey Year]
	LEFT JOIN DimensionRank DRANK
		on EE.schNo = DRANK.schNo and EE.LifeYear = DRANK.svyYear

END
GO
GRANT EXECUTE ON [dbo].[sp_PIVSchoolsData] TO [pSchoolRead] AS [dbo]
GO

