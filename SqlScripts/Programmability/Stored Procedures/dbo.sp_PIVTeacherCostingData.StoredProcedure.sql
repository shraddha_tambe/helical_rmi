SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 11 2007
-- Description:	Teachers pivot table data
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVTeacherCostingData]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT *
INTO #ebt
from
dbo.tfnESTIMATE_BestSurveyTeachers(DEFAULT,DEFAULT)

-- this figures out the midyear salary for every survey year/salary point
--- with about 200 salary points, this should never be gigantic

Select * into #salary
from
(
Select svyYear, SPR.*  from SalaryPointRates SPR
inner join
	(Select spCode, svyYear, max(spEffective) maxDate from SalaryPointRates, Survey

		WHERE spEffective <= common.DateSerial(svyYear,7,1)
		group by spCode, svyYear) SPM
on SPR.spCode = SPM.spCode
	and SPR.spEffective = SPM.MaxDate
) Rates

-- business rules for best salary guess


Select TS.tchsID, tchRole, TS.tchSalaryScale, R.spCode, L.salDefaultCostPoint
, isnull(R.spCode, L.salDefaultCostPoint) SurveyCostPoint
, tSalaryPoint
, trSalaryPointMedian roleSalaryPoint
, case
		when isnull(R.spCode, L.salDefaultCostPoint) > tSalaryPoint then tSalaryPoint
		else isnull(isnull(isnull(R.spCode, L.salDefaultCostPoint),tSalaryPoint),trSalaryPointMedian)
end SalaryPointToUse
into #SurveySalaryPoint
from TeacherSurvey TS
inner join TeacherIdentity TI
on TS.tID = TI.tID
left join (Select DISTINCT spCode from SalaryPointRates) R
on TS.tchSalaryScale = R.spCode
left join lkpSalaryLevel L
on TS.tchSalaryScale = L.salLevel
left join lkpTeacherRole tRole
	on tRole.codeCode = tchRole


-- build rank
exec BuildRank

SELECT ESTIMATE.LifeYear AS [Survey Year],
ESTIMATE.Estimate,
ESTIMATE.bestYear AS [Teacher Data Year],
ESTIMATE.Offset AS [Teacher Data Age],
ESTIMATE.bestssID AS [Teacher Data Survey ID],
ESTIMATE.ActualssqLevel AS [Survey Year Quality Level],
ESTIMATE.bestssqLevel AS [Data Quality Level],
ESTIMATE.surveyDimensionssID,
TS.tchsID AS TeacherSurveyID,
1 AS NumTeachers,
tSurname + ', ' + tGiven TeacherName,
tPayroll [Payroll No],
GEN.*,
------------case when tchSalaryScale is null then
------------	else
------------		case when

--IIf(Not IsNull([tchSalaryScale]),IIf(InStr([tchSalaryScale],"-"),Left([tchSalaryScale],InStr([tchSalaryScale],"-")-1),[tchSalaryScale])) AS SalaryLevel,
TS.tchRole as RoleCode,
trTeacherRole.codeDescription AS [Role],
SSP.SalaryPointToUse as  CostingSalaryPoint,
lkpSalaryPoints.salLevel as CostingSalaryLevel,			-- for now
TI.tSalaryPoint as CurrentSalaryPoint,
TS.tchSalaryScale as SurveySalaryPoint,
#Salary.spEffective as SalaryRateEffectiveDate,
#Salary.spSalary as SalaryPointAnnualSalary,
TS.tchSalary AS SurveySalary,
isnull(#Salary.spSalary, TS.tchSalary * 26) CostingSalary,
TS.tchSponsor AS SalaryPaidByCode,
AUTH.Authority AS SalaryPaidBy,
AUTH.AuthorityGroupCode AS SalaryGovCode,
AUTH.AuthorityGroup AS SalaryGov,
TS.tchCitizenship AS Citizenship,
TS.tchQual AS HighestQualificationCode,
Q.codeDescription AS HighestQualification,
TS.tchEdQual AS HighestEdQualificationCode,
QE.codeDescription AS HighestEdQualification,
TS.tchSubjectMajor AS MajorSubject,
TS.tchSubjectMinor AS MinorSubject,
TS.tchSubjectMinor2 AS MinorSubject2,
TS.tchSubjectTrained AS SubjectTrainedToTeach,
trTeacherStatus.statusDesc AS Status,
(ESTIMATE.LifeYear-Year([tDOB])) AS Age,
IsNull(qc.Qualified,'N') AS Qualified,
IsNull(qc.Certified,'N') AS Certified,
Q.codeGroup AS QualGroup,
case Qualified when 'Y' then 1 else 0 end AS NumQualified,
case Certified when 'Y' then 1 else 0 end AS NumCertified,
SECTOR.secDesc AS TeacherSector,
TS.tchSector AS TeacherSectorCode,
TS.tchClass AS LevelTaught,
case tchFullPart when 'P' then tchFTE else 1 end as FTE,

--IIf([NumFullTime],'Y','N') AS FullTime,
case tchFullPart when 'P'  then 'N' else 'Y' end as FullTime,

--IIf([NumFulltime],0,1) AS NumPartTime,
case tchFullPart when 'P' then 1 else 0 end as NumPartTime,
--IIf([tchFullPart]='P',0,1) AS NumFullTime,
case tchFullPart when 'P' then 0 else 1 end as NumFullTime,

TS.tchTAM AS Duties,
TI.tID AS TeacherIdentityID
, R.[District Rank]
, R.[District Decile]
, R.[District Quartile]
, R.[Rank]
, R.Decile
, R.Quartile


FROM #ebt ESTIMATE
	INNER JOIN TeacherSurvey TS on ESTIMATE.bestssID = TS.ssID
	INNER JOIN TeacherIdentity TI on TI.tID = TS.tID
	INNER JOIN #SurveySalaryPoint SSP on TS.tchsID = SSP.tchsID
	LEFT JOIN DimensionGender GEN on TI.tSex = GEN.GenderCode
	LEFT JOIN DimensionAuthority AUTH on Ts.tchSponsor = AUTH.authorityCode
	LEFT JOIN EducationSectors SECTOR ON TS.tchSector = SECTOR.secCode
	LEFT JOIN trTeacherRole ON TS.tchRole = trTeacherRole.codeCode
	LEFT JOIN TRTeacherQual AS Q ON TS.tchQual = Q.codeCode
	LEFT JOIN TRTeacherQual AS QE ON TS.tchEdQual = QE.codeCode
	LEFT JOIN tchsIDQualifiedCertified AS qc ON TS.tchsID = qc.tchsID
	LEFT JOIN DimensionLevel ON TS.tchClass = DimensionLevel.LevelCode
	LEFT JOIN trTeacherStatus ON TS.tchStatus = trTeacherStatus.statusCode
	LEFT JOIN TeacherSurveyISCED ISCED ON TS.tchsID = ISCED.tchsID
	LEFT JOIN ISCEDLevelSub ON ISCED.TSISCED = ISCEDLevelSub.ilsCode
	LEFT JOIN DimensionRank R on
		ESTIMATE.lifeYear = R.svyYEar and
		ESTIMATE.schNo = R.schNo
	LEFT JOIN #Salary
		ON #Salary.spCode = SSP.SalaryPointToUse and
			#Salary.svyYear = ESTIMATE.[LifeYear]
	LEFT JOIN lkpSalaryPoints
		ON SSP.SalaryPointToUse = lkpSalaryPoints.spCode

DROP TABLE #ebt

END
GO

