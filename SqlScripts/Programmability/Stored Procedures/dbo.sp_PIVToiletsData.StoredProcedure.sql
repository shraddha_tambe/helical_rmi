SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	data for toilets pivot
-- =============================================
CREATE PROCEDURE [dbo].[sp_PIVToiletsData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
--
SELECT
EBT.LifeYear AS [Survey Year],
EBT.Estimate,
EBT.bestYear AS [Year of Data],
EBT.Offset AS [Age of Data],
EBT.bestssqLevel AS [Data Quality Level],
EBT.ActualssqLevel AS [Survey Year Quality Level],
EBT.surveyDimensionssID, Toilets.toiNum AS [Number],
-- legacy for a much earlier data version
case [toiCondition]
	when '1' then 'A'
	when '2' then 'B'
	when '3' then 'C'
	else toiCondition
end AS Condition,
lkpToiletTypes.ttypName AS [Type],
Toilets.toiUse AS UsedBy
FROM dbo.tfnESTIMATE_BestSurveyToilets() AS EBT
	INNER JOIN Toilets
		ON EBT.bestssID = Toilets.ssID
	INNER JOIN lkpToiletTypes
		ON lkpToiletTypes.ttypName = Toilets.toiType
END
GO
GRANT EXECUTE ON [dbo].[sp_PIVToiletsData] TO [pSchoolRead] AS [dbo]
GO

