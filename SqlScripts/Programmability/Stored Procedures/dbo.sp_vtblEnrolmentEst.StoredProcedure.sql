SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12/11/2007
-- Description:	Return the estimated enrolments
-- =============================================
CREATE PROCEDURE [dbo].[sp_vtblEnrolmentEst]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select *
into #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()
BuildRank
SELECT   DS.*
,
EE.schNo, EE.LifeYear AS [Survey Year], dbo.Enrollments.enAge, dbo.Enrollments.enLevel,
                      dbo.Enrollments.enM, dbo.Enrollments.enF,
					EE.bestssID,
					EE.surveyDimensionssID,
					EE.Estimate,
                      EE.Offset AS [Age of Data], EE.bestYear AS [Year of Data],
                      EE.bestssqLevel AS DataQualityLevel, EE.ActualssqLevel AS SurveyYearQualityLevel,
                      dbo.Enrollments.enAge - (dbo.Survey.svyPSAge + dbo.lkpLevels.lvlYear - 1) AS AgeOffset, dbo.Survey.svyYear - dbo.Enrollments.enAge AS YearOfBirth
						, R.[District Rank]
						, R.[District Decile]
						, R.[District Quartile]
						, R.[Rank]
						, R.[Decile]
						, R.[Quartile]

FROM   	#EBSE EE
	INNER JOIN dbo.Enrollments
		ON EE.bestssID = dbo.Enrollments.ssID
	INNER JOIN dbo.lkpLevels
		ON dbo.Enrollments.enLevel = dbo.lkpLevels.codeCode
	INNER JOIN dbo.Survey
		ON EE.LifeYear = dbo.Survey.svyYear
	LEFT JOIN DimensionRank R
		on R.svyYEar = EE.LifeYear
		and R.schNo = EE.schNo
	LEFT JOIN DimensionSchoolYear DS

			ON DS.schNo = EE.schNo
			AND DS.LifeYear = EE.LifeYEar

END
GO
GRANT EXECUTE ON [dbo].[sp_vtblEnrolmentEst] TO [pEnrolmentRead] AS [dbo]
GO

