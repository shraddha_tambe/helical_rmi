SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-02-07
-- Description:	Read Enrolments as XML
-- =============================================
CREATE PROCEDURE [dbo].[xmlPupilTables]
	-- Add the parameters for the stored procedure here

	@SchNo nvarchar(50) ,
	@SurveyYear int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


Select E.*
, M.tdefRows
, m.tDefCols
, m.tDefCode TableDef
, m.tdefName codeName
, m.tDefDataCode dataCode
,	 case
		--when tdefRows is null then null			-- to support pupiltable formats
		when tdefRows like 'AGE%' then cast(ptAge as nvarchar(10))
		when tdefRows like 'LEVEL%' then ptLevel
		else ptRow
	end rowTag
	, case
		when tdefCols is null then ptLevel		-- level is the default for columns
		when tdefCols like 'AGE%' then cast(ptAge as nvarchar(10))
		when tdefCols like 'LEVEL%' then ptLevel
		else ptCol
	end as colTag
	-- level becomes yearLevel tag on values (page) for some tertiary formats
	-- we can put level as an attribute on on the values tag
	-- if it does not go anywhere else
	,case
		when tdefCols is null then null		-- becuase level is the default for columns
		when tdefCols like 'LEVEL%' then null
		when tdefRows like 'LEVEL%' then null	-- level is in the row
		else ptLevel
	end as pageLevel


into #e
from PupilTables E
		inner join SchoolSurvey S
			on E.ssId = S.ssID
		inner join metaPupilTableDEfs M
		on E.ptTable = m.tdefCode
	WHERE S.schNo = @schNo and S.svyYEar = @SurveyYEar


declare @xmlREsult xml

Select @XMLResult =
(
Select Tag
, Parent
, TableName as [Grid!1!Tag]
, TableDef as [Grid!1!TableDef]
, TableCode as [Grid!1!TableCode] -- back to putting the ptCode here, it's more abstracted from the UI to do this
, null as [values!2!ID!Hide]
, pageTag as [values!2!Tag]
, pageLevel as [values!2!yearLevel]
, rowTag as [row!3!Tag]
, colTag as [col!4!Tag]
, null as [data!5!ID!Hide]
, M as [data!5!M]
, F as [data!5!F]

from

(Select DISTINCT
	1 as Tag
	, null as Parent
	, codeName as TableName
	, TableDef
	, ptCode as TableCode
	, null as pageTag
	, null as pageLevel
	, null as rowTag
	, null as colTag
	, null as M
	, null as F
from #e

UNION ALL
Select DISTINCT
	2 as Tag
	, 1 as Parent
	, codeName as TableName
	, TableDef
	, ptCode as TableCode
	, ptPage as pageTag
	, pageLevel as pageLevel
	, null as rowTag
	, null as colTag
	, null as M
	, null as F
from #e

UNION ALL
Select DISTINCT
	3 as Tag
	, 2 as Parent
	, codeName as TableName
	, TableDef
	, ptCode as TableCode
	, ptPage as pageTag
	, pageLevel as pageLevel
	, rowTag as rowTag
	, null as colTag
	, null as M
	, null as F
from #e

UNION ALL
Select DISTINCT
	4 as Tag
	, 3 as Parent
	, codeName as TableName
	, TableDef
	, ptCode as TableCode
	, ptPage as pageTag
	, pageLevel as pageLevel
	, rowTag as rowTag
	, colTag as colTag

	, null as M
	, null as F
from #e

UNION ALL
Select DISTINCT
	5 as Tag
	, 4 as Parent
	, codeName as TableName
	, TableDef
	, ptCode as TableCode
	, ptPage as pageTag
	, pageLevel as pageLevel
	, rowTag as rowTag
	, colTag as colTag
	, ptM as M
	, ptF as F
from #e
) u
ORDER BY


 [Grid!1!Tag]
,  [Grid!1!TableDef]
,  [Grid!1!TableCode]
---,  [Grid!1!TableDataCode]
, [Values!2!ID!Hide]
, [values!2!Tag]
, [values!2!yearLevel]
, [row!3!Tag]
, [col!4!Tag]

for xml explicit
)
Select @XMLResult
drop table #e
END
GO
GRANT EXECUTE ON [dbo].[xmlPupilTables] TO [pSchoolRead] AS [dbo]
GO

