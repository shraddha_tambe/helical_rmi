SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[PIVRepeaters]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @data TABLE
(
-- now return the sum across these

	schNo nvarchar(50)
	, [Survey Year] int
	, LevelCode nvarchar(10)
	, Age int
	, enrolM int
	, enrolF int
	, repM int
	, repF int
	, Estimate int
	, [Age of Data] int NULL
	, [Year of Data] int NULL
	, [Survey Year Quality Level] int NULL
	, [Data Quality Level] int NULL
)

	INSERT INTO @data
	EXEC dbo.sp_PIVRepeaters


END
GO

