SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 2 2010
-- Description:	Recordset combining school levels enrolments with Projections
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[ProjectedEnrolment]
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50) = null,
	@Scenario int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #tmp
(
	schNo nvarchar(50)
	, RecordType int
	, EnrolYear int
	, Scenario	nvarchar(50)
	, EnrolM	int
	, EnrolF	int
	, Enrol		int
	, ISCED0	int
	, ISCED1	int
	, ISCED2	int
	, ISCED3	int
	, ISCED4	int
	, ISCED5	int
)

Select *
INTO #ee
FROM dbo.tfnESTIMATE_BestSurveyEnrolments()

INSERT INTO #tmp
Select schNo,Estimate, LifeYear, null
, sum(m) EnrolM
, sum(F) EnrolF
, sum(tot) Enrol
, sum(case ilCode when 'ISCED0' then Tot else null end)
, sum(case ilCode when 'ISCED1' then Tot else null end)
, sum(case ilCode when 'ISCED2' then Tot else null end)
, sum(case ilCode when 'ISCED3' then Tot else null end)
, sum(case ilCode when 'ISCED4' then Tot else null end)
, sum(case ilCode when 'ISCED5' then Tot else null end)

FROM
(
SELECT schNo, Estimate, LifeYear, ilCode, sum(enM) M, sum(enF) F, isnull(sum(enM),0) + isnull(sum(enF),0) Tot
from #ee
INNER JOIN Enrollments
	ON #ee.bestssID = Enrollments.ssID
INNER JOIN lkpLevels L
	On Enrollments.enLevel = L.codeCode
INNER JOIN ISCEDLevelSub
	ON L.ilsCode = ISCEDLevelSub.ilsCode

GROUP BY schNo, LifeYear, Estimate, ilCode
) Sub
GROUP BY SchNo, EStimate, LifeYear


INSERT INTO #tmp
Select schNo,2, epYear, escnName
, sum(m) EnrolM
, sum(F) EnrolF
, sum(tot) Enrol
, sum(case ilCode when 'ISCED0' then Tot else null end)
, sum(case ilCode when 'ISCED1' then Tot else null end)
, sum(case ilCode when 'ISCED2' then Tot else null end)
, sum(case ilCode when 'ISCED3' then Tot else null end)
, sum(case ilCode when 'ISCED4' then Tot else null end)
, sum(case ilCode when 'ISCED5' then Tot else null end)

FROM
(
SELECT schNo, epYear, escnName, ilCode, sum(epdM) M, sum(epdF) F, isnull(sum(epdM),0) + isnull(sum(epdF),0) + isnull(sum(epdU),0) Tot
FROM EnrolmentScenario SCN
INNER JOIN EnrolmentProjection EP
	ON SCN.escnID = EP.escnID
INNER JOIN EnrolmentProjectionData EPD
	ON ep.epID = EPD.epID
INNER JOIN lkpLevels L
	On EPD.epdLevel = L.codeCode
INNER JOIN ISCEDLevelSub
	ON L.ilsCode = ISCEDLevelSub.ilsCode

GROUP BY schNo, epYear, escnName, ilCode
) Sub
GROUP BY SchNo,  epYear, escnName

Select #tmp.*
, DS.schName
, DS.iName
, dS.dName
from #tmp
	INNER JOIN pSchoolRead.DimensionSchool DS
		ON #tmp.schNo = DS.schNo

DROP TABLE #tmp
DROP TABLE #ee


END
GO

