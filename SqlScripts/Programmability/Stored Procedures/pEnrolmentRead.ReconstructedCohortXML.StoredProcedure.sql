SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 6 2010
-- Description:	XML Reconstructed cohort data for a single school.
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[ReconstructedCohortXML]
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50),
	@Year int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  DECLARE @data TABLE
(
	schNo nvarchar(50)
	, svyYear int
	, YearOfEd int
	, LevelCode nvarchar(10)

	, EnrolM int  NULL
	, enrolF int NULL
	, Enrol int NULL

	, EnrolNYM int NULL
	, EnrolNYF int NULL
	, EnrolNY int NULL

	, EnrolNYNextLevelM int  NULL
	, EnrolNYNextLevelF int NULL
	, EnrolNYNextLevel int NULL

	, RepM int NULL
	, RepF  int NULL
	, Rep  int NULL

	, RepNYM int NULL
	, RepNYF int NULL
	, RepNY int NULL


	, RepNYNextLevelM int NULL
	, RepNYNextLevelF int NULL
	, RepNYNextLevel int NULL


	, Estimate smallint
	, [Year Of Data] int
	, [Age of Data] int
	, surveyDimensionssID int
	, [Estimate NY] smallint
	, [Year Of Data NY] int
	, [Age of Data NY] int


	, SlopedEnrolM int NULL
	, SlopedEnrolF int NULL
	, SlopedEnrol int NULL

	, RepeatRateM decimal(8,6) NULL
	, RepeatRateF decimal(8,6) NULL
	, RepeatRate decimal(8,6) NULL

	, PromoteRateM decimal(8,6) NULL
	, PromoteRateF decimal(8,6) NULL
	, PromoteRate decimal(8,6) NULL

	, seq int

	, TransitionRateM decimal(8,6) NULL
	, TransitionRateF decimal(8,6) NULL
	, TransitionRate decimal(8,6) NULL

)

INSERT INTO @Data
EXEC pEnrolmentRead.ReconstructedCohort 0, @SchoolNo, @Year


Select
DISTINCT
1 as Tag
,null as Parent
,schNo [ReconstructedCohort!1!schoolNo]
, svyYEar [ReconstructedCohort!1!year]
, null  [ClassLevel!2!levelCode]
, null [ClassLevel!2!yearOfEd]
, null [Rates!3!gender]
, null [Rates!3!enrol]
, null [Rates!3!slopedEnrol]
, null [Rates!3!rep]
, null [Rates!3!enrolNY]
, null [Rates!3!repNY]
, null [Rates!3!enrolNYNextLevel]
, null [Rates!3!repNYNextLevel]
, null [Rates!3!repeatRate]
, null [Rates!3!promoteRate]
, null [Rates!3!transitionRate]
from @Data

UNION ALL

SELECT
2 as Tag
,1 as Parent
,schNo [ReconstructedCohort!1!schoolNo]
, svyYEar [ReconstructedCohort!1!year]
, levelCode  [ClassLevel!2!levelCode]
, yearOfEd [ClassLevel!2!yearOfEd]
, null [Rates!3!gender]
, null [Rates!3!enrol]
, null [Rates!3!slopedEnrol]
, null [Rates!3!rep]
, null [Rates!3!enrolNY]
, null [Rates!3!repNY]
, null [Rates!3!enrolNYNextLevel]
, null [Rates!3!repNYNextLevel]
, null [Rates!3!repeatRate]
, null [Rates!3!promoteRate]
, null [Rates!3!transitionRate]

from @Data

UNION ALL

SELECT
3 as Tag
,2 as Parent
,schNo [ReconstructedCohort!1!schoolNo]
, svyYEar [ReconstructedCohort!1!year]
, levelCode  [ClassLevel!2!levelCode]
, yearOfEd [ClassLevel!2!yearOfEd]
, 'M' [Rates!3!gender]
, enrolM [Rates!3!enrol]
, slopedEnrolM [Rates!3!slopedEnrol]
, RepM [Rates!3!rep]
, enrolNYM [Rates!3!enrolNY]
, RepNYM [Rates!3!repNY]
, enrolNYNextLevelM [Rates!3!enrolNYNextLevel]
, RepNYNextLevelM [Rates!3!repNYNextLevel]
, repeatRateM [Rates!3!repeatRate]
, promoteRateM [Rates!3!promoteRate]
, transitionRateM [Rates!3!transitionRate]

from @Data

UNION ALL

SELECT
3 as Tag
,2 as Parent
,schNo [ReconstructedCohort!1!schoolNo]
, svyYEar [ReconstructedCohort!1!year]
, levelCode  [ClassLevel!2!levelCode]
, yearOfEd [ClassLevel!2!yearOfEd]
, 'F' [Rates!3!gender]
, enrolF [Rates!3!enrol]
, slopedEnrolF [Rates!3!slopedEnrol]
, RepF [Rates!3!rep]
, enrolNYF [Rates!3!enrolNY]
, RepNYF [Rates!3!repNY]
, enrolNYNextLevelF [Rates!3!enrolNYNextLevel]
, RepNYNextLevelF [Rates!3!repNYNextLevel]
, repeatRateF [Rates!3!repeatRate]
, promoteRateF [Rates!3!promoteRate]
, transitionRateF [Rates!3!transitionRate]

from @Data

UNION ALL

SELECT
3 as Tag
,2 as Parent
,schNo [ReconstructedCohort!1!schoolNo]
, svyYEar [ReconstructedCohort!1!year]
, levelCode  [ClassLevel!2!levelCode]
, yearOfEd [ClassLevel!2!yearOfEd]
, 'all' [Rates!3!gender]
, enrol [Rates!3!enrol]
, slopedEnrol [Rates!3!slopedEnrol]
, Rep [Rates!3!rep]
, enrolNY [Rates!3!enrolNY]
, RepNY [Rates!3!repNY]
, enrolNYNextLevel [Rates!3!enrolNYNextLevel]
, RepNYNextLevel [Rates!3!repNYNextLevel]
, repeatRate [Rates!3!repeatRate]
, promoteRate [Rates!3!promoteRate]
, transitionRate [Rates!3!transitionRate]

from @Data

ORDER BY [ReconstructedCohort!1!schoolNo]
, [ReconstructedCohort!1!year]
,  [ClassLevel!2!yearOfEd]
, [Rates!3!gender]
, Tag

FOR XML EXPLICIT
END
GO

