SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[SchoolTotalsXML]
	-- Add the parameters for the stored procedure here
	@ReportYear int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare @xml xml =
(
Select
Tag
, Parent
, stCode [SchoolType!1!type]
, schNo [S!2!n]
, dID [S!2!d]
, Enrol [S!2!e]
FROM
(
Select
1 Tag
, null Parent
, stCode
, null schNo
, null dID
, 0 Enrol
, 0 Est
FROM SchoolTypes
UNION

Select
2 Tag
, 1 Parent
, [SchoolTypeCode]
, EE.schNo
, [District Code]
, bestEnrol
, EE.Estimate
FROM
dbo.tfnESTIMATE_BestSurveyEnrolments() EE
INNER JOIN DimensionSchoolsurveyNoYear DSS
ON EE.surveyDimensionssId = DSS.[Survey Id]
WHERE EE.LifeYear = @ReportYear
) U

ORDER BY stCode, Enrol ASC
FOR XML EXPLICIT
)

Select @xml =
(
Select @xml
FOR XML PATH('Enrol')
)

Select @xml xmlEnrol
END
GO

