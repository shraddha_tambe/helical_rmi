SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 10 2009
-- Description:	xml representation of an Enrolment Scenario
-- =============================================
CREATE PROCEDURE [pEnrolmentRead].[xmlEnrolmentScenario]
	-- Add the parameters for the stored procedure here
	@ScenarioID int
	, @SchoolNo nvarchar(50) = null
	, @ProjectionYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


-- first create the output table

SELECT escnID, epID
INTO #Projn
FROM
EnrolmentPRojection
WHERE
	escnID = @ScenarioID
	AND (schNo = @SchoolNo or @schoolNo is null)
	AND (epYear = @ProjectionYear or @ProjectionYear is null)


create table #out
(
	Tag int
	, Parent int null
	, escnID int
	, escnName nvarchar(50)
	, escnDescription nvarchar(50)
	, epID int null
	, epYear int null
	, schNo nvarchar(50) null
	, dID nvarchar(10) null
	, epLocked bit null
	, epdAge int
	, epdLevel nvarchar(10)
	, epdM int null
	, epdF int null
	, epdU int null
)

-- first the header record
INSERT INTO #out
(Tag, Parent, escnID, escnName, escnDescription)
SELECT
	1
	, null
	, escnID
	, escnName
	, escnDescription
FROM
	EnrolmentScenario
	WHERE escnID = @ScenarioID


INSERT INTO #out
(Tag, Parent, escnID)
SELECT
	2
	, 1
	, escnID
FROM
	EnrolmentScenario
	WHERE escnID = @ScenarioID

INSERT INTO #out
(Tag, Parent, escnID,
epID, epYEar, schNo, dID, epLocked)
SELECT
	3
	, 2
	, #projn.escnID
	, #projn.epID
	, epYEar
	, schNo
	, dID
	, case when epLocked = 0 then null else epLocked end
FROM
	#projn
	INNER JOIN EnrolmentProjection
	ON #Projn.epID = EnrolmentProjection.epID


INSERT INTO #out
(Tag, Parent, escnID,
epID, epdAge)
SELECT DISTINCT
	4
	, 3
	, escnID
	, #projn.epID
	, case when epdAge = 0 then null else epdAge end
FROM
	#projn
	INNER JOIN EnrolmentProjectionData
	ON #projn.epId = EnrolmentProjectionData.epID
	WHERE escnID = @ScenarioID

INSERT INTO #out
(Tag, Parent, escnID,
epID, epdAge, epdLevel)
SELECT DISTINCT
	5
	, 4
	, escnID
	, #projn.epID
	, epdAge
	, epdLevel
FROM
	#projn
	INNER JOIN EnrolmentProjectionData
	ON #projn.epId = EnrolmentProjectionData.epID

INSERT INTO #out
(Tag, Parent, escnID,
epID, epdAge, epdLevel, epdM, epdF, epdU)
SELECT
	6
	, 5
	, escnID
	, #projn.epID
	, epdAge
	, epdLevel
	, case when epdM = 0 then null else epdM end
	,  case when epdF = 0 then null else epdF end
	,  case when epdU = 0 then null else epdU end
FROM
	#Projn
	INNER JOIN EnrolmentProjectionData
	ON #projn.epId = EnrolmentProjectionData.epID

declare @xmlResult xml

set @xmlResult =
(
Select Tag
, Parent
, escnID as [EnrolmentScenario!1!ID]
, escnName as [EnrolmentScenario!1!Name]
, escnID as [Projections!2!ID!Hide]
, epID as [Projection!3!ID]
, epYear as [Projection!3!projectionYear]
, schNo as [Projection!3!schoolNo]
, dID as [Projection!3!district]
, epLocked as [Projection!3!locked]
, epID as [row!4!ID!Hide]
, epdAge as [row!4!age]
, epID as [col!5!ID!Hide]
, epdLevel as [col!5!classLevel]
, epID as [data!6!ID!Hide]
, epdM as [data!6!M]
, epdF as [data!6!F]
, epdU as [data!6!U]

from
	#out
ORDER BY
	[EnrolmentScenario!1!ID]
	, [Projections!2!ID!Hide]
	, [Projection!3!ID]
	, [row!4!age]
	, [col!5!classLevel]
	, Tag

FOR XML EXPLICIT
)


Select @xmlREsult xmlResult
drop table #out
drop table #projn
END
GO

