SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pEnrolmentWrite].[PupilTableSave]
	-- this version is used in the web site, and takes slightly different
	-- Add the parameters for the stored procedure here
	@SchoolNo nvarchar(50),
	@SurveyYear int,
	@TableName nvarchar(50),
	@grid xml ,
	@user nvarchar(50) = null
AS
BEGIN
	SET NOCOUNT ON;
	-- test here if we need to create the survey....

	select @user = isnull(@user, original_login())

	declare @ssId int

	DECLARE @Survey TABLE
	(
		ssID int
	)
	Select @ssID = ssID

	from SchoolSurvey
	WHERE schNo = @schoolNo
	AND svyYear = @SurveyYear

	if (@ssId is null)
		INSERT INTO @Survey
		exec pSurveyWrite.CreateSchoolSurvey @SchoolNo, @SurveyYear

	if @TableName = 'ENROL' begin
		exec pEnrolmentWrite.UpdateEnrolment2 @SchoolNo, @SurveyYear, @grid, @user

	end
	else begin
		exec pEnrolmentWrite.PupilTableSave2 @SchoolNo, @SurveyYear, @TableName, @grid, @user
	end
	exec pEnrolmentRead.PupilTableRead  @SchoolNo, @SurveyYear, @TableName

END
GO

