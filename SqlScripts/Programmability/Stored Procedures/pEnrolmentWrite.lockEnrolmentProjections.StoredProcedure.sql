SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 /2/2010
-- Description:	Lock a set of enrolment projections
-- =============================================
CREATE PROCEDURE [pEnrolmentWrite].[lockEnrolmentProjections]
	-- Add the parameters for the stored procedure here
	@ScenarioID int = 0,
	@Year int = 0,
	@SchoolFilter xml = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @UseFilter int
	CREATE TABLE #schools
	(
		schNo nvarchar(50)
	)
	select @UseFilter = 0

	if @SchoolFilter is not null begin

		INSERT INTO #Schools
		EXECUTE pSchoolRead.stdSchoolfilter @SchoolFilter
		select @UseFilter = 1

		Select * from #schools
	end

    -- Insert statements for procedure here
	UPDATE EnrolmentProjection
	SET epLocked = 1
	WHERE escnID =  @ScenarioID
		AND (epYear = @YEar or @Year is null)
		AND (@UseFilter = 0 or schNo = any (Select schNo from #Schools))

END
GO

