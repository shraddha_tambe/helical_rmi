SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 05 2010
-- Description:
-- =============================================
CREATE PROCEDURE [pEstablishmentRead].[WorkforceAuthorityTotals]
	-- Add the parameters for the stored procedure here
	@AsAtDate datetime = null ,
	@ColumnType int = 0,
	@FilledOnly int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @str nvarchar(4000)
	declare @strTot nvarchar(4000)
	declare @strSum nvarchar(4000)
	declare @ColumnField sysname

	declare @FilledOnlyClause nvarchar(4000)

	declare @BlankToken nvarchar(20)

	Select @BlankToken = '(blank)'


if (@AsAtDate is null)
	Select @AsAtDate = common.today()


If @FilledOnly = 1
	Select @FilledOnlyClause = ' AND estpNo in (Select estpNo from TeacherAppointment WHERE taDate <= @P1 and (taEndDate >= @P1 or taEndDate is null))'
else
	Select @FilledOnlyClause = ''


CREATE TABLE #extbColumnNames
(
	col nvarchar(50)
	, sorter int
)
if (@ColumnType = 0) begin

	INSERT INTO #extbColumnNames
	Select DISTINCT rgSalaryLevelRange
	, 1
	FROM RoleGrades
	ORDER BY
	rgSalaryLevelRange

	Select @ColumnField = 'rgSalaryLevelRange'
end

if (@ColumnType = 1) begin
	INSERT INTO #extbColumnNames
	Select DISTINCT codeCode
	, row_number() OVER (ORDER BY secSort, R.secCode, R.codeSort, codeCode)
	FROM lkpTeacherRole R
	LEFT JOIN EducationSectors ES
		ON R.secCode = ES.secCode

	WHERE codeCode in
	(Select roleCode from RoleGrades

		WHERE rgCode in (Select estpRoleGrade from Establishment)
		OR rgCode in (Select estrRoleGrade from SchoolEstablishmentRoles)
	)

	Select @ColumnField = 'roleCode'

end

if (@ColumnType = 2) begin
	INSERT INTO #extbColumnNames
	Select DISTINCT secCode
	, row_number() OVER (ORDER BY secSort)
	FROM EducationSectors

	Select @ColumnField = 'secCode'
end


INSERT INTO #extbColumnNames
VALUES (@BlankToken, 0)


	Select  @str = isnull(@str,'') + quotename(col,'[') + ', '
		, @strTot = isnull(@strTot,'') +' sum('+ quotename(col,'[') + ')' + quotename(col,'[') + ', '
		, @strSum = isnull(@strSum,'') + 'isnull(' + quotename(col,'[')  + ',0) + '
	from #extbColumnNames
	ORDER BY sorter, col

-- get rid of the trailing spaces and commas
	Select @str = left(@str, len(rtrim(@str)) - 1)
	Select @strTot = left(@strTot, len(rtrim(@strTot)) - 1)
	Select @strSum = left(@strSum, len(rtrim(@strSum)) - 1)


	declare @strSQL nvarchar(4000)
	Select @strSQL =
	'

Select isnull(pt.Auth,#####) rowCode, isnull(authName,#####) rowName, ***** Total, pt.*
INTO #planAuthTotals
from
(
Select isnull([@@@@@],#####) [@@@@@], count(*) Num, case when estpScope is not null then estpAuth else schAuth end Auth  from Establishment E
LEFT JOIN Schools S
	ON S.schNo = E.schNo
LEFT JOIN RoleGrades RG
ON RG.rgCode = E.estpRoleGrade
LEFT JOIN lkpTeacherRole R
	ON RG.roleCode = R.codeCode
WHERE estpActiveDate <= @P1
	AND (estpClosedDate >= @P1 or estpClosedDate is null)
!!!!!
GROUP BY case when estpScope is not null then estpAuth else schAuth end, [@@@@@]
) R
PIVOT
(
	sum(Num)
FOR [@@@@@]  in (%%%%%)
) pt
LEFT JOIN TRAuthorities A
ON pt.Auth = A.authCode
ORDER BY rowName;
Select * from #PlanAuthTotals;
Select $$$$$ , sum(Total) Total
FROM #PlanAuthTotals;
DROP TABLE #PlanAuthTotals

'
select @strSQL = replace(@strSQL,'#####', quotename(@BlankToken,''''))

select @strSQL = replace(@strSQL,'@@@@@', @ColumnField)

select @strSQL = replace(@strSQL,'%%%%%', @str)

select @strSQL = replace(@strSQL,'$$$$$', @strTot)

select @strSQL = replace(@strSQL,'*****', @strSum)

select @strSQL = replace(@strSQL,'!!!!!', @FilledOnlyClause)

print @strSQL

declare  @ParmDef nvarchar(400)
Select @ParmDef = N'@P1 datetime'

exec sp_executesql @strSQL, @ParmDef, @P1 = @AsAtDate


Select * from #extbColumnNames ORDER BY sorter, col
DROP TABLE #extbColumnNames
END
GO

