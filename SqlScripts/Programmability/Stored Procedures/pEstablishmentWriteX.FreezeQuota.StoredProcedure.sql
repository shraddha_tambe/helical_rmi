SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 25 10 2009
-- Description:	Freeze or unfreeze an establishment quota
-- =============================================
CREATE PROCEDURE [pEstablishmentWriteX].[FreezeQuota]
	-- Add the parameters for the stored procedure here
	@schoolNo nvarchar(50)
	, @Year int
	, @UnFreeze bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @err int,
	@ErrorMessage NVARCHAR(4000),
	@ErrorSeverity INT,
	@ErrorState INT;

declare @estID int


	begin try

		begin transaction

		SELECT @estID = estID
		FROM
			SchoolEstablishment
		WHERE
			schNo = @schoolNo
			AND estYear = @Year


		if (@Unfreeze = 1) begin

			UPDATE SchoolEstablishmentRoles
				SET estrCountUser = null
			FROM
				SchoolEstablishmentRoles
				WHERE estID = @estID

		end

		if (@Unfreeze = 0) begin

			UPDATE SchoolEstablishmentRoles
				SET estrCountUser = estrQuota
			FROM
				SchoolEstablishmentRoles
				WHERE estID = @estID


		end

		DELETE FROM SchoolEstablishmentRoles
		WHERE
			estID = @estID
			AND estrQuota is null
			AND estrCountUser is null

		UPDATE SchoolEstablishment
			SET estLocked = case @Unfreeze when 1 then 0 when 0 then 1 end
			WHERE estID = @estID

		commit transaction
	end try
--- catch block
	begin catch

		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch


	-- commit open transaction if any
	if @@trancount > 0
		commit transaction
END
GO

