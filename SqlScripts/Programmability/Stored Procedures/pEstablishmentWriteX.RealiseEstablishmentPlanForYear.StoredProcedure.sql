SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-09-11
-- Description:	create and align establishment positions
-- =============================================
CREATE PROCEDURE [pEstablishmentWriteX].[RealiseEstablishmentPlanForYear]
	-- Add the parameters for the stored procedure here
	@EstYear int
	, @ReportOnly int = 0
	, @SchoolNo nvarchar(50) = null
	, @Authority nvarchar(10) = null
	, @SchoolType nvarchar(5) = null
WITH EXECUTE AS 'pineapples'	-- need this to escalate security to allow disable/enable trigger
AS
BEGIN
	SET NOCOUNT ON;
	SET ANSI_WARNINGS OFF;

	declare @CurrentPositions int
	declare @nextSeq int
	declare @AuthorityDate datetime
	declare @TerminateDate datetime

	declare @NumPositions int

	-- statistics to return
	declare @PositionsCreated int
	declare @PositionsClosed int
	declare @PositionsRegraded int
	declare @SchoolsInBatch int
	declare @SchoolsToChange int
	declare @SchoolsRemaining int

declare  @data table
(
	schNo nvarchar(50)
	, roleGrade nvarchar(10)
	, numPlanned int
	, numPositions int
	, numFilled int
	, numFilledSalaryPointComplies int
	, numFilledUnderPay int
	, NumUnfilled int
	, NumRequired int
	, FilledPayFN money
	, FuturePay money
	, FuturePaySalaryPointComplies money		-- the future pay of those who are not currently underpaid


)

declare @PositionsToClose table
(
	estpNo nvarchar(20)
)
begin try;

	-- first , how many active positions are there?
	DISABLE TRIGGER ALL
		On dbo.Establishment

	begin transaction

	Select @AuthorityDate = estcAuthorityDate
	, @TerminateDate = isnull(estcTerminateDate,common.dateserial(@EstYear,1,0))
	from EstablishmentControl
	WHERE estcYear = @EstYEar

	if (@AuthorityDate is null)
		raiserror('Authority date not set in Establishment Year setup',16,1)


-- get the working data
		insert into @data
		exec pEstablishmentRead.PlannedEstablishmentData @estYear
					, 1				-- use authority date
					, @SchoolNo
					, @Authority
					, @SchoolType

Select @SchoolsInBatch = count(DISTINCT SchNo)
FROM
	@Data D

Select @SchoolsToChange = count(DISTINCT SchNo)
FROM
	@Data D
WHERE NumREquired <> 0


/***********************************************************************************
New Positions
************************************************************************************/
-- first work out how many new positions at each school


	Select SchNo
		, sum(NumRequired) TotalNewPos
	Into #totalNew
	FROM @data D
	WHERE NumRequired > 0
	GROUP BY D.schNo

-- get a temp table that has all the new required position numbers
	Select #totalNew.SchNo
		, T.counter
		, t.PosNo
	INTO #NewPosNums
	FROM #totalNew
		CROSS APPLY
			-- this function gets the next nnn numbers for a given school, the record also contains a counter
			 dbo.MakeEstablishmentNumbers(#totalNew.schNo, #totalNew.TotalNewPos) T
		ORDER BY T.posNo


-- now we can insert the new positions
	INSERT INTO Establishment
		(EstpNo, schNo, estpRoleGrade, estpActiveDate, estpTitle)
	SELECT
		#NewPosNums.PosNo
		, PData.SchNo
		, PData.RoleGrade
		, @AuthorityDate
		, PData.RoleGradeDesc
	FROM
	(
	Select

		D.schNo
		, D.RoleGrade
		, RG.rgDescription RoleGradeDesc
		, row_number() over (PARTITION BY D.SchNo ORDER By D.RoleGrade) RowNum
	from @data D
	LEFT JOIN RoleGrades RG
		ON D.RoleGrade = RG.rgCode
	CROSS JOIN metaNumbers N

	WHERE N.num >= 1 and N.num <= D.NumRequired			-- this forces NumREquired <= 1
	) PData

	INNER JOIN #NewPosNums
		ON #NewPosNums.schNo = PData.SchNo
		AND #NewPosNums.counter = PData.RowNum


	Select @PositionsCreated = @@ROWCOUNT

	print @PositionsCreated
/***********************************************************************************
Close Positions
************************************************************************************/
-- we keep these in a temp table to help with regrades
INSERT INTO @PositionsToClose
Select estpNo
		from
		(
			SELECT
				E.estpNo
				, E.schNo
				, E.estpRoleGrade
				, row_number() OVER (Partition BY E.schNo, E.estpRoleGrade ORDER BY E.estpNo )RN
				, numPlanned
				, numPositions
				, isnull(numPositions,0) - isnull(numPlanned,0) Net
			FROM Establishment E
			INNER JOIN @data D
				on D.schNo = E.schNo
				and D.roleGrade = E.estpRoleGrade
			WHERE (E.schNo = @schoolNo or @SchoolNo is null)
				and estpClosedDate is null
				and E.estpNo not In
					(Select estpNo from TeacherAppointment
						WHERE (taEndDate is null
								or taEndDate >= @AuthorityDate)
								and estpNo is not null
					)
		) sub
		WHERE Net > 0
			and sub.RN <= Net

-- to make the terminate date work correctly, terminate on Terminate date if possible
-- Otherwise terminaet on @AuthorityDate - 1
	UPDATE Establishment
		SET estpClosedDate = @TerminateDate
	WHERE estpNo IN
	(
		Select estpNo FROM @PositionsToClose
	)
	AND EstpNo NOT in
	(
		Select estpNo from TeacherAppointment
			WHERE
				taEndDate > @TerminateDate
				and estpNo is not null
	)

	Select @PositionsClosed = @@ROWCOUNT
	-- this second pass deals with the (remote) possibility that the appointment ends betweenthe terminate and authority dates
	UPDATE Establishment
		SET estpClosedDate = dateadd(day, -1 , @AuthorityDate)
	WHERE estpNo IN
	(
		Select estpNo FROM @PositionsToClose
	)
	AND EstpNo in
	(
		Select estpNo from TeacherAppointment
			WHERE
				taEndDate > @TerminateDate
				and estpNo is not null
	)

	Select @PositionsClosed = @PositionsClosed + @@ROWCOUNT

print 'End Closes'
/***********************************************************************************
Regrades
************************************************************************************/
	Select Pos.schNo
			, pos.RoleGrade OldRoleGrade
			, pos.estpNo OldPos
			, pos.taID
			, pos.tID
			, pos.spCode
			, thePlan.roleGrade NewRoleGrade
			, thePlan.estpNo NewPos
		--, thePlan.*
	INTO #regrades
	FROM
	(
		Select D.*
			, RN.roleCode
			, E.estpNo
			, Appts.taID
			, Appts.tID
			, Appts.spCode
		from @data D
			INNER JOIN RoleGrades RN
			ON D.RoleGrade = RN.rgCode
			INNER JOIN Establishment E
				ON D.roleGrade = E.estpRoleGrade
				AND D.schNo = E.schNo
			INNER JOIN (Select * from TeacherAppointment WHERE taDate <= @authorityDate
							AND ( @AuthorityDate <= taEndDate or taEndDate is null) ) Appts
				ON Appts.estpNo = E.estpNo
			INNER JOIN @PositionsToClose PP
				ON PP.estpNo = E.estpNo
		WHERE
			D.NumPositions = 1  and isnull(D.NumPlanned,0) = 0 and D.NumFilled = 1
			-- only consider positions terminated in this Activity

	) Pos
	INNER JOIN
		 (
			Select D.*
				, RN.roleCode
				, E.estpNo
			from @data D
				INNER JOIN RoleGrades RN
				ON D.RoleGrade = RN.rgCode
			INNER JOIN Establishment E
				ON D.roleGrade = E.estpRoleGrade
				AND D.schNo = E.schNo
			WHERE
				isnull(D.NumPositions,0) = 0  and D.NumPlanned = 1
				-- new position is active at the authority date
				AND (E.estpActiveDate <= @authorityDate
							AND ( @AuthorityDate <= E.estpClosedDate or E.estpClosedDate is null)
					)
		) thePlan
	ON Pos.schNo = thePlan.Schno
		AND POS.roleCode = thePlan.roleCode

-- now that we have identified the regrades, we should terminate the current appointment and shift to the new position
--
SELECT @PositionsREgraded = count(*)
from #regrades


if (@PositionsRegraded > 0) begin
		UPDATE TeacherAppointment
			SET taEndDate = dateadd(day, -1, @AuthorityDate)
		WHERE
			taID in (Select taID from #regrades)
			--AND tStartDate < @AuthorityDate

	print 'before update estab'

		UPDATE Establishment
			SET estpClosedDate = dateadd(day, -1, @AuthorityDate)
		WHERE
			estpNo in (Select OldPos from #regrades)


			INSERT INTO TeacherAppointment
				(tID, estpNo, taDate, spCode)
			SELECT TID, NewPos, @AuthorityDate, spCode
			FROM #regrades


			select @PositionsRegraded = @@ROWCOUNT
end

if (@ReportOnly = 0)
	commit transaction;

	ENABLE TRIGGER ALL
		On dbo.Establishment

-- we'll return two recordsets
-- the first is the list of schools that are NOT realised - these require manual attention
-- the second is statistics

-- get the working data again
		DELETE from @data

		insert into @data
		exec pEstablishmentRead.PlannedEstablishmentData @estYear
					, 1				-- use authority date
					, @SchoolNo
					, @Authority
					, @SchoolType

-- return the schools


Select @SchoolsRemaining = count(DISTINCT SchNo)
FROM
	@Data D
WHERE NumREquired <> 0


Select @SchoolsInBatch SchoolsInBatch
		, @SchoolsToChange SchoolsToChange
		, @PositionsCreated PositionsCreated
		, @PositionsClosed PositionsClosed
		, @PositionsRegraded PositionsRegraded
		, @SchoolsRemaining SchoolsRemaining
		, @ReportOnly ReportOnly

Select schNo
	, schName
	, Schtype
	, schAuth
FROM
	Schools
WHERE
	schNo IN
			(
			SELECT DISTINCT schNo
			from @data D
			WHERE NumRequired <> 0
			)

if (@ReportOnly = 1)
	rollback transaction


	end try
	begin catch

		ENABLE TRIGGER ALL ON Establishment
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		if @@trancount > 0
			begin
				rollback transaction
				select @errorMessage = @errorMessage + ' The transaction was rolled back.'
			end

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		return @err
	end catch

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

    -- Insert statements for procedure here
END
GO

