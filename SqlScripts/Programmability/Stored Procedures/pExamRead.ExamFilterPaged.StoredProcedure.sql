SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 13 03 2018
-- Description:	Exam filter
--              TODO - Add ability to search by schools returning exams they participated in
-- =============================================
CREATE PROCEDURE [pExamRead].[ExamFilterPaged]
	-- Add the parameters for the stored procedure here

	@Columnset int = 1,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@ExamID int = null,
    @ExamCode nvarchar(10) = null,
    @ExamYear int = null,
    @ExamDate datetime = null
AS
BEGIN
	SET NOCOUNT ON;

		DECLARE @keys TABLE
	(
		Id int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pExamRead.ExamFilterIDS
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir

		, @ExamID
		, @ExamCode
		, @ExamYear
		, @ExamDate

--- column sets ----
	SELECT E.exID
	, E.exCode
	, E.exYear
	, E.exDate
	, E.exCandidates
	, E.exMarks
	FROM Exams E
		INNER JOIN @keys K
			ON E.exID = K.ID
	ORDER BY recNo


--- summary --------
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
END
GO

