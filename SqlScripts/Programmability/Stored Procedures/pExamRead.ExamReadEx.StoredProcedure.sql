SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 18 04 2018
-- Description:	Extended read of exam for web site
-- =============================================
CREATE PROCEDURE [pExamRead].[ExamReadEx]
	@ExamID int
AS
BEGIN

	SET NOCOUNT ON;

    Select *
 	FROM Exams E
	WHERE exID = @examID

	-- TODO Any other table we could pull here?

END
GO

