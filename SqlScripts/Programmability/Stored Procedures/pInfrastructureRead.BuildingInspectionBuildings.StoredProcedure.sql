SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 4 1 2009
-- Description:
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[BuildingInspectionBuildings]
	-- Add the parameters for the stored procedure here
	@inspID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT Buildings.bldID
	  , Buildings.bldgCode
	  , Buildings.bldgTitle
	  , Buildings.bldgDescription
	  , bldgL
	  , bldgW
	  , bldgR
	  , bldgClosed		-- we need to be able to distinguish if the building was closed on this inspection
	  , bldgCloseDate
	  , brevClosed
	  , [brevID]
      ,case when brevID is null then bldgCode else brevCode end Code
      , case when brevID is null then bldgSubType else brevSubType end SubType
      ,case when brevID is null then bldgTitle else brevTitle end Title
      ,case when brevID is null then bldgDescription else brevDescription end [Description]
      ,case when brevID is null then bldgL else brevL end L
      ,case when brevID is null then bldgW else brevW end W
      ,case when brevID is null then bldgA else brevA end A
      ,case when brevID is null then bldgH else brevH end H
      ,case when brevID is null then bldgR else brevR end R
      ,case when brevID is null then bldgLevels else brevLevels end Levels
      ,case when brevID is null then bldgLat else [brevLat] end gpsLat
      ,case when brevID is null then bldgLong else [brevLong] end gpsLong
       ,case when brevID is null then bldgMatW else brevMatW end MatW
       ,case when brevID is null then bldgMatF else brevMatF end MatF
       ,case when brevID is null then bldgMatR else brevMatR end MatR
       , case when brevID is null then bldgClosed else brevClosed end Closed
      ,[brevNoteW]
      ,[brevNoteF]
      ,[brevNoteR]
      , brevSeq Seq
      ,BR.[bvcCode]
      ,[brevYear]
      ,case when brevID is null then bldgCondition else [brevCondition] end Condition
      ,[brevNote]
      , LastInspection
  FROM Buildings
	INNER JOIN SchoolInspection SI
		ON Buildings.schNo = SI.schNo
		AND SI.inspID = @inspID
	LEFT JOIN (select bldID, max(inspStart) LastInspection from
			SchoolInspection SI2
			INNER JOIN BuildingReview ON SI2.inspID = BuildingReview.inspID
			GROUP BY bldID) LastInsp
		on LastInsp.bldID = Buildings.bldID
	LEFT JOIN (Select * from BuildingReview WHERE inspID = @inspID) BR
		ON Buildings.bldID = BR.bldID

  WHERE
	-- filter out closed buildings
	-- building is ommitted if it was closed before this inspection, and is not part of this inspection
	bldgClosed = 0
	or brevID is not null
	or bldgCloseDate >= SI.inspStart
    -- Insert statements for procedure here
END
GO

