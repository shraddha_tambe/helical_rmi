SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 16 2 2009
-- Description:
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[BuildingPlan]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select *
INTO #tmp

FRom
(
Select SchNo
		, [bldID]
      ,[bldgCode]
      ,[bldgTitle]
      ,[bldgDescription]
      ,[bldgSubType]
	, 'B' RecordType
	  ,bldgMaterials
	  , bldgYear
      ,[bldgRoomsClass]
      ,[bldgRoomsOHT]
      ,[bldgRoomsStaff]
      ,[bldgRoomsAdmin]
      ,[bldgRoomsStorage]
      ,[bldgRoomsDorm]
      ,[bldgRoomsKitchen]
      ,[bldgRoomsDining]
      ,[bldgRoomsLibrary]
      ,[bldgRoomsHall]
      ,[bldgRoomsSpecialTuition]
      ,[bldgRoomsOther]
from Buildings
UNION ALL
	Select WorkITems.SchNo
	, WorkITems.[bldID]
      ,[bldgCode]
      ,[bldgTitle]
      ,[bldgDescription]
          ,[bldgSubType]
	, 'W' RecordType
	  ,bldgMaterials
	  , bldgYear

           ,[witmRoomsClass]
      ,[witmRoomsOHT]
      ,[witmRoomsStaff]
      ,[witmRoomsAdmin]
      ,[witmRoomsStorage]
      ,[witmRoomsDorm]
      ,[witmRoomsKitchen]
      ,[witmRoomsDining]
      ,[witmRoomsLibrary]
      ,[witmRoomsSpecialTuition]
      ,[witmRoomsHall]
      ,[witmRoomsOther]
From WorkItems LEFT JOIN Buildings
	ON WorkITems.bldID = Buildings.bldID
) U

Select * from #tmp ORDER BY schNo,  RecordType, BldID

DROP TABLE #tmp
    -- Insert statements for procedure here
END
GO

