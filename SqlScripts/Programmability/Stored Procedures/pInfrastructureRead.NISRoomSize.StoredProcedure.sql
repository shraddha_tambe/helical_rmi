SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 12 2007
-- Description:	NIS Room size
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[NISRoomSize]
	-- Add the parameters for the stored procedure here
	@SurveyYear int = null,
	@SchNo nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- we gein by collecting the 3 estimate queries
-- for anyrooms, enrolments and teachers
Select *
INTO #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

Select *
INTO #ebsr
from dbo.tfnESTIMATE_BestSurveyAnyRooms(@SurveyYear, @SchNo)

Select *
INTO #ebst
from dbo.tfnESTIMATE_BestSurveyTeachers(@SurveyYear, @SchNo)


Select
ER.LifeYear [Survey Year],
ER.SchNo,
ER.bestssID [roomDatassID],
ER.surveyDimensionssID,
EE.bestssID [enroldatassID],
ET.bestssID [teacherdatassID],
STD.stdName,
STD.stdItem,
STD.stdYear AS [Standard Year],
STD.stdValue AS MinSize,
std.stdValueMax AS [MaxSize],
std.stdperPupil AS PerPupil,
std.stdperTeacher AS PerTeacher,
stdProRateCeiling AS ProRataMax,
SS.ssEnrol Enrol,
TT.NumTeachers,
dbo.fnNISEffectiveMin(stdValue, stdPerPupil, stdPerTeacher, stdProRateCeiling, ssEnrol, NumTeachers) EffectiveMinSize,
R.rmType RoomType,
R.rmSize RoomSize,
R.rmNo AS RoomNo,
R.rmCondition as RoomCondition,


ER.bestYear AS [Room Data Year],
EE.bestYear AS [Year of Enrolment Data] ,
1 AS [Number Of Rooms],
--lkpConditionCodes.codeDescription AS RoomCondition,
case when R.rmSize is null then 0 else 1 end SizeSupplied,
case when R.rmSize is null then null
	 when R.rmSize < dbo.fnNISEffectiveMin(stdValue, stdPerPupil,
		stdPerTeacher, stdProRateCeiling, ssEnrol, NumTeachers)
 then 0
	when R.rmSize > isnull(stdValueMax,99999999) then 0
	else 1
end 	AS SizeComplies, -- we got there!!!
case when R.rmSize < dbo.fnNISEffectiveMin(stdValue, stdPerPupil,
		stdPerTeacher, stdProRateCeiling, ssEnrol, NumTeachers)
 then 'too small'
	when R.rmSize> stdValueMax then 'too big'
	else null
end
 AS NonComplianceReason,
case when r.rmSize>stdValueMax then 1 else 0 end  AS TooLarge,
case when r.rmSize<dbo.fnNISEffectiveMin(stdValue, stdPerPupil,
		stdPerTeacher, stdProRateCeiling, ssEnrol, NumTeachers)
then 1 else 0 end AS TooSmall


FROM #ebsr ER
INNER JOIN #ebse EE
	ON ER.schNo = EE.schNo AND ER.LifeYear = EE.LifeYear
INNER JOIN #ebst ET
	ON ER.schNo = ET.schNo AND ER.LifeYear = ET.LifeYear
INNER JOIN schoolyearApplicableStandards STD
	ON ER.LifeYear = STD.LifeYear AND ER.schNo = STD.schNo
INNER JOIN Rooms R
	ON ER.bestssID = R.ssID AND STD.stdItem = R.rmType
INNER JOIN SchoolSurvey SS
	ON EE.bestssID = SS.ssID	-- to get teh aggregate enrolment
INNER JOIN (Select ssID, count(tchsID) numTeachers from TeacherSurvey group by ssID) TT
	ON ET.bestssID = TT.ssID
WHERE STD.stdName = 'ROOM_SIZE'
	-- don;t consider classrooms in this calculation
--	AND STD.stdItem <> 'CLASS'
------------ these extra conditions are redundant logially, but do improve performance
----------	AND (STD.LifeYear = @SurveyYear or @SurveyYear is null)
----------	AND (STD.schNo = @Schno or @schNo is null)

END
GO

