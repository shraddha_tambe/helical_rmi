SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 20 12 2007
-- Description:	NIS Toilets
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[NISToilets]
	-- Add the parameters for the stored procedure here
	@SurveyYear int = null,
	@SchNo nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
-- we gein by collecting the 3 estimate queries
-- for anyrooms, enrolments and teachers
Select *
INTO #ebse
from dbo.tfnESTIMATE_BestSurveyEnrolments()

Select *
INTO #ebsToi
from dbo.tfnESTIMATE_BestSurveyToilets()

Select *
INTO #ebst
from dbo.tfnESTIMATE_BestSurveyTeachers(@SurveyYear, @SchNo)

Select subq.*,
toiNum as NumToCount,
case when toiNum is null then null
	 when toiNum < EffectiveMinNum  then 0
	when toiNum > isnull([MaxSize],99999999) then 0
	else 1
end 	AS NumComplies, -- we got there!!!
case when toiNum < EffectiveMinNum
 then 'too small'
	when toiNum> [MaxSize] then 'too big'
	else null
end
 AS NonComplianceReason,
case when toiNum >[MaxSize] then 1 else 0 end  AS TooLarge,
case when toiNum < EffectiveMinNum then 1 else 0 end AS TooSmall
from
(
Select
ER.LifeYear [Survey Year],
ER.SchNo,
ER.bestssID [roomDatassID],
ER.surveyDimensionssID,
ER.bestyear [Room Data Year],
EE.bestssID [enroldatassID],
ET.bestssID [teacherdatassID],
STD.stdName,
STD.stdItem,
STD.stdYear AS [Standard Year],
STD.stdValue AS MinSize,
std.stdValueMax AS [MaxSize],
std.stdperPupil AS PerPupil,
std.stdperTeacher AS PerTeacher,
stdProRateCeiling AS ProRataMax,
case when stdItem = 'Boys' then ssEnrolM
	when stdItem = 'Girls' then ssEnrolF
	else null
end Enrol,
TT.NumTeachers,
dbo.fnNISEffectiveMin(stdValue, stdPerPupil, stdPerTeacher, stdProRateCeiling,
case when stdItem = 'Boys' then ssEnrolM
	when stdItem = 'Girls' then ssEnrolF
	else null
end, NumTeachers) EffectiveMinNum,
toiUse ToiletUse,
toiNum


FROM #ebsToi ER
INNER JOIN #ebse EE
	ON ER.schNo = EE.schNo AND ER.LifeYear = EE.LifeYear
INNER JOIN #ebst ET
	ON ER.schNo = ET.schNo AND ER.LifeYear = ET.LifeYear
INNER JOIN schoolyearApplicableStandards STD
	ON ER.LifeYear = STD.LifeYear AND ER.schNo = STD.schNo
INNER JOIN Toilets R
	ON ER.bestssID = R.ssID AND STD.stdItem = R.toiUse
INNER JOIN SchoolSurvey SS
	ON EE.bestssID = SS.ssID	-- to get teh aggregate enrolment
INNER JOIN (Select ssID, count(tchsID) numTeachers from TeacherSurvey group by ssID) TT
	ON ET.bestssID = TT.ssID
WHERE STD.stdName = 'Toilets'
) subq
END
GO

