SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 11 1 2010
-- Description:	Retireve Work Orders based on Criteria
-- =============================================
CREATE PROCEDURE [pInfrastructureRead].[WorkItemFilter]

-- parameters to select by Work Order

@Status nvarchar(15) = null
, @SupplierCode nvarchar(10) = null
, @FundSource nvarchar(15) = null
, @ManagedBy int = null

, @PlannedDateStart datetime = null, @PlannedDateEnd datetime = null
, @WorkApprovedDateStart datetime = null, @WorkApprovedDateEnd datetime = null
, @FinanceApprovedDateStart datetime = null, @FinanceApprovedDateEnd datetime = null
, @TenderCloseDateStart datetime = null, @TenderCloseDateEnd datetime = null
, @ContractDateStart datetime = null, @ContractDateEnd datetime = null
, @CommencedDateStart datetime = null, @CommencedDateEnd datetime = null
, @PlannedCompletionDateStart datetime = null, @PlannedCompletionDateEnd datetime = null
, @CompletionDateStart datetime = null, @CompletionDateEnd datetime = null
, @SignOffDateStart datetime = null, @SignOffDateEnd datetime = null

, @BudgetCostMin money = null, @BudgetCostMax money = null
, @ContractCostMin money = null, @ContractCostMax money = null
, @InvoiceCostMin  money = null, @InvoiceCostMax money = null
, @VarianceMin  money = null, @VarianceMax money = null

, @SchoolNo nvarchar(50) = null
, @WorkItemType nvarchar(15) = null
, @Progress nvarchar(15) = null
, @BuildingID int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

begin try

	SELECT WorkOrders.woID
	, woRef
	, woStatus
	, woDesc
	, woPlanned
	, woBudget
	, woWorkApproved
	, woFinanceApproved
	, woSourceOfFunds
	, woCostCentre, woTenderClose, woContracted, supCode, woContractValue
	, woCommenced, woPlannedCompletion, woCompleted, woInvoiceValue
	, woSignoff, woSignOffUser

-- the work item

	, witmID
	, witmType
      ,WorkItems.schNo
      , schName
      , schType
      , schAuth
      ,Schools.iCode
      , igroup
      , iName
      ,dName
      ,WorkITems.bldID
      ,bldgTitle
      ,witmType
      ,witmQty
      ,witmDesc
      ,witmEstCost
      ,witmSourceOfFunds
      ,witmCostCentre
      ,witmContractValue
      ,witmProgress
      , TRWorkItemProgress.codeDescription ItemProgress
      ,witmProgressDate
      ,witmActualCost
      ,witmInspectedBy
      ,witmEstBaseCost
      ,witmEstBaseUnit
      , witmBldCreateType
      ,witmRoomsClass
      ,witmRoomsOHT
      ,witmRoomsStaff
      ,witmRoomsAdmin
      ,witmRoomsStorage
      ,witmRoomsDorm
      ,witmRoomsKitchen
      ,witmRoomsDining
      ,witmRoomsLibrary
      ,witmRoomsSpecialTuition
      ,witmRoomsHall
      ,witmRoomsOther
	, case witmProgress when 'COM' then 1 end progressCOM
	, case witmProgress when 'DELAY' then 1 end progressDELAY
	, case witmProgress when 'DP' then 1 end progressDP
	, case witmProgress when 'INSP' then 1 end progressINSP
	, case witmProgress when 'IP' then 1 end progressIP
	, case witmProgress when 'XXX' then 1 end progressXXX

	, [woInvoiceValue]-[woContractValue] AS Variance
FROM WorkOrders
	LEFT JOIN WorkITems
		ON WorkOrders.woID = WorkItems.woID
	LEFT JOIN Schools
		ON WorkItems.schNo = Schools.schNo
	LEFT JOIN Islands
		ON Schools.iCode = Islands.iCode
	LEFT JOIN Districts
		ON Islands.iGRoup = Districts.dID
	LEFT JOIN Buildings
		ON WorkItems.bldID = Buildings.bldID
	LEFT JOIN TRWorkItemProgress
		ON WorkItems.witmProgress = TRWorkITemProgress.codeCode

WHERE
	(woStatus = @Status or @Status is null)
	AND (supCode = @SupplierCode or @SupplierCode is null)


	AND (woSourceOfFunds = @FundSource or @FundSource is null)

	AND (@ManagedBy = woDonorManaged or @ManagedBy is null)

	AND (woPlanned >= @PlannedDateStart or @PlannedDateStart is null)
		AND (woPlanned <= @PlannedDateEnd or @PlannedDateEnd is null)

	AND (woWorkApproved >= @WorkApprovedDateStart or @WorkApprovedDateStart is null)
		AND (woWorkApproved <= @WorkApprovedDateEnd or @WorkApprovedDateEnd is null)

	AND (woFinanceApproved >= @FinanceApprovedDateStart or @FinanceApprovedDateStart is null)
		AND (woFinanceApproved <= @FinanceApprovedDateEnd or @FinanceApprovedDateEnd is null)

	AND (woTenderClose >= @TenderCloseDateStart or @TenderCloseDateStart is null)
		AND (woTenderClose <= @TenderCloseDateEnd or @TenderCloseDateEnd is null)

	AND (woContracted >= @ContractDateStart or @ContractDateStart is null)
		AND (woContracted <= @ContractDateEnd or @ContractDateEnd is null)

	AND (woCommenced >= @CommencedDateStart or @CommencedDateStart is null)
		AND (woCommenced <= @CommencedDateEnd or @CommencedDateEnd is null)

	AND (woCompleted >= @CompletionDateStart or @CompletionDateStart is null)
		AND (woCompleted <= @CompletionDateEnd or @CompletionDateEnd is null)

	AND (woPlannedCompletion >= @PlannedCompletionDateStart or @PlannedCompletionDateStart is null)
		AND (woPlannedCompletion <= @PlannedCompletionDateEnd or @PlannedCompletionDateEnd is null)

	AND (woSignOff >= @SignOffDateStart or @SignOffDateStart is null)
		AND (woSignOff <= @SignOffDateEnd or @SignOffDateEnd is null)

-- costs

	AND (woBudget >= @BudgetCostMin or @BudgetCostMin is null)
		AND (woBudget <= @BudgetCostMax or @BudgetCostMax is null)

-- slightly different to the work order one, becuase, we are looking for the pseicifc
-- work items that meet the criteria, not the work order that has an item that meets the criteria
-- and look for a subitem
	AND (WorkItems.schNo = @schoolNo or @SchoolNo is null)
			AND (witmType = @WorkITemType or @WorkITemType is null)
			AND (WorkItems.bldID = @buildingID or @BuildingID is null)
			AND (witmProgress = @Progress or @Progress is null)

end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

