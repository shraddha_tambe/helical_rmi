SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2 12 2017
-- Description:	Polymorphic read of school inspection
-- =============================================
CREATE PROCEDURE [pInspectionRead].[ReadInspection]
	-- Add the parameters for the stored procedure here
	@inspID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @inspType nvarchar(20)

-- find the inspection type
Select @inspType = inspTypeCode
FROM pInspectionRead.SchoolInspections
WHERE inspID = @inspID

-- Is there a custom query for this type?
declare @found int
Select @found = count(*) from INFORMATION_SCHEMA.TABLES
WHERE table_Schema = 'pInspectionRead'
AND TABLE_NAME = @inspType

if (@found > 0) begin
	-- yes there is a custom query for this
	declare @sql nvarchar(400) = 'Select * from pInspectionRead.' + @inspType + ' WHERE inspID = @ID'
	declare @ParmDefinition nvarchar(50) = N'@ID int';
	exec sp_executesql @sql, @ParmDefinition, @ID=@inspID
end
else begin
 Select * from pInspectionRead.SchoolInspections
end


END
GO

