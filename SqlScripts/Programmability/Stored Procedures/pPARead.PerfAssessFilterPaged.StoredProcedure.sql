SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1/12/2015
-- Description:	Field of study filter paged
-- =============================================
CREATE PROCEDURE [pPARead].[PerfAssessFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 1,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

	@PerfAssessID int = null,
	@TeacherID int = null,
	@ConductedBy nvarchar(50) = null,
	@SchoolNo nvarchar(50) = null,
	@District nvarchar(10) = null,
	@Island nvarchar(10) = null,

	@StartDate datetime = null,
	@EndDate datetime = null,


	@subScoreCode nvarchar(10) = null,
	@subScoreMin float = null,
	@subScoreMax float = null,

	@xmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @keys TABLE
	(
		ID int
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pPARead.PerfAssessFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir

		, @PerfAssessID
		, @TeacherID
		, @ConductedBy

		, @SchoolNo
		, @District
		, @Island

		, @StartDate
		, @EndDate


		, @subScoreCode
		, @subScoreMin
		, @subScoreMax

		, @xmlFilter

	If (@PageSize > 0 AND @NumMatches < @Pagesize / 4)
	Select @ColumnSet = 0

	If @ColumnSet = 0

		SELECT PA.*
		from PAAssessment PA
			INNER JOIN
			@Keys K
			on PA.paID = K.ID

	if @ColumnSet = 1
		SELECT PA.*
		from PAAssessmentRpt PA
			INNER JOIN
			@Keys K
			on PA.paID = K.ID

	if @ColumnSet = 2
		SELECT PA.*
		, isnull(@subScoreCode, PA.pafrmCode) Item
		, H.Score
		from PAAssessmentRpt PA
			INNER JOIN @Keys K
				on PA.paID = K.ID
			LEFT JOIN paHierarchyScore H
				on PA.paId = H.paID
				AND H.FullID = isnull(@subScoreCode, PA.pafrmCode)


	-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

