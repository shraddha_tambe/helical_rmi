SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:
-- 6 11 2012 added collate to temp tables
-- added primary key to @keys table
-- =============================================
CREATE PROCEDURE [pSchoolRead].[SchoolFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters

	@SchoolNo   nvarchar(50) = null,
	@SchoolName nvarchar(50) = null,
    @SearchAlias int = 0, -- 1 = search 2 = search for exclusion
    @AliasSource nvarchar(50) = null,
    @AliasNot int = 0,
    @SchoolType nvarchar(10) = null,
	@SchoolClass nvarchar(10) = null,
	@District nvarchar(10) = null,
    @Island nvarchar(10) = null,
	@ElectorateN nvarchar(10) = null,
	@ElectorateL nvarchar(10) = null,
	@Authority nvarchar(10) = null,
	@Language nvarchar(10) = null,
	@InfrastructureClass nvarchar(10) = null,
	@InfrastructureSize nvarchar(10) = null,
	@NearestShipping nvarchar(50) = null,
	@NearestBank nvarchar(50) = null,
	@NearestPo nvarchar(50) = null,
	@NearestAirstrip nvarchar(50) = null,
	@NearestClinic nvarchar(50) = null,
	@Nearesthospital nvarchar(50) = null,
	@SearchIsExtension int = null,
	@PArentSchool nvarchar(50) = null,
	@SEarchHasExtension int = null,
	@ExtensionReferenceYear int = null,	-- look on school year history - if null, use the Active Year
	@YearEstablished int = null,
	@RegistrationStatus nvarchar(10) = null,	-- see case for allowed values
	@SearchRegNo int = 0,				--search for the school name text in the reg no too
	@CreatedAfter datetime = null,
	@EditedAfter datetime = null,
	@IncludeClosed int = 0,
	@ListName nvarchar(20) = null,
	@ListValue nvarchar(10)= null,
	@AuthorityGovt nvarchar(1) = null,
	@XmlFilter xml = null


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;
	SET FMTONLY OFF;	-- becuase Entity Framework 4.0 is a typical Microsoft 2 foot deep pool!


 	DECLARE @keys TABLE
	(
		schNo nvarchar(50) PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pSchoolRead.SchoolFilterIDS
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir

	--filter parameters
	, @SchoolNo
	, @SchoolName
	, @SearchAlias
	, @AliasSource
	, @AliasNot
	, @SchoolType
	, @SchoolClass
	, @District
	, @Island
	, @ElectorateN
	, @ElectorateL
	, @Authority
	, @Language
	, @InfrastructureClass
	, @InfrastructureSize
	, @NearestShipping
	, @NearestBank
	, @NearestPO
	, @NearestAirstrip
	, @NearestClinic
	, @NearestHospital
	, @SearchIsExtension
	, @ParentSchool
	, @SearchHasExtension
	, @ExtensionReferenceYear
	, @YearEstablished
	, @RegistrationStatus
	, @SearchRegNo
	, @CreatedAfter
	, @EditedAfter
	, @IncludeClosed
	, @ListName
	, @ListValue
	, @AuthorityGovt
	, @xmlFilter


-------------------------------------
-- return results

if (@Columnset = 0) begin

Select S.*
, I.iName
, I.iGroup
, D.dName
, N.codeDescription ElectNName
, L.codeDescription ElectLName
from Schools S
	INNER JOIN @Keys K
			on S.schNo = K.schNo
	LEFT OUTER JOIN Islands I
		on S.iCode = I.iCode
	LEFT OUTER JOIN Districts D
		on D.dID = I.iGroup
	LEFT OUTER JOIN lkpElectorateN N
		ON S.schElectN = N.codeCode
	LEFT OUTER JOIN lkpElectorateL L
		ON S.schElectL = L.codeCode
	--LEFT OUTER JOIN (Select syParent, count(schNo) NumExtensions from SchoolYearHistory
	--					WHERE syYear = isnull(@ExtensionReferenceYear, common.ActiveSurveyYear())
	--					GROUP BY syParent
	--				) Extensions
	--	ON S.schNo = Extensions.syParent
	--LEFT OUTER JOIN (Select schNo PSchNo, syParent
	--					FROM SchoolYearHistory
	--					WHERE syYear = isnull(@ExtensionReferenceYear, common.ActiveSurveyYear())
	--				) Parent

	ORDER BY K.recNo
end

if (@Columnset = 1) begin
	Select S.schNo, S.schName, S.schType, S.schAuth, S.schClass, SS.svyYear
	, R.ssID, SS.ssEnrol, SS.ssEnrolM, SS.ssEnrolF,  TT.TNum

	FROM SChools S
	INNER JOIN @Keys K
			on S.schNo = K.schNo
	LEFT JOIN
	(Select ssID , schNo
	, row_number() OVER (PARTITION BY schNo ORDER BY svyYEar DESC) rn
	From SchoolSurvey
	) R
	ON S.schNo = R.schNo
	AND R.rn = 1
	LEFT JOIN SchoolSurvey SS
	ON R.ssID = SS.ssID
	LEFT JOIN (
		select ssID, count(*) TNum from TeacherSurvey
		GROUP BY ssID
	) TT
	ON R.ssID = TT.ssID

end
if (@Columnset = 2) begin

Select S.schNo, S.schName, S.schType, S.schAuth, S.schClass
	, schParent, schReg, schRegDate, schRegStatus, schRegStatusDate
	, schEst, schEstBy, schClosed, schCloseReason, schParent
	, pRowversion, pCreateUser, pCreateDateTime, pEditUser, pEditDateTime

	FROM Schools S
	INNER JOIN @Keys K
			on S.schNo = K.schNo


end
-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K

END
GO

