SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2010
-- Description:	Part of the scatter chart system
-- Used to produce one series of data by school - enrolments
-- a year is mandatory, may also be itemised by ClassLevel
-- =============================================
CREATE PROCEDURE [pSchoolRead].[schoolDataSetEnrolment]
	-- Add the parameters for the stored procedure here
	@Year int
	, @SchoolNo nvarchar(50) = null
	, @ClassLevel nvarchar(10) = null
AS
BEGIN

	Select * from pSchoolRead.tfnSchoolDataSetEnrolment(@Year, @SchoolNo, @ClassLevel, NULL)

END
GO

