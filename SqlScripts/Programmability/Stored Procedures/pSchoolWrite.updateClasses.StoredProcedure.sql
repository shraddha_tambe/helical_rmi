SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 8 1 2008
-- Description:	update primary classes from xml
-- =============================================
CREATE PROCEDURE [pSchoolWrite].[updateClasses]
	-- Add the parameters for the stored procedure here
	@SurveyID int ,
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

-- delete existing
-- the xml may pass in classType, minSeq and maxSeq
-- defining the range of items in scope. this should be used for deleting
--- the preexisitng - ie we only delete what is being replaced.

	declare @classType nvarchar(1)
	declare @minSeq int
	declare @maxSeq int

	select @classType = classType,
			@minSeq = minSeq,
			@maxSeq = maxSeq
	FROM
		OPENXML (@idoc, '/Classes',2)
		WITH (
				classType nvarchar(1) '@classType',
				minSeq int '@minSeq',
				maxSeq int '@maxSeq'
				)

begin transaction

begin try

	if (@classType) is null
		-- not passed by VEMIS @ school
		--
		begin
			DELETE from ClassTeacher WHERE pcId in
			(select pcID from PrimaryClass WHERE ssID = @surveyID)

			DELETE from Classes WHERE ssID = @surveyID			-- cascade does the rest
		end
	else
		-- as passed in from classes form 2007-2009
		begin
			DELETE from ClassTeacher WHERE pcId in
			(select pcID from PrimaryClass
				WHERE ssID = @surveyID
					AND pcCat = @classType
					AND pcSeq between @minSeq and @maxSeq)

			DELETE from Classes WHERE
				ssID = @surveyID			-- cascade does the rest
					AND pcCat = @classType
					AND pcSeq between @minSeq and @maxSeq
		end

-- write back the classes
-- first deal with the update where the class has the real roomID (rmID)
   INSERT INTO Classes(ssID, pcSeq, pcCat
		, pcHrsWeek, pcLang, subjCode, rmID)	-- new fields for 2010

	Select @SurveyID, seq, classType
			, weeklyHours, langCode, subjCode, roomID
		from OPENXML (@idoc, '/Classes/Class',2)
		WITH (
				seq int '@seq'
				, classType nvarchar(1) '../@classType'
				, weeklyHours float '@weeklyHours'
				, langCode nvarchar(5) '@langCode'
				, subjCode nvarchar(10) '@subjectCode'
				, roomID int '@roomID'
				, roomGUID uniqueidentifier '@roomGUID'			--- the guid is supplied by pineapples@school
																-- to match the guid passed on the room record
				)

		WHERE roomID is not null

-- next the case where we have the roomGUID
   INSERT INTO Classes(ssID, pcSeq, pcCat
		, pcHrsWeek, pcLang, subjCode, rmID)	-- new fields for 2010

	Select @SurveyID, seq, classType
			, weeklyHours, langCode, subjCode, R.rmID		-- here the rmID is coming from rooms, found by the guid
		from OPENXML (@idoc, '/Classes/Class',2)
		WITH (
				seq int '@seq'
				, classType nvarchar(1) '../@classType'
				, weeklyHours float '@weeklyHours'
				, langCode nvarchar(5) '@langCode'
				, subjCode nvarchar(10) '@subjectCode'
				, roomID int '@roomID'
				, roomGUID uniqueidentifier '@roomGUID'			--- the guid is supplied by pineapples@school
																-- to match the guid passed on the room record
				) X
		inner join Rooms R on X.roomGUID = R.rmGUID


	-- insert the level info

		INSERT INTO ClassLevel
		(
			pcID, pclLevel, pcLNum, pclM, pclF
		)
		Select pcID, levelCode, enrol
				, enrolM, enrolF
		FROM
		PrimaryClass,
		OPENXML (@idoc, '/Classes/Class/Levels/Level',2)
			WITH (
					seq int '../../@seq',
					levelCode nvarchar(10) '@levelCode',			-- these attributes changed 21 07 2009 camelcase
					enrol int '@enrol'
					, enrolM int '@enrolM'
					, enrolF int '@enrolF'
					) PCL
		WHERE PrimaryClass.pcSeq = PCL.seq
		AND ssID = @surveyID

		select * from primaryclass where ssID = @surveyID
		select pcl.* from primaryclass
		inner join primaryclasslevel pcl on primaryclass.pcID = pcl.pcID
		where ssID = @surveyID

	-- insert the teacher info
	-- the challengeis to find the tchsId created when the teacher with this tag was loaded
	-- tag here is the primary key tsmisID from the SMIS system

		INSERT INTO PrimaryClassTeacher
		(pcID , tchsID, pctSeq)
		SELECT pcID, tchsID, TeacherSeq
		FROM PrimaryClass PC, TeacherSurvey TS,
		OPENXML (@idoc, '/Classes/Class/Teachers/Teacher',2)
			WITH (
					seq int '../../@seq',
					smisID nvarchar(10) '@Tag',
					tID int '@tID',
					TeacherSeq int '@TeacherSeq'
					) XMLT
		WHERE PC.pcSeq = xmlt.seq
		AND xmlt.smisID = TS.tchsmisID
		AND TS.ssID = @surveyID
		AND PC.ssID = @surveyID

	-- pineapples itself supplies the tid, not the smisid .
	-- since these are mutually exclusive , we can do the inserts
	-- based on tid too

		INSERT INTO PrimaryClassTeacher
		(pcID , tchsID, pctSeq)
		SELECT pcID, tchsID, TeacherSeq
		FROM PrimaryClass PC, TeacherSurvey TS,
		OPENXML (@idoc, '/Classes/Class/Teachers/Teacher',2)
			WITH (
					seq int '../../@seq',
					smisID nvarchar(10) '@Tag',
					tID int '@tID',
					TeacherSeq int '@TeacherSeq'
					) XMLT
		WHERE PC.pcSeq = xmlt.seq
		AND xmlt.tID = TS.tID
		AND TS.ssID = @surveyID
		AND PC.ssID = @surveyID
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction

-- the stuff below we won;t worry about in the transaction

-- if called from a survey form, and we already have category and sequence that we do not want to change, exit here
if not (@classType is null)
	return 0


-- this section is for the data arriving from SMIS
-- it classifies according to whether the school is S, J, or C
-- and renumbers in ascending sequence
-- this part is not used when this sp is called from a survey form
-- there are too many issues with shifting items betwen the various tabs on the form
UPDATE Primaryclass
	SET pcCat = 'C'
WHERE ssID = @SurveyID
and subjCode is null		-- 21 07 2009 if importing full class data, we don't need to include subject specific classes in the old format display
AND pcID in (Select pcID from PrimaryClassLevel GROUP BY pcID having count(pcID) > 1)

UPDATE Primaryclass
	SET pcCat = 'J'
WHERE ssID = @SurveyID
AND pcCat is null
and subjCode is null
AND pcID in (Select pcID from PrimaryClassTeacher GROUP BY pcID having count(pcID) > 1)

-- single classes
UPDATE PrimaryClass
	SET pcCat = 'S'
WHERE ssID = @SurveyID
AND pcCat is null
and subjCode is null
update PrimaryClass SET pcSeq = null WHERE ssId = @surveyID

declare @i int
select @i = 0

declare @pcID int
select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'C' and ssID = @surveyID and pcSeq is null
while @pcId is not null
	begin

		UPDATE PrimaryClass
			SET pcSeq = @i
		WHERE pcID =@pcID
		select @i = @i + 1
	select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'C' and ssID = @surveyID and pcSeq is null

	end

select @i = 0

select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'J' and ssID = @surveyID and pcSeq is null
while @pcId is not null
	begin

		UPDATE PrimaryClass
			SET pcSeq = @i
		WHERE pcID =@pcID
		select @i = @i + 1
	select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'J' and ssID = @surveyID and pcSeq is null

	end

select @i = 0

select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'S' and ssID = @surveyID and pcSeq is null
while @pcId is not null
	begin

		UPDATE PrimaryClass
			SET pcSeq = @i
		WHERE pcID =@pcID
		select @i = @i + 1
	select @pcID = min(pcID) from PrimaryClass WHERE pcCat = 'S' and ssID = @surveyID and pcSeq is null

	end
END
GO

