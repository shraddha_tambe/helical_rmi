SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 12 2007
-- Description:	update teachers from smis xml format
-- =============================================
CREATE PROCEDURE [pSchoolWrite].[updateStreams]
	-- Add the parameters for the stored procedure here
	@SurveyID int,
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
----	SET NOCOUNT ON;


	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml


begin transaction

begin try
	-- delete exisitng records
	DELETE
	FROM Streams
	WHERE ssID = @SurveyID

	INSERT INTO Streams
	( ssID, stmLevel, 	stmNum, stmPupils)

	Select
		@surveyID,
		levelCode,
		NumStreams,
		Enrol

	from OPENXML (@idoc, '/Streams/Stream',2)
	WITH (
			levelCode nvarchar(10) '@levelCode'		,
			NumStreams int '@NumStreams',
			Enrol int '@Enrol'
		)
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction


END
GO

