SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 11 2009
-- Description:	Create a new org unit riole
-- =============================================
CREATE PROCEDURE [pSecurityOps].[createOrgRole]
	-- Add the parameters for the stored procedure here
	@OrgUnitName sysname
WITH EXECUTE AS 'pineapples'
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @sql nvarchar(200)

	IF  NOT EXISTS (SELECT * FROM sys.database_principals
					WHERE name = @OrgUnitName AND type = 'R') begin

			select @sql = N'CREATE ROLE ' + @OrgUnitName
			exec sp_executesql @sql
		-- now we can add the extended property

		EXEC sys.sp_addextendedproperty @name=N'PineapplesUser', @value=N'OrgUnit' ,
				 @level0type=N'USER',@level0name=@OrgUnitName
	end

END
GO

