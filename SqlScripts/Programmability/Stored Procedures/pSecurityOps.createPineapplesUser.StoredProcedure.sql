SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 11 2009
-- Description:	Create a datbase user. Optional we may create a login at the same time, and add to a role.
-- =============================================
CREATE PROCEDURE [pSecurityOps].[createPineapplesUser]
	-- Add the parameters for the stored procedure here
	@UserName sysname ,
	@WindowsLogin nvarchar(100),
	@AddToRole sysname = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @parms nvarchar(400)
	declare @sql nvarchar(1000)
	declare @str nvarchar(1000)

begin try

	begin transaction

-- 15 04 2010 a bit of the user-friendlies here
	if @WindowsLogin like '\\%'
		set @WindowsLogin = right(@WindowsLogin, len(@WindowsLogin) - 2)

	if not exists (Select [name] from sys.server_principals WHERE [name] = @WindowsLogin) begin
		IF (IS_SRVROLEMEMBER ('securityadmin') = 0 )
			raiserror('You must be a member of the server role ''securityadmin'' to add new logins',16,10)

		select @sql = replace(N'CREATE LOGIN [%%%] FROM WINDOWS WITH DEFAULT_DATABASE = $$$','%%%',@WindowsLogin)
		select @sql = replace(@sql,'$$$', db_name())

		exec sp_executesql @sql
		Select @str = 'Login created for user '+ @WindowsLogin
	end
	else
		Select @str = @WindowsLogin + ' is already a login on this server'

	if not exists (Select [name] from sys.database_principals WHERE [name] = @UserName) begin
		IF (IS_MEMBER ('db_accessadmin') = 0 )
			raiserror('You must be a member of the database role ''db_accessadmin'' to add new users',16,10)

		select @sql = replace(N'CREATE USER [%%%] FOR LOGIN [$$$]','%%%',@UserName)
		select @sql = replace(@sql,'$$$', @WindowsLogin)

		exec sp_executesql @sql
		Select @str = @str + '\n\nUser created in ' + db_name() + ' for login ' + @WindowsLogin
	end
	else
		Select @str = @str + '\n\nUser ' + @Username + ' already exists in ' + db_name()

	if (@AddToRole is not null) begin
		IF (IS_MEMBER ('db_securityadmin') = 0 )
			raiserror('You must be a member of the database role ''db_securityadmin'' to add new users',16,10)

		exec sp_addrolemember @addToRole, @UserName
		select @str = @str + '\n\n' + @Username + ' added to role ' + @AddToRole
	end

	commit transaction
	Select @str
end try

/****************************************************************
Generic catch block
****************************************************************/
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE() + @str,
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
END
GO

