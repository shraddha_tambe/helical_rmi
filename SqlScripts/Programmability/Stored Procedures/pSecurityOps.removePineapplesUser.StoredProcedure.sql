SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 11 2009
-- Description:	Remove a user
-- =============================================
-- 0 = rfempve from role
-- 1 = drop from db
-- 2 = drop from db and server
CREATE PROCEDURE [pSecurityOps].[removePineapplesUser]
	-- Add the parameters for the stored procedure here
	@UserName sysname ,
	@Role sysname,
	@Option int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @sql nvarchar(1000)
	declare @parms nvarchar(100)
	declare @str nvarchar(1000)

	If (@Option = 1) begin
		exec sp_droprolemember @Role, @UserName
	end

	If @Option = 3 begin
		select @sql = 'DROP USER ' + @UserName
		exec sp_executesql @sql
		Select @str = 'User dropped from ' + db_name()
	end

	If @Option = 3 begin
		select @sql = 'DROP USER ' + @UserName
		exec sp_executesql @sql
		Select @str = 'User dropped from ' + db_name()

		declare @WindowsLogin sysname

		Select @WindowsLogin = SUSER_SNAME(sID)
		from sys.database_principals
		WHERE
			name = @Username

--		select @sql = N'DROP LOGIN ' + @WindowsLogin
--		exec sp_executesql @sql
--		Select @str = @str +'\n\nLogin ' + @WindowsLogin + ' dropped '


	end

    -- Insert statements for procedure here
END
GO

