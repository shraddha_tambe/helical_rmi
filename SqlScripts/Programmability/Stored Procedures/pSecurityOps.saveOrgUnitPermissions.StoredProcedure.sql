SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 11 2009
-- Description:	Save security permissions frm Text
-- =============================================
CREATE PROCEDURE [pSecurityOps].[saveOrgUnitPermissions]
	-- Add the parameters for the stored procedure here
	@orgUnit nvarchar(50) = 0,
	@xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- the xml looks like <Permissions><Permission dataArea= basic= extended= high= admin= ops= /></Permissions
    -- Insert statements for procedure here
	declare @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml

	Select *
	INTO #Permissions
	from openxml(@idoc,'/Permissions/Permission',2)
	with
		(
			dataArea nvarchar(20) '@dataArea'
			, [basic] nvarchar(2) '@basic'
			, [extended] nvarchar(2) '@extended'
			, [high] nvarchar(2) '@high'
			, [admin] nvarchar(1) '@admin'
			, [ops] nvarchar(1) '@ops'
		) P

	Select * from #Permissions


	declare @NumToGo int


	declare @dataArea nvarchar(20)
	declare @basic nvarchar(2)
	declare @extended nvarchar(2)
	declare @high nvarchar(2)
	declare @admin nvarchar(1)
	declare @ops nvarchar(1)

	select @NumToGo = count(*) 	from #Permissions

	while (@NumToGo > 0)	begin
		Select TOP 1
				@dataArea = dataArea
				, @basic = [basic]
				, @extended = [extended]
				, @high = [high]
				, @admin = [admin]
				, @ops = [ops]
		FROM #Permissions


		exec pSecurityOps.configureOrgUnitPermissions @orgUnit, @dataArea, @basic,
						@extended, @high, @admin, @ops

		Delete
		from #Permissions
		WHERE dataArea = @dataArea

		select @NumToGo = count(*) 	from #Permissions

	end

END
GO

