SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Brian Lewis and Ghislain Hachey
-- Create date: 27 04 2017
-- Description:
-- =============================================
CREATE PROCEDURE [pSurveyRead].[SchoolAccreditationFilterPaged]
	-- Add the parameters for the stored procedure here

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

--filter parameters

	@SAID int = null,
    @School nvarchar(50) = null, -- e.g. 'Geek High School'
	@SchoolNo nvarchar(50) = null, --'e.g. GHS100'
	@InspYear nvarchar(50) = null, --- e.g. '2016'
	-- adding district and authority in order to enforce security on these
	@District nvarchar(10) = null,
	@Authority nvarchar(10) = null,

	@XmlFilter xml = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	SET NOCOUNT ON;

 	DECLARE @keys TABLE
	(
		saID int PRIMARY KEY
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pSurveyRead.SchoolAccreditationFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	--filter parameters
	@SAID,
	@School,
	@SchoolNo,
	@InspYear,

	@District,
	@Authority,

	@xmlFilter


-------------------------------------
-- return results

Select SchoolAccreditations.*
FROM pInspectionRead.SchoolAccreditations AS SchoolAccreditations
INNER JOIN @Keys AS K ON SchoolAccreditations.saID = K.saID

-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K


END
GO

