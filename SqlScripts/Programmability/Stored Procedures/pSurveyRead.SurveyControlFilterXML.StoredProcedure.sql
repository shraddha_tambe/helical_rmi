SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3/6/2009
-- Description:
-- =============================================
CREATE PROCEDURE [pSurveyRead].[SurveyControlFilterXML]
	-- Add the parameters for the stored procedure here
	@SurveyControlFilter xml
AS
BEGIN
	SET NOCOUNT ON;
	declare @SummaryOnly int
	declare	@SurveyYear int
	declare @District nvarchar(10)
	declare @Island nvarchar(10)
	declare @SchoolType nvarchar(10)
	declare @Authority nvarchar(10)
	declare @FileLocation nvarchar(30)
	declare @StatusChoice int
	declare @SortField nvarchar(50)
	declare @SortDirection nvarchar(30)


	Select @SummaryOnly = isnull(@SurveyControlFilter.value('./SurveyControlFilter[1]/@summaryOnly','int'),0)

	Select @SurveyYear = @SurveyControlFilter.value('./SurveyControlFilter[1]/@surveyYear','int')
	Select @District = @SurveyControlFilter.value('./SurveyControlFilter[1]/@district','nvarchar(10)')
	Select @Island = @SurveyControlFilter.value('./SurveyControlFilter[1]/@island','nvarchar(10)')
	Select @SchoolType = @SurveyControlFilter.value('./SurveyControlFilter[1]/@schoolType','nvarchar(10)')
	Select @Authority = @SurveyControlFilter.value('./SurveyControlFilter[1]/@authority','nvarchar(10)')
	Select @FileLocation = @SurveyControlFilter.value('./SurveyControlFilter[1]/@fileLocation','nvarchar(30)')
	Select @StatusChoice = isnull(@SurveyControlFilter.value('./SurveyControlFilter[1]/@statusChoice','int'),0)
	Select @SortField = isnull(@SurveyControlFilter.value('./SurveyControlFilter[1]/@sortField','nvarchar(50)'),0)
	Select @SortDirection = isnull(@SurveyControlFilter.value('./SurveyControlFilter[1]/@sortDirection','nvarchar(50)'),0)


	EXEC pSurveyRead.SurveyControlFilter @SummaryOnly, @SurveyYEar, @District, @Island
					, @SchoolType, @Authority, @FileLocation, @StatusChoice, @SortField, @SortDirection

END
GO

