SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pSurveyRead].[getLinkedInspectionID]
	@SurveyId int
	, @InspType nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @inspID int

	Select @inspID = inspID
	from SchoolInspection SI
	INNER JOIN SurveyInspectionSet SIS
		ON SI.inspSetID = SIS.inspsetID
	INNER JOIN SchoolSurvey SS
		ON SIS.svyYear = SS.svyYear
		AND SI.schNo = SS.schNo
	WHERE SS.ssID = @SurveyID
		AND SIS.intyCode = @InspType

	Select @inspID
	return isnull(@inspID,0)
END
GO

