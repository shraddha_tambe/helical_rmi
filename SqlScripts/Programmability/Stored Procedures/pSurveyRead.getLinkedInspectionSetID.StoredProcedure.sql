SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pSurveyRead].[getLinkedInspectionSetID]
	@SurveyId int
	, @InspType nvarchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select inspsetID
	from SurveyInspectionSet SIS
	INNER JOIN SchoolSurvey SS
		ON SIS.svyYear = SS.svyYear
	WHERE SS.ssID = @SurveyID
		AND SIS.intyCode = @InspType

END
GO

