SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	update parent committe / school council
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfParentCommittee]
	@SurveyID int
	, @xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the PC block

	<field name="PC">
			<field name="Exists">
				<value>Y</value>
			</field>
			<field name="Meet">
				<value>4</value>
			</field>
			<field name="Members">
				<field name="M">
					<value>3</value>
				</field>
				<field name="F">
					<value>4</value>
				</field>
			</field>
			<field name="Support">
				<value>1</value>
			</field>
		</field>
		<field name="SC">
			<field name="Exists">
				<value>Y</value>
			</field>
			<field name="Meet">
				<value>4</value>
			</field>
		</field>
	</field>

note that PC and SC may be siblings if they are passed via a containing block
this is updated directly onto the SchoolSurveyTable
*/
begin try
	UPDATE schoolSurvey
		SET ssSchoolCouncil = case SCExists when 'Y' then 1 when 'N' then 0 else null end
		, ssSCMeet = SCMeet
		, ssSCActivities = SCActivities
		, ssSCMembersM = SCMembersM
		, ssSCMembersF = SCMembersF

		, ssParentCommittee = case PCExists when 'Y' then 1 when 'N' then 0 else null end
		, ssPCMeet = PCMeet
		, ssPCMembersM = PCMembersM
		, ssPCMembersF = PCMembersF
		, ssPCSupport = PCSupport

	FROM OPENXML(@idoc, '/x:field',2)		-- base is the Funding Node
	WITH
	(
											-- keep the // to be agnostic about the actual node that is passed in....
											-- this structure allows e.g.<PC><SC> or <SC> or <Org><PC/><SC/></Org> etc
		SCExists			nvarchar(1)		'//x:field[@name="SC"]/x:field[@name="Exists"]/x:value'
		, SCMeet			int				'//x:field[@name="SC"]/x:field[@name="Meet"]/x:value'
		, SCActivities		nvarchar(400)	'//x:field[@name="SC"]/x:field[@name="Activities"]/x:value'
		, SCMembersM		int				'//x:field[@name="SC"]/x:field[@name="Members"]/x:field[@name="M"]/x:value'
		, SCMembersF		int				'//x:field[@name="SC"]/x:field[@name="Members"]/x:field[@name="F"]/x:value'

		, PCExists			nvarchar(1)		'//x:field[@name="PC"]/x:field[@name="Exists"]/x:value'
		, PCMeet			int				'//x:field[@name="PC"]/x:field[@name="Meet"]/x:value'
		, PCMembersM		int				'//x:field[@name="PC"]/x:field[@name="Members"]/x:field[@name="M"]/x:value'
		, PCMembersF		int				'//x:field[@name="PC"]/x:field[@name="Members"]/x:field[@name="F"]/x:value'
		, PCSupport			int				'//x:field[@name="PC"]/x:field[@name="Support"]/x:value'

	) X
	WHERE SchoolSurvey.ssID = @SurveyID
	exec audit.xfdfInsert @SurveyID, 'Survey updated','ParentCommittee',@@rowcount

end try
begin catch
   DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage,'ParentCommittee'

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err

end catch
END
GO

