SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 30 10 2014
-- Description:	update "mini preschool survey" found in KI primary form
-- =============================================
CREATE PROCEDURE [pSurveyWrite].[xfdfPreschool]
	@SurveyID int
	, @xml xml
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	declare @idoc int
	-- the xfdf file has the default adobe namespace
	-- we have to alias this namespace ( as 'x' ) so as to be able to use
	-- the xpath expressions
	declare @xmlns nvarchar(500) = '<root xmlns:x="http://ns.adobe.com/xfdf/"/>'
	EXEC sp_xml_preparedocument @idoc OUTPUT, @xml, @xmlns


/*
Format of the Preschool block
<field name="Preschool">
			<field name="00">
				<field name="Name">
					<value>Boo Far Num</value>
				</field>
				<field name="Funded">
					<value>Chinese Association</value>
				</field>
				<field name="Years">
					<value>3</value>
				</field>
				<field name="M">
					<value>40</value>
				</field>
				<field name="F">
					<value>30</value>
				</field>
				<field name="All" />				-- also supported without gender split

			</field>
			<field name="01">
			...

*/

-- this is simple enough to update with using table variables

begin transaction


begin try

	-- clear any exisitng entries
	DELETE from PreschoolSurvey
	WHERe ssId = @SurveyID


	INSERT INTO PreSchoolSurvey
	(
		ssID
		, presSeq
		, presName
		, presFunded
		, presYearsOffered
		, presPupils
		, presM
		, presF
	)
	SELECT
		@SurveyID
		, convert(int,r)
		, name
		, funded
		, years
		, tot
		, m
		, f
	FROM OPENXML(@idoc, '/x:field/x:field',2)		-- base is the row counter Node name="00","01 etc
	WITH
	(
		r					nvarchar(2)		'@name'
		, name				nvarchar(50)	'x:field[@name="Name"]/x:value'
		, funded			nvarchar(20)	'x:field[@name="Funded"]/x:value'
		, years				int				'x:field[@name="Years"]/x:value'
		, tot				int				'x:field[@name="All"]/x:value'
		, m					int				'x:field[@name="M"]/x:value'
		, f					int				'x:field[@name="F"]/x:value'
	) X

	exec audit.xfdfInsert @SurveyID, 'Preschool survey records inserted','Preschool',@@rowcount
end try

begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

	exec audit.xfdfError @SurveyID, @ErrorMessage,'Preschool'

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch

-- and commit
if @@trancount > 0
	commit transaction


END
GO

