SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 7 4 2014
-- Description:	List of teacher surveys at a school
-- =============================================
CREATE PROCEDURE [pTeacherRead].[SurveyTeacherList]
	-- Add the parameters for the stored procedure here
	@SurveyID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT *
	FROM
		pTeacherRead.TeacherSurveyV
	WHERE ssID = @SurveyID


END
GO

