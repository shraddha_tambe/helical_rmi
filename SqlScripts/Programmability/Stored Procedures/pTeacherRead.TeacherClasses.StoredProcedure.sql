SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 3 10 2009
-- Description:	Return info on the classes taught by a teacher
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherClasses]
	-- Add the parameters for the stored procedure here
	@TeacherID int ,
	@SurveyYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Select SS.svyYear
		, ss.schNo
		, schName
		, Classes.pcHrsWeek
		, sum(pclF) PupilsF
		, sum(pclM) PupilsM
		, sum(isnull(pclF,0) + isnull(pclM,0) + isnull(pclNum,0)) PupilsTotal
		, TRLanguage.langName
		, TRSubjects.subjName
	from SchoolSurvey SS
		INNER JOIN Schools ON Schools.schNo = SS.schNo
		INNER JOIN Classes ON Classes.ssID = SS.ssID
		INNER JOIN ClassTeacher CT on Classes.pcID = CT.pcID
		INNER JOIN ClassLevel CL ON Classes.pcID = CL.pcID
		INNER JOIN TeacherSurvey TS ON TS.tchsID = CT.tchsID
		LEFT JOIN TRLanguage ON Classes.pcLang = TRLanguage.langCode
		LEFT JOIN TRSubjects on Classes.subjCode = TRSubjects.subjCode

	WHERE
		TS.tID = @TeacherID
		AND (SS.svyYEar = @SurveyYear or @SurveyYear is null)
	GROUP BY
		SS.svyYear
		, ss.schNo
		, schName
		, Classes.pcHrsWeek
		, TRLAnguage.langName
		, TRSubjects.subjName

	ORDER BY SS.svyYEar DESC, Classes.pcHrsWeek DESC
    -- Insert statements for procedure here

END
GO

