SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pTeacherRead].[TeacherFilterIDs]

	@NumMatches int OUTPUT,
	@PageSize int = 0,
	@PageNo int = 0,
	@SortColumn nvarchar(30) = null,
	@SortDir int = 0,

	@TeacherID			int = null,
	@Surname			nvarchar(50) = null,
	@GivenName			nvarchar(50) = null,
	@PayrollNo			nvarchar(50) = null,
	@RegistrationNo		nvarchar(50) = null,
	@PRovidentFundNo	nvarchar(50) = null,

	@Gender				nvarchar(1) = null,
	@DoB				datetime = null,
	@DobEnd				datetime = null,

	@Language			nvarchar(10) = null,
	@PaidBy				nvarchar(10) = null,

	@Qualification		nvarchar(10) = null,
	@EdQualification	nvarchar(10) = null,

	@Subject			nvarchar(20) = null,
	@SearchSubjectTaught	int = 1,		-- 0 means not search - this is only meaningful if you specify a subject and only want to search trained to teach
	@SearchTrained			int = 0,

	@AtSchool			nvarchar(50) = null,
	@AtSchoolType		nvarchar(10) = null,
	@InYear				int = null,
	@Role				nvarchar(10) = null,

	@LevelTaught		nvarchar(10) = null,
	@YearOfEdMin		int = -100,
	@YearOfEdMax		int = -100,

	@ISCEDSubClass		nvarchar(10) = null,

	@UseOr				int = 0,
	@CrossSearch		int = 0,
	@SoundSearch		int = 0,

	@District			nvarchar(10) = null,
	@Authority			nvarchar(10) = null,


	@XmlFilter			xml = null

as
	SET NOCOUNT ON;

--- to do XmlFilter processing


	-- first off we see what arguments have been passed, to figure out
	-- what tables need to be searched
	declare @TablesToUse int
	declare @UseClasses int
	-- 0 only teacheridentity
	-- 1 teacher identity, teachersurvey
	-- 2 teacher identity, teachersurvey, schoolsurvey (year, school, schooltype)
	-- 3 + primary classes (searching for a single class level
	-- 4 + lkplevels (searching for a range of class levels

	select @UseClasses =
		case
			when (@YearOfEdMax <> -100) then 1
			when (@LevelTaught is not null) then 1
			else 0
		end

	select @TablesToUse =
		case
			when (@AtSchool is not null) then 1
			when (@AtSchoolType is not null) then 1
			when (@InYear <> 0) then 1
			when (@Role is not null) then 1
			when (@EdQualification is not null) then 1
			when (@Qualification is not null) then 1
			when (@Subject is not null) then 1
			when (@PaidBy is not null) then 1
			when (@Language is not null) then 1
			else 0

	end

declare @SurnameSoundex nvarchar(5)
declare @GivenNameSoundex nvarchar(5)

	if not (@Surname is null)

		begin
			set @SurnameSoundex = soundex(@Surname)
			/* if surname or given name do not contain pattern characters %_
				add % to the end */

			if patindex('%[%_]%',@Surname) = 0
				set @Surname = @Surname + '%'
		end
	if not (@GivenName is null)

		begin
			set @GivenNameSoundex = soundex(@GivenName)
			/* if surname or given name do not contain pattern characters %_
				add % to the end */

			if patindex('%[%_]%',@GivenName) = 0
				set @GivenName = @GivenName + '%'
		end
-- set the dob end to the dob start if it is null
	if (@DobEnd is null)
		set @DobEnd = @Dob

declare @s0 nvarchar(2000)
declare @s1 nvarchar(2000)
declare @s2 nvarchar(2000)
declare @s3 nvarchar(2000)
declare @sql nvarchar(4000)

	DECLARE @keysAll TABLE
	(
	selectedID int
	, recNo int IDENTITY PRIMARY KEY
	)


declare @params nvarchar(2000)

-- put the subquery into a temp table

declare @AccessControlled int = 0
declare @accessControlSchools TABLE
(
	schNo nvarchar(50)
)

declare @controlledTeachers TABLE
(
	tID int
)

declare @tClass table
(
	tid int
)

-- cut to the chase and do access control if it is needed
if (
	@district is not null OR @Authority is not null
	OR @AtSchool is not null
	) BEGIN
	INSERT INTO @accessControlSchools
	SELECT schNo from Schools S
		INNER JOIN lkpIslands I
			ON S.iCode = I.iCode
		WHERE (schAuth = @authority or @authority is null)
			AND (I.iGroup = @district or @district is null)
			AND (S.schNo = @AtSchool  or @AtSchool is null)

	INSERT INTO @controlledTeachers
	SELECT Distinct tID
	FROM
	( SElect tID
	FROM TeacherAppointment TA
		INNER JOIN @accessControlSchools S
			ON TA.schNo = S.schNo
	UNION
	SELECT tID
	FROM TeacherSurvey TS
		INNER JOIN SchoolSurvey SS
			ON TS.ssID = SS.ssID
		INNER JOIN @accessControlSchools S
			ON SS.schNo = S.schNo
	) SUB
	Select @AccessControlled = 1
END

if (@UseOr = 1) begin
		-- "or" version - ie ANY Criteria
		If @PageSize = 0
			Select @PageSize = 300
				, @PageNo = 1

		INSERT INTO @keysAll
		(selectedID)
		Select tID


		from

		(
		Select TeacherIdentity.tID
		, tSurname
		, case when tSurname like @Surname and  tGiven like @GivenName then 160
			when tSurname like @Surname and @GivenNameSoundex=tGivenSoundex then 120
			when tGiven like @GivenName and @SurnameSoundex=tSurnameSoundex then 120
			when tGiven like @Surname and  tSurname like @GivenName then 100
			when tGiven like @Surname and @GivenNameSoundex=tSurnameSoundex then 60
			when tGiven like @GivenName and @SurnameSoundex=tGivenSoundex then 60
			when tSurname like @Surname then 40
			when tGiven like @GivenName then 40
			when tSurname like @GivenName then 20
			when tGiven like @Surname  then 20
			when @SurnameSoundex=tSurnameSoundex and @GivenNameSoundex=tGivenSoundex then 20
			when @SurnameSoundex=tGivenSoundex and @GivenNameSoundex=tSurnameSoundex then 20
			when @SurnameSoundex=tSurnameSoundex then 10
			when @GivenNameSoundex=tSurnameSoundex then 10
			else 0
		  end
			+
			case when tSex = @Gender then 2 else 0 end
			+
			case when tDOB = @DoB then 60
				when tDob >= @dob and  tDob <= @dobEnd then 10
				when tDob <= @dobEnd then 5
				when tDob >= @dob then 5
				else 0
			end
			+
			case when tPayroll = @PayrollNo then 120 else 0 end
			MatchScore

		 from TeacherIdentity
		WHERE tID in
		-- the core of the query is here in the nested select this ensure only 1 record per tID
		( Select TeacherIdentity.tID
			from TeacherIdentity
				LEFT JOIN TeacherSurvey ON TeacherIdentity.tID = TeacherSurvey.tID
				LEFT JOIN SchoolSurvey ON TeacherSurvey.ssID = SchoolSurvey.ssID
				LEFT JOIN ClassTeacher on ClassTeacher.tchsID = TeacherSurvey.tchsID
				LEFT JOIN Classes ON Classes.pcID = ClassTeacher.pcID
				LEFT JOIN ClassLevel on Classes.pcID = ClassLevel.pcID
				left join lkpLevels on lkpLevels.codeCode = ClassLevel.pclLevel
				LEFT JOIN @controlledTeachers CT ON TeacherIdentity.tID = CT.tID
			WHERE
			(
				(

						(tSurname like @Surname  or (@SoundSearch = 1 and @SurnameSoundex=tSurnameSoundex))
					OR
						(tGiven like @GivenName or (@SoundSearch = 1  and @GivenNameSoundex=tGivenSoundex))

				)
				OR
				-- cROSSsEARCH
				(
					@CrossSearch = 1 and
					(
						(tGiven like @Surname or (@SoundSearch = 1 and tGivenSoundex=@SurNameSoundex))
					OR
						(tSurname like @GivenName or ( @SoundSearch = 1 and @GivenNameSoundex=tSurnameSoundex))
					)
				)
			) -- end name search


						OR (tPayroll=@PayrollNo )
						OR (tRegister = @RegistrationNo)
						OR (tProvident = @ProvidentFundNo )

						OR	( -- date of birth match
							 (@dob is null and tDob <= @dobEnd)
							or (@dobEnd is null and tDob =@dob)
							or (tDob >= @dob and  tDob <= @dobEnd)
							)

		-- 7 4 2014 don't use gender as a selection in Or case, returns too many reocrds
		-- gender does contribute to the scoring
		--				OR (tSex = @Gender)
		-- end of teacheridentity fields
		--	next check TeacherSurvey

							OR (tchLangMajor = @Language or tchLangMinor = @Language)
							-- paid by
							OR (tchSponsor = @PaidBy)


			-- qualification - if passed in as _, search for a null.
			-- this allows us to find teachers with no qualification

							OR ( (tchQual is null and @Qualification = '_')
									OR (tchQual = @Qualification)
									)
			-- education qualification is handled the same
							OR ( (tchEdQual is null and @EdQualification = '_')
									OR (tchEdQual = @EdQualification)
									)

			/* subject - search the subject based on '0 = no, 1 = major, 2 = include minor
				search n trained to teach if searchTrained = 1 */

							OR (
									(tchSubjectMajor = @subject AND @searchSubjectTaught = 1)
									OR (@Subject in (tchSubjectMajor, tchSubjectMinor, tchsubjectMinor2) and @SearchsubjectTaught = 2)
									OR (tchSubjectTrained = @Subject AND @searchTrained=1))

			/* role */
							OR (tchRole = @Role)

			-- school/school type/year
							OR ((schNo = @AtSchool)
									OR (ssSchType = @AtSchoolType )
									AND (svyYear = @InYear or @InYear = 0 or @InYear is null))

			-- level tauight
							AND (pclLevel = @LevelTaught
													OR tchClass = @levelTaught
													OR tchClassMax = @LevelTaught
													OR lvlYear between @YearOfEdMin and @YearOfEDMax
								)
			-- ACCESS CONTROL is ANDED even in OR mode
							AND (@AccessControlled = 0 OR CT.tID is not null)

			)

		) S
		ORDER BY MatchScore DESC, tSurname


	end
	else	begin	--@UseOr = 0

		INSERT INTO @KeysAll
		(selectedID)
		Select tID

		from
		(
		Select TeacherIdentity.*
		/*
		,
		 case when tSurname like @Surname and  tGiven like @GivenName then 160
			when tSurname like @Surname and @GivenNameSoundex=tGivenSoundex then 120
			when tGiven like @GivenName and @SurnameSoundex=tSurnameSoundex then 120
			when tGiven like @Surname and  tSurname like @GivenName then 100
			when tGiven like @Surname and @GivenNameSoundex=tSurnameSoundex then 60
			when tGiven like @GivenName and @SurnameSoundex=tGivenSoundex then 60
			when tSurname like @Surname then 40
			when tGiven like @GivenName then 40
			when tSurname like @GivenName then 20
			when tGiven like @Surname  then 20
			when @SurnameSoundex=tSurnameSoundex and @GivenNameSoundex=tGivenSoundex then 20
			when @SurnameSoundex=tGivenSoundex and @GivenNameSoundex=tSurnameSoundex then 20
			when @SurnameSoundex=tSurnameSoundex then 10
			when @GivenNameSoundex=tSurnameSoundex then 10
			else 0
		  end
			+
			case when tSex = @Gender then 2 else 0 end
			+
			case when tDOB = @DoB then 60
				when tDob >= @dob and  tDob <= @dobEnd then 10
				when tDob <= @dobEnd then 5
				when tDob >= @dob then 5
				else 0
			end
			+
			case when tPayroll = @PayrollNo then 120 else 0 end
			MatchScore
*/
		 from TeacherIdentity

		WHERE tID in
		-- the core of the query is here in the nested select this ensure only 1 record per tID
		( Select TeacherIdentity.tID
			from TeacherIdentity
				LEFT JOIN TeacherSurvey ON TeacherIdentity.tID = TeacherSurvey.tID
				LEFT JOIN SchoolSurvey ON TeacherSurvey.ssID = SchoolSurvey.ssID
				LEFT JOIN ClassTeacher on ClassTeacher.tchsID = TeacherSurvey.tchsID
				LEFT JOIN Classes ON Classes.pcID = ClassTeacher.pcID
				LEFT JOIN ClassLevel on Classes.pcID = ClassLevel.pcID
				left join lkpLevels on lkpLevels.codeCode = ClassLevel.pclLevel
				LEFT JOIN @controlledTeachers CT ON TeacherIdentity.tID = CT.tID
			WHERE
			(TeacherIdentity.tID = @TeacherID or @TeacherId is null)
			AND
			(
				(
					(@Surname is null
						or
						(tSurname like @Surname  or (@SoundSearch = 1 and @SurnameSoundex=tSurnameSoundex))
					)
					AND
						(@GivenName is null
							or
							(tGiven like @GivenName or (@SoundSearch = 1  and @GivenNameSoundex=tGivenSoundex))
						)
				)
				OR
				-- cROSSsEARCH
				(
					@CrossSearch = 1 and
					(@Surname is null
						or
						(tGiven like @Surname or (@SoundSearch = 1 and tGivenSoundex=@SurNameSoundex))
					)
					AND
						(@GivenName is null
							or
							(tSurname like @GivenName or ( @SoundSearch = 1 and @GivenNameSoundex=tSurnameSoundex))
						)
				)
			) -- end name search


						AND (tPayroll=@PayrollNo or @PayrollNo is null)
						AND (tRegister = @RegistrationNo or @RegistrationNo is null)
						AND (tProvident = @ProvidentFundNo or @ProvidentFundNo is null)

						AND	(
							(@dob is null and @dobEnd is null)
							or (@dob is null and tDob <= @dobEnd)
							or (@dobEnd is null and tDob =@dob)
							or (tDob >= @dob and  tDob <= @dobEnd)
							)


						AND (tSex = @Gender or @Gender is null)
		-- end of teacheridentity fields
		--	next check TeacherSurvey

							AND (tchLangMajor = @Language or tchLangMinor = @Language or @Language is null)
							-- paid by
							AND (tchSponsor = @PaidBy or @PaidBy is null)


			-- qualification - if passed in as _, search for a null.
			-- this allows us to find teachers with no qualification

							AND ( (tchQual is null and @Qualification = '_')
									OR (tchQual = @Qualification)
									OR (@Qualification is null))
			-- education qualification is handled the same
							AND ( (tchEdQual is null and @EdQualification = '_')
									OR (tchEdQual = @EdQualification)
									OR (@EdQualification is null))

			/* subject - search the subject based on '0 = no, 1 = major, 2 = include minor
				search n trained to teach if searchTrained = 1 */

							AND ((@Subject is null)
									OR (tchSubjectMajor = @subject AND @searchSubjectTaught = 1)
									OR (@Subject in (tchSubjectMajor, tchSubjectMinor, tchsubjectMinor2) and @SearchsubjectTaught = 2)
									OR (tchSubjectTrained = @Subject AND @searchTrained=1))

			/* role */
							AND (tchRole = @Role or @Role is null)

			-- school/school type/year
							AND ((schNo = @AtSchool or @AtSchool is null)
									AND (ssSchType = @AtSchoolType or @AtSchoolType is null)
									AND (svyYear = @InYear or @InYear = 0 or @InYear is null))

			-- level tauight
							AND (pclLevel = @LevelTaught
													OR tchClass = @levelTaught
													OR tchClassMax = @LevelTaught
													OR @levelTaught is null
													OR lvlYear between @YearOfEdMin and @YearOfEDMax
								)
			-- ACCESS CONTROL
							AND (@AccessControlled = 0 OR CT.tID is not null)


			)

		) S
		ORDER BY case @sortColumn
					when 'Surname' then tSurname
					when 'tSurname' then tSurname
					when 'Given' then tGiven
					when 'tGiven' then tGiven
					when 'Payroll' then tPayroll
					when 'tPayroll' then tPayroll
					when 'Sex' then tSex
					when 'tSex' then tSex
					else null
				end,
				case @SortColumn
					when 'DOB' then tDoB
					when 'tDoB' then tDOB
					else null
				end,
				case @sortcolumn
					when 'YearStarted' then tYearStarted
					when 'tYearStarted' then tYearStarted
					else null
				end ,
				tID
	end


	SELECT @NumMatches = @@ROWCOUNT		-- this returns the total matches

	If @SortDir = 1 begin


		SELECT selectedID
		, RecNo
		FROM
		(
			Select selectedID
			, @NumMatches - RecNo + 1 RecNo
			FROM @KeysAll
		) S
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
		ORDER BY RecNo

	end
	else begin

		print @PageSize
		SELECT selectedID
		, RecNo
		FROM @KeysAll
		WHERE (@PageSize = 0
				or
					@PageSize >= @NumMatches
				or
						RecNo between @PageSize * (@PageNo - 1) + 1
						and @PageSize * (@PageNo)

				)
	end
GO

