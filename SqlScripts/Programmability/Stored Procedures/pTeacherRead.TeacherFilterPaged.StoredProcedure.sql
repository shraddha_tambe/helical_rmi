SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pTeacherRead].[TeacherFilterPaged]

	@ColumnSet int = 0,
	@PageSize int = 0,
	@PageNo int = 1,
	@SortColumn sysname = null,
	@SortDir int = 0,

	@TeacherID			int = null,
	@Surname			nvarchar(50) = null,
	@GivenName			nvarchar(50) = null,
	@PayrollNo			nvarchar(50) = null,
	@RegistrationNo		nvarchar(50) = null,
	@PRovidentFundNo	nvarchar(50) = null,

	@Gender				nvarchar(1) = null,
	@DoB				datetime = null,
	@DobEnd				datetime = null,

	@Language			nvarchar(10) = null,
	@PaidBy				nvarchar(10) = null,

	@Qualification		nvarchar(10) = null,
	@EdQualification	nvarchar(10) = null,

	@Subject			nvarchar(20) = null,
	@SearchSubjectTaught	int = 1,		-- 0 means not search - this is only meaningful if you specify a subject and only want to search trained to teach
	@SearchTrained			int = 0,

	@AtSchool			nvarchar(50) = null,
	@AtSchoolType		nvarchar(10) = null,
	@InYear				int = null,
	@Role				nvarchar(10) = null,

	@LevelTaught		nvarchar(10) = null,
	@YearOfEdMin		int = -100,
	@YearOfEdMax		int = -100,

	@ISCEDSubClass		nvarchar(10) = null,

	@UseOr				int = 0,
	@CrossSearch		int = 0,
	@SoundSearch		int = 0,

	@District			nvarchar(10) = null,
	@Authority			nvarchar(10) = null,

	@XmlFilter xml = null
as
	SET NOCOUNT ON;
	-- first off we see what arguments have been passed, to figure out
	-- what tables need to be searched

	DECLARE @keys TABLE
	(
		ID int
		, recNo int
	)

	DECLARE @NumMatches int

	INSERT INTO @keys
	EXEC pTeacherRead.TeacherFilterIDs
		@NumMatches OUT,
		@PageSize,
		@PageNo,
		@SortColumn,
		@SortDir,

	@TeacherID,
	@Surname,
	@GivenName,
	@PayrollNo,
	@RegistrationNo,
	@PRovidentFundNo,

	@Gender			,
	@DoB			,
	@DobEnd			,

	@Language		,
	@PaidBy			,

	@Qualification	,
	@EdQualification,

	@Subject,
	@SearchSubjectTaught,

	@SearchTrained,

	@AtSchool	,
	@AtSchoolType,
	@InYear		,
	@Role		,

	@LevelTaught,
	@YearOfEdMin,
	@YearOfEdMax,

	@ISCEDSubClass,

	@UseOr,
	@CrossSearch,
	@SoundSearch,

	@District,
	@Authority,

	@XmlFilter


	If @ColumnSet = 0

		SELECT TI.*
		from TeacherIdentity TI
			INNER JOIN @Keys K on TI.tID = K.ID


	If @ColumnSet = 1

		SELECT TI.*
		from TeacherIdentity TI
			INNER JOIN @Keys K on TI.tID = K.ID

	if @ColumnSet = 2
	-- most recent survey
		Select LS.*
			from TeacherLatestSurvey LS
				INNER JOIN @Keys K on LS.tID = K.ID


-- 20 - 29 reserved for performance assessment --
if @Columnset = 20
	-- latest pa
	Select PA.*
		from paTeacherLatestAssessmentRpt PA
		INNER JOIN @Keys K on PA.tID = K.ID

-- 30 to 39 reserved for appointments --

-- 40 to 49 reserved for payslips --


	-- finally the summary
		SELECT @NumMatches NumMatches
		, min(RecNo) PageFirst
		, max(recNo) PageLast
		, @PageSize PageSize
		, @PageNo PageNo
		, @Columnset columnSet
		FROM
		@Keys K
return
GO

