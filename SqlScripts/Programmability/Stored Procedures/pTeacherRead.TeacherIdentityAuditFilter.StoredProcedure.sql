SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 24 11 2009
-- Description:	Return the teacher Identity Audit data
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherIdentityAuditFilter]
	-- Add the parameters for the stored procedure here
	@TeacherID int = null
	, @User nvarchar(100) = null
	, @DateStart datetime = null
	, @DateEnd datetime = null
	, @Context nvarchar(100) = null
	, @EditedField sysname = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF (@EditedField is null) begin

	Select *
	, null as editedField
	, null as oldValue
	, null as newValue
	, cast( tAuditChanges as nvarchar(4000)) tAuditChangesXML
	from TeacherIdentityAudit
	WHERE
		(tID = @TeacherID or @TeacherID is null)
	AND (tAuditUser = @User or @USer is null)
	AND (tAuditDate >= @DateStart or @DateStart is null)
	AND (tAuditDate <= @DateEnd or @DateEnd is null)
	AND (tauditContext = @Context or @Context is null)
	ORDER BY tAuditDate DESC
	end

else	begin
	SELECT *
	FROM
	(
	SELECT
		TIA.*
		, N.edit.value('@Field','nvarchar(100)') editedField
		, N.edit.value('@before','nvarchar(100)') oldValue
		, N.edit.value('@after','nvarchar(100)') newValue
		, cast( tAuditChanges as nvarchar(4000)) tAuditChangesXML

		FROM
			 TeacherIdentityAudit TIA
			CROSS APPLY tAuditChanges.nodes('/Edits/Edit')  N(Edit)
		WHERE
			(tID = @TeacherID or @TeacherID is null)
			AND (tAuditUser = @User or @USer is null)
			AND (tAuditDate >= @DateStart or @DateStart is null)
			AND (tAuditDate <= @DateEnd or @DateEnd is null)
			AND (tauditContext = @Context or @Context is null)
	) sub
	WHERE editedField = @EditedField
	ORDER BY tAuditDate DESC

end

END
GO

