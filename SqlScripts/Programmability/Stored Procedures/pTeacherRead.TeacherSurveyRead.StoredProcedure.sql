SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Name
-- Create date:
-- Description:
-- =============================================
CREATE PROCEDURE [pTeacherRead].[TeacherSurveyRead]
	-- Add the parameters for the stored procedure here
	@TeacherSurveyID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT *
	FROM
		pTeacherRead.TeacherSurveyV
	WHERE tchsID = @TeacherSurveyID


END
GO

