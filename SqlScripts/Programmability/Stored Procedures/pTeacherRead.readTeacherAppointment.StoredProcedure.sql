SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [pTeacherRead].[readTeacherAppointment]
	-- Add the parameters for the stored procedure here
	@AppointmentID int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		Select App.*
		, Schools.schName
		, Schools.schAuth
		, R.codeDescription RoleDesc
		, TI.tPayroll
		, TI.tNamePrefix
		, TI.tGiven
		, TI.tMiddleNames
		, TI.tSurname
		, TI.tNameSuffix
		, TI.tFullName
		, TI.tDOB
		, TI.tSex
		, TI.tProvident
		, D.dPayrollID			-- the "location" of the position is the province
		From
		(Select
			TA.taID
			, TA.tID
			, TA.taType
			, TA.taDate
			, TA.taEndDate
			, TA.estpNo
			, TA.spCode
			, E.estpTitle
			-- this is a little kludge for backward compatibility
			, isnull(TA.SchNo,E.SchNo) schNo
			, isnull(TA.tarole,RG.roleCode) taRole
			, RG.rgCode
			, RG.rgSalaryLevelMin
			, RG.rgSalaryLevelMax
		from TeacherAppointment TA
			LEFT JOIN Establishment E ON E.estpNo = TA.estpNo
			LEFT JOIN RoleGrades RG on E.estpRoleGRade = RG.rgCode
		) App
		INNER JOIN Teacheridentity TI on TI.tID = App.tID
		LEFT JOIN Schools ON Schools.schNo = App.SchNo
		LEFT JOIN TRTeacherRole R ON R.codeCode = App.taRole
		LEFT JOIN Islands I
			ON Schools.iCode = I.iCode
		LEFT JOIN Districts D
			ON I.iGroup = D.dID

		WHERE
			App.taID = @AppointmentID

		return


END
GO

