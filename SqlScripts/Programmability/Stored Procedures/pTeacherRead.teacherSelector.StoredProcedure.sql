SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [pTeacherRead].[teacherSelector]
	@s nvarchar(50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @ss nvarchar(100)
declare @sg nvarchar(100)
declare @k int
select @k =charindex(@s,',')
--if (@k >= 0) begin
----select @ss = left(@s, @k-1)
--end
if (isnumeric(@s) = 1) begin
select top 30 tPayroll + ': ' + tSurname + ', ' + tGiven N, tID C, tPayroll P
	From TeacherIdentity
	 WHERE tPayroll like @s + '%'
end
else begin
	select top 30 tSurname + ', ' + tGiven N, tID C, tPayroll P
	From
	(Select TOP 30 tsurname, tGiven, tID, tPayroll
	from TeacherIdentity
	WHERE tSurname like @s +'%'
	UNION
	Select TOP 30 tsurname, tGiven, tID, tPayroll
	from TeacherIdentity
	WHERE tSurname like '%' + @s +'%'
	) SUB
end
select @@rowcount num
END
GO

