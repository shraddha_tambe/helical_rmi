SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 10 2009
-- Description:	Information reconcile of payslips
-- =============================================
CREATE PROCEDURE [pTeacherReadX].[PayPeriodCompare]
	-- Add the parameters for the stored procedure here
	@PayPeriod datetime = null
	, @PrevPeriod datetime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


if (@PayPeriod is null)
	select @PayPeriod =max(tpsPeriodEnd)
		from TeacherPayslips


if (@PrevPeriod is null)
	Select @PrevPeriod = max(tpsPeriodEnd)
	from TeacherPayslips
	WHERE tpsPeriodEnd < @PayPEriod


create table #paynos
	(
		payno nvarchar(20)
	)

create table #psRec
	(tpsPayroll nvarchar(20)
	, PrevID int NULL
	, prevSalaryPoint nvarchar(10) NULL
	, prevPosition nvarchar(20) NULL
	, prevPayPointCode nvarchar(20) NULL
	, ID int NULL
	, SalaryPoint nvarchar(10) NULL
	, Position nvarchar(20) NULL
	, PayPointcode nvarchar(20) NULL
	, tpsShortName nvarchar(50) NULL
	, Added int
	, Dropped int
	, SalaryPointChanged int
	, PositionChanged int
	, PayPointCodeChanged int
	)

INSERT INTO #paynos
Select DISTINCT tpsPayroll
From TeacherPayslips
WHERE tpsPeriodEnd in (@PayPeriod, @PrevPeriod)


INSERT INTO #psrec
Select u2.tpsPayroll
		, PrevID
		, PrevSalaryPoint
		, PrevPosition
		, PrevPayPointCode
		, ID
		, SalaryPoint
		, Position
		, PayPointCode
		, PS.tpsShortName
		, case when PrevID is null then 1 else 0 end Added
		, case when ID is null then 1 else 0 end Dropped
		, case when PrevSalaryPoint <> SalaryPoint then 1 else 0 end SalaryPointChanged
		, case when PrevPosition <> Position then 1 else 0 end PositionChanged
		, case when PrevPayPointCode <> PayPointCode then 1 else 0 end PayPointCodeChanged
from
	(
	Select u.tpsPayroll

		, min(PrevID) PrevID
		, min(PrevSalaryPoint) PrevSalaryPoint
		, min(PrevPosition) PrevPosition
		, min(PrevPayPointCode) PrevPayPointCode

		, min(ID)  ID
		, min(SalaryPoint) SalaryPoint
		, min(Position) Position
		, min(PayPointCode) PayPointCode

	from
		(
		select #paynos.payno tpsPayroll
			, tpsID PrevID
			, tpsSalaryPoint PRevSalaryPoint
			, tpsPosition PrevPosition
			, tpsPayPoint PrevPayPointCode
			, null ID
			, null SalaryPoint
			, null Position
			, null PayPointCode
		from #paynos
			LEFT JOIN (Select * from TeacherPayslips WHERE tpsPeriodEnd = @PrevPeriod) PS
			on #Paynos.payno = PS.tpsPayroll

		UNION
		select #paynos.payno  tpsPayroll
			,null
			,null
			,null
			,null
			, tpsID ID
			, tpsSalaryPoint SalaryPoint
			, tpsPosition Position
			, tpsPayPoint PayPointcode
		from #paynos
			LEFT JOIN (Select * from TeacherPayslips WHERE tpsPeriodEnd = @PayPeriod) PS
			on #Paynos.payno = PS.tpsPayroll

		) u
		group by u.tpsPayroll    -- Insert statements for procedure here
	) u2
	INNER JOIN TeacherPayslips PS
		on PS.tpsId = isnull(u2.ID, u2.PrevID)

Select * FROM #psrec


-- now we'll return some stats in a second resultset - DAO consumers need not apply!
--
declare @PrevCount int
declare @PrevGross int
declare @ThisCount int
declare @ThisGross int


Select @PrevCount = count(tpsID)
		, @PrevGross = sum(tpsGross)
from
	TeacherPayslips
WHERE
	tpsPeriodEnd = @PrevPeriod

-- current totals
Select @ThisCount = count(tpsID)
		, @ThisGross = sum(tpsGross)
from
	TeacherPayslips
WHERE
	tpsPeriodEnd = @PayPeriod


Select
	@PrevPeriod	PayPeriod1
	, @PayPeriod	PayPeriod2
	, @PrevCount Count1
	, @PrevGross Gross1
	, @ThisCount Count2
	, @ThisGross Gross2
	, Added
	, Dropped
	, SalaryPointChanged
	, PayPointCodeChanged
	, PositionChanged

from
	(
	Select
		sum(Added) Added
		, sum(Dropped) Dropped
		, sum(SalaryPointChanged) SalaryPointChanged
		, sum(PositionChanged) PositionChanged
		, sum(PayPointCodeChanged) PayPointCodeChanged
	from
		#psrec
	) u

drop table #Paynos
drop table #psRec

--


END
GO

