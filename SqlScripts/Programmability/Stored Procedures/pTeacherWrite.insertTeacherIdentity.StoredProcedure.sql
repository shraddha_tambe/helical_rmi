SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 12 2007
-- Description:	update teachers from smis xml format
-- =============================================
CREATE PROCEDURE [pTeacherWrite].[insertTeacherIdentity]
	-- Add the parameters for the stored procedure here

	@NamePRefix nvarchar(50),
	@Given nvarchar(50) ,
	@MiddleNames nvarchar(50),
	@Surname nvarchar(50) ,
	@NameSuffix nvarchar(50),

	@Sex nvarchar(1) ,
	@DOB datetime ,

	@Register nvarchar(50) ,
	@Provident nvarchar(50) ,
	@Payroll nvarchar(50),

	@YearStarted int  = null,
	@DatePSAppointed datetime  = null,

	@YearFinished int  = null,
	@DatePSClosed datetime  = null,
	@SalaryPoint nvarchar(10) = null ,
	@SalaryPointDate datetime  = null,
	@payptCode nvarchar(10) = null ,
	@EditContext nvarchar(50) = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
----	SET NOCOUNT ON;

begin try
declare @newID TABLE
(tID int)

INSERT INTO [TeacherIdentity]
           (
           [tNamePrefix]
           ,[tGiven]
           ,[tMiddleNames]
           ,[tSurname]
           ,[tNameSuffix]

           ,[tSex]
           ,[tDOB]

           ,[tRegister]
           ,[tProvident]
           , tPayroll

           ,[tYearStarted]
           ,[tYearFinished]
           ,[tDatePSAppointed]
           ,[tDatePSClosed]

           ,[tSalaryPoint]
           ,[tSalaryPointDate]
           ,tPayPtcode
		   ,pEditcontext

  )
   OUTPUT INSERTED.tID INTO @newID

     VALUES
           (
           @NamePrefix
          , @Given
          , @MiddleNames
          , @Surname
          , @NameSuffix

           ,@Sex
        	,@DOB

			, @Register
			, 	@Provident
			, @Payroll

			, 	@YearStarted
			,  	@YearFinished
			,	@DatePSAppointed
			,	@DatePSClosed
			, 	@SalaryPoint
			,  	@SalaryPointDate
			, 	@payptCode
			, @EditContext
           )
Select *
from TeacherIdentity
	INNER JOIN @newID N
		ON TeacherIdentity.tID = N.tID

if (@@ROWCOUNT = 0)
	Select *
	from TeacherIdentity
	WHERE tID = SCOPE_IDENTITY()


end try
begin catch

    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000),
        @ErrorSeverity INT,
        @ErrorState INT;
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	if @@trancount > 0
		begin
			rollback transaction
			select @errorMessage = @errorMessage + ' The transaction was rolled back.'
		end

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	return @err
end catch
-- and commit

if @@trancount > 0
	commit transaction


-- update all the required fields on TeacherIdentity affected by the survey
END
GO

