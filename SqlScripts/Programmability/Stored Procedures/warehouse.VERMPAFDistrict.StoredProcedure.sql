SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 23 10 2017
-- Description:	Creates an XML document representing the high level indicators used in the Vanuatu VERM PAF
-- this version is built from the warehouse where possible,
-- and filters to a single district (aka province, state)
-- Currently supports school counts, and enrolment ratios at district level
-- =============================================
CREATE PROCEDURE [warehouse].[VERMPAFDistrict]
	-- Add the parameters for the stored procedure here
	@districtCode nvarchar(10)
	, @Year int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   declare @xmlLevelER xml			-- level enrolment ratiows
   declare @xmlPCR xml				-- pupil classroom
   declare @xmlER	xml				-- education level ratios
   declare @XMLSR xml				-- survival - reconstructed cohort
   declare @xmlTQC xml				-- teacher qual cert
   declare @xmltext xml				-- textbook pupils by ed level
   declare @xmlExp	xml				-- expenditure
   declare @xmlSchoolCounts xml		-- school counts

   declare @verm xml				-- the entire return

-- all these SPs may emit a recordset (@SendASXML = 0)
-- or else XML @SendAsXML = 1 or 2 will set the xml OUT parameter
-- if @SendASXML = 2, the xml is NOT returned as a resultset
-- this allows a caller such as the current stored proc to avoid having to catch the
-- output of the query using INSERT....EXEC
-- this is importnant becuase INSERT...EXEC cannot be nested
-- and some of the procedures (e.g. EnrolmentRatios)
-- themselves use INSERT....EXEC to source data from another SP


	exec warehouse._vermSchoolCountsDistrict @districtCode, 1, @xmlSchoolCounts OUT
    exec warehouse._vermEdLevelERDistrict @districtCode, 2,@xmlER OUT

----------	-- use the warehouse version
----------    exec warehouse._vermLevelER 2, @xmlLevelER OUT

----------    exec dbo.PupilClassRoomRatio 2, @xmlPCR OUT

----------    exec warehouse._vermSurvival 2, @xmlSR OUT

------------ the newer format for getting this now depends on Teacher Appointments and TeacherTRaining
------------ rather than TeacherSurvey
------------ for backward compatibility, flag control this

----------declare @newTQ int
----------select @newTQ = paramInt from sysParams
----------WHERE paramName = 'TEACHER_QUALCERT_SOURCE'
----------if @newTQ = 1
----------	exec dbo.sp_EFA11DataAppt 1,2, @xmlTQC OUT
----------else
----------    exec warehouse._vermSector 2, @xmlTQC OUT

----------	exec dbo.xmlEFA7 1, @xmlExp OUT		-- finance

----------	exec dbo.pupilTextbookRatio 1,2, @xmlText OUT


	-- system parameters

	declare @nation nvarchar(100)
	declare @Ministry nvarchar(200)
	declare @MinShort nvarchar(20)
	declare @AppNameFull nvarchar(200)
	declare @AppName nvarchar(20)

	select @nation = common.sysParam('USER_NATION')
	select @Ministry = common.sysParam('USER_ORG_FULL')
	select @MinShort = common.sysParam('USER_ORG')
	select @AppNameFull = common.sysParam('APP_NAME_FULL')
	select @AppName = common.sysParam('APP_NAME')

	declare @districtName nvarchar(50)
	select @districtName = dName
	From Districts
	WHERE dID = @districtCode

	declare @xmlParams xml

	SELECT @xmlParams
	=
	(
	SELECT
	@nation [@nation]
	,@Ministry [@ministryName]
	,@Minshort [@ministry]
	,@AppNameFull  [@appNameFull]
	,@AppName [@appName]
	, @districtCode [@districtCode]
	, @districtName [@districtName]
	FOR XML PATH('Params')
	)
 -- wrap these shards of xml into a single document
    SELECT @verm
    =
    (
    SELECT
    @@Servername [@server]
    , DB_NAME() [@database]
    , getutcdate() [@createdate]
    , @xmlParams
    , @xmlSchoolCounts
    , @xmlER
    , @xmlLevelER
    , @XMLSR
    , @xmlTQC
    , @xmlExp
    , @xmlPCR
   , @xmlText
    FOR XML PATH('VERM')
    )


    SELECT @VERM

END
GO

