SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: <Create Date,,>
-- Description:	Calculates key statistics of the Flow model, at the national level
-- this populates the Survial node of the VERMPAF Xml file
-- =============================================
CREATE PROCEDURE [warehouse].[_vermSurvival]
	-- Add the parameters for the stored procedure here
	@SendASXML int = 0
	, @xmlOut xml  = null OUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


-- this nestngs allow the calculation of progressively more dependant results
SELECT
SurveyYear
, U.YearOfEd
, levelCode
, avg(enrolM) enrolM
, avg(enrolF) enrolF
, avg(enrol) enrol
, avg(repM) repM
, avg(repF) repF
, avg(rep) rep
, avg(PRM) PRM
, avg(PRF) PRF
, avg(PR) PR
, avg(RRM) RRM
, avg(RRF) RRF
, avg(RR) RR
, avg(DRM) DRM
, avg(DRF) DRF
, avg(DR) DR
, avg(TRM) TRM
, avg(TRF) TRF
, avg(TR) TR
, cast(1 as float) SRM
, cast (1 as float) SRF
, cast (1 as float) SR
INTO #results
FRom
(
select SurveyYear
, YearOfEd yearOfEd
, enrol enrolM
, null enrolF
, null enrol
, rep repM
, null repF
, null rep
, PromoteRate PRM
, null PRF
, null PR
, RepeatRate RRM
, null RRF
, null RR
, DropoutRate DRM
, null DRF
, null DR
, SurvivalRate TRM
, null TRF
, null TR
From warehouse.NationFlow
WHERE GenderCode = 'M'
UNION ALL
select SurveyYear
, yearOfEd
, null enrolM
, enrol enrolF
, null enrol
, null enrolM
, rep repF
, null rep
, null PRM
, PromoteRate PRF
, null PR
, null RRM
, RepeatRate RRF
, null RR
, null DRM
, DropoutRate DRF
, null DR
, null TRM
, SurvivalRate TRF
, null TR
From warehouse.NationFlow
WHERE GenderCode = 'F'
UNION ALL
select SurveyYear
, yearOfEd
, null enrolM
, null enrolF
, enrol enrol
, null enrolM
, null repF
, rep rep
, null PRM
, null PRF
, PromoteRate PR
, null RRM
, null RRF
, RepeatRate RR
, null DRM
, null DRF
, DropoutRate DR

, null TRM
, null TRF
, SurvivalRate TR
From warehouse.NationFlow
WHERE GenderCode is null
) U
	INNER JOIN ListDefaultPathLevels DL
			ON DL.YearOfEd = U.yearOfEd
GROUP BY SurveyYEar, U.yearOfEd, DL.levelCode

declare @i int
declare @max int
SELECT @max = max(lvlYear)
from lkpLevels

-- this part multiplies the transitions rates together
Select @i = 1
while (@i < @max) begin
	UPDATE #Results
	SET SRM = T.SRM * T.TRM
	, SRF = T.SRF * T.TRF
	, SR = T.SR * T.TR
	FROM #REsults
		INNER JOIN #results T
	ON #results.SurveyYEar = T.SurveyYear
		AND #results.yearOfEd = T.yearOfEd + 1
		AND T.yearofEd = @i

	SELECT @i = @i + 1

end

IF @SendASXML = 0 begin
	SELECT * from #results

end

IF @SendASXML <> 0 begin

	declare @XML xml
	SELECT @XML =
	(
		SElect surveyYear [@year]
		, levelCode [@levelCode]
		,yearOfEd	[@yearOfEd]
		, EnrolM
		, EnrolF
		, Enrol
		, RepM
		, RepF
		, Rep
		, cast(PRM as decimal(8,5)) PRM
		, cast(PRF as decimal(8,5)) PRF
		, cast(PR as decimal(8,5)) PR
		, cast(RRM as decimal(8,5)) RRM
		, cast(RRF as decimal(8,5)) RRF
		, cast(RR as decimal(8,5)) RR
		, cast(DRM as decimal(8,5)) DRM
		, cast(DRF as decimal(8,5)) DRF
		, cast(DR as decimal(8,5)) DR
		, cast(TRM as decimal(8,5)) TRM
		, cast(TRF as decimal(8,5)) TRF
		, cast(TR as decimal(8,5)) TR
		, cast(SRM as decimal(8,5)) SRM
		, cast(SRF as decimal(8,5)) SRF
		, cast(SR as decimal(8,5)) SR
	FROM #results

	FOR XML PATH('Survival')
	)

	SELECT @XMLOut =
	(
		SELECT @xml
		FOR XML PATH('Survivals')
	)

	IF @SendASXML = 1
		SELECT @XMLOut
end

drop table #results


END
GO

