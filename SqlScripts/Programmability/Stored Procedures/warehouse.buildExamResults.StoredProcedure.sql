SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 10 10 2017
-- Description:	Rebuild the examination warehouse tables
--              ExamSchoolREsults and ExamStateResults
--              The tables are rebuilt from ExamCandidates and ExamCandidatereSults
-- ie this relies on the NEW (FSM) Xml loaded exam structure for Standard Tests of Achievement
-- ARGS: @examID - delete and reinstate this single examID
-- in this mode, it is called from loadExam - we may as well recreate the warehouse table as soon as batch loading the exam?
-- @year - recalculate every exam from this year forward - this will become the standard for warehouse regeneration

-- =============================================
CREATE PROCEDURE [warehouse].[buildExamResults]
@examID int = null
, @baseYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- first clear the record we need to clear from ExamSchoolResults
DELETE
From warehouse.ExamSchoolResults
WHERE (examYear >= @baseYear or @baseYear is null)
AND (examID = @examID or @examID is null)

DELETE
From warehouse.ExamStateResults
WHERE (examYear >= @baseYear or @baseYear is null)
AND (examID = @examID or @examID is null)

-- rebuild school results from base tables
INSERT INTO warehouse.examSchoolResults
([examID]
,[examCode]
,[examYear]
,[examName]
,[StateID]
,[State]
,[schNo]
,[Gender]
,[standardID]
,[standardCode]
,[standardDesc]
,[benchmarkID]
,[benchmarkCode]
,[achievementLevel]
,[achievementDesc]
,[Candidates])
Select
X.exID					examID
, X.exCode				examCode
, X.exYear				examYear
, XT.exName				examName
, C.dID					StateID
, D.dName				[State]
, C.schNo				schNo
, C.excGender			Gender
, B.exstdID				standardID
, STD.exstdCode			standardCode
, STD.exstdDescription	standardDesc
, R.exbnchID			benchmarkID
, B.exbnchCode			benchmarkCode
, R.excrLevel			achievementLevel
, A.exalDescription		achievementDesc
, count(R.excrID)		Candidates
from ExamCandidateResults R
INNER JOIN ExamCandidates C
	ON R.excID = C.excID
INNER JOIN Exams X
	ON C.exID = X.exID
INNER JOIN lkpExamTypes XT
	ON X.exCode = XT.exCode
INNER JOIN Districts D
	ON D.dID = C.dID
INNER JOIN examBenchmarks B
	ON R.exbnchID = B.exbnchID
INNER JOIN examStandards STD
	ON B.exstdID = STD.exstdID
INNER JOIN examAchievementLevels A
	ON A.exID = C.exID
	AND A.exalVal = R.excrLevel
WHERE
	(X.exYear >= @baseYear or @baseYear is null)
	AND (X.exID = @examID or @examID is null)

GROUP BY
X.exID
, X.exCode
, X.exYear
, XT.exName
, C.dID
, D.dName
, C.schNo
, C.excGender
, B.exstdID
, STD.exstdCode
, STD.exstdDescription
, R.exbnchID
, B.exbnchCode
, R.excrLevel
, A.exalDescription

INSERT INTO warehouse.ExamStateResults
(
[examID]
,[examCode]
,[examYear]
,[examName]
,[StateID]
,[State]

,[Gender]
,[standardID]
,[standardCode]
,[standardDesc]
,[benchmarkID]
,[benchmarkCode]
,[achievementLevel]
,[achievementDesc]
,[Candidates]
)
SELECT
[examID]
,[examCode]
,[examYear]
,[examName]
,[StateID]
,[State]

,[Gender]
,[standardID]
,[standardCode]
,[standardDesc]
,[benchmarkID]
,[benchmarkCode]
,[achievementLevel]
,[achievementDesc]
,sum(Candidates) Candidates
FROM warehouse.ExamSchoolResults
WHERE
	(examYear >= @baseYear or @baseYear is null)
	AND (examID = @examID or @examID is null)
GROUP BY
[examID]
,[examCode]
,[examYear]
,[examName]
,[StateID]
,[State]

,[Gender]
,[standardID]
,[standardCode]
,[standardDesc]
,[benchmarkID]
,[benchmarkCode]
,[achievementLevel]
,[achievementDesc]
END
GO

