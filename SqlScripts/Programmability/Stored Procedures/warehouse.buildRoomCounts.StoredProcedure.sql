SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 15 2 2017
-- Description:	build warehouse classrom
-- =============================================
CREATE PROCEDURE [warehouse].[buildRoomCounts]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SET NOCOUNT ON;

DELETE FROM warehouse.roomCounts
WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
print 'warehouse.cohort deletes - rows:' + convert(nvarchar(10), @@rowcount)

INSERT INTO warehouse.roomCounts
(
	schNo
	, SurveyYear
	, rmType
	, numRooms
	, avgSize
	, NewestRoom
	, rmMaterials
	, Estimate
)
Select E.schNo
, E.LifeYear SurveyYear
, E.rmType
, numRooms
, avgSize
, NewestRoom
, rmMaterials
, E.Estimate

FROM ESTIMATE_BestSurveyAnyRooms E
 INNER JOIN
 (
	Select rmType, rmMaterials, ssID, count(*) NumRooms
	, avg(rmSize) AvgSize
	, max(rmYear) NewestRoom
	from Rooms
	GROUP BY rmType, rmMaterials, ssID
) T
	ON E.bestssID = T.ssID
	AND E.rmType = t.rmType

WHERE (E.LifeYear >= @StartFromYear or @StartFromYear is null)

END
GO

