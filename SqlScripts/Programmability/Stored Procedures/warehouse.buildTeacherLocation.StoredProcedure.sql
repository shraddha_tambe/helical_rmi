SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 13 11 2016
-- Description:	Collect the school and role of teachers, using appointments if available,
-- confirmed (or contradicted) by TeacherSurvey
-- This procedure is responsible for populating these tables:
-- warehouse.TeacherQual
-- * the qualifications earned by each teacher, and the year in which acquired (or reported)
-- warehouse.TecherLocation
-- * where each teacher is in a year. This can be determined from the TEacherSurvey or the appointment
-- warehouse.schoolTeacherCount
-- * number of teachers at each school
-- =============================================
CREATE PROCEDURE [warehouse].[buildTeacherLocation]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		DELETE
		FROM warehouse.teacherQual
		WHERE (yr >= @StartFromYear or @StartFromYEar is null)

		print 'warehouse.teacherQual deletes - rows:' + convert(nvarchar(10), @@rowcount)

		INSERT INTO Warehouse.TeacherQual
		Select *
		FROM (
			Select tID
				, min(svyYear) yr
				, tchQual
				FROM TEacherSurvey TS
				INNER JOIN SchoolSurvey SS
					ON TS.ssID = SS.ssID
				WHERE tchQual is not null
				GROUP BY tID, tchQual
			UNION
				Select tID
				, min(svyYear) yr
				, tchEdQual
				FROM TEacherSurvey TS
				INNER JOIN SchoolSurvey SS
					ON TS.ssID = SS.ssID
				WHERE tchEdQual is not null
				GROUP BY tID, tchEDQual
				) U
		WHERE (yr >= @StartFromYear or @StartFromYEar is null)

		print 'warehouse.teacherQual inserts - rows:' + convert(nvarchar(10), @@rowcount)

---------------------------------------------------------------------
-- TEACHER LOCATION ---
---------------------------------------------------------------------

-- update of exisitng table

-- there is a significant performane problem here with filtering on svyYear is done directly
-- so lets write a simple table out for it
declare @yrs TABLE
(
	svyYear int
	, svyCensusDate datetime
)

INSERT INTO @yrs
Select svyYear
, svyCensusDate
from Survey
WHERE (svyYear >= @StartFromYear or @StartFromYEar is null)


	DELETE
	from warehouse.TeacherLocation
	WHERE (SurveyYear >= @StartFromYear or @StartFromYear is null)
	print 'warehouse.teacherLocation deletes - rows:' + convert(nvarchar(10), @@rowcount)

	INSERT INTO warehouse.TeacherLocation
	Select S.TID
	, svyYear SurveyYear
	, tSex GenderCode
	, year(tDoB) BirthYear
	, case
		when svyYear - year(tDoB) >= 60 then '60+'
		when svyYear - year(tDoB) >= 50 then '50-59'
		when svyYear - year(tDoB) >= 40 then '40-49'
		when svyYear - year(tDoB) >= 30 then '30-39'
		when svyYear - year(tDoB) >= 20 then '20-29'
		when svyYear - year(tDoB) >= 0 then '<20'
		else null end AgeGroup
	, ApptSchNo
	, ApptRole
	, ApptYear
	, ApptSchEstimate
	, SurveySchNo
	, SurveyRole
	, SurveyDataYear
	, Estimate
	, SurveySector
	, ISCEDSubClass
	, SurveySupport
	, case
			-- no disagreement between survey and appt ( survey may be estimate)
			when SurveySchNo = ApptSchNo then SurveySchNo
			-- conflict but survey is not estimate - use survet
			when SurveySchNo <> ApptSchNo and Estimate = 0 then SurveySchNo
			-- conflict but survey is estimate and appt is newer than last survey - so use it
			when SurveySchNo <> ApptSchNo and Estimate = 1 and ApptYear >= SurveyDataYear then ApptSchNo
			-- conflict and survey is estimate but is more recent than the appt - so use it
			when SurveySchNo <> ApptSchNo and Estimate = 1 and ApptYear < SurveyDataYear then SurveySchNo
			else coalesce(ApptSchNo, SurveySchNo) end SchNo
	, case when SurveySchNo = ApptSchNo and Estimate = 0 then 'C' -- confirmed
			when SurveySchNo = ApptSchNo then 'CE'				-- matching, but survey is estimate
			-- survey contradicts appt - using school ( see rules for SchNo)
			when surveySchNo <> ApptSchNo and Estimate = 0 then 'XS'
			-- conflict but survey is estimate and appt is newer than last survey - so use it
			-- but flag if the appt school returned a survey and the teacher was not on it
			when SurveySchNo <> ApptSchNo and Estimate = 1 and ApptYear >= SurveyDataYear and ApptSchEstimate = 0 then 'XA?'

			when SurveySchNo <> ApptSchNo and Estimate = 1 and ApptYear >= SurveyDataYear then 'XA'
			-- conflict but survey is estimate and rolled back to this year from a future year
			-- if an appt is available, we'll use it
			-- again flag if the school did not report the teacher
			when SurveySchNo <> ApptSchNo and Estimate = 1 and SurveyDataYear > svyYear and apptSchEstimate = 0 then 'XA?'
			when SurveySchNo <> ApptSchNo and Estimate = 1 and SurveyDataYear > svyYear then 'XA'
			-- conflict and survey is estimate but is more recent than the appt - so use it
			when SurveySchNo <> ApptSchNo and Estimate = 1 then 'XE'
			-- left with the cases that survey or appt null
			-- ApptSchEstimate flags whether we got a survey from the appt school
			-- if we have not, we can assume the appointment is correct, but unvalidated
			when ApptSchNo is not null and ApptSchEstimate = 0 then 'A?'						--
			-- if we did get the school survey, but this teacher is not on there, and there is not other estimate for this teacher
			-- they are probably not there
			when ApptSchNo is not null then 'A'						--
			when SurveySchNo is not null and Estimate = 0 then 'S'
			when SurveySchNo is not null and Estimate = 1 then 'E'
			end Source
	, case
			-- no disagreement between survey and appt ( survey may be estimate)
			when SurveyRole = ApptRole then SurveyRole
			-- conflict but survey is not estimate - use survet
			when SurveySchNo <> ApptSchNo and Estimate = 0 then SurveyRole
			-- conflict but survey is estimate and appt is newer than last survey - so use it
			when SurveySchNo <> ApptSchNo and Estimate = 1 and ApptYear >= SurveyDataYear then ApptRole
			-- conflict but survey is estimate and survey is rolled back - use appt
			when SurveySchNo <> ApptSchNo and Estimate = 1 and SurveyDataYear > svyYear then ApptRole
			-- conflict and survey is estimate but is more recent than the appt - so use it
			when SurveySchNo <> ApptSchNo and Estimate = 1 then SurveyRole
			else coalesce(ApptSchNo, SurveySchNo) end Role

	, SurveySector				Sector
	, convert(int, 0)			Certified
	, convert(int, 0)			Qualified
	-- record any extra surveys this teacher may have reported in the year
	-- for example, they may be on an actual at their new school, and still on Estimate from last school
	, convert(int, 0)			XtraSurvey

	FROM
	(
		Select TID
		, svyYear
		, min(ApptSchNo) ApptSchNo
		, min(ApptRole) ApptRole
		, min(ApptYear) ApptYear
		, min(ApptSchEstimate) ApptSchEstimate
		, min(SurveySchNo) SurveySchNo
		, min(SurveyDataYear) SurveyDataYear
		, min(Estimate) Estimate
		, min(SurveyRole) SurveyRole
		, min(SurveySector) SurveySector
		, min(ISCEDSubClass) ISCEDSubClass
		, min(SurveySupport) SurveySupport
		FROM
		(
		Select TA.tID
		, SVY.svyYear
		, E.schNo ApptSchNo
		, RG.roleCode ApptRole
		, year(taDate) ApptYear
		, EST.Estimate ApptSchEstimate
		, null SurveySchNo
		, null SurveyDataYear
		, null Estimate
		, null SurveyRole
		, null SurveySector
		, null ISCEDSubClass			-- not an obvious way to get this frin appointment
		, null SurveySupport
		from TeacherAppointment TA
			INNER JOIN TeacherIDentity TI
				ON TA.tID = TI.tID
			INNER JOIN @yrs SVY
				ON TA.taDate <= SVY.svyCensusDate
				AND (TA.taEndDate is null or TA.taEndDate >= svyCensusDate)
			INNER JOIN Establishment E
				ON TA.estpNo = E.estpNo
			LEFT JOIN RoleGrades RG
				ON E.estpRoleGrade = RG.rgCode
			LEFT JOIN ESTIMATE_BestSurveyTeachers EST
				ON SVY.svyYEar = LifeYear
				AND E.schNo = EST.schNo
		-- performance is markedly better when this criteria is applied before the UNION

		UNION ALL
		-- we need to be careful oly to get one record for a teacher
		-- if they have changed schools, they may appear on multiple estimates for a given year
		Select tID
			, svyYear
			, null ApptSchNo
			, null ApptRole
			, null ApptYear
			, null ApptSchEstimate
			, schNo
			, SurveyDataYear
			, Estimate
			, SurveyRole
			, SurveySector
			, ISCEDSubClass
			, SurveySupport
			FROM (
				SELECT TS.tID
				, EST.LifeYEar svyYear
				, EST.schNo
				, EST.bestYear SurveyDataYear
				, EST.Estimate
				, TS.tchRole SurveyRole
				, coalesce(TS.tchSector, DL.sectorCode) SurveySector
				, DL.[ISCED SubClass] ISCEDSubClass
				, TS.tchSupport SurveySupport
				-- the teacher may appear on more than one survey in a year ( one actual + multiple Estimate?)
				-- find the best survey to represent them
				, Row_number() OVER ( Partition by TS.tID, LifeYear ORDER BY Estimate ASC,
					case when bestYear < LifeYear then 0 else 1 end ASC,
					case when bestYear < LifeYear then bestYear * -1 else bestYear end ASC) RowNo
				FROM ESTIMATE_BestSurveyTeachers EST
					INNER JOIN TeacherSurvey TS
						ON EST.bestssID = TS.ssID
					-- added ISCED to here
					LEFT JOIN DimensionLevel DL
						ON coalesce(TS.tchClassMax, TS.tchClass) = DL.LevelCode
				) TSurveys
			WHERE RowNo = 1
			AND (svyYear >= @StartFromYear or @StartFromYEar is null)
		) U


		GROUP BY tID, svyYEar
	) S
	INNER JOIN TeacherIdentity TI
		ON S.tID = TI.tID
	WHERE coalesce(ApptSchNo, SurveySchNo) is not null


	print 'warehouse.teacherLocation inserts - rows:' + convert(nvarchar(10), @@rowcount)

---------------------------------------------------------------------------------------
-- Sector and ISCED codes
---------------------------------------------------------------------------------------
-- sector and ISCED are already populated using the techer survey class/ class max if these are available

-- try to patch any missing sector values before calculating cert / qual

-- first attempt - get the School type of the school in the year
-- If all class levels in that school type have the samee sector, use that sector

UPDATE warehouse.TeacherLocation
SET Sector = coalesce(Sector,SectorCode)
, ISCEDSubClass = CalcSector.ISCEDSubClass
from warehouse.TeacherLocation

INNER JOIN warehouse.bestSurvey BSS
	ON warehouse.TeacherLocation.SchNo = BSS.schNo
	AND warehouse.TeacherLocation.SurveyYear = BSS.SurveyYear
 INNER JOIN warehouse.dimensionSchoolSurvey DSS
	ON DSS.[Survey ID] = BSS.SurveyDimensionID
INNER JOIN
(
Select stCode
, case when min(L.secCode) = max(L.secCode) then max(L.secCode) end SectorCode
, max(L.ilsCode) ISCEDSubClass
FROM metaSchoolTypeLevelMap
INNER JOIN lkpLevels L
	ON tlmLevel = L.codecode
	GROUP BY stCode
) CalcSector
ON CalcSector.stCode = DSS.SchoolTypeCode
WHERE (Sector is null or  warehouse.TeacherLocation.ISCEDSubClass is null)

print 'warehouse.teacherLocation update sector - rows:' + convert(nvarchar(10), @@rowcount)


-- now find all the qualifications earned that are prior to a reporting year
-- we'll take these as anything that has appeared on the school survey
-- to get Certified and qual, look at all the qualifications the teacher has in years <= sumarveyyear
-- then
-- first figure out if we are differentiating qual cert based on sectr - ie are the qual cert records all the same?
declare @defaultSector nvarchar(10)

Select @defaultSector = max(ytqSector)
from SurveyYearTeacherQual


UPDATE warehouse.TeacherLocation
SET Qualified = BestQual
, Certified = BestCert
FROM warehouse.TeacherLocation
INNER JOIN
(
Select LOC.tID
, LOC.SurveyYear
, LOC.Sector
, max(convert(int,ytqQualified)) BestQual
, max(convert(int,ytqCertified)) BestCert
from SurveyYearTeacherQual Q
INNER JOIN warehouse.TeacherLocation LOC
	ON Q.svyYear = LOC.SurveyYear
	AND (
		isnull(Q.ytqSector,'') = isnull(LOC.Sector,'')		-- allow for sector NULL ie not used
		OR
			@defaultSector is null		-- ie there is no sector breakdown; this is really just a simple shortcut
		)
INNER JOIN warehouse.TeacherQual TQ
	ON  LOC.tID = TQ.tID
	AND Q.ytqQual = TQ.tchQual
	AND TQ.yr <= LOC.SurveyYear
GROUP BY LOC.tID, LOC.SurveyYear, LOC.Sector
	) YTQ
ON warehouse.TeacherLocation.tID = YTQ.tID
AND warehouse.TeacherLocation.SurveyYear = YTQ.SurveyYear

print 'warehouse.teacherLocation update certifed / qualifed - rows:' + convert(nvarchar(10), @@rowcount)

-- now pick up those dups
UPDATE warehouse.TeacherLocation
SET XtraSurvey = NumSurveys - 1
FROM warehouse.TeacherLocation
INNER JOIN
(
Select LifeYear, tID
, count(*) NumSurveys
		FROM ESTIMATE_BestSurveyTeachers EST
			INNER JOIN TeacherSurvey TS
				ON EST.bestssID = TS.ssID
GROUP BY LifeYear, tID
HAVING count(*) > 1
) U
ON warehouse.TeacherLocation.tID = U.tID
AND warehouse.TeacherLocation.SurveyYear = U.LifeYear

print 'warehouse.teacherLocation flag dups - rows:' + convert(nvarchar(10), @@rowcount)

---------------------------------------------------------------
-- School Teacher Summary
---------------------------------------------------------------

		DELETE
		FROM warehouse.schoolTeacherCount
		WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)
		print 'warehouse.schoolTeacherCount deletes - rows:' + convert(nvarchar(10), @@rowcount)

		INSERT INTO warehouse.schoolTeacherCount
           (SchNo
           ,SurveyYear
           ,GenderCode
           ,AgeGroup
           ,DistrictCode
           ,AuthorityCode
           ,SchoolTypeCode
           ,Sector
           ,ISCEDSubClass
           ,Support
           ,NumTeachers
           ,Certified
           ,Qualified
           ,CertQual
           ,Estimate)
		Select TL.SchNo
		, TL.SurveyYear
		, GenderCode
		, AgeGroup
		, [District Code] DistrictCode
		, AuthorityCode
		, SchoolTypeCode
		, Sector
		, ISCEDSubClass
		, SurveySupport

		, count(*) NumTeachers
		, sum(Certified) Certified
		, sum(Qualified) Qualified
		, sum(case when Certified = 1 and Qualified = 1 then 1 else 0 end) CertQual
		, sum (case when Source in ('E','XE', 'CE', 'XA', 'XA?') then 1 else 0 end ) Estimate
		From warehouse.TeacherLocation TL
		LEFT JOIN warehouse.bestSurvey BSS
			ON TL.SurveyYear = BSS.SurveyYear
			AND TL.schNo = BSS.schNo
		LEFT JOIN warehouse.dimensionSchoolSurvey DSS
			ON DSS.[Survey Id] = BSS.SurveyDimensionID
		WHERE Source not in ('A','A?')
		AND (TL.SurveyYear >= @StartFromYear or @StartFromYEar is null)

		Group BY TL.schNo, TL.SurveyYear, TL.AgeGroup, GenderCode, [District Code], [AuthorityCode], SchooltypeCode
		, Sector, ISCEDSubClass, SurveySupport
		print 'warehouse.schoolTeacherCount inserts - rows:' + convert(nvarchar(10), @@rowcount)

END
GO

