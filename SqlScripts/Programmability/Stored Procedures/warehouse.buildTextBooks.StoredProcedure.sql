SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 15 2 2017
-- Description:	Build textboox / reader totals by level
-- =============================================
CREATE PROCEDURE [warehouse].[buildTextBooks]
	-- Add the parameters for the stored procedure here
	@StartFromYear int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DELETE FROM warehouse.textbookCounts
WHERE (SurveyYear >= @StartFromYear or @StartFromYEar is null)

INSERT INTO warehouse.textbookCounts
(
	schNo
	, SurveyYear
	, Resource
	, Number
	, Condition
	, ClassLevel
	, Estimate
)
Select S.SchNo
	, svyYear SurveyYear
	, RES.Resource
	, Number
	, Condition
	, ClassLevel
	, E.Estimate
FROM SchoolLifeYears S
	INNER JOIN ESTIMATE_BestSurveyResourceCategory E
		ON E.resName = 'Text Books'
		AND E.schNo = S.schNo
		AND E.LifeYear = S.svyYEar
	INNER JOIN (
		Select ssID
		, resName Resource
		, resNumber Number
		, resCondition Condition
		, codeDescription ClassLevel
		FROM Resources
		LEFT JOIN lkpLevels
			ON ResLevel = codeCode
		WHERE resName in ('Text Books', 'Readers')
	) RES
		ON E.bestssID = RES.ssID
WHERE (Number is not null)
	AND (E.LifeYear >= @StartFromYear or @StartFromYEar is null)
END
GO

