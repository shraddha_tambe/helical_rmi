CREATE SCHEMA [Aurion]
GO
GRANT DELETE ON SCHEMA::[Aurion] TO [pEstablishmentOps] AS [dbo]
GO
GRANT EXECUTE ON SCHEMA::[Aurion] TO [pEstablishmentOps] AS [dbo]
GO
GRANT INSERT ON SCHEMA::[Aurion] TO [pEstablishmentOps] AS [dbo]
GO
GRANT SELECT ON SCHEMA::[Aurion] TO [pEstablishmentOps] AS [dbo]
GO
GRANT UPDATE ON SCHEMA::[Aurion] TO [pEstablishmentOps] AS [dbo]
GO
GRANT VIEW DEFINITION ON SCHEMA::[Aurion] TO [public] AS [dbo]
GO

