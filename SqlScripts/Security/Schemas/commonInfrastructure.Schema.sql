CREATE SCHEMA [commonInfrastructure]
GO
GRANT INSERT ON SCHEMA::[commonInfrastructure] TO [pInfrastructureAdmin] AS [pineapples]
GO
GRANT SELECT ON SCHEMA::[commonInfrastructure] TO [pInfrastructureAdmin] AS [pineapples]
GO
GRANT UPDATE ON SCHEMA::[commonInfrastructure] TO [pInfrastructureAdmin] AS [pineapples]
GO
GRANT SELECT ON SCHEMA::[commonInfrastructure] TO [public] AS [pineapples]
GO
GRANT VIEW DEFINITION ON SCHEMA::[commonInfrastructure] TO [public] AS [pineapples]
GO

