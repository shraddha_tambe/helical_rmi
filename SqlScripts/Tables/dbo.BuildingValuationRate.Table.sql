SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuildingValuationRate](
	[bvID] [int] IDENTITY(1,1) NOT NULL,
	[bvcCode] [nvarchar](10) NOT NULL,
	[bvYear] [int] NOT NULL,
	[bszCode] [nvarchar](10) NOT NULL,
	[bvVal] [money] NOT NULL,
 CONSTRAINT [PK_BuildingValuationRate] PRIMARY KEY CLUSTERED 
(
	[bvID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[BuildingValuationRate] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[BuildingValuationRate] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[BuildingValuationRate] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[BuildingValuationRate] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[BuildingValuationRate] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_YearValSize] ON [dbo].[BuildingValuationRate]
(
	[bvYear] ASC,
	[bvcCode] ASC,
	[bszCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Year of the valuation' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BuildingValuationRate', @level2type=N'COLUMN',@level2name=N'bvYear'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'In a given year, for a combination of valuation class and size class, the valuation rate per square metre.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BuildingValuationRate'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BuildingValuationRate'
GO

