SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Buildings](
	[bldID] [int] IDENTITY(1,1) NOT NULL,
	[bldgCode] [nvarchar](10) NOT NULL,
	[bldgTitle] [nvarchar](50) NOT NULL,
	[bldgDescription] [nvarchar](2000) NULL,
	[bldgSubType] [nvarchar](10) NULL,
	[bldgL] [decimal](18, 2) NULL,
	[bldgW] [decimal](18, 2) NULL,
	[bldgA] [decimal](18, 2) NULL,
	[bldgIrregular] [bit] NOT NULL,
	[bldgH] [decimal](18, 2) NULL,
	[bldgE] [decimal](18, 2) NULL,
	[bldgR] [decimal](18, 2) NULL,
	[bldgLevels] [int] NULL,
	[bldgLat] [decimal](10, 6) NULL,
	[bldgLong] [decimal](10, 6) NULL,
	[bldgMaterials] [nvarchar](10) NULL,
	[bldgMatW] [nvarchar](10) NULL,
	[bldgNoteW] [nvarchar](200) NULL,
	[bldgMatF] [nvarchar](10) NULL,
	[bldgNoteF] [nvarchar](200) NULL,
	[bldgMatR] [nvarchar](10) NULL,
	[bldgNoteR] [nvarchar](200) NULL,
	[bvcCode] [nvarchar](10) NULL,
	[bldgYear] [int] NULL,
	[bldgCapitalCost] [money] NULL,
	[bldgCloseDate] [smalldatetime] NULL,
	[bldgClosed]  AS (case when [bldgCloseDate] IS NULL then (0) else (1) end) PERSISTED NOT NULL,
	[bldgYearEnd]  AS (datepart(year,[bldgCloseDate])),
	[bldgEndMode] [nvarchar](10) NULL,
	[bldgCondition] [nvarchar](1) NULL,
	[bldgValuation] [money] NULL,
	[bldgValuationDate] [smalldatetime] NULL,
	[schNo] [nvarchar](50) NULL,
	[bldgNote] [ntext] NULL,
	[bldgRoomsClass] [int] NULL,
	[bldgRoomsOHT] [int] NULL,
	[bldgRoomsStaff] [int] NULL,
	[bldgRoomsAdmin] [int] NULL,
	[bldgRoomsStorage] [int] NULL,
	[bldgRoomsDorm] [int] NULL,
	[bldgRoomsKitchen] [int] NULL,
	[bldgRoomsDining] [int] NULL,
	[bldgRoomsLibrary] [int] NULL,
	[bldgRoomsHall] [int] NULL,
	[bldgRoomsSpecialTuition] [int] NULL,
	[bldgRoomsOther] [int] NULL,
	[bldgRoomsAll]  AS (((((((((((isnull([bldgRoomsClass],(0))+isnull([bldgRoomsOHT],(0)))+isnull([bldgRoomsStaff],(0)))+isnull([bldgRoomsAdmin],(0)))+isnull([bldgRoomsStorage],(0)))+isnull([bldgRoomsDorm],(0)))+isnull([bldgRoomsKitchen],(0)))+isnull([bldgRoomsDining],(0)))+isnull([bldgRoomsLibrary],(0)))+isnull([bldgRoomsHall],(0)))+isnull([bldgRoomsSpecialTuition],(0)))+isnull([bldgRoomsOther],(0))),
	[bldgSeq] [int] NULL,
 CONSTRAINT [PK_Buildings] PRIMARY KEY CLUSTERED 
(
	[bldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Buildings] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Buildings] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Buildings] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Buildings] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Buildings] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[Buildings] ADD  CONSTRAINT [DF_Buildings_bldgIrregular]  DEFAULT ((0)) FOR [bldgIrregular]
GO
ALTER TABLE [dbo].[Buildings]  WITH CHECK ADD  CONSTRAINT [FK_Buildings_lkpBuildingMaterialsR] FOREIGN KEY([bldgMatR])
REFERENCES [dbo].[lkpBuildingMaterials] ([bmatCode])
GO
ALTER TABLE [dbo].[Buildings] CHECK CONSTRAINT [FK_Buildings_lkpBuildingMaterialsR]
GO
ALTER TABLE [dbo].[Buildings]  WITH CHECK ADD  CONSTRAINT [FK_Buildings_lkpBuildingMaterialsw] FOREIGN KEY([bldgMatW])
REFERENCES [dbo].[lkpBuildingMaterials] ([bmatCode])
GO
ALTER TABLE [dbo].[Buildings] CHECK CONSTRAINT [FK_Buildings_lkpBuildingMaterialsw]
GO
ALTER TABLE [dbo].[Buildings]  WITH CHECK ADD  CONSTRAINT [FK_Buildings_lkpBuildingSubType] FOREIGN KEY([bldgSubType])
REFERENCES [dbo].[lkpBuildingSubType] ([bsubCode])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Buildings] CHECK CONSTRAINT [FK_Buildings_lkpBuildingSubType]
GO
ALTER TABLE [dbo].[Buildings]  WITH CHECK ADD  CONSTRAINT [FK_Buildings_lkpBuildingTypes] FOREIGN KEY([bldgCode])
REFERENCES [dbo].[lkpBuildingTypes] ([bdlgCode])
GO
ALTER TABLE [dbo].[Buildings] CHECK CONSTRAINT [FK_Buildings_lkpBuildingTypes]
GO
ALTER TABLE [dbo].[Buildings]  WITH CHECK ADD  CONSTRAINT [FK_Buildings_lkpBuildingValuationClass] FOREIGN KEY([bvcCode])
REFERENCES [dbo].[lkpBuildingValuationClass] ([bvcCode])
GO
ALTER TABLE [dbo].[Buildings] CHECK CONSTRAINT [FK_Buildings_lkpBuildingValuationClass]
GO
ALTER TABLE [dbo].[Buildings]  WITH CHECK ADD  CONSTRAINT [FK_Buildings_MaterialF] FOREIGN KEY([bldgMatF])
REFERENCES [dbo].[lkpBuildingMaterials] ([bmatCode])
GO
ALTER TABLE [dbo].[Buildings] CHECK CONSTRAINT [FK_Buildings_MaterialF]
GO
ALTER TABLE [dbo].[Buildings]  WITH CHECK ADD  CONSTRAINT [FK_Buildings_Schools] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Buildings] CHECK CONSTRAINT [FK_Buildings_Schools]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Elevation of floor above ground, in metres ; ie building on stilts' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Buildings', @level2type=N'COLUMN',@level2name=N'bldgE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'just a place to temporalily hold the seq that may be submitted on the buildingreview' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Buildings', @level2type=N'COLUMN',@level2name=N'bldgSeq'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Master table of school buildings. SchNo is a foreign key defining the school at which the building is located.  Building type code defines the type of the building.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Buildings'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Buildings' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Buildings'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Buildings'
GO

