SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Census](
	[cYear] [int] NOT NULL,
	[iCode] [nvarchar](2) NOT NULL,
	[cYOB] [int] NOT NULL,
	[cMale] [int] NULL,
	[cFemale] [int] NULL,
 CONSTRAINT [PK_Census] PRIMARY KEY CLUSTERED 
(
	[cYear] ASC,
	[iCode] ASC,
	[cYOB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[Census] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Census] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Census] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[Census] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Census] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[Census] ADD  CONSTRAINT [DF__Census__cYear__41B8C09B]  DEFAULT ((0)) FOR [cYear]
GO
ALTER TABLE [dbo].[Census] ADD  CONSTRAINT [DF__Census__cYOB__42ACE4D4]  DEFAULT ((0)) FOR [cYOB]
GO
ALTER TABLE [dbo].[Census] ADD  CONSTRAINT [DF__Census__cMale__43A1090D]  DEFAULT ((0)) FOR [cMale]
GO
ALTER TABLE [dbo].[Census] ADD  CONSTRAINT [DF__Census__cFemale__44952D46]  DEFAULT ((0)) FOR [cFemale]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Not currently used.
Census may be reactivated when the results of 2009/2010 census reound are available for loading.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Census'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'not used' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Census'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Population' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Census'
GO

