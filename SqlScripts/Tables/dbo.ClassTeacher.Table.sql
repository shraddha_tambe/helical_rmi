SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClassTeacher](
	[pctID] [int] IDENTITY(1,1) NOT NULL,
	[pcID] [int] NULL,
	[tchsID] [int] NULL,
	[pctSeq] [smallint] NULL,
	[pctHrsWeek] [float] NULL,
	[tID] [int] NULL,
 CONSTRAINT [aaaaaPrimaryClassTeacher1_PK] PRIMARY KEY NONCLUSTERED 
(
	[pctID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ClassTeacher] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ClassTeacher] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ClassTeacher] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ClassTeacher] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ClassTeacher] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_PrimaryClassTeacher_PCID] ON [dbo].[ClassTeacher]
(
	[pcID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClassTeacher] ADD  CONSTRAINT [DF__PrimaryCla__pcID__5C37ACAD]  DEFAULT ((0)) FOR [pcID]
GO
ALTER TABLE [dbo].[ClassTeacher] ADD  CONSTRAINT [DF__PrimaryCl__pctSe__5E1FF51F]  DEFAULT ((0)) FOR [pctSeq]
GO
ALTER TABLE [dbo].[ClassTeacher]  WITH CHECK ADD  CONSTRAINT [FK_ClassTeacher_TeacherSurvey] FOREIGN KEY([tchsID])
REFERENCES [dbo].[TeacherSurvey] ([tchsID])
GO
ALTER TABLE [dbo].[ClassTeacher] CHECK CONSTRAINT [FK_ClassTeacher_TeacherSurvey]
GO
ALTER TABLE [dbo].[ClassTeacher]  WITH CHECK ADD  CONSTRAINT [PrimaryClassTeacher_FK00] FOREIGN KEY([pcID])
REFERENCES [dbo].[Classes] ([pcID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ClassTeacher] CHECK CONSTRAINT [PrimaryClassTeacher_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teachers related to a class. Class PK clsId is the foreign key. Generally will not be more than 2 records (joint class) in here, this is prevented by the user interfaces. 
Key to teacher is tchsId ie pointing to a teacher survey for the same school.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClassTeacher'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Classes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClassTeacher'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ClassTeacher'
GO

