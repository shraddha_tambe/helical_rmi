SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EducationPaths](
	[pathCode] [nvarchar](10) NOT NULL,
	[pathName] [nvarchar](50) NULL,
	[pathDefault] [bit] NULL,
	[pathNameL1] [nvarchar](50) NULL,
	[pathNameL2] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaEducationPaths1_PK] PRIMARY KEY NONCLUSTERED 
(
	[pathCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[EducationPaths] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[EducationPaths] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[EducationPaths] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[EducationPaths] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[EducationPaths] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[EducationPaths] ADD  CONSTRAINT [DF__Education__pathD__1F63A897]  DEFAULT ((0)) FOR [pathDefault]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'An EducationPath is a pathway through the education system, noving from one class level to another at the next Year of Education. 
The default Path is a device to provide a "default class level" for every year of education. In some systems, there may not be more than 1 class level per year of education, but systems using TVET beside senior secondary will probably have this. The default path is useful to not create duplicates when reporting on Enrolment Ratios. Ie population and enrolment are calculated for each year of education, but this is presented against the "default class level" for that year of education for reading clarity.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationPaths'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Enrolment' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'EducationPaths'
GO

