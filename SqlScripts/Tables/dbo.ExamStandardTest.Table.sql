SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamStandardTest](
	[stID] [int] IDENTITY(1,1) NOT NULL,
	[exID] [int] NULL,
	[schNo] [nvarchar](50) NULL,
	[stSchName] [nvarchar](200) NULL,
	[stGender] [nvarchar](1) NULL,
	[stSubject] [nvarchar](50) NULL,
	[stComponent] [nvarchar](50) NULL,
	[st0] [int] NULL,
	[st1] [int] NULL,
	[st2] [int] NULL,
	[st3] [int] NULL,
	[st4] [int] NULL,
	[st5] [int] NULL,
	[st6] [int] NULL,
	[st7] [int] NULL,
	[st8] [int] NULL,
	[st9] [int] NULL,
 CONSTRAINT [aaaaaExamStandardTest1_PK] PRIMARY KEY NONCLUSTERED 
(
	[stID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ExamStandardTest] TO [pExamRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ExamStandardTest] TO [pExamWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ExamStandardTest] TO [pExamWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ExamStandardTest] TO [pExamWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ExamStandardTest] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ExamStandardTest]  WITH CHECK ADD  CONSTRAINT [ExamStandardTest_FK00] FOREIGN KEY([exID])
REFERENCES [dbo].[Exams] ([exID])
GO
ALTER TABLE [dbo].[ExamStandardTest] CHECK CONSTRAINT [ExamStandardTest_FK00]
GO
ALTER TABLE [dbo].[ExamStandardTest]  WITH CHECK ADD  CONSTRAINT [ExamStandardTest_FK01] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ExamStandardTest] CHECK CONSTRAINT [ExamStandardTest_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Holds results from a Standard Test of Achievement, such as STAKI, SISTA, VANSTA. By school, Gender, Subject and Component, captures the number of pupils who score in each band 1-5.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamStandardTest'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Exams' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamStandardTest'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Exams' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamStandardTest'
GO

