SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExamSubjectGrades](
	[exrID] [int] IDENTITY(1,1) NOT NULL,
	[exID] [int] NULL,
	[schNo] [nvarchar](50) NULL,
	[exrSubject] [nvarchar](50) NULL,
	[exrGender] [nvarchar](1) NULL,
	[exrGrade] [real] NULL,
	[exrCandidates] [int] NULL,
	[exrRaw] [real] NULL,
	[exrStandard] [real] NULL,
	[exrFinal] [int] NULL,
	[exrSch] [int] NULL,
 CONSTRAINT [aaaaaExamSubjectGrades1_PK] PRIMARY KEY NONCLUSTERED 
(
	[exrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ExamSubjectGrades] TO [pExamRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ExamSubjectGrades] TO [pExamWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ExamSubjectGrades] TO [pExamWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ExamSubjectGrades] TO [pExamWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ExamSubjectGrades] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[ExamSubjectGrades] ADD  CONSTRAINT [DF__ExamSubjec__exID__23BE4960]  DEFAULT ((0)) FOR [exID]
GO
ALTER TABLE [dbo].[ExamSubjectGrades] ADD  CONSTRAINT [DF__ExamSubje__exrGr__24B26D99]  DEFAULT ((0)) FOR [exrGrade]
GO
ALTER TABLE [dbo].[ExamSubjectGrades] ADD  CONSTRAINT [DF__ExamSubje__exrCa__25A691D2]  DEFAULT ((0)) FOR [exrCandidates]
GO
ALTER TABLE [dbo].[ExamSubjectGrades] ADD  CONSTRAINT [DF__ExamSubje__exrRa__269AB60B]  DEFAULT ((0)) FOR [exrRaw]
GO
ALTER TABLE [dbo].[ExamSubjectGrades] ADD  CONSTRAINT [DF__ExamSubje__exrSt__278EDA44]  DEFAULT ((0)) FOR [exrStandard]
GO
ALTER TABLE [dbo].[ExamSubjectGrades] ADD  CONSTRAINT [DF__ExamSubje__exrFi__2882FE7D]  DEFAULT ((0)) FOR [exrFinal]
GO
ALTER TABLE [dbo].[ExamSubjectGrades] ADD  CONSTRAINT [DF__ExamSubje__exrSc__297722B6]  DEFAULT ((0)) FOR [exrSch]
GO
ALTER TABLE [dbo].[ExamSubjectGrades]  WITH CHECK ADD  CONSTRAINT [ExamSubjectGrades_FK00] FOREIGN KEY([exID])
REFERENCES [dbo].[Exams] ([exID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ExamSubjectGrades] CHECK CONSTRAINT [ExamSubjectGrades_FK00]
GO
ALTER TABLE [dbo].[ExamSubjectGrades]  WITH CHECK ADD  CONSTRAINT [ExamSubjectGrades_FK01] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ExamSubjectGrades] CHECK CONSTRAINT [ExamSubjectGrades_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ExamSubject Grades holds the grades scored by students in specific subjects. ExID is the foreign key, linking to Exams. Data is grouped by school, gender, and grade.
This table is maintained by the Atlas importer/exporter.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamSubjectGrades'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Exams' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamSubjectGrades'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Exams' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExamSubjectGrades'
GO

