SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Exams](
	[exID] [int] IDENTITY(1,1) NOT NULL,
	[exCode] [nvarchar](10) NULL,
	[exYear] [int] NULL,
	[exUser] [nvarchar](50) NULL,
	[exDate] [datetime] NULL,
	[exCandidates] [int] NULL,
	[exMarks] [int] NULL,
	[exSubjectScore] [float] NULL,
	[exCandidateScore] [float] NULL,
	[exScoringModel] [nvarchar](50) NULL,
	[exsmNumScores] [int] NULL,
	[exsmMandatory] [nvarchar](255) NULL,
	[exsmExcluded] [nvarchar](255) NULL,
	[exExporterVersion] [nvarchar](20) NULL,
	[exExportDate] [datetime] NULL,
	[docID] [uniqueidentifier] NULL,
 CONSTRAINT [aaaaaExams1_PK] PRIMARY KEY NONCLUSTERED 
(
	[exID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Exams] TO [pExamRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Exams] TO [pExamWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Exams] TO [pExamWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Exams] TO [pExamWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Exams] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF__Exams__exCandida__3C89F72A]  DEFAULT ((0)) FOR [exCandidates]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF__Exams__exMarks__3D7E1B63]  DEFAULT ((0)) FOR [exMarks]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF__Exams__exSubject__3E723F9C]  DEFAULT ((0)) FOR [exSubjectScore]
GO
ALTER TABLE [dbo].[Exams] ADD  CONSTRAINT [DF__Exams__exCandida__3F6663D5]  DEFAULT ((0)) FOR [exCandidateScore]
GO
ALTER TABLE [dbo].[Exams]  WITH CHECK ADD  CONSTRAINT [Exams_FK00] FOREIGN KEY([exCode])
REFERENCES [dbo].[lkpExamTypes] ([exCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Exams] CHECK CONSTRAINT [Exams_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Each record in Exams represents an instance of a particular examination held in one year.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Exams'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Exams' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Exams'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Exams' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Exams'
GO

