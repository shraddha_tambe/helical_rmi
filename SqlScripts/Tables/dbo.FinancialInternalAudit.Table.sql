SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FinancialInternalAudit](
	[faiID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[faiPolicyExists] [smallint] NULL,
	[faiAllBanked] [smallint] NULL,
	[faiDailyBank] [money] NULL,
	[faiUnbankedLimit] [money] NULL,
	[faiBankTravelTime] [datetime] NULL,
	[faiCashStorage] [nvarchar](50) NULL,
	[faiCashStorageComment] [ntext] NULL,
	[faiComputers] [int] NULL,
	[faiDate] [datetime] NULL,
	[faiPaymentsLY] [money] NULL,
	[faiPaymentsYTD] [money] NULL,
	[faiChequesLY] [money] NULL,
	[faiChequesYTD] [money] NULL,
	[faiFloat] [money] NULL,
	[faiBankRecDate] [datetime] NULL,
	[faiBankRecChecked] [smallint] NULL,
	[faiComment] [ntext] NULL,
	[faiBursarGiven] [nvarchar](50) NULL,
	[faiBursarFamily] [nvarchar](50) NULL,
	[faiBursarStartDate] [datetime] NULL,
	[faiBursarContractor] [nvarchar](50) NULL,
	[faiBursarContract] [smallint] NULL,
	[faiBursarContractDuration] [nvarchar](50) NULL,
	[faiBursarQual] [nvarchar](255) NULL,
	[faiBursarWorkshops] [int] NULL,
	[faiBursarWorkshopCount] [int] NULL,
	[faiBursarTrainingNeed] [int] NULL,
	[faiBursarTrainingNeedDetail] [ntext] NULL,
	[faiBursarHTAssist] [smallint] NULL,
	[faiBursarHTAssistDetail] [ntext] NULL,
	[faiBursarComments] [ntext] NULL,
 CONSTRAINT [aaaaaFinancialInternalAudit1_PK] PRIMARY KEY NONCLUSTERED 
(
	[faiID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[FinancialInternalAudit] TO [pFinanceRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[FinancialInternalAudit] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[FinancialInternalAudit] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[FinancialInternalAudit] TO [pFinanceWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[FinancialInternalAudit] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_FinancialInternalAudit_SSID] ON [dbo].[FinancialInternalAudit]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FinancialInternalAudit] ADD  CONSTRAINT [DF__Financial__faiPa__09746778]  DEFAULT ((0)) FOR [faiPaymentsLY]
GO
ALTER TABLE [dbo].[FinancialInternalAudit] ADD  CONSTRAINT [DF__Financial__faiPa__0A688BB1]  DEFAULT ((0)) FOR [faiPaymentsYTD]
GO
ALTER TABLE [dbo].[FinancialInternalAudit] ADD  CONSTRAINT [DF__Financial__faiCh__0B5CAFEA]  DEFAULT ((0)) FOR [faiChequesLY]
GO
ALTER TABLE [dbo].[FinancialInternalAudit] ADD  CONSTRAINT [DF__Financial__faiCh__0C50D423]  DEFAULT ((0)) FOR [faiChequesYTD]
GO
ALTER TABLE [dbo].[FinancialInternalAudit] ADD  CONSTRAINT [DF__Financial__faiFl__0D44F85C]  DEFAULT ((0)) FOR [faiFloat]
GO
ALTER TABLE [dbo].[FinancialInternalAudit]  WITH CHECK ADD  CONSTRAINT [FinancialInternalAudit_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FinancialInternalAudit] CHECK CONSTRAINT [FinancialInternalAudit_FK00]
GO
ALTER TABLE [dbo].[FinancialInternalAudit]  WITH CHECK ADD  CONSTRAINT [CK FinancialInternalAudit faiAllBanked] CHECK  (([faiAllBanked]=(0) OR [faiAllBanked]=(-1)))
GO
ALTER TABLE [dbo].[FinancialInternalAudit] CHECK CONSTRAINT [CK FinancialInternalAudit faiAllBanked]
GO
ALTER TABLE [dbo].[FinancialInternalAudit]  WITH CHECK ADD  CONSTRAINT [CK FinancialInternalAudit faiBankRecChecked] CHECK  (([faiBankRecChecked]=(-1) OR [faiBankRecChecked]=(0)))
GO
ALTER TABLE [dbo].[FinancialInternalAudit] CHECK CONSTRAINT [CK FinancialInternalAudit faiBankRecChecked]
GO
ALTER TABLE [dbo].[FinancialInternalAudit]  WITH CHECK ADD  CONSTRAINT [CK FinancialInternalAudit faiBursarContract] CHECK  (([faiBursarContract]=(-1) OR [faiBursarContract]=(0)))
GO
ALTER TABLE [dbo].[FinancialInternalAudit] CHECK CONSTRAINT [CK FinancialInternalAudit faiBursarContract]
GO
ALTER TABLE [dbo].[FinancialInternalAudit]  WITH CHECK ADD  CONSTRAINT [CK FinancialInternalAudit faiBursarHTAssist] CHECK  (([faiBursarHTAssist]=(-1) OR [faiBursarHTAssist]=(0)))
GO
ALTER TABLE [dbo].[FinancialInternalAudit] CHECK CONSTRAINT [CK FinancialInternalAudit faiBursarHTAssist]
GO
ALTER TABLE [dbo].[FinancialInternalAudit]  WITH CHECK ADD  CONSTRAINT [CK FinancialInternalAudit faiBursarTrainingNeed] CHECK  (([faiBursarTrainingNeed]=(-1) OR [faiBursarTrainingNeed]=(0)))
GO
ALTER TABLE [dbo].[FinancialInternalAudit] CHECK CONSTRAINT [CK FinancialInternalAudit faiBursarTrainingNeed]
GO
ALTER TABLE [dbo].[FinancialInternalAudit]  WITH CHECK ADD  CONSTRAINT [CK FinancialInternalAudit faiBursarWorkshops] CHECK  (([faiBursarWorkshops]=(-1) OR [faiBursarWorkshops]=(0)))
GO
ALTER TABLE [dbo].[FinancialInternalAudit] CHECK CONSTRAINT [CK FinancialInternalAudit faiBursarWorkshops]
GO
ALTER TABLE [dbo].[FinancialInternalAudit]  WITH CHECK ADD  CONSTRAINT [CK FinancialInternalAudit faiPolicyExists] CHECK  (([faiPolicyExists]=(-1) OR [faiPolicyExists]=(0)))
GO
ALTER TABLE [dbo].[FinancialInternalAudit] CHECK CONSTRAINT [CK FinancialInternalAudit faiPolicyExists]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Specific set of questions related to the Vanuatu Financial Internal Audit survey. ssID links to school survey, and is a unique key: ie there is at most one FinancialInternalAudit per SchoolSurvey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FinancialInternalAudit'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'FinancialInternalAudit'
GO

