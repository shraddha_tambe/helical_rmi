SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ISTWaitList](
	[istwID] [int] IDENTITY(1,1) NOT NULL,
	[istcCode] [nvarchar](20) NOT NULL,
	[tID] [int] NOT NULL,
	[istwNote] [ntext] NULL,
 CONSTRAINT [ISTWaitList_PK] PRIMARY KEY NONCLUSTERED 
(
	[istwID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[ISTWaitList] TO [pISTRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[ISTWaitList] TO [pISTWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[ISTWaitList] TO [pISTWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISTWaitList] TO [pISTWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[ISTWaitList] TO [pTeacherWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ISTWaitList] TO [pTeacherWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[ISTWaitList] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [istcCode] ON [dbo].[ISTWaitList]
(
	[istcCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [TeacherIdentityISTWaitList] ON [dbo].[ISTWaitList]
(
	[tID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ISTWaitList]  WITH CHECK ADD  CONSTRAINT [ISTCourseISTWaitList] FOREIGN KEY([istcCode])
REFERENCES [dbo].[ISTCourse] ([istcCode])
GO
ALTER TABLE [dbo].[ISTWaitList] CHECK CONSTRAINT [ISTCourseISTWaitList]
GO
ALTER TABLE [dbo].[ISTWaitList]  WITH CHECK ADD  CONSTRAINT [TeacherIdentityISTWaitList] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])
GO
ALTER TABLE [dbo].[ISTWaitList] CHECK CONSTRAINT [TeacherIdentityISTWaitList]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ISTWaitList is the set of people waiting to attend a particular IST Course. ISTCource is a foreign key on wait list; ie the Wait list is associated to a course. 
When a session becomes available, attendees may be moved into the ISTSession enrolment list (ISTEnrol) from ISTWaitList.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTWaitList'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'IST' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTWaitList'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Inservice Training' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ISTWaitList'
GO

