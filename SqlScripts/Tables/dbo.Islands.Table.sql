SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Islands](
	[iCode] [nvarchar](2) NOT NULL,
	[iName] [nvarchar](50) NULL,
	[iSeq] [int] NULL,
	[iGroup] [nvarchar](5) NULL,
	[iOuter] [nvarchar](50) NULL,
	[igisID] [int] NULL,
	[iMap] [image] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaaIslands1_PK] PRIMARY KEY NONCLUSTERED 
(
	[iCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[Islands] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Islands] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Islands] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[Islands] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Islands] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[Islands] ADD  CONSTRAINT [DF__Islands__iSeq__6C6E1476]  DEFAULT ((0)) FOR [iSeq]
GO
ALTER TABLE [dbo].[Islands]  WITH CHECK ADD  CONSTRAINT [Islands_FK00] FOREIGN KEY([iGroup])
REFERENCES [dbo].[Districts] ([dID])
GO
ALTER TABLE [dbo].[Islands] CHECK CONSTRAINT [Islands_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Islnds in the country. Each school is on an island - ie iCode is a foreign key on schools.
Each Island is part of a District.
Therefore, the District of the school is determined by Island.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Islands'
GO

