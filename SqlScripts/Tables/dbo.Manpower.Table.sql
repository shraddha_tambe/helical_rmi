SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manpower](
	[ManID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](250) NULL,
	[RolePayScaleRange] [nvarchar](250) NULL,
	[givenname] [nvarchar](250) NULL,
	[familyname] [nvarchar](250) NULL,
	[Gender] [nvarchar](250) NULL,
	[Authority] [nvarchar](50) NULL,
	[schoolname] [nvarchar](250) NULL,
	[SchNo] [nvarchar](50) NULL,
	[TPF] [nvarchar](50) NULL,
	[tID] [int] NULL,
	[NPF] [nvarchar](50) NULL,
	[PositionSeq] [int] NULL,
	[PositionNo] [nvarchar](50) NULL,
	[NamePrefix] [nvarchar](50) NULL,
	[NameFirst] [nvarchar](50) NULL,
	[NameMiddle] [nvarchar](50) NULL,
	[NameLast] [nvarchar](50) NULL,
	[NameSuffix] [nvarchar](50) NULL,
 CONSTRAINT [Manpower_PK] PRIMARY KEY NONCLUSTERED 
(
	[ManID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Manpower] TO [pTeacherReadX] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [tID] ON [dbo].[Manpower]
(
	[tID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

