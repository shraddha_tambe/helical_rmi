SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[POR](
	[porID] [int] IDENTITY(1,1) NOT NULL,
	[porEA] [nvarchar](50) NOT NULL,
	[tID] [int] NOT NULL,
	[flowID] [int] NOT NULL,
	[porStatus] [nvarchar](10) NOT NULL,
	[porDate] [datetime] NOT NULL,
	[porReceived] [datetime] NOT NULL,
	[schNo] [nvarchar](20) NULL,
	[porDetails] [ntext] NULL,
	[porAdjStart] [datetime] NULL,
	[porAdjEnd] [datetime] NULL,
	[porAdjDays] [int] NULL,
	[porAdjLvlBase] [nvarchar](50) NULL,
	[porAdjLvl] [nvarchar](50) NULL,
	[porAdjAmtBase] [money] NULL,
	[porAdjAmtNew] [money] NULL,
	[porAdjAmtDay] [money] NULL,
	[porAdjTotal] [money] NULL,
	[porAllowTravel] [money] NULL,
	[porAllowHousing] [money] NULL,
	[porAllowDanger] [money] NULL,
	[porAllowBoardingDuty] [money] NULL,
	[porAllowInducement] [money] NULL,
	[porAllowLongService] [money] NULL,
	[porAllowCharge] [money] NULL,
	[porAdjPermanent] [bit] NOT NULL,
	[porRoleCurrent] [nvarchar](50) NULL,
	[porRoleNew] [nvarchar](50) NULL,
	[porMOFconfirmed] [nvarchar](10) NULL,
	[porMOFDate] [datetime] NULL,
	[porMofNote] [ntext] NULL,
	[porSchNoCurrent] [nvarchar](15) NULL,
	[porOfficer] [nvarchar](50) NULL,
	[porTSCConfirmed] [nvarchar](50) NULL,
	[porTSCDate] [datetime] NULL,
	[porTSCNote] [ntext] NULL,
	[porTSex] [nvarchar](50) NULL,
	[porTDOB] [datetime] NULL,
	[porTBank] [nvarchar](50) NULL,
	[porTBSB] [nvarchar](20) NULL,
	[porTBankAcc] [nvarchar](50) NULL,
	[porTNamePrefix] [nvarchar](20) NULL,
	[porTGiven] [nvarchar](50) NULL,
	[porTMiddleNames] [nvarchar](50) NULL,
	[porTSurname] [nvarchar](50) NULL,
	[porTNameSuffix] [nvarchar](20) NULL,
	[porTShortName]  AS ([dbo].[TeacherName]((1),[porTNamePrefix],[PORTGiven],[PORtMiddleNames],[portSurname],[portNameSuffix])),
	[porTFullName]  AS ([dbo].[TeacherName]((2),[porTNamePrefix],[PORTGiven],[PORtMiddleNames],[portSurname],[portNameSuffix])),
	[porTLongName]  AS ([dbo].[TeacherName]((3),[porTNamePrefix],[PORTGiven],[PORtMiddleNames],[portSurname],[portNameSuffix])),
	[portGivenSoundex]  AS (soundex([portGiven])) PERSISTED,
	[portSurnameSoundex]  AS (soundex([portSurname])) PERSISTED,
	[porEstpNo] [nvarchar](20) NULL,
	[portaID] [int] NULL,
 CONSTRAINT [POR_PK] PRIMARY KEY NONCLUSTERED 
(
	[porID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[POR] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[POR] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[POR] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[POR] TO [pTeacherReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[POR] TO [pTeacherWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[POR] TO [pTeacherWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[POR] TO [pTeacherWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[POR] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [flowID] ON [dbo].[POR]
(
	[flowID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [tID] ON [dbo].[POR]
(
	[tID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__tID__38C4533E]  DEFAULT ((0)) FOR [tID]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__flowID__39B87777]  DEFAULT ((0)) FOR [flowID]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAdjDays__3AAC9BB0]  DEFAULT ((0)) FOR [porAdjDays]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAdjAmtBa__3BA0BFE9]  DEFAULT ((0)) FOR [porAdjAmtBase]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAdjAmtNe__3C94E422]  DEFAULT ((0)) FOR [porAdjAmtNew]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAdjAmtDa__3D89085B]  DEFAULT ((0)) FOR [porAdjAmtDay]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAdjTotal__3E7D2C94]  DEFAULT ((0)) FOR [porAdjTotal]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAllowTra__3F7150CD]  DEFAULT ((0)) FOR [porAllowTravel]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAllowHou__40657506]  DEFAULT ((0)) FOR [porAllowHousing]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAllowDan__4159993F]  DEFAULT ((0)) FOR [porAllowDanger]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAllowBoa__424DBD78]  DEFAULT ((0)) FOR [porAllowBoardingDuty]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAllowInd__4341E1B1]  DEFAULT ((0)) FOR [porAllowInducement]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAllowLon__443605EA]  DEFAULT ((0)) FOR [porAllowLongService]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF__POR__porAllowCha__452A2A23]  DEFAULT ((0)) FOR [porAllowCharge]
GO
ALTER TABLE [dbo].[POR] ADD  CONSTRAINT [DF_POR_porAdjPermanent]  DEFAULT ((0)) FOR [porAdjPermanent]
GO
ALTER TABLE [dbo].[POR]  WITH CHECK ADD  CONSTRAINT [FK_POR_Flows] FOREIGN KEY([flowID])
REFERENCES [workflow].[Flows] ([flowID])
GO
ALTER TABLE [dbo].[POR] CHECK CONSTRAINT [FK_POR_Flows]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Appointment ID of any appointment raised from this POR. REquired to allows generation of the appointment documentation.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POR', @level2type=N'COLUMN',@level2name=N'portaID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Personnel Ocurrence Record - this is a workflow to process some change in Teacher posting or status, most particuary, the hire of a new teacher.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POR'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'POR' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'POR'
GO

