SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayPointCodes](
	[payptCode] [nvarchar](10) NOT NULL,
	[payptDesc] [nvarchar](50) NULL,
	[payptDescL1] [nvarchar](50) NULL,
	[payptDescL2] [nvarchar](50) NULL,
 CONSTRAINT [PK_PayPointCodes] PRIMARY KEY CLUSTERED 
(
	[payptCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[PayPointCodes] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[PayPointCodes] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[PayPointCodes] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[PayPointCodes] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[PayPointCodes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[PayPointCodes] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Paypoint codes as used in the payroll system. Will become obselete in SI as paypoint codes are synchronised with School Numbers.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PayPointCodes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers (Payroll)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PayPointCodes'
GO

