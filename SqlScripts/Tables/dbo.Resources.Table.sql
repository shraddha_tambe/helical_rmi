SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Resources](
	[resID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[resName] [nvarchar](50) NULL,
	[resLevel] [nvarchar](50) NULL,
	[rmID] [int] NULL,
	[resSplit] [nvarchar](50) NULL,
	[resAvail] [smallint] NULL,
	[resAdequate] [smallint] NULL,
	[resNumber] [int] NULL,
	[resQty] [int] NULL,
	[resCondition] [smallint] NULL,
	[resMaterial] [nvarchar](1) NULL,
	[resLocation] [nvarchar](50) NULL,
	[resNote] [nvarchar](100) NULL,
	[resFunctioning] [smallint] NULL,
 CONSTRAINT [aaaaaResources1_PK] PRIMARY KEY NONCLUSTERED 
(
	[resID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Resources] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[Resources] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[Resources] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Resources] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Resources] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_Resources_ssID_Category] ON [dbo].[Resources]
(
	[ssID] ASC,
	[resName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Resources] ADD  CONSTRAINT [DF__Resources__ssID__26CFC035]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[Resources]  WITH CHECK ADD  CONSTRAINT [FK_Resources_SchoolSurvey] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Resources] CHECK CONSTRAINT [FK_Resources_SchoolSurvey]
GO
ALTER TABLE [dbo].[Resources]  WITH CHECK ADD  CONSTRAINT [CK Resources resAdequate] CHECK  (([resAdequate]=(-1) OR [resAdequate]=(0)))
GO
ALTER TABLE [dbo].[Resources] CHECK CONSTRAINT [CK Resources resAdequate]
GO
ALTER TABLE [dbo].[Resources]  WITH CHECK ADD  CONSTRAINT [CK Resources resAvail] CHECK  (([resAvail]=(-1) OR [resAvail]=(0)))
GO
ALTER TABLE [dbo].[Resources] CHECK CONSTRAINT [CK Resources resAvail]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Records of resources on a survey. Resources are grouped by category, and identified by Resource definitions form the table metaResourceDefs. ssID is the foreign key.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Resources'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Resources' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Resources'
GO

