SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolInspection](
	[inspID] [int] IDENTITY(1,1) NOT NULL,
	[schNo] [nvarchar](50) NULL,
	[inspPlanned] [datetime] NULL,
	[inspStart] [datetime] NULL,
	[inspEnd] [datetime] NULL,
	[inspNote] [ntext] NULL,
	[inspBy] [nvarchar](50) NULL,
	[inspsetID] [int] NULL,
	[inspXml] [xml] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [SchoolInspection_PK] PRIMARY KEY NONCLUSTERED 
(
	[inspID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolInspection] TO [pInspectionRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolInspection] TO [pInspectionWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolInspection] TO [pInspectionWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolInspection] TO [pInspectionWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolInspection] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [InspectionSetSchoolInspection] ON [dbo].[SchoolInspection]
(
	[inspsetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolInspection]  WITH CHECK ADD  CONSTRAINT [InspectionSetSchoolInspection] FOREIGN KEY([inspsetID])
REFERENCES [dbo].[InspectionSet] ([inspsetID])
GO
ALTER TABLE [dbo].[SchoolInspection] CHECK CONSTRAINT [InspectionSetSchoolInspection]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Header table for school inspections. This is "polymorphic" - different inspection types can have different forms for display, and possiby different related data. System defined type BLDG - building review - is the principal example.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolInspection'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Inspection' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolInspection'
GO

