SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolSurvey](
	[ssID] [int] IDENTITY(1,1) NOT NULL,
	[svyYear] [smallint] NOT NULL,
	[schNo] [nvarchar](50) NOT NULL,
	[ssSchType] [nvarchar](10) NOT NULL,
	[ssLang] [nvarchar](5) NULL,
	[ssLangLower] [nvarchar](5) NULL,
	[ssSurveyFormat] [nvarchar](50) NULL,
	[ssPrinFirstName] [nvarchar](50) NULL,
	[ssPrinSurname] [nvarchar](50) NULL,
	[ssAuth] [nvarchar](10) NULL,
	[ssOperator] [nvarchar](50) NULL,
	[ssElectL] [nvarchar](5) NULL,
	[ssElectN] [nvarchar](5) NULL,
	[ssMuni] [nvarchar](5) NULL,
	[ssISClass] [nvarchar](10) NULL,
	[ssISSize] [nvarchar](10) NULL,
	[ssLandOwner] [nvarchar](10) NULL,
	[ssLandUse] [nvarchar](50) NULL,
	[ssLandUseExpiry] [datetime] NULL,
	[ssSponsor] [nvarchar](50) NULL,
	[ssIncome] [money] NULL,
	[ssFundRaising] [money] NULL,
	[ssDonationPriv] [money] NULL,
	[ssDonationChurch] [money] NULL,
	[ssDonationOther] [money] NULL,
	[ssGovtGrant] [money] NULL,
	[ssExpSupplies] [money] NULL,
	[ssExpBooks] [money] NULL,
	[ssExpOther] [money] NULL,
	[ssSchoolCouncil] [bit] NULL,
	[ssSCMeet] [smallint] NULL,
	[ssParentCommittee] [bit] NULL,
	[ssPCMeet] [smallint] NULL,
	[ssPCSupport] [smallint] NULL,
	[ssTotalPupils] [int] NULL,
	[ssTotalClasses] [int] NULL,
	[ssTotalTeachers] [int] NULL,
	[ssPreSchoolM] [int] NULL,
	[ssPreSchoolF] [int] NULL,
	[ssSizeSite] [int] NULL,
	[ssSizePlayground] [int] NULL,
	[ssSizeFarm] [int] NULL,
	[ssSizeFarmUsed] [int] NULL,
	[ssSizeRating] [smallint] NULL,
	[ssSiteSecure] [smallint] NULL,
	[ssRubbishFreq] [nvarchar](10) NULL,
	[ssRubbishMethod] [nvarchar](10) NULL,
	[ssToiletSea] [bit] NULL,
	[ssWaterRating] [smallint] NULL,
	[ssTchHouse] [int] NULL,
	[ssTchHouseOnSite] [int] NULL,
	[ssTchHouseOffSite] [int] NULL,
	[ssTchHouseM] [int] NULL,
	[ssTchHouseN] [int] NULL,
	[ssNote] [ntext] NULL,
	[ssReview] [bit] NULL,
	[ssReviewComment] [ntext] NULL,
	[ssFacilitiesComment] [ntext] NULL,
	[ssSvcBank] [nvarchar](30) NULL,
	[ssSvcAir] [nvarchar](30) NULL,
	[ssSvcShip] [nvarchar](30) NULL,
	[ssSvcClinic] [nvarchar](30) NULL,
	[ssSvcPost] [nvarchar](30) NULL,
	[ssSvcHospital] [nvarchar](30) NULL,
	[ssSvcSupplies] [nvarchar](30) NULL,
	[ssExt] [bit] NULL,
	[ssIsSchool1] [smallint] NULL,
	[ssNearestSchool1] [nvarchar](50) NULL,
	[ssParent] [nvarchar](50) NULL,
	[ssAdminLocn] [nvarchar](50) NULL,
	[ssAdminDistance] [float] NULL,
	[ssAdminTime] [float] NULL,
	[ssNearestPS] [nvarchar](50) NULL,
	[ssPSDistance] [float] NULL,
	[ssPSTime] [float] NULL,
	[ssCatch] [ntext] NULL,
	[ssAccess] [nvarchar](2) NULL,
	[ssPlan0] [int] NULL,
	[ssPlan1] [int] NULL,
	[ssPlan2] [int] NULL,
	[ssPlan3] [int] NULL,
	[ssPlan4] [int] NULL,
	[ssBuildingType] [nvarchar](10) NULL,
	[ssBuildingTypeOther] [nvarchar](50) NULL,
	[ssOperatedBy] [nvarchar](50) NULL,
	[ssMissingData] [bit] NULL,
	[ssMissingDataNotSupplied] [bit] NULL,
	[ssEnrolM] [int] NULL,
	[ssEnrolF] [int] NULL,
	[ssEnrol] [int] NULL,
	[ssFarmL] [int] NULL,
	[ssFarmUsedL] [int] NULL,
	[ssFarmUsedW] [int] NULL,
	[ssFarmW] [int] NULL,
	[ssPlaygroundL] [int] NULL,
	[ssPlaygroundW] [int] NULL,
	[ssSiteL] [int] NULL,
	[ssSiteW] [int] NULL,
	[ssSCApproved] [bit] NULL,
	[ssPNAReason] [nvarchar](10) NULL,
	[ssAcaYearEndM] [int] NULL,
	[ssAcaYearEndY] [int] NULL,
	[ssAcaYearStartM] [int] NULL,
	[ssAcaYearStartY] [int] NULL,
	[ssCoreSubject1] [nvarchar](50) NULL,
	[ssCoreSubject2] [nvarchar](50) NULL,
	[ssCurriculum] [nvarchar](100) NULL,
	[ssPCMembersF] [smallint] NULL,
	[ssPCMembersM] [smallint] NULL,
	[ssRandM] [int] NULL,
	[ssRandMResp] [nvarchar](10) NULL,
	[ssRubbishNote] [nvarchar](50) NULL,
	[ssSiteHost] [nvarchar](10) NULL,
	[ssSiteHostDetail] [nvarchar](100) NULL,
	[ssEdSystem] [nvarchar](10) NULL,
	[ssMediumOfInstruction] [nvarchar](50) NULL,
	[ssSCActivities] [nvarchar](400) NULL,
	[ssSCMembersF] [smallint] NULL,
	[ssSCMembersM] [smallint] NULL,
	[ssShifts] [int] NULL,
	[ssClassesShift1] [int] NULL,
	[ssClassesShift2] [int] NULL,
	[ssClassesShiftAll] [int] NULL,
	[ssSuppliesCurriculum] [smallint] NULL,
	[ssSuppliesInUse] [smallint] NULL,
	[ssSuppliesStudent] [nvarchar](10) NULL,
	[ssSuppliesTeacher] [nvarchar](10) NULL,
	[ssNumBuildings] [int] NULL,
	[ssNumClassrooms] [int] NULL,
	[ssNumClassroomsPoor] [int] NULL,
	[ssNumTeachers] [int] NULL,
	[ssBudget] [int] NULL,
	[ssStartDay] [datetime] NULL,
	[ssSource] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaSchoolSurvey1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_SchoolSurvey_School_Year] UNIQUE NONCLUSTERED 
(
	[schNo] ASC,
	[svyYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SchoolSurvey] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SchoolSurvey] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolSurvey] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolSurvey] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolSurvey] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_SchoolSurvey_Ranker] ON [dbo].[SchoolSurvey]
(
	[ssID] ASC,
	[schNo] ASC
)
INCLUDE ( 	[ssSchType],
	[ssEnrol]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_SchoolSurvey_Year] ON [dbo].[SchoolSurvey]
(
	[svyYear] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__svyYe__1FCDBCEB]  DEFAULT ((0)) FOR [svyYear]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssInc__20C1E124]  DEFAULT ((0)) FOR [ssIncome]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssFun__21B6055D]  DEFAULT ((0)) FOR [ssFundRaising]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssDon__22AA2996]  DEFAULT ((0)) FOR [ssDonationPriv]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssDon__239E4DCF]  DEFAULT ((0)) FOR [ssDonationChurch]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssDon__24927208]  DEFAULT ((0)) FOR [ssDonationOther]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssGov__25869641]  DEFAULT ((0)) FOR [ssGovtGrant]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssExp__267ABA7A]  DEFAULT ((0)) FOR [ssExpSupplies]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssExp__276EDEB3]  DEFAULT ((0)) FOR [ssExpBooks]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssExp__286302EC]  DEFAULT ((0)) FOR [ssExpOther]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssSch__29572725]  DEFAULT ((0)) FOR [ssSchoolCouncil]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssPar__2A4B4B5E]  DEFAULT ((0)) FOR [ssParentCommittee]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssPre__2B3F6F97]  DEFAULT ((0)) FOR [ssPreSchoolM]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssPre__2C3393D0]  DEFAULT ((0)) FOR [ssPreSchoolF]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssSiz__3A81B327]  DEFAULT ((0)) FOR [ssSizeFarmUsed]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssToi__3B75D760]  DEFAULT ((0)) FOR [ssToiletSea]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssRev__3F466844]  DEFAULT ((0)) FOR [ssReview]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssExt__403A8C7D]  DEFAULT ((0)) FOR [ssExt]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssMis__412EB0B6]  DEFAULT ((0)) FOR [ssMissingData]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF__SchoolSur__ssMis__4222D4EF]  DEFAULT ((0)) FOR [ssMissingDataNotSupplied]
GO
ALTER TABLE [dbo].[SchoolSurvey] ADD  CONSTRAINT [DF_SchoolSurvey_ssSCApproved]  DEFAULT ((0)) FOR [ssSCApproved]
GO
ALTER TABLE [dbo].[SchoolSurvey]  WITH CHECK ADD  CONSTRAINT [FK_SchoolSurvey_Authority] FOREIGN KEY([ssAuth])
REFERENCES [dbo].[Authorities] ([authCode])
GO
ALTER TABLE [dbo].[SchoolSurvey] CHECK CONSTRAINT [FK_SchoolSurvey_Authority]
GO
ALTER TABLE [dbo].[SchoolSurvey]  WITH CHECK ADD  CONSTRAINT [FK_SchoolSurvey_Schools] FOREIGN KEY([schNo])
REFERENCES [dbo].[Schools] ([schNo])
GO
ALTER TABLE [dbo].[SchoolSurvey] CHECK CONSTRAINT [FK_SchoolSurvey_Schools]
GO
ALTER TABLE [dbo].[SchoolSurvey]  WITH CHECK ADD  CONSTRAINT [FK_SchoolSurvey_Survey] FOREIGN KEY([svyYear])
REFERENCES [dbo].[Survey] ([svyYear])
GO
ALTER TABLE [dbo].[SchoolSurvey] CHECK CONSTRAINT [FK_SchoolSurvey_Survey]
GO
ALTER TABLE [dbo].[SchoolSurvey]  WITH CHECK ADD  CONSTRAINT [CK SchoolSurvey ssSiteSecure] CHECK  (([ssSiteSecure]=(-1) OR [ssSiteSecure]=(0)))
GO
ALTER TABLE [dbo].[SchoolSurvey] CHECK CONSTRAINT [CK SchoolSurvey ssSiteSecure]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 10 2009
-- Description:	Check update of Schools after saving SchoolSurvey to maintain 'latest' school type etc
-- =============================================
CREATE TRIGGER [dbo].[ChangeSchoolSurvey] 
   ON  [dbo].[SchoolSurvey] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- 
UPDATE Schools
	SET 
		schType = isnull(ssSchType, schType)
		, schAuth = isnull(ssAuth, schAuth)
		, schElectL = isnull(ssElectL, schElectL)
		, schElectN = isnull(ssElectN, schElectN)
		, schLang = isnull(ssLang, schLang)
from
	Schools
	INNER JOIN
	(
	Select SS.schNo, ssSchType, ssAuth, ssElectL, ssElectN, ssLang
		, ROW_NUMBER() over (PARTITION BY SS.SchNo ORDER BY SS.svyYear DESC) SurveyCounter
	from 
		SchoolSurvey SS
		INNER JOIN
		(Select schNo from INSERTED UNION Select schNo from DELETED) AffectedSchools
		ON SS.SchNo = AffectedSchools.schNo
	) SurveyData
	ON Schools.schNo = SurveyData.schNo

WHERE SurveyCounter = 1

END
GO
ALTER TABLE [dbo].[SchoolSurvey] ENABLE TRIGGER [ChangeSchoolSurvey]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 10 2009
-- Description:	Check update of Schools after saving SchoolSurvey to maintain 'latest' school type etc
-- =============================================
CREATE TRIGGER [dbo].[SchoolSurvey_RelatedData]
   ON  [dbo].[SchoolSurvey] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

-- leave this code for further consideration....
-- but we really don't want to update Schools now from SchoolSurvey
	RETURN
UPDATE Schools
	SET 
		schType = ssSchType
		, schAuth = ssAuth
		, schElectL = ssElectL
		, schElectN = ssElectN
		, schLang = ssLang
from
	Schools
	INNER JOIN
	(
	Select SS.schNo, ssSchType, ssAuth, ssElectL, ssElectN, ssLang
		, ROW_NUMBER() over (PARTITION BY SS.SchNo ORDER BY SS.svyYear DESC) SurveyCounter
	from 
		SchoolSurvey SS
		INNER JOIN
		(Select schNo from INSERTED UNION Select schNo from DELETED) AffectedSchools
		ON SS.SchNo = AffectedSchools.schNo
	) SurveyData
	ON Schools.schNo = SurveyData.schNo

WHERE SurveyCounter = 1

END
GO
ALTER TABLE [dbo].[SchoolSurvey] ENABLE TRIGGER [SchoolSurvey_RelatedData]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Some surveys have recorded aggregate totals of classes, teachers and pupils, as well as detailed grids of enrolment numbers and teacher lists. These fields should only be used as checksums - use ssEnrol to get the total enrolment for this survey, or add from Enrollments table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey', @level2type=N'COLUMN',@level2name=N'ssTotalPupils'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'''Total teachers'' is entered on some survey formats.  Should not be used for reporting, only to cross check the teacher data stored in TeacherSurvey table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey', @level2type=N'COLUMN',@level2name=N'ssTotalTeachers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ssSupplies.... fields added in Kiribati 2014 survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey', @level2type=N'COLUMN',@level2name=N'ssSuppliesInUse'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of teachers at the school. this is aggregated fronm teacher records. This is distinct from ssTotalTeachers, which is captured on a survey form as a cross check' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey', @level2type=N'COLUMN',@level2name=N'ssNumTeachers'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Total school budget' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey', @level2type=N'COLUMN',@level2name=N'ssBudget'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date that tuitioon begins at this school in the survey year ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey', @level2type=N'COLUMN',@level2name=N'ssStartDay'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Source of the survey. May point to a file in the fileDB' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey', @level2type=N'COLUMN',@level2name=N'ssSource'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Header for the school survey. All the data on the survey is related to this by the ssID - primary key on this table. 
The combination of schNo / svyYear is a unique constraint. schNo is foregn key from Schools, svyYEar is foregin key from survey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The authority code must be defined in Authorities' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchoolSurvey', @level2type=N'CONSTRAINT',@level2name=N'FK_SchoolSurvey_Authority'
GO

