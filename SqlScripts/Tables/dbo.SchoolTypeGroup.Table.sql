SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolTypeGroup](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](200) NOT NULL,
 CONSTRAINT [aaaaaSchoolTypeGroup1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[SchoolTypeGroup] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[SchoolTypeGroup] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[SchoolTypeGroup] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SchoolTypeGroup] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[SchoolTypeGroup] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SchoolTypeGroup] TO [public] AS [dbo]
GO

