SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SetBooks](
	[cuID] [int] IDENTITY(1,1) NOT NULL,
	[cuYear] [int] NOT NULL,
	[bkCode] [nvarchar](20) NULL,
	[cuLevel] [nvarchar](10) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_SetBooks] PRIMARY KEY CLUSTERED 
(
	[cuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SetBooks] TO [pSchoolRead] AS [dbo]
GO
ALTER TABLE [dbo].[SetBooks]  WITH CHECK ADD  CONSTRAINT [FK_SetBooks_Books] FOREIGN KEY([bkCode])
REFERENCES [dbo].[Books] ([bkCode])
GO
ALTER TABLE [dbo].[SetBooks] CHECK CONSTRAINT [FK_SetBooks_Books]
GO
ALTER TABLE [dbo].[SetBooks]  WITH CHECK ADD  CONSTRAINT [FK_SetBooks_lkpLevels] FOREIGN KEY([cuLevel])
REFERENCES [dbo].[lkpLevels] ([codeCode])
GO
ALTER TABLE [dbo].[SetBooks] CHECK CONSTRAINT [FK_SetBooks_lkpLevels]
GO

