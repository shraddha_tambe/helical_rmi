SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SourceOfFundsGroup](
	[sfgCode] [nvarchar](10) NOT NULL,
	[sfgDesc] [nvarchar](50) NULL,
 CONSTRAINT [aaaaaSourceOfFundsGroup1_PK] PRIMARY KEY NONCLUSTERED 
(
	[sfgCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SourceOfFundsGroup] TO [pFinanceRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SourceOfFundsGroup] TO [pFinanceWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SourceOfFundsGroup] TO [pFinanceWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SourceOfFundsGroup] TO [pFinanceWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SourceOfFundsGroup] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Caegorisation of source of funds for reporting and layut on form.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SourceOfFundsGroup'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SourceOfFundsGroup'
GO

