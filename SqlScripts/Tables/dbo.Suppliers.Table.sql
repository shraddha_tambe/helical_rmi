SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Suppliers](
	[supCode] [nvarchar](10) NOT NULL,
	[supName] [nvarchar](50) NULL,
	[supAddr1] [nvarchar](255) NULL,
	[supAddr2] [nvarchar](50) NULL,
	[supAddr3] [nvarchar](50) NULL,
	[supAddrCity] [nvarchar](50) NULL,
	[supAddrCountry] [nvarchar](50) NULL,
	[supAddrPostCode] [nvarchar](10) NULL,
	[supPh] [nvarchar](50) NULL,
	[supFax] [nvarchar](50) NULL,
	[supEmail] [nvarchar](50) NULL,
	[supContact] [nvarchar](50) NULL,
 CONSTRAINT [Suppliers_PK] PRIMARY KEY NONCLUSTERED 
(
	[supCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[Suppliers] TO [pInfrastructureReadX] AS [dbo]
GO
GRANT DELETE ON [dbo].[Suppliers] TO [pInfrastructureWriteX] AS [dbo]
GO
GRANT INSERT ON [dbo].[Suppliers] TO [pInfrastructureWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Suppliers] TO [pInfrastructureWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[Suppliers] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [supAddrPostCode] ON [dbo].[Suppliers]
(
	[supAddrPostCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Suppliers for Work Orders. Includes contact details' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Suppliers'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Work Orders' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Suppliers'
GO

