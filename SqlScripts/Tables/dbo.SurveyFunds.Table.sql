SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyFunds](
	[svfID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[svfType] [nvarchar](10) NULL,
	[svfAmount] [money] NULL,
	[svfDetails] [nvarchar](250) NULL,
	[svfDate] [datetime] NULL,
	[svfOther] [nvarchar](100) NULL,
	[svfFee] [money] NULL,
	[svfFeePer] [nvarchar](1) NULL,
	[svfFeePercent] [float] NULL,
	[svfFeeIsCharged] [int] NULL,
 CONSTRAINT [PK_SurveyFunds] PRIMARY KEY CLUSTERED 
(
	[svfID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SurveyFunds] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyFunds] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyFunds] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyFunds] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SurveyFunds] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_SurveyFunds_SSID] ON [dbo].[SurveyFunds]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SurveyFunds] ADD  CONSTRAINT [DF__SurveyFund__ssID__0D7A0286]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[SurveyFunds] ADD  CONSTRAINT [DF__SurveyFun__svfAm__0E6E26BF]  DEFAULT ((0)) FOR [svfAmount]
GO
ALTER TABLE [dbo].[SurveyFunds] ADD  CONSTRAINT [DF__SurveyFun__svfFe__0F624AF8]  DEFAULT ((0)) FOR [svfFeePercent]
GO
ALTER TABLE [dbo].[SurveyFunds]  WITH CHECK ADD  CONSTRAINT [FK_SurveyFunds_SchoolSurvey] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SurveyFunds] CHECK CONSTRAINT [FK_SurveyFunds_SchoolSurvey]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Funding details recorded on the school survey. Related to SchoolSurvey, and SourceOfFunds.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyFunds'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Finance' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyFunds'
GO

