SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyYearCourses](
	[scrsID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[scrsName] [nvarchar](50) NULL,
	[scrsFaculty] [nvarchar](50) NULL,
	[scrsDept] [nvarchar](50) NULL,
	[scrsDurationPerAnnum] [int] NULL,
	[scrsCost] [money] NULL,
	[scrsAccred] [nvarchar](50) NULL,
	[scrsYears] [int] NULL,
	[scrsPlaces] [int] NULL,
	[scrsTeachers] [int] NULL,
	[scrsApplicantsM] [int] NULL,
	[scrsApplicantsF] [int] NULL,
	[scrsIntakeM] [int] NULL,
	[scrsIntakeF] [int] NULL,
	[scrsGraduatesM] [int] NULL,
	[scrsGraduatesF] [int] NULL,
	[scrsMode] [nvarchar](10) NULL,
 CONSTRAINT [aaaaaSurveyYearCourses1_PK] PRIMARY KEY NONCLUSTERED 
(
	[scrsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SurveyYearCourses] TO [pSurveyRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyYearCourses] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyYearCourses] TO [pSurveyWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyYearCourses] TO [pSurveyWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SurveyYearCourses] TO [public] AS [dbo]
GO
CREATE NONCLUSTERED INDEX [IX_SurveyYearCourses_SSID] ON [dbo].[SurveyYearCourses]
(
	[ssID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SurveyYearCourses]  WITH CHECK ADD  CONSTRAINT [SurveyYearCourses_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SurveyYearCourses] CHECK CONSTRAINT [SurveyYearCourses_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Courses offered by a teritary insitution by Faculty. Linked to SchoolSurvey. SurveyYearCourses is related table tracking course details by year.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearCourses'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Survey' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearCourses'
GO

