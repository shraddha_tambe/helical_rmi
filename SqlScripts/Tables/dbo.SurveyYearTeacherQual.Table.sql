SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyYearTeacherQual](
	[ytqID] [int] IDENTITY(1,1) NOT NULL,
	[svyYear] [int] NULL,
	[ytqSector] [nvarchar](3) NULL,
	[ytqQual] [nvarchar](20) NULL,
	[ytqQualified] [bit] NULL,
	[ytqCertified] [bit] NULL,
 CONSTRAINT [aaaaaSurveyYearTeacherQual1_PK] PRIMARY KEY NONCLUSTERED 
(
	[ytqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[SurveyYearTeacherQual] TO [pSurveyRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[SurveyYearTeacherQual] TO [pSurveyWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[SurveyYearTeacherQual] TO [pSurveyWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[SurveyYearTeacherQual] TO [pSurveyWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[SurveyYearTeacherQual] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_SurveyYearTeacherQualYSQ] ON [dbo].[SurveyYearTeacherQual]
(
	[svyYear] ASC,
	[ytqSector] ASC,
	[ytqQual] ASC
)
INCLUDE ( 	[ytqQualified],
	[ytqCertified]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SurveyYearTeacherQual] ADD  CONSTRAINT [DF__SurveyYea__svyYe__54968AE5]  DEFAULT ((0)) FOR [svyYear]
GO
ALTER TABLE [dbo].[SurveyYearTeacherQual] ADD  CONSTRAINT [DF__SurveyYea__ytqQu__558AAF1E]  DEFAULT ((0)) FOR [ytqQualified]
GO
ALTER TABLE [dbo].[SurveyYearTeacherQual] ADD  CONSTRAINT [DF__SurveyYea__ytqCe__567ED357]  DEFAULT ((0)) FOR [ytqCertified]
GO
ALTER TABLE [dbo].[SurveyYearTeacherQual]  WITH CHECK ADD  CONSTRAINT [SurveyYearTeacherQual_FK00] FOREIGN KEY([ytqSector])
REFERENCES [dbo].[EducationSectors] ([secCode])
GO
ALTER TABLE [dbo].[SurveyYearTeacherQual] CHECK CONSTRAINT [SurveyYearTeacherQual_FK00]
GO
ALTER TABLE [dbo].[SurveyYearTeacherQual]  WITH CHECK ADD  CONSTRAINT [SurveyYearTeacherQual_FK01] FOREIGN KEY([ytqQual])
REFERENCES [dbo].[lkpTeacherQual] ([codeCode])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SurveyYearTeacherQual] CHECK CONSTRAINT [SurveyYearTeacherQual_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'index on year/sector/qualification' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearTeacherQual', @level2type=N'INDEX',@level2name=N'IX_SurveyYearTeacherQualYSQ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'For a given survey year and ed sector, defines which Teacher qualifications make the teacher certified and/or Qualified. Basis of EFA9 / 10 reporting.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearTeacherQual'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SurveyYearTeacherQual'
GO

