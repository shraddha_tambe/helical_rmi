SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherAppointmentArchive](
	[taID] [int] IDENTITY(1,1) NOT NULL,
	[tID] [int] NULL,
	[taDate] [datetime] NULL,
	[taType] [nvarchar](20) NULL,
	[SchNo] [nvarchar](50) NULL,
	[taRole] [nvarchar](50) NULL,
	[spCode] [nvarchar](10) NULL,
	[taSalary] [money] NULL,
	[estrID] [int] NULL,
	[estpNo] [nvarchar](20) NULL,
	[taEndDate] [datetime] NULL,
	[taFlag] [nvarchar](10) NULL,
	[taNote] [nvarchar](400) NULL,
	[taHasNote] [int] NOT NULL,
	[taCreateDate] [datetime] NULL,
	[taCreateUser] [nvarchar](100) NULL,
	[taEditDate] [datetime] NULL,
	[taEditUser] [nvarchar](100) NULL,
	[taRoleGrade] [nvarchar](20) NULL
) ON [PRIMARY]
GO

