SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherHousing](
	[thID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NULL,
	[thType] [nvarchar](50) NULL,
	[thNum] [int] NULL,
 CONSTRAINT [aaaaaTeacherHousing1_PK] PRIMARY KEY NONCLUSTERED 
(
	[thID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[TeacherHousing] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[TeacherHousing] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherHousing] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherHousing] TO [pSchoolWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherHousing] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[TeacherHousing] ADD  CONSTRAINT [DF__TeacherHou__ssID__4183B671]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[TeacherHousing] ADD  CONSTRAINT [DF__TeacherHo__thNum__4277DAAA]  DEFAULT ((0)) FOR [thNum]
GO
ALTER TABLE [dbo].[TeacherHousing]  WITH CHECK ADD  CONSTRAINT [TeacherHousing_FK00] FOREIGN KEY([thType])
REFERENCES [dbo].[lkpTeacherHousingTypes] ([codeCode])
GO
ALTER TABLE [dbo].[TeacherHousing] CHECK CONSTRAINT [TeacherHousing_FK00]
GO
ALTER TABLE [dbo].[TeacherHousing]  WITH CHECK ADD  CONSTRAINT [TeacherHousing_FK01] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TeacherHousing] CHECK CONSTRAINT [TeacherHousing_FK01]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'OBSELETE - ported to Resources, then to Buildings.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherHousing'
GO

