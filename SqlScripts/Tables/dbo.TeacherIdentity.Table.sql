SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherIdentity](
	[tID] [int] IDENTITY(1,1) NOT NULL,
	[tRegister] [nvarchar](50) NULL,
	[tPayroll] [nvarchar](50) NULL,
	[tProvident] [nvarchar](50) NULL,
	[tDOB] [datetime] NULL,
	[tDOBEst] [bit] NULL,
	[tSex] [nvarchar](1) NULL,
	[tNamePrefix] [nvarchar](50) NULL,
	[tGiven] [nvarchar](50) NOT NULL,
	[tMiddleNames] [nvarchar](50) NULL,
	[tSurname] [nvarchar](50) NOT NULL,
	[tNameSuffix] [nvarchar](20) NULL,
	[tGivenSoundex]  AS (soundex([tGiven])) PERSISTED,
	[tSurnameSoundex]  AS (soundex([tSurname])) PERSISTED,
	[tSrcID] [int] NULL,
	[tSrc] [nvarchar](50) NULL,
	[tComment] [ntext] NULL,
	[tYearStarted] [int] NULL,
	[tYearFinished] [int] NULL,
	[tDatePSAppointed] [datetime] NULL,
	[tDatePSClosed] [datetime] NULL,
	[tCloseReason] [nvarchar](50) NULL,
	[tPayErr] [bit] NULL,
	[tPayErrUser] [nvarchar](50) NULL,
	[tPayErrDate] [datetime] NULL,
	[tEstErr] [bit] NULL,
	[tEstErrUser] [nvarchar](50) NULL,
	[tEstErrDate] [datetime] NULL,
	[tSalaryPoint] [nvarchar](10) NULL,
	[tSalaryPointDate] [datetime] NULL,
	[tSalaryPointNextIncr] [datetime] NULL,
	[tLangMajor] [nvarchar](5) NULL,
	[tLangMinor] [nvarchar](5) NULL,
	[tpayptCode] [nvarchar](10) NULL,
	[tImage] [int] NULL,
	[tDateRegister] [datetime] NULL,
	[tDateRegisterEnd] [datetime] NULL,
	[tRegisterEndReason] [nvarchar](50) NULL,
	[tShortName]  AS ([dbo].[TeacherName]((1),[tNamePrefix],[tGiven],[tMiddleNames],[tSurname],[tNameSuffix])),
	[tFullName]  AS ([dbo].[TeacherName]((2),[tNamePrefix],[tGiven],[tMiddleNames],[tSurname],[tNameSuffix])),
	[tLongName]  AS ([dbo].[TeacherName]((3),[tNamePrefix],[tGiven],[tMiddleNames],[tSurname],[tNameSuffix])),
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditContext] [nvarchar](50) NULL,
	[pRowversion] [timestamp] NOT NULL,
	[tRegisterStatus] [nvarchar](20) NULL,
	[tPhoto] [uniqueidentifier] NULL,
 CONSTRAINT [aaaaaTeacherIdentity1_PK] PRIMARY KEY NONCLUSTERED 
(
	[tID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[TeacherIdentity] TO [pineapples] AS [dbo]
GO
GRANT SELECT ON [dbo].[TeacherIdentity] TO [pTeacherRead] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherIdentity] TO [pTeacherWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherIdentity] TO [public] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tID]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tRegister]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tPayroll]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tProvident]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tDOB]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tDOBEst]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tSex]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tNamePrefix]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tGiven]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tMiddleNames]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tSurname]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tNameSuffix]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tGivenSoundex]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tSurnameSoundex]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tSrcID]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tSrc]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tComment]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tYearStarted]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tYearFinished]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tDatePSAppointed]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tDatePSClosed]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tCloseReason]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tPayErr]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tPayErrUser]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tPayErrDate]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tEstErr]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tEstErrUser]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tEstErrDate]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tSalaryPoint]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tSalaryPointDate]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tSalaryPointNextIncr]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tLangMajor]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tLangMinor]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tpayptCode]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tImage]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tDateRegister]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tDateRegisterEnd]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tRegisterEndReason]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tShortName]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tFullName]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([tLongName]) TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([pCreateDateTime]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([pCreateUser]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([pEditDateTime]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([pEditUser]) TO [pineapples] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherIdentity] ([pEditContext]) TO [pTeacherWrite] AS [dbo]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
CREATE NONCLUSTERED INDEX [IX_GivenSoundex] ON [dbo].[TeacherIdentity]
(
	[tGivenSoundex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_Payroll] ON [dbo].[TeacherIdentity]
(
	[tPayroll] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_Surname] ON [dbo].[TeacherIdentity]
(
	[tSurname] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
CREATE NONCLUSTERED INDEX [IX_SurnameSoundex] ON [dbo].[TeacherIdentity]
(
	[tSurnameSoundex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TeacherIdentity] ADD  CONSTRAINT [DF__TeacherId__tDOBE__44FF419A]  DEFAULT ((0)) FOR [tDOBEst]
GO
ALTER TABLE [dbo].[TeacherIdentity] ADD  CONSTRAINT [DF__TeacherId__tSrcI__45F365D3]  DEFAULT ((0)) FOR [tSrcID]
GO
ALTER TABLE [dbo].[TeacherIdentity] ADD  CONSTRAINT [DF__TeacherId__tPayE__46E78A0C]  DEFAULT ((0)) FOR [tPayErr]
GO
ALTER TABLE [dbo].[TeacherIdentity] ADD  CONSTRAINT [DF__TeacherId__tEstE__47DBAE45]  DEFAULT ((0)) FOR [tEstErr]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[TeacherIdentity_AuditDelete] 
   ON  [dbo].[TeacherIdentity] 
   
   WITH EXECUTE as 'pineapples'
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from DELETED
	
	
	exec @AuditID =  audit.LogAudit 'TeacherIdentity', null, 'D', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKey, arowName)
	SELECT DISTINCT @AuditID
	, tID
	,  isnull(tFullName,'') 
	 + ' ' + isnull(tPayroll,'') 
 
	FROM DELETED	



END
GO
ALTER TABLE [dbo].[TeacherIdentity] ENABLE TRIGGER [TeacherIdentity_AuditDelete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[TeacherIdentity_AuditInsert] 
   ON  [dbo].[TeacherIdentity] 
    
   WITH EXECUTE as 'pineapples'

   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from INSERTED
	
	
	exec @AuditID =  audit.LogAudit 'TeacherIdentity', null, 'I', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKey)
	SELECT DISTINCT @AuditID
	, tID
	FROM INSERTED	

-- update the created and edited fields
	UPDATE TeacherIdentity
		SET pCreateDateTime = aLog.auditDateTime
			, pCreateUser = alog.auditUser

			, pEditDateTime = aLog.auditDateTime
			, pEditUser = alog.auditUser
	FROM INSERTED I
		CROSS JOIN Audit.auditLog alog
	WHERE TeacherIdentity.tID = I.tID
	AND aLog.auditID = @auditID


END
GO
ALTER TABLE [dbo].[TeacherIdentity] ENABLE TRIGGER [TeacherIdentity_AuditInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-0912
-- Description:	Logging trigger
-- =============================================
CREATE TRIGGER [dbo].[TeacherIdentity_AuditUpdate] 
   ON  [dbo].[TeacherIdentity] 

	-- pineapples has access to write to TeacherIdentityAudit
	WITH EXECUTE AS 'pineapples'
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @AuditID int



		declare @NumChanges int
		
		Select @NumChanges = count(*) from INSERTED
		
		
		exec @AuditID =  audit.LogAudit 'TeacherIdentity', null, 'U', @Numchanges
		
		INSERT INTO audit.AuditRow
		(auditID, arowKey)
		SELECT DISTINCT @AuditID
		, tID
		FROM INSERTED	



		
		INSERT INTO audit.AuditColumn
		(arowID, acolName, acolBefore, acolAfter)	
		
		SELECT arowID
		, ColumnName
		, DValue
		, IValue
		FROM audit.AuditRow Row
		INNER JOIN 
		(
			SELECT 
			I.tID
			, case num
				when 1 then 'tID'
				when 2 then 'tRegister'
				when 3 then 'tPayroll'
				when 4 then 'tProvident'
				when 5 then 'tDOB'
				when 6 then 'tDOBEst'
				when 7 then 'tSex'
				when 8 then 'tNamePrefix'
				when 9 then 'tGiven'
				when 10 then 'tMiddleNames'
				when 11 then 'tSurname'
				when 12 then 'tNameSuffix'
				when 13 then 'tGivenSoundex'
				when 14 then 'tSurnameSoundex'
				when 15 then 'tSrcID'
				when 16 then 'tSrc'
				when 17 then 'tComment'
				when 18 then 'tYearStarted'
				when 19 then 'tYearFinished'
				when 20 then 'tDatePSAppointed'
				when 21 then 'tDatePSClosed'
				when 22 then 'tCloseReason'
				when 23 then 'tPayErr'
				when 24 then 'tPayErrUser'
				when 25 then 'tPayErrDate'
				when 26 then 'tEstErr'
				when 27 then 'tEstErrUser'
				when 28 then 'tEstErrDate'
				when 29 then 'tSalaryPoint'
				when 30 then 'tSalaryPointDate'
				when 31 then 'tSalaryPointNextIncr'
				when 32 then 'tLangMajor'
				when 33 then 'tLangMinor'
				when 34 then 'tpayptCode'
				when 35 then 'tImage'
				when 36 then 'tDateRegister'
				when 37 then 'tDateRegisterEnd'
				when 38 then 'tRegisterEndReason'
			end ColumnName
			, case num
				when 1 then cast(D.tID as nvarchar(50))
				when 2 then cast(D.tRegister as nvarchar(50))
				when 3 then cast(D.tPayroll as nvarchar(50))
				when 4 then cast(D.tProvident as nvarchar(50))
				when 5 then cast(D.tDOB as nvarchar(50))
				when 6 then cast(D.tDOBEst as nvarchar(50))
				when 7 then cast(D.tSex as nvarchar(50))
				when 8 then cast(D.tNamePrefix as nvarchar(50))
				when 9 then cast(D.tGiven as nvarchar(50))
				when 10 then cast(D.tMiddleNames as nvarchar(50))
				when 11 then cast(D.tSurname as nvarchar(50))
				when 12 then cast(D.tNameSuffix as nvarchar(50))
				when 15 then cast(D.tSrcID as nvarchar(50))
				when 16 then cast(D.tSrc as nvarchar(50))
				--when 17 then cast(D.tComment as nvarchar(50))
				when 18 then cast(D.tYearStarted as nvarchar(50))
				when 19 then cast(D.tYearFinished as nvarchar(50))
				when 20 then cast(D.tDatePSAppointed as nvarchar(50))
				when 21 then cast(D.tDatePSClosed as nvarchar(50))
				when 22 then cast(D.tCloseReason as nvarchar(50))
				when 23 then cast(D.tPayErr as nvarchar(50))
				when 24 then cast(D.tPayErrUser as nvarchar(50))
				when 25 then cast(D.tPayErrDate as nvarchar(50))
				when 26 then cast(D.tEstErr as nvarchar(50))
				when 27 then cast(D.tEstErrUser as nvarchar(50))
				when 28 then cast(D.tEstErrDate as nvarchar(50))
				when 29 then cast(D.tSalaryPoint as nvarchar(50))
				when 30 then cast(D.tSalaryPointDate as nvarchar(50))
				when 31 then cast(D.tSalaryPointNextIncr as nvarchar(50))
				when 32 then cast(D.tLangMajor as nvarchar(50))
				when 33 then cast(D.tLangMinor as nvarchar(50))
				when 34 then cast(D.tpayptCode as nvarchar(50))
				when 35 then cast(D.tImage as nvarchar(50))
				when 36 then cast(D.tDateRegister as nvarchar(50))
				when 37 then cast(D.tDateRegisterEnd as nvarchar(50))
				when 38 then cast(D.tRegisterEndReason as nvarchar(50))
			end dValue
			, case num
				when 1 then cast(I.tID as nvarchar(50))
				when 2 then cast(I.tRegister as nvarchar(50))
				when 3 then cast(I.tPayroll as nvarchar(50))
				when 4 then cast(I.tProvident as nvarchar(50))
				when 5 then cast(I.tDOB as nvarchar(50))
				when 6 then cast(I.tDOBEst as nvarchar(50))
				when 7 then cast(I.tSex as nvarchar(50))
				when 8 then cast(I.tNamePrefix as nvarchar(50))
				when 9 then cast(I.tGiven as nvarchar(50))
				when 10 then cast(I.tMiddleNames as nvarchar(50))
				when 11 then cast(I.tSurname as nvarchar(50))
				when 12 then cast(I.tNameSuffix as nvarchar(50))
				when 15 then cast(I.tSrcID as nvarchar(50))
				when 16 then cast(I.tSrc as nvarchar(50))
				--when 17 then cast(I.tComment as nvarchar(50))
				when 18 then cast(I.tYearStarted as nvarchar(50))
				when 19 then cast(I.tYearFinished as nvarchar(50))
				when 20 then cast(I.tDatePSAppointed as nvarchar(50))
				when 21 then cast(I.tDatePSClosed as nvarchar(50))
				when 22 then cast(I.tCloseReason as nvarchar(50))
				when 23 then cast(I.tPayErr as nvarchar(50))
				when 24 then cast(I.tPayErrUser as nvarchar(50))
				when 25 then cast(I.tPayErrDate as nvarchar(50))
				when 26 then cast(I.tEstErr as nvarchar(50))
				when 27 then cast(I.tEstErrUser as nvarchar(50))
				when 28 then cast(I.tEstErrDate as nvarchar(50))
				when 29 then cast(I.tSalaryPoint as nvarchar(50))
				when 30 then cast(I.tSalaryPointDate as nvarchar(50))
				when 31 then cast(I.tSalaryPointNextIncr as nvarchar(50))
				when 32 then cast(I.tLangMajor as nvarchar(50))
				when 33 then cast(I.tLangMinor as nvarchar(50))
				when 34 then cast(I.tpayptCode as nvarchar(50))
				when 35 then cast(I.tImage as nvarchar(50))
				when 36 then cast(I.tDateRegister as nvarchar(50))
				when 37 then cast(I.tDateRegisterEnd as nvarchar(50))
				when 38 then cast(I.tRegisterEndReason as nvarchar(50))
			end IValue

		FROM INSERTED I 
		INNER JOIN DELETED D
			ON I.tID = D.tID
		CROSS JOIN metaNumbers
		WHERE num between 1 and 38
		) Edits
		ON Edits.tID = Row.arowKey
		WHERE Row.auditID = @AuditID
		AND isnull(DVAlue,'') <> isnull(IValue,'')

		
		-- update the last edited field	
		UPDATE TeacherIdentity
			SET pEditDateTime = aLog.auditDateTime
				, pEditUser = alog.auditUser
		FROM INSERTED I
			CROSS JOIN Audit.auditLog alog
		WHERE TeacherIdentity.tID = I.tID
		AND aLog.auditID = @auditID
	
END
GO
ALTER TABLE [dbo].[TeacherIdentity] ENABLE TRIGGER [TeacherIdentity_AuditUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 22 06 2010
-- Description:	Enforce unique payroll numbers, registration numbers, Provident numbers
-- =============================================
CREATE TRIGGER [dbo].[TeacherIdentity_Validations]
   ON  [dbo].[TeacherIdentity] 
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
begin try
	if (UPDATE(tPayroll) and common.Sysparam('TEACHER_UNIQUEPAYROLL') = 'Y') begin
		IF Exists(
					Select TI.tPayroll
					FROM TeacherIdentity TI
						INNER JOIN INSERTED
							ON TI.tPayroll = INSERTED.tPayroll		-- this must be non-null
					GROUP BY TI.tPayroll
					HAVING count(TI.tID) > 1
				) begin
				
				RAISERROR('<ValidationError field="tPayroll">
						This update would create a duplicate value in Payroll number.
						</ValidationError>', 16,1)
		end
	end
	
	if (UPDATE(tProvident) and common.Sysparam('TEACHER_UNIQUEPROVIDENT') = 'Y') begin
		IF Exists(
					Select TI.tProvident
					FROM TeacherIdentity TI
						INNER JOIN INSERTED
							ON TI.tProvident = INSERTED.tProvident		-- this must be non-null
					GROUP BY TI.tProvident
					HAVING count(TI.tID) > 1
				) begin
				
				RAISERROR('<ValidationError field="tProvident">
						This update would create a duplicate value in Provident number.
						</ValidationError>', 16,1)
		end
	end	

	if (UPDATE(tRegister) and common.Sysparam('TEACHER_UNIQUEREG') = 'Y') begin
		IF Exists(
					Select TI.tRegister
					FROM TeacherIdentity TI
						INNER JOIN INSERTED
							ON TI.tRegister = INSERTED.tRegister		-- this must be non-null
					GROUP BY TI.tRegister
					HAVING count(TI.tID) > 1
				) begin
				
				RAISERROR('<ValidationError field="tProvident">
						This update would create a duplicate value in Registration number.
						</ValidationError>', 16,1)
		end
	end	


end try
		

		
 /****************************************************************
Generic catch block
****************************************************************/
begin catch
	
    DECLARE @err int,
		@ErrorMessage NVARCHAR(4000), 
        @ErrorSeverity INT, 
        @ErrorState INT; 
	Select @err = @@error,
		 @ErrorMessage = ERROR_MESSAGE(),
		 @ErrorSeverity = ERROR_SEVERITY(),
		 @ErrorState = ERROR_STATE()


	rollback transaction
	select @errorMessage = @errorMessage + ' The transaction was rolled back.'

    RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 
end catch
END
GO
ALTER TABLE [dbo].[TeacherIdentity] ENABLE TRIGGER [TeacherIdentity_Validations]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'ID of the current photo for this teacher' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherIdentity', @level2type=N'COLUMN',@level2name=N'tPhoto'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Principal table representing a single teacher.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherIdentity'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherIdentity'
GO

