SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherNotes](
	[tchnID] [int] IDENTITY(1,1) NOT NULL,
	[tID] [int] NOT NULL,
	[tchnCreateDateTime] [datetime] NULL,
	[tchnCreateUser] [nvarchar](50) NULL,
	[tchnSubject] [nvarchar](250) NULL,
	[tchnNote] [ntext] NOT NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditContext] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pRowversion] [timestamp] NOT NULL,
 CONSTRAINT [PK_TeacherNotes] PRIMARY KEY CLUSTERED 
(
	[tchnID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[TeacherNotes] TO [pTeacherReadX] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherNotes] TO [pTeacherWriteX] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherNotes] TO [pTeacherWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherNotes] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[TeacherNotes] ADD  CONSTRAINT [DF_TeacherNotes_tchnCreateDateTime]  DEFAULT (getdate()) FOR [tchnCreateDateTime]
GO
ALTER TABLE [dbo].[TeacherNotes] ADD  CONSTRAINT [DF_TeacherNotes_tchnCreateUser]  DEFAULT (suser_sname()) FOR [tchnCreateUser]
GO
ALTER TABLE [dbo].[TeacherNotes]  WITH CHECK ADD  CONSTRAINT [FK_TeacherNotes_TeacherIdentity] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])
GO
ALTER TABLE [dbo].[TeacherNotes] CHECK CONSTRAINT [FK_TeacherNotes_TeacherIdentity]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Logical creation details - for information about the note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherNotes', @level2type=N'COLUMN',@level2name=N'tchnCreateDateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teacher notes are maintained through the Teacher Consultation form.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherNotes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherNotes'
GO

