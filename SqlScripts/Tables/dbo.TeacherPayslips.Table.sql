SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherPayslips](
	[tpsID] [int] IDENTITY(1,1) NOT NULL,
	[tpsPeriodStart] [smalldatetime] NOT NULL,
	[tpsPeriodEnd] [smalldatetime] NOT NULL,
	[tpsGross] [money] NULL,
	[tpsTitle] [nvarchar](70) NULL,
	[tpsSalaryPoint] [nvarchar](50) NULL,
	[tpsPosition] [nvarchar](20) NULL,
	[tpsPayPoint] [nvarchar](10) NULL,
	[tpsPayroll] [nvarchar](20) NOT NULL,
	[tpsNamePrefix] [nvarchar](20) NULL,
	[tpsGiven] [nvarchar](50) NULL,
	[tpsMiddleNames] [nvarchar](50) NULL,
	[tpsSurname] [nvarchar](50) NULL,
	[tpsNameSuffix] [nvarchar](20) NULL,
	[tpsShortName]  AS ([dbo].[TeacherName]((1),[tpsNamePrefix],[tpsGiven],[tpsMiddleNames],[tpsSurname],[tpsNameSuffix])),
	[tpsPaySlip] [ntext] NULL,
	[tpsXML] [xml] NULL,
 CONSTRAINT [PK_TeacherPayslips] PRIMARY KEY CLUSTERED 
(
	[tpsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[TeacherPayslips] TO [pTeacherReadX] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherPayslips] TO [pTeacherWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherPayslips] TO [public] AS [dbo]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TeacherPayslips] ON [dbo].[TeacherPayslips]
(
	[tpsPayroll] ASC,
	[tpsPeriodStart] ASC,
	[tpsPeriodEnd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_TeacherPayslips_PayPoint] ON [dbo].[TeacherPayslips]
(
	[tpsPayPoint] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_TeacherPayslips_Payroll] ON [dbo].[TeacherPayslips]
(
	[tpsPayroll] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_TeacherPayslips_PeriodStart2] ON [dbo].[TeacherPayslips]
(
	[tpsPeriodStart] ASC,
	[tpsPayroll] ASC
)
INCLUDE ( 	[tpsPeriodEnd],
	[tpsGross],
	[tpsPayPoint],
	[tpsSalaryPoint],
	[tpsPosition]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_TeacherPayslips_Position] ON [dbo].[TeacherPayslips]
(
	[tpsPosition] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TeacherPayslipsPeriodEnd] ON [dbo].[TeacherPayslips]
(
	[tpsPeriodEnd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
GO
CREATE PRIMARY XML INDEX [XML_IX_TeacherPayslips_tpsXML] ON [dbo].[TeacherPayslips]
(
	[tpsXML]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 14 7 2010
-- Description:	Update the XML on a payslip from its payslip text
--
-- =============================================
CREATE TRIGGER [dbo].[TeacherPayslips_RelatedData]
   ON  [dbo].[TeacherPayslips]
   AFTER INSERT, UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE TeacherPayslips
	SET tpsXML = pTeacherReadX.fnParsePayslip(TeacherPayslips.tpsPayslip )
	
	WHERE TeacherPayslips.tpsPayslip is not null
	AND TpsId = any (Select tpsID from INSERTED)


INSERT INTO TeacherPayslipItemTypes
		(payItemType)
		Select
		value
		FRom
		(
		SELECT DISTINCT i.value('./@pc', 'nvarchar(20)') value
		FROM TeacherPayslips
		CROSS APPLY
		TPSXML.nodes('Payslip/ThisPay/Item') as T(i)
		WHERE TpsId = any (Select tpsID from INSERTED)
		) Items
		WHERE Value not in (Select payItemType from TeacherPayslipItemTypes)
END
GO
ALTER TABLE [dbo].[TeacherPayslips] ENABLE TRIGGER [TeacherPayslips_RelatedData]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teacher Payslips includes the text of imported payslips, and data fields that are parsed from that text. The payroll number/pay period end is a unique key for payslips - payslips match to a teacher identity based on tPayroll.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherPayslips'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherPayslips'
GO

