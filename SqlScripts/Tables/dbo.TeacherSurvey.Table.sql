SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherSurvey](
	[tchsID] [int] IDENTITY(1,1) NOT NULL,
	[ssID] [int] NOT NULL,
	[tchSort] [int] NULL,
	[tchEmplNo] [nvarchar](50) NULL,
	[tchRegister] [nvarchar](20) NULL,
	[tchProvident] [nvarchar](50) NULL,
	[tchUnion] [nvarchar](50) NULL,
	[tchNamePrefix] [nvarchar](50) NULL,
	[tchFirstName] [nvarchar](50) NULL,
	[tchMiddleNames] [nvarchar](50) NULL,
	[tchFamilyName] [nvarchar](50) NULL,
	[tchNameSuffix] [nvarchar](20) NULL,
	[tchSalaryScale] [nvarchar](50) NULL,
	[tchSalary] [money] NULL,
	[tchDOB] [datetime] NULL,
	[tchGender] [nvarchar](1) NULL,
	[tchCitizenship] [nvarchar](50) NULL,
	[tchIsland] [nvarchar](2) NULL,
	[tchDistrict] [nvarchar](5) NULL,
	[tchSponsor] [nvarchar](50) NULL,
	[tchFTE] [float] NULL,
	[tchFullPart] [nvarchar](1) NULL,
	[tchYears] [int] NULL,
	[tchYearsSchool] [int] NULL,
	[tchTrained] [nvarchar](10) NULL,
	[tchEdQual] [nvarchar](50) NULL,
	[tchQual] [nvarchar](20) NULL,
	[tchSubjectTrained] [nvarchar](70) NULL,
	[tchCivilStatus] [nvarchar](50) NULL,
	[tchNumDep] [int] NULL,
	[tchHouse] [int] NULL,
	[tchSubjectMajor] [nvarchar](50) NULL,
	[tchSubjectMinor] [nvarchar](50) NULL,
	[tchSubjectMinor2] [nvarchar](50) NULL,
	[tchClass] [nvarchar](50) NULL,
	[tchClassMax] [nvarchar](50) NULL,
	[tchJoint] [bit] NULL,
	[tchComposite] [bit] NULL,
	[tchStatus] [nvarchar](5) NULL,
	[tchRole] [nvarchar](50) NULL,
	[tchTAM] [nvarchar](1) NULL,
	[tID] [int] NOT NULL,
	[tchECE] [nvarchar](50) NULL,
	[tchInserviceYear] [int] NULL,
	[tchInservice] [nvarchar](50) NULL,
	[tchSector] [nvarchar](3) NULL,
	[tchYearStarted] [int] NULL,
	[tchSpouseFirstName] [nvarchar](50) NULL,
	[tchSpouseFamilyName] [nvarchar](50) NULL,
	[tchSpouseOccupation] [nvarchar](50) NULL,
	[tchSpouseEmployer] [nvarchar](50) NULL,
	[tchSpousetID] [int] NULL,
	[tchLangMajor] [nvarchar](5) NULL,
	[tchLangMinor] [nvarchar](5) NULL,
	[tchsmisID] [int] NULL,
	[tchApptTerms] [nvarchar](10) NULL,
	[tchSalaryCurr] [nvarchar](3) NULL,
	[tchSalaryPeriod] [nvarchar](10) NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[tcheSource] [nvarchar](50) NULL,
	[tcheData] [xml] NULL,
	[tchSupport] [nvarchar](10) NULL,
	[tchNote] [nvarchar](200) NULL,
	[tchAdjusted] [nvarchar](10) NULL,
 CONSTRAINT [aaaaaTeacherSurvey1_PK] PRIMARY KEY NONCLUSTERED 
(
	[tchsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[TeacherSurvey] TO [pTeacherOps] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherSurvey] TO [pTeacherOps] AS [dbo]
GO
GRANT SELECT ON [dbo].[TeacherSurvey] TO [pTeacherOps] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherSurvey] TO [pTeacherOps] AS [dbo]
GO
GRANT SELECT ON [dbo].[TeacherSurvey] TO [pTeacherRead] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherSurvey] TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherSurvey] TO [pTeacherWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherSurvey] TO [public] AS [dbo]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_TeacherSurvey_SSID] ON [dbo].[TeacherSurvey]
(
	[ssID] ASC,
	[tID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_TeacherSurvey_TID] ON [dbo].[TeacherSurvey]
(
	[tID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSur__ssID__6ABAD62E]  DEFAULT ((0)) FOR [ssID]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSu__tchSo__6BAEFA67]  DEFAULT ((0)) FOR [tchSort]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSu__tchSa__6CA31EA0]  DEFAULT ((0)) FOR [tchSalary]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSu__tchFT__6D9742D9]  DEFAULT ((0)) FOR [tchFTE]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSu__tchYe__6E8B6712]  DEFAULT ((0)) FOR [tchYears]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSu__tchYe__6F7F8B4B]  DEFAULT ((0)) FOR [tchYearsSchool]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSu__tchNu__7073AF84]  DEFAULT ((0)) FOR [tchNumDep]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSu__tchJo__7167D3BD]  DEFAULT ((0)) FOR [tchJoint]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSu__tchCo__725BF7F6]  DEFAULT ((0)) FOR [tchComposite]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSurv__tID__73501C2F]  DEFAULT ((0)) FOR [tID]
GO
ALTER TABLE [dbo].[TeacherSurvey] ADD  CONSTRAINT [DF__TeacherSu__tchIn__74444068]  DEFAULT ((0)) FOR [tchInserviceYear]
GO
ALTER TABLE [dbo].[TeacherSurvey]  WITH CHECK ADD  CONSTRAINT [TeacherSurvey_FK00] FOREIGN KEY([ssID])
REFERENCES [dbo].[SchoolSurvey] ([ssID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TeacherSurvey] CHECK CONSTRAINT [TeacherSurvey_FK00]
GO
ALTER TABLE [dbo].[TeacherSurvey]  WITH CHECK ADD  CONSTRAINT [TeacherSurvey_FK01] FOREIGN KEY([tchStatus])
REFERENCES [dbo].[lkpTeacherStatus] ([statusCode])
GO
ALTER TABLE [dbo].[TeacherSurvey] CHECK CONSTRAINT [TeacherSurvey_FK01]
GO
ALTER TABLE [dbo].[TeacherSurvey]  WITH CHECK ADD  CONSTRAINT [TeacherSurvey_FK02] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])
GO
ALTER TABLE [dbo].[TeacherSurvey] CHECK CONSTRAINT [TeacherSurvey_FK02]
GO
ALTER TABLE [dbo].[TeacherSurvey]  WITH CHECK ADD  CONSTRAINT [TeacherSurvey_FK03] FOREIGN KEY([tchRole])
REFERENCES [dbo].[lkpTeacherRole] ([codeCode])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[TeacherSurvey] CHECK CONSTRAINT [TeacherSurvey_FK03]
GO
ALTER TABLE [dbo].[TeacherSurvey]  WITH CHECK ADD  CONSTRAINT [TeacherSurvey_FK04] FOREIGN KEY([tchTrained])
REFERENCES [dbo].[lkpTeacherTraining] ([codeCode])
GO
ALTER TABLE [dbo].[TeacherSurvey] CHECK CONSTRAINT [TeacherSurvey_FK04]
GO
ALTER TABLE [dbo].[TeacherSurvey]  WITH CHECK ADD  CONSTRAINT [TeacherSurvey_FK05] FOREIGN KEY([tchSector])
REFERENCES [dbo].[EducationSectors] ([secCode])
GO
ALTER TABLE [dbo].[TeacherSurvey] CHECK CONSTRAINT [TeacherSurvey_FK05]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[TeacherSurvey_AuditDelete] 
   ON  [dbo].[TeacherSurvey] 
   
   WITH EXECUTE as 'pineapples'
   AFTER DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from DELETED
	
	
	exec @AuditID =  audit.LogAudit 'TeacherSurvey', null, 'D', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKey, arowName)
	SELECT DISTINCT @AuditID
	, tchsID
	,  isnull(tchFamilyName,'') 

 
	FROM DELETED	



END
GO
ALTER TABLE [dbo].[TeacherSurvey] ENABLE TRIGGER [TeacherSurvey_AuditDelete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 17 05 2010
-- Description:	
-- =============================================
CREATE TRIGGER [dbo].[TeacherSurvey_AuditInsert] 
   ON  [dbo].[TeacherSurvey] 
    
   WITH EXECUTE as 'pineapples'

   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @NumChanges int
	declare @AuditID int
	
	Select @NumChanges = count(*) from INSERTED
	
	
	exec @AuditID =  audit.LogAudit 'TeacherSurvey', null, 'I', @Numchanges
	
	INSERT INTO audit.AuditRow
	(auditID, arowKey)
	SELECT DISTINCT @AuditID
	, tchsID
	FROM INSERTED	

-- update the created and edited fields
	UPDATE TeacherSurvey
		SET pCreateDateTime = aLog.auditDateTime
			, pCreateUser = alog.auditUser

			, pEditDateTime = aLog.auditDateTime
			, pEditUser = alog.auditUser
	FROM INSERTED I
		CROSS JOIN Audit.auditLog alog
	WHERE TeacherSurvey.tchsID = I.tchsID
	AND aLog.auditID = @auditID


END
GO
ALTER TABLE [dbo].[TeacherSurvey] ENABLE TRIGGER [TeacherSurvey_AuditInsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2009-0912
-- Description:	Logging trigger
-- =============================================
CREATE TRIGGER [dbo].[TeacherSurvey_AuditUpdate] 
   ON  [dbo].[TeacherSurvey] 

	-- pineapples has access to write to TeacherIdentityAudit
	WITH EXECUTE AS 'pineapples'
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @AuditID int



		declare @NumChanges int
		
		Select @NumChanges = count(*) from INSERTED
		
		
		exec @AuditID =  audit.LogAudit 'TeacherSurvey', null, 'U', @Numchanges
		
		INSERT INTO audit.AuditRow
		(auditID, arowKey)
		SELECT DISTINCT @AuditID
		, tchsID
		FROM INSERTED	



		
		--INSERT INTO audit.AuditColumn
		--(arowID, acolName, acolBefore, acolAfter)	
		
		--SELECT arowID
		--, ColumnName
		--, DValue
		--, IValue
		--FROM audit.AuditRow Row
		--INNER JOIN 
		--(
		--	SELECT 
		--	I.tID
		--	, case num
		--		when 1 then 'tID'
		--		when 2 then 'tRegister'
		--		when 3 then 'tPayroll'
		--		when 4 then 'tProvident'
		--		when 5 then 'tDOB'
		--		when 6 then 'tDOBEst'
		--		when 7 then 'tSex'
		--		when 8 then 'tNamePrefix'
		--		when 9 then 'tGiven'
		--		when 10 then 'tMiddleNames'
		--		when 11 then 'tSurname'
		--		when 12 then 'tNameSuffix'
		--		when 13 then 'tGivenSoundex'
		--		when 14 then 'tSurnameSoundex'
		--		when 15 then 'tSrcID'
		--		when 16 then 'tSrc'
		--		when 17 then 'tComment'
		--		when 18 then 'tYearStarted'
		--		when 19 then 'tYearFinished'
		--		when 20 then 'tDatePSAppointed'
		--		when 21 then 'tDatePSClosed'
		--		when 22 then 'tCloseReason'
		--		when 23 then 'tPayErr'
		--		when 24 then 'tPayErrUser'
		--		when 25 then 'tPayErrDate'
		--		when 26 then 'tEstErr'
		--		when 27 then 'tEstErrUser'
		--		when 28 then 'tEstErrDate'
		--		when 29 then 'tSalaryPoint'
		--		when 30 then 'tSalaryPointDate'
		--		when 31 then 'tSalaryPointNextIncr'
		--		when 32 then 'tLangMajor'
		--		when 33 then 'tLangMinor'
		--		when 34 then 'tpayptCode'
		--		when 35 then 'tImage'
		--		when 36 then 'tDateRegister'
		--		when 37 then 'tDateRegisterEnd'
		--		when 38 then 'tRegisterEndReason'
		--	end ColumnName
		--	, case num
		--		when 1 then cast(D.tID as nvarchar(50))
		--		when 2 then cast(D.tRegister as nvarchar(50))
		--		when 3 then cast(D.tPayroll as nvarchar(50))
		--		when 4 then cast(D.tProvident as nvarchar(50))
		--		when 5 then cast(D.tDOB as nvarchar(50))
		--		when 6 then cast(D.tDOBEst as nvarchar(50))
		--		when 7 then cast(D.tSex as nvarchar(50))
		--		when 8 then cast(D.tNamePrefix as nvarchar(50))
		--		when 9 then cast(D.tGiven as nvarchar(50))
		--		when 10 then cast(D.tMiddleNames as nvarchar(50))
		--		when 11 then cast(D.tSurname as nvarchar(50))
		--		when 12 then cast(D.tNameSuffix as nvarchar(50))
		--		when 15 then cast(D.tSrcID as nvarchar(50))
		--		when 16 then cast(D.tSrc as nvarchar(50))
		--		--when 17 then cast(D.tComment as nvarchar(50))
		--		when 18 then cast(D.tYearStarted as nvarchar(50))
		--		when 19 then cast(D.tYearFinished as nvarchar(50))
		--		when 20 then cast(D.tDatePSAppointed as nvarchar(50))
		--		when 21 then cast(D.tDatePSClosed as nvarchar(50))
		--		when 22 then cast(D.tCloseReason as nvarchar(50))
		--		when 23 then cast(D.tPayErr as nvarchar(50))
		--		when 24 then cast(D.tPayErrUser as nvarchar(50))
		--		when 25 then cast(D.tPayErrDate as nvarchar(50))
		--		when 26 then cast(D.tEstErr as nvarchar(50))
		--		when 27 then cast(D.tEstErrUser as nvarchar(50))
		--		when 28 then cast(D.tEstErrDate as nvarchar(50))
		--		when 29 then cast(D.tSalaryPoint as nvarchar(50))
		--		when 30 then cast(D.tSalaryPointDate as nvarchar(50))
		--		when 31 then cast(D.tSalaryPointNextIncr as nvarchar(50))
		--		when 32 then cast(D.tLangMajor as nvarchar(50))
		--		when 33 then cast(D.tLangMinor as nvarchar(50))
		--		when 34 then cast(D.tpayptCode as nvarchar(50))
		--		when 35 then cast(D.tImage as nvarchar(50))
		--		when 36 then cast(D.tDateRegister as nvarchar(50))
		--		when 37 then cast(D.tDateRegisterEnd as nvarchar(50))
		--		when 38 then cast(D.tRegisterEndReason as nvarchar(50))
		--	end dValue
		--	, case num
		--		when 1 then cast(I.tID as nvarchar(50))
		--		when 2 then cast(I.tRegister as nvarchar(50))
		--		when 3 then cast(I.tPayroll as nvarchar(50))
		--		when 4 then cast(I.tProvident as nvarchar(50))
		--		when 5 then cast(I.tDOB as nvarchar(50))
		--		when 6 then cast(I.tDOBEst as nvarchar(50))
		--		when 7 then cast(I.tSex as nvarchar(50))
		--		when 8 then cast(I.tNamePrefix as nvarchar(50))
		--		when 9 then cast(I.tGiven as nvarchar(50))
		--		when 10 then cast(I.tMiddleNames as nvarchar(50))
		--		when 11 then cast(I.tSurname as nvarchar(50))
		--		when 12 then cast(I.tNameSuffix as nvarchar(50))
		--		when 15 then cast(I.tSrcID as nvarchar(50))
		--		when 16 then cast(I.tSrc as nvarchar(50))
		--		--when 17 then cast(I.tComment as nvarchar(50))
		--		when 18 then cast(I.tYearStarted as nvarchar(50))
		--		when 19 then cast(I.tYearFinished as nvarchar(50))
		--		when 20 then cast(I.tDatePSAppointed as nvarchar(50))
		--		when 21 then cast(I.tDatePSClosed as nvarchar(50))
		--		when 22 then cast(I.tCloseReason as nvarchar(50))
		--		when 23 then cast(I.tPayErr as nvarchar(50))
		--		when 24 then cast(I.tPayErrUser as nvarchar(50))
		--		when 25 then cast(I.tPayErrDate as nvarchar(50))
		--		when 26 then cast(I.tEstErr as nvarchar(50))
		--		when 27 then cast(I.tEstErrUser as nvarchar(50))
		--		when 28 then cast(I.tEstErrDate as nvarchar(50))
		--		when 29 then cast(I.tSalaryPoint as nvarchar(50))
		--		when 30 then cast(I.tSalaryPointDate as nvarchar(50))
		--		when 31 then cast(I.tSalaryPointNextIncr as nvarchar(50))
		--		when 32 then cast(I.tLangMajor as nvarchar(50))
		--		when 33 then cast(I.tLangMinor as nvarchar(50))
		--		when 34 then cast(I.tpayptCode as nvarchar(50))
		--		when 35 then cast(I.tImage as nvarchar(50))
		--		when 36 then cast(I.tDateRegister as nvarchar(50))
		--		when 37 then cast(I.tDateRegisterEnd as nvarchar(50))
		--		when 38 then cast(I.tRegisterEndReason as nvarchar(50))
		--	end IValue

		--FROM INSERTED I 
		--INNER JOIN DELETED D
		--	ON I.tID = D.tID
		--CROSS JOIN metaNumbers
		--WHERE num between 1 and 38
		--) Edits
		--ON Edits.tID = Row.arowKey
		--WHERE Row.auditID = @AuditID
		--AND isnull(DVAlue,'') <> isnull(IValue,'')

		
		-- update the last edited field	
		UPDATE TeacherSurvey
			SET pEditDateTime = aLog.auditDateTime
				, pEditUser = alog.auditUser
		FROM INSERTED I
			CROSS JOIN Audit.auditLog alog
		WHERE TeacherSurvey.tchsID = I.tchsID
		AND aLog.auditID = @auditID
	
END
GO
ALTER TABLE [dbo].[TeacherSurvey] ENABLE TRIGGER [TeacherSurvey_AuditUpdate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 12 05 2010
-- Description:	Enforce unique Teacher survey for a teacher in a survey year
-- =============================================
CREATE TRIGGER [dbo].[TeacherSurvey_Validations] 
   ON  [dbo].[TeacherSurvey] 
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	begin try    
		declare @Count int
		declare @FirstYear int
	    
	    Select @Count = count(*)
	    FROM 
			( SELECT tID, svyYear
				FROM INSERTED
					INNER JOIN SchoolSurvey SS
					ON INSERTED.ssID = SS.ssID
				GROUP BY tID, svyYEar
				HAVING count(*) > 1
			) SUB
			
	 
	  
		SELECT @FirstYear = min(I.svyYear)
		, @Count = count(*)
	    
		from (Select svyYear, tID , tchsID
		from INSERTED 
		INNER JOIN SchoolSurvey SS
		ON INSERTED.ssID = SS.ssID
		) I
		INNER JOIN
		( Select svyYear, tID, tchsID
		FROM TeacherSurvey TS
		INNER JOIN SchoolSurvey SS
		ON TS.ssID = SS.ssID
	    
		) T
		ON I.svyYear = T.svyYear
		AND I.tID = T.tID
		WHERE I.tchsId <> T.tchsID
	    
		IF @Count > 0 begin
			declare @A1 nvarchar(4)
			SELECT @A1 = cast(@FirstYEar as nvarchar(4))
			RAISERROR('<ValidationError>
					  There is already a survey for this teacher in survey year %s
					 </ValidationError>'
						,16,0,  @A1 ) 
		end
	end try
	/****************************************************************
	Generic catch block for trigger
	****************************************************************/
	begin catch
		
		DECLARE @err int,
			@ErrorMessage NVARCHAR(4000), 
			@ErrorSeverity INT, 
			@ErrorState INT; 
		Select @err = @@error,
			 @ErrorMessage = ERROR_MESSAGE(),
			 @ErrorSeverity = ERROR_SEVERITY(),
			 @ErrorState = ERROR_STATE()


		rollback transaction
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState); 

	end catch
	

END
GO
ALTER TABLE [dbo].[TeacherSurvey] ENABLE TRIGGER [TeacherSurvey_Validations]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'terms/nature of appointment e.g. Attached Relief/Probabtioner etc . This is so that roles can be rationalised without losing any info' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherSurvey', @level2type=N'COLUMN',@level2name=N'tchApptTerms'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Used to distinguish between ''academic'' staff - those directly involved in teaching or ''pedagogical'' activites, and ''support'' staff - cleaners, gardeners, receptionist, etc.  Leave Null to indicate teaching staff - support staff can be differentiated if required into categories' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherSurvey', @level2type=N'COLUMN',@level2name=N'tchSupport'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Note about status of this record, particularly if manually adjusted after import process' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherSurvey', @level2type=N'COLUMN',@level2name=N'tchNote'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contents of the record - usually populted from an import process, have been manually fixed/adjusted. See contents of Note' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherSurvey', @level2type=N'COLUMN',@level2name=N'tchAdjusted'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Teacher Survey is the data entered for a teacher as part of the school survey. It therefore has two critical foreign keys - ssID , linking the survey to a School Survey, and tID, linking the TeacherSurvey to a TeacherIdentity.
Teacher survey maintains an historical record of some data found on TeacherIdentity - esp Name. The values from the most recent TeacherSurvey are written back to TeacherIdentity.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherSurvey'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessDB', @value=N'Teacher survey forms are unbound
Data is read using
IDataSourceTeacher.TeacherSurveyRead
which access
pTeacherRead.TeacherSurveyRead
Form us saved using
IDataSourceTeacher.TeacherSurveyWrite
SQL implmentation defaults to originak Jet implmentation: DAO update using linked tables.

Update via Pineapples@School uses pTeacherWrite.UpdateTeachers to process XML.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherSurvey'
GO
EXEC sys.sp_addextendedproperty @name=N'pDataAccessUI', @value=N'to support changes over time, so that the display form represents the survey form at the time the survey was collected, the survey form to use for TeacherSurvey is stored on SurveyYearSchoolTypes. New implementations should all use frmTeacherSurvey_2009.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherSurvey'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherSurvey'
GO

