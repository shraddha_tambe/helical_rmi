SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherTraining](
	[trID] [int] IDENTITY(1,1) NOT NULL,
	[tID] [int] NULL,
	[trYear] [int] NULL,
	[trPre] [nvarchar](4) NULL,
	[trComplete] [bit] NULL,
	[trQual] [nvarchar](20) NULL,
	[trInstitution] [nvarchar](50) NULL,
	[trDuration] [int] NULL,
	[trDurationUnit] [nvarchar](10) NULL,
	[trMajor] [nvarchar](50) NULL,
	[trMinor] [nvarchar](50) NULL,
	[trProgress] [nvarchar](50) NULL,
	[trProgressPerc] [int] NULL,
	[trComment] [nvarchar](50) NULL,
	[trEffectiveDate] [datetime] NULL,
	[trExpirationDate] [datetime] NULL,
	[pCreateDateTime] [datetime] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pRowversion] [timestamp] NOT NULL,
 CONSTRAINT [aaaaaTeacherTraining1_PK] PRIMARY KEY NONCLUSTERED 
(
	[trID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT SELECT ON [dbo].[TeacherTraining] TO [pTeacherRead] AS [dbo]
GO
GRANT INSERT ON [dbo].[TeacherTraining] TO [pTeacherWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[TeacherTraining] TO [pTeacherWrite] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TeacherTraining] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[TeacherTraining] ADD  CONSTRAINT [DF__TeacherTrai__tID__45BE5BA9]  DEFAULT ((0)) FOR [tID]
GO
ALTER TABLE [dbo].[TeacherTraining] ADD  CONSTRAINT [DF__TeacherTr__trCom__46B27FE2]  DEFAULT ((1)) FOR [trComplete]
GO
ALTER TABLE [dbo].[TeacherTraining] ADD  CONSTRAINT [DF__TeacherTr__trDur__47A6A41B]  DEFAULT ((0)) FOR [trDuration]
GO
ALTER TABLE [dbo].[TeacherTraining] ADD  CONSTRAINT [DF__TeacherTr__trPro__489AC854]  DEFAULT ((0)) FOR [trProgressPerc]
GO
ALTER TABLE [dbo].[TeacherTraining]  WITH CHECK ADD  CONSTRAINT [FK_TeacherTraining_TeacherIdentity] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TeacherTraining] CHECK CONSTRAINT [FK_TeacherTraining_TeacherIdentity]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Detailed records of teacher qualifications and training.  TeacherIdentity tID is the foreign key.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherTraining'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TeacherTraining'
GO

