SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpBuildingSizeClass](
	[bszCode] [nvarchar](10) NOT NULL,
	[bszDescription] [nvarchar](50) NOT NULL,
	[bszDescriptionL1] [nvarchar](50) NULL,
	[bszDescriptionL2] [nvarchar](50) NULL,
	[bszMin] [int] NOT NULL,
	[bszMax] [int] NOT NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [PK_lkpBuildingSizeClass] PRIMARY KEY CLUSTERED 
(
	[bszCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpBuildingSizeClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpBuildingSizeClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpBuildingSizeClass] TO [pInfrastructureAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpBuildingSizeClass] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpBuildingSizeClass] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Min floor area (m2) for this size class' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBuildingSizeClass', @level2type=N'COLUMN',@level2name=N'bszMin'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Partition of building floor area. Used to classify buildings, and as part of building valuation calculation, where together with BuildingValuationClass, it defines a rate per sq metre.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBuildingSizeClass'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpBuildingSizeClass'
GO

