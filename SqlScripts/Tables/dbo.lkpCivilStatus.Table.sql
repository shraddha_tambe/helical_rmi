SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpCivilStatus](
	[codeCode] [nvarchar](50) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSort] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpCivilStatus1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpCivilStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpCivilStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpCivilStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpCivilStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpCivilStatus] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpCivilStatus] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpCivilStatus] ADD  CONSTRAINT [DF__lkpCivilS__codeS__7B5B524B]  DEFAULT ((0)) FOR [codeSort]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maritial Status. Foregin key on Teacher Survey.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpCivilStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpCivilStatus'
GO

