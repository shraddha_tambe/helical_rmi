SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpConditionCodes](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[codeCodeN] [smallint] NULL,
	[codeInitial] [nvarchar](3) NULL,
	[codeInitialL1] [nvarchar](3) NULL,
	[codeInitialL2] [nvarchar](3) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpConditionCodes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpConditionCodes] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpConditionCodes] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpConditionCodes] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpConditionCodes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpConditionCodes] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Codes representing the conditions of buildings. System defined.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpConditionCodes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpConditionCodes'
GO

