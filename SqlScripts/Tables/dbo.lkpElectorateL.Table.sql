SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpElectorateL](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSort] [int] NULL,
	[dID] [nvarchar](5) NULL,
	[censusCode] [nvarchar](4) NULL,
	[codeStart] [int] NULL,
	[codeEnd] [int] NULL,
	[codeOld] [nvarchar](50) NULL,
	[elgisID] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpElectorateL1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpElectorateL] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpElectorateL] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpElectorateL] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpElectorateL] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpElectorateL] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpElectorateL] ADD  CONSTRAINT [DF__lkpElecto__codeS__67A95F59]  DEFAULT ((0)) FOR [codeSort]
GO
ALTER TABLE [dbo].[lkpElectorateL] ADD  CONSTRAINT [DF__lkpElecto__codeS__689D8392]  DEFAULT ((0)) FOR [codeStart]
GO
ALTER TABLE [dbo].[lkpElectorateL] ADD  CONSTRAINT [DF__lkpElecto__codeE__6991A7CB]  DEFAULT ((0)) FOR [codeEnd]
GO
ALTER TABLE [dbo].[lkpElectorateL]  WITH CHECK ADD  CONSTRAINT [lkpElectorateL_FK00] FOREIGN KEY([dID])
REFERENCES [dbo].[Districts] ([dID])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[lkpElectorateL] CHECK CONSTRAINT [lkpElectorateL_FK00]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Local Electorates. A foreign key on schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpElectorateL'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpElectorateL'
GO

