SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpElectorateN](
	[codeCode] [nvarchar](10) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeSort] [int] NULL,
	[engisID] [int] NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpElectorateN1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpElectorateN] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpElectorateN] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpElectorateN] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpElectorateN] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpElectorateN] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpElectorateN] ADD  CONSTRAINT [DF__lkpElecto__codeS__078C1F06]  DEFAULT ((0)) FOR [codeSort]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'National Electorate. A foreign key on schools. Also on SchoolSurvey, since boundaries may not be stable over time.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpElectorateN'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Schools' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpElectorateN'
GO

