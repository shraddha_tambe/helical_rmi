SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpExamTypes](
	[exCode] [nvarchar](10) NOT NULL,
	[exName] [nvarchar](50) NULL,
	[exLevel] [nvarchar](10) NULL,
	[exFirstOffered] [int] NULL,
	[exRegion] [bit] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpExamTypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[exCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpExamTypes] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpExamTypes] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpExamTypes] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpExamTypes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpExamTypes] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpExamTypes] ADD  CONSTRAINT [DF__lkpExamTy__exFir__4B422AD5]  DEFAULT ((0)) FOR [exFirstOffered]
GO
ALTER TABLE [dbo].[lkpExamTypes] ADD  CONSTRAINT [DF__lkpExamTy__exReg__4C364F0E]  DEFAULT ((0)) FOR [exRegion]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Type of Exams. A foreign key on Exams.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpExamTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Exams' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpExamTypes'
GO

