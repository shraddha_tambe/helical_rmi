SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpTeacherRole](
	[codeCode] [nvarchar](50) NOT NULL,
	[codeDescription] [nvarchar](50) NULL,
	[codeDescriptionL1] [nvarchar](50) NULL,
	[codeDescriptionL2] [nvarchar](50) NULL,
	[codeSort] [int] NULL,
	[trsalLevelMin] [nvarchar](5) NULL,
	[trsalLevelMax] [nvarchar](5) NULL,
	[trSalaryPointMin] [nvarchar](10) NULL,
	[trSalaryPointMax] [nvarchar](10) NULL,
	[trSalaryPointMedian] [nvarchar](10) NULL,
	[secCode] [nvarchar](3) NULL,
	[trReportsTo] [nvarchar](50) NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpTeacherRole1_PK] PRIMARY KEY NONCLUSTERED 
(
	[codeCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpTeacherRole] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpTeacherRole] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpTeacherRole] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpTeacherRole] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpTeacherRole] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpTeacherRole] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[lkpTeacherRole] ADD  CONSTRAINT [DF__lkpTeache__codeS__61316BF4]  DEFAULT ((0)) FOR [codeSort]
GO
ALTER TABLE [dbo].[lkpTeacherRole]  WITH CHECK ADD  CONSTRAINT [FK_lkpTeacherRole_EducationSectors] FOREIGN KEY([secCode])
REFERENCES [dbo].[EducationSectors] ([secCode])
GO
ALTER TABLE [dbo].[lkpTeacherRole] CHECK CONSTRAINT [FK_lkpTeacherRole_EducationSectors]
GO
ALTER TABLE [dbo].[lkpTeacherRole]  WITH CHECK ADD  CONSTRAINT [lkpTeacherRole_FK00] FOREIGN KEY([trsalLevelMin])
REFERENCES [dbo].[lkpSalaryLevel] ([salLevel])
GO
ALTER TABLE [dbo].[lkpTeacherRole] CHECK CONSTRAINT [lkpTeacherRole_FK00]
GO
ALTER TABLE [dbo].[lkpTeacherRole]  WITH CHECK ADD  CONSTRAINT [lkpTeacherRole_FK01] FOREIGN KEY([trsalLevelMax])
REFERENCES [dbo].[lkpSalaryLevel] ([salLevel])
GO
ALTER TABLE [dbo].[lkpTeacherRole] CHECK CONSTRAINT [lkpTeacherRole_FK01]
GO
ALTER TABLE [dbo].[lkpTeacherRole]  WITH CHECK ADD  CONSTRAINT [lkpTeacherRole_FK02] FOREIGN KEY([trSalaryPointMin])
REFERENCES [dbo].[lkpSalaryPoints] ([spCode])
GO
ALTER TABLE [dbo].[lkpTeacherRole] CHECK CONSTRAINT [lkpTeacherRole_FK02]
GO
ALTER TABLE [dbo].[lkpTeacherRole]  WITH CHECK ADD  CONSTRAINT [lkpTeacherRole_FK03] FOREIGN KEY([trSalaryPointMax])
REFERENCES [dbo].[lkpSalaryPoints] ([spCode])
GO
ALTER TABLE [dbo].[lkpTeacherRole] CHECK CONSTRAINT [lkpTeacherRole_FK03]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Job roles of teachers. Role is collected on the Teacher Survey. Roles are further divided into RoleGrades. Appointments and salary and Establishment Plan are defined at the RoleGRade level.

The Role is linked to a Education Secgtor - so the teachers role determines their sector for reporting purposes.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpTeacherRole'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Teachers' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpTeacherRole'
GO

