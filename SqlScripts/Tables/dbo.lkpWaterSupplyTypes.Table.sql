SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[lkpWaterSupplyTypes](
	[wsType] [nvarchar](50) NOT NULL,
	[wsCleanSafe] [smallint] NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
	[pRowversion] [timestamp] NULL,
 CONSTRAINT [aaaaalkpWaterSupplyTypes1_PK] PRIMARY KEY NONCLUSTERED 
(
	[wsType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[lkpWaterSupplyTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[lkpWaterSupplyTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpWaterSupplyTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[lkpWaterSupplyTypes] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[lkpWaterSupplyTypes] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[lkpWaterSupplyTypes] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Water supply types. Although this duplicates the resource Defs for water supply, it is required currently to hold the definition of Clean and Safe. Ie the Condition Level at which we consider this type of water clean and safe.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpWaterSupplyTypes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Infrastructure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'lkpWaterSupplyTypes'
GO

