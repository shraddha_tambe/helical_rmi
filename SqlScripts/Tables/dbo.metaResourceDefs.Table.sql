SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metaResourceDefs](
	[mresID] [int] IDENTITY(1,1) NOT NULL,
	[mresCat] [nvarchar](50) NULL,
	[mresSeq] [int] NULL,
	[mresName] [nvarchar](50) NULL,
	[mresNameL1] [nvarchar](50) NULL,
	[mresNameL2] [nvarchar](50) NULL,
	[mresPromptAdq] [bit] NULL,
	[mresPromptAvail] [bit] NULL,
	[mresPromptNum] [bit] NULL,
	[mresPromptCondition] [bit] NULL,
	[mresPromptQty] [bit] NULL,
	[mresBuildingType] [nvarchar](10) NULL,
	[mresBuildingSubType] [nvarchar](10) NULL,
	[mresPromptNote] [bit] NOT NULL,
 CONSTRAINT [aaaaametaResourceDefs1_PK] PRIMARY KEY NONCLUSTERED 
(
	[mresID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[metaResourceDefs] TO [pSchoolAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[metaResourceDefs] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaResourceDefs] TO [pSchoolAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[metaResourceDefs] TO [pSchoolAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[metaResourceDefs] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[metaResourceDefs] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[metaResourceDefs] ADD  CONSTRAINT [DF__metaResou__mresS__03F0984C]  DEFAULT ((0)) FOR [mresSeq]
GO
ALTER TABLE [dbo].[metaResourceDefs] ADD  CONSTRAINT [DF__metaResou__mresP__04E4BC85]  DEFAULT ((0)) FOR [mresPromptAdq]
GO
ALTER TABLE [dbo].[metaResourceDefs] ADD  CONSTRAINT [DF__metaResou__mresP__05D8E0BE]  DEFAULT ((0)) FOR [mresPromptAvail]
GO
ALTER TABLE [dbo].[metaResourceDefs] ADD  CONSTRAINT [DF__metaResou__mresP__06CD04F7]  DEFAULT ((0)) FOR [mresPromptNum]
GO
ALTER TABLE [dbo].[metaResourceDefs] ADD  CONSTRAINT [DF__metaResou__mresP__07C12930]  DEFAULT ((0)) FOR [mresPromptCondition]
GO
ALTER TABLE [dbo].[metaResourceDefs] ADD  CONSTRAINT [DF_metaResourceDefs_mresPromptQty]  DEFAULT ((0)) FOR [mresPromptQty]
GO
ALTER TABLE [dbo].[metaResourceDefs] ADD  CONSTRAINT [DF_metaResourceDefs_mresPromptNote]  DEFAULT ((0)) FOR [mresPromptNote]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'These fields are just to allow the translate of teacher housing data from resources to buildings format' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaResourceDefs', @level2type=N'COLUMN',@level2name=N'mresBuildingType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Definitions of Resources. Resources are grouped by Category. The category and Resource Code both appear on the Resources table.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaResourceDefs'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'metaData' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'metaResourceDefs'
GO

