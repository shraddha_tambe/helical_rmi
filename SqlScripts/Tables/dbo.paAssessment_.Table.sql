SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paAssessment_](
	[paID] [int] IDENTITY(1,1) NOT NULL,
	[tID] [int] NOT NULL,
	[paSchNo] [nvarchar](50) NOT NULL,
	[paDate] [date] NULL,
	[paConductedBy] [nvarchar](100) NULL,
	[paYear]  AS (datepart(year,[paDate])) PERSISTED,
	[pafrmCode] [nvarchar](10) NOT NULL,
	[pRowversion] [timestamp] NOT NULL,
	[pCreateUser] [nvarchar](50) NULL,
	[pCreateDateTime] [datetime] NULL,
	[pEditUser] [nvarchar](50) NULL,
	[pEditDateTime] [datetime] NULL,
 CONSTRAINT [PK_paAssessment] PRIMARY KEY CLUSTERED 
(
	[paID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_paAssessment_School] ON [dbo].[paAssessment_]
(
	[paSchNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_paAssessment_Teacher] ON [dbo].[paAssessment_]
(
	[tID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[paAssessment_]  WITH CHECK ADD  CONSTRAINT [FK_paAssessment__paFramework_] FOREIGN KEY([pafrmCode])
REFERENCES [dbo].[paFrameworks_] ([pafrmCode])
GO
ALTER TABLE [dbo].[paAssessment_] CHECK CONSTRAINT [FK_paAssessment__paFramework_]
GO
ALTER TABLE [dbo].[paAssessment_]  WITH CHECK ADD  CONSTRAINT [FK_paAssessment_School] FOREIGN KEY([paSchNo])
REFERENCES [dbo].[Schools] ([schNo])
GO
ALTER TABLE [dbo].[paAssessment_] CHECK CONSTRAINT [FK_paAssessment_School]
GO
ALTER TABLE [dbo].[paAssessment_]  WITH CHECK ADD  CONSTRAINT [FK_paAssessment_Teacher] FOREIGN KEY([tID])
REFERENCES [dbo].[TeacherIdentity] ([tID])
GO
ALTER TABLE [dbo].[paAssessment_] CHECK CONSTRAINT [FK_paAssessment_Teacher]
GO

