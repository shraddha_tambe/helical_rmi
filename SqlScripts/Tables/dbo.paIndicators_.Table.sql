SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[paIndicators_](
	[paindID] [int] IDENTITY(1,1) NOT NULL,
	[paindCode] [nvarchar](10) NOT NULL,
	[paindText] [nvarchar](250) NULL,
	[paelmID] [int] NOT NULL,
 CONSTRAINT [PK_paIndicators_Element] PRIMARY KEY CLUSTERED 
(
	[paelmID] ASC,
	[paindCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[paIndicators_]  WITH CHECK ADD  CONSTRAINT [FK_paIndicators__paElements_] FOREIGN KEY([paelmID])
REFERENCES [dbo].[paElements_] ([paelmID])
GO
ALTER TABLE [dbo].[paIndicators_] CHECK CONSTRAINT [FK_paIndicators__paElements_]
GO

