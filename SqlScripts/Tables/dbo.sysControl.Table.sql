SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sysControl](
	[bkLastDate] [datetime] NULL,
	[compactLastDate] [datetime] NULL,
	[bkPeriod] [nvarchar](1) NULL,
	[bkDay] [int] NULL,
	[compactPeriod] [nvarchar](1) NULL,
	[compactDay] [int] NULL
) ON [PRIMARY]
GO
GRANT DELETE ON [dbo].[sysControl] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[sysControl] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[sysControl] TO [pAdminWrite] AS [dbo]
GO
GRANT SELECT ON [dbo].[sysControl] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[sysControl] TO [public] AS [dbo]
GO
ALTER TABLE [dbo].[sysControl] ADD  CONSTRAINT [DF__sysContro__bkDay__081890F0]  DEFAULT ((1)) FOR [bkDay]
GO
ALTER TABLE [dbo].[sysControl] ADD  CONSTRAINT [DF__sysContro__compa__090CB529]  DEFAULT ((1)) FOR [compactDay]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Under JET versions, stored backup and compact parameters. Still does in Scholar.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sysControl'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'Utility' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'sysControl'
GO

