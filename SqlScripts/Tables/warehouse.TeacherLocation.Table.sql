SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[TeacherLocation](
	[TID] [int] NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[GenderCode] [nvarchar](1) NULL,
	[BirthYear] [int] NULL,
	[AgeGroup] [varchar](5) NULL,
	[ApptSchNo] [nvarchar](50) NULL,
	[ApptRole] [nvarchar](50) NULL,
	[ApptYear] [int] NULL,
	[ApptSchEstimate] [int] NULL,
	[SurveySchNo] [nvarchar](50) NULL,
	[SurveyRole] [nvarchar](50) NULL,
	[SurveyDataYear] [int] NULL,
	[Estimate] [int] NULL,
	[SurveySector] [nvarchar](3) NULL,
	[ISCEDSubClass] [nvarchar](10) NULL,
	[SurveySupport] [nvarchar](10) NULL,
	[SchNo] [nvarchar](50) NULL,
	[Source] [varchar](3) NULL,
	[Role] [nvarchar](50) NULL,
	[Sector] [nvarchar](3) NULL,
	[Certified] [int] NULL,
	[Qualified] [int] NULL,
	[XtraSurvey] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'warehouse.TeacherLocation sits on the transition between 

-- teacher reporting derived from the SchoolSurvey; and
-- teacher reporting derived from Appointments, and teacher training records.

The tables Establishment and Teacher Appointments define the strcuture of the teaching establishment - ie the colleciton of teaching positions, and the teachers that are appointed to those positions. 

This is where we expect the teachers to be. 

TeacherSurvey holds the records of where teachers are reported to be by the School Survey. This is where teachers are.

Ideally, teachersurvey functions as a veriification of TeacherAppointments, that is, we check that teachers are where they are supposed to be and report excpetions.

This table brings these two sets of data together (In fact considerably more detailed views in the Establishment planning component can also perform this reconciliation.)

In newer systems, we may hae the requirement to report on teacher number directly from Appointments, rather than from TeacherSurvey. 

Similarly, Teacher qualiifications , used to computer Qualified/ Certified status, have been taken off TeacherSurvey. More recent changes will use any earlier survey record to source qualifications, rather than simply the survey in the current year. This protects against missing data in this area. 

Further, teacher qualifications may now be sourced from TeacherTraning records, since a Qualification code can be added onto that table.
' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'TeacherLocation'
GO

