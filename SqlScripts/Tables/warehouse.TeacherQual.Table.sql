SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[TeacherQual](
	[tID] [int] NOT NULL,
	[yr] [smallint] NULL,
	[tchQual] [nvarchar](50) NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Intermedaite table to assist in calculation of Certified / Qualified.
This table shows, for each teacher, the qualifications reported, and the first year of that report. 

Assembled from both the School Survey, and any named qualifications reported on TeacherTraining.' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'TeacherQual'
GO

