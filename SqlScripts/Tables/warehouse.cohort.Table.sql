SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[cohort](
	[SurveyYear] [int] NULL,
	[Estimate] [int] NULL,
	[YearOfEd] [int] NULL,
	[GenderCode] [nvarchar](1) NOT NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[Enrol] [int] NULL,
	[Rep] [int] NULL,
	[RepNY] [int] NULL,
	[TroutNY] [int] NULL,
	[EnrolNYNextLevel] [int] NULL,
	[RepNYNextLevel] [int] NULL,
	[TrinNYNextLevel] [int] NULL
) ON [PRIMARY]
GO

