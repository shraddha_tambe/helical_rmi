SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[measureEnrolSchoolG](
	[schNo] [nvarchar](50) NOT NULL,
	[SurveyYear] [int] NOT NULL,
	[Estimate] [int] NOT NULL,
	[SurveyDimensionID] [int] NULL,
	[genderCode] [nvarchar](1) NOT NULL,
	[Enrol] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Enrolment total by school and gender.

SurveyDimensionID is provided on this, so you can join to dimensionSchoolSurvey to produce enrolment totals aggregated by any data item on the survey, or in the set of custom school lists.' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'measureEnrolSchoolG'
GO

