SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [warehouse].[schoolTeacherCount](
	[SchNo] [nvarchar](50) NULL,
	[SurveyYear] [int] NOT NULL,
	[GenderCode] [nvarchar](1) NULL,
	[AgeGroup] [varchar](5) NULL,
	[DistrictCode] [nvarchar](5) NULL,
	[AuthorityCode] [nvarchar](10) NULL,
	[SchoolTypeCode] [nvarchar](10) NULL,
	[Sector] [nvarchar](3) NULL,
	[ISCEDSubClass] [nvarchar](10) NULL,
	[Support] [nvarchar](10) NULL,
	[NumTeachers] [int] NULL,
	[Certified] [int] NULL,
	[Qualified] [int] NULL,
	[CertQual] [int] NULL,
	[Estimate] [int] NULL
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'Ms_Description', @value=N'Number of teacher at each school in a survey year. Normalised by Gender.

District Code, AuthorityCode and SchoolType code are provided to allow simple aggregations on these variables.
' , @level0type=N'SCHEMA',@level0name=N'warehouse', @level1type=N'TABLE',@level1name=N'schoolTeacherCount'
GO

