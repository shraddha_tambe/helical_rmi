SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [workflow].[Actions](
	[actID] [int] IDENTITY(1,1) NOT NULL,
	[actName] [nvarchar](50) NULL,
	[actRole] [nvarchar](50) NULL,
	[acttype] [int] NULL,
	[actArg] [nvarchar](255) NULL,
 CONSTRAINT [metawrkActions_PK] PRIMARY KEY NONCLUSTERED 
(
	[actID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [workflow].[Actions] ADD  CONSTRAINT [DF__Actions__acttype__4FA7B896]  DEFAULT ((0)) FOR [acttype]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The types of Actions that may be performed on a Personnel Occurrence Record.  The definition of each action comprises its name, the security role that may perform this action, and the definition of the Action itself. The action definition is an integer indicating the type of action, and arguments, which are particular to each type. 
This table is attached as workflow_Actions in Pineapples' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'Actions'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Workflow and POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'Actions'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'Actions'
GO

