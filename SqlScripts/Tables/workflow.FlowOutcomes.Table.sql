SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [workflow].[FlowOutcomes](
	[wfoID] [int] IDENTITY(1,1) NOT NULL,
	[wfqID] [int] NULL,
	[wfoOutcome] [nvarchar](50) NULL,
	[wfoStatus] [nvarchar](10) NULL,
	[wfoAnnotation] [ntext] NULL,
 CONSTRAINT [metawrkFlowOutcomes_PK] PRIMARY KEY NONCLUSTERED 
(
	[wfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [metawrkFlowActionsmetawrkFlowOutcomes] ON [workflow].[FlowOutcomes]
(
	[wfqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [metawrkStatusmetawrkFlowOutcomes] ON [workflow].[FlowOutcomes]
(
	[wfoStatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [wfqID] ON [workflow].[FlowOutcomes]
(
	[wfqID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [workflow].[FlowOutcomes] ADD  CONSTRAINT [DF__FlowOutco__wfqID__583CFE97]  DEFAULT ((0)) FOR [wfqID]
GO
ALTER TABLE [workflow].[FlowOutcomes]  WITH CHECK ADD  CONSTRAINT [FK_FlowOutcomes_FlowActions] FOREIGN KEY([wfqID])
REFERENCES [workflow].[FlowActions] ([wfqID])
ON DELETE CASCADE
GO
ALTER TABLE [workflow].[FlowOutcomes] CHECK CONSTRAINT [FK_FlowOutcomes_FlowActions]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Attached as workflow_FlowOutcomes. 
The possible outcomes associated to each Action in a Flow. The outcome determines the new status of the POR hance the next action in the sequence.
wfqID - the FlowAction primary key, is foregin key on this table.' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'FlowOutcomes'
GO
EXEC sys.sp_addextendedproperty @name=N'pDiagramName', @value=N'Workflow and POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'FlowOutcomes'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'FlowOutcomes'
GO

