/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2012 (11.0.6251)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2012
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

CREATE SCHEMA [workflow]
GO
GRANT EXECUTE ON SCHEMA::[workflow] TO [pEstablishmentRead] AS [dbo]
GO
GRANT SELECT ON SCHEMA::[workflow] TO [pEstablishmentRead] AS [dbo]
GO
GRANT DELETE ON SCHEMA::[workflow] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT EXECUTE ON SCHEMA::[workflow] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT INSERT ON SCHEMA::[workflow] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT UPDATE ON SCHEMA::[workflow] TO [pEstablishmentWrite] AS [dbo]
GO
GRANT EXECUTE ON SCHEMA::[workflow] TO [pTeacherReadX] AS [dbo]
GO
GRANT SELECT ON SCHEMA::[workflow] TO [pTeacherReadX] AS [dbo]
GO
GRANT VIEW DEFINITION ON SCHEMA::[workflow] TO [pTeacherReadX] AS [dbo]
GO
GRANT DELETE ON SCHEMA::[workflow] TO [pTeacherWriteX] AS [dbo]
GO
GRANT EXECUTE ON SCHEMA::[workflow] TO [pTeacherWriteX] AS [dbo]
GO
GRANT INSERT ON SCHEMA::[workflow] TO [pTeacherWriteX] AS [dbo]
GO
GRANT SELECT ON SCHEMA::[workflow] TO [pTeacherWriteX] AS [dbo]
GO
GRANT TAKE OWNERSHIP ON SCHEMA::[workflow] TO [pTeacherWriteX] AS [dbo]
GO
GRANT UPDATE ON SCHEMA::[workflow] TO [pTeacherWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON SCHEMA::[workflow] TO [pTeacherWriteX] AS [dbo]
GO
GRANT VIEW DEFINITION ON SCHEMA::[workflow] TO [public] AS [dbo]
GO
