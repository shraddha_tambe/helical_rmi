SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [workflow].[porStatus](
	[statusCode] [nvarchar](10) NOT NULL,
	[statusDesc] [nvarchar](50) NULL,
	[statusAnnotation] [ntext] NULL,
 CONSTRAINT [metawrkStatus_PK] PRIMARY KEY NONCLUSTERED 
(
	[statusCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON [workflow].[porStatus] TO [pAdminWrite] AS [dbo]
GO
GRANT INSERT ON [workflow].[porStatus] TO [pAdminWrite] AS [dbo]
GO
GRANT UPDATE ON [workflow].[porStatus] TO [pAdminWrite] AS [dbo]
GO
GRANT DELETE ON [workflow].[porStatus] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT INSERT ON [workflow].[porStatus] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT UPDATE ON [workflow].[porStatus] TO [pEstablishmentAdmin] AS [dbo]
GO
GRANT DELETE ON [workflow].[porStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT INSERT ON [workflow].[porStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT UPDATE ON [workflow].[porStatus] TO [pTeacherAdmin] AS [dbo]
GO
GRANT SELECT ON [workflow].[porStatus] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [workflow].[porStatus] TO [public] AS [dbo]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table of POR Status values. The status is changed by procvessing Actions with various Outcomes.' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'porStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'pSystemTopic', @value=N'POR' , @level0type=N'SCHEMA',@level0name=N'workflow', @level1type=N'TABLE',@level1name=N'porStatus'
GO

