SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Aurion].[EffectiveGLSalaryAccount0]
AS

select ORG.*
	, SEC.secGLSalaries  GLSalaryAccount
	, 100 as Perc

from Aurion.AuthoritySectorOrgUnits ORG
	INNER JOIN EducationSectors SEC
	ON SEC.secCode = ORG.secCode
UNION
	Select A.authCode
	, 'SUPER'
	, authSuperOrgUnitNumber
	,secGLSalaries GLSalaryAccount
	, case secCode when 'PRI' then 70 else 30 end

FROM Authorities A
	CROSS JOIN
		(Select * from EducationSectors WHERE secCode in ( 'PRI','SEC')) ES
GO

