SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [cenopdf].[NewTeachers]
AS
	Select cenopdf.ClassLevelItems('CS') [T.00.Class.Min_ItemsText]
	, cenopdf.ClassLevelItems('CS') [T.00.Class.Max_ItemsText]
GO

