SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ghislain Hachey
-- Create date: 2018
-- Description:	Exam field options
--
--- Placeholder view to get Exam Filter to work
--- Pivot table and chart feature not priority
-- =============================================
CREATE VIEW [common].[examFieldOptions]
AS
Select f , g, seq
from
(
Select 'Authority' f
, null g
, 0 seq
UNION
Select 'School Type' f
, null g
, 0 seq
UNION
Select vocabTerm f
, 1 g
, 0 seq
from sysVocab
WHERE vocabName = 'District'
UNION
Select vocabTerm f
, null g
, 0 seq
from sysVocab
WHERE vocabName = 'National Electorate'
UNION
Select vocabTerm f
, null g
, 0 seq
from sysVocab
WHERE vocabName = 'Local Electorate'


) S
GO

