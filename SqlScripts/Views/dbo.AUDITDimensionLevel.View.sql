SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AUDITDimensionLevel]
AS
SELECT
lkpLevels.codeCode AS LevelCode,
lkpLevels.codeDescription AS LevelDescription,
Count(DimensionLevel.LevelCode) AS CountOfLevelCode
FROM
dbo.lkpLevels LEFT JOIN
dbo.DimensionLevel ON
lkpLevels.codeCode=DimensionLevel.LevelCode
GROUP BY
lkpLevels.codeCode,
lkpLevels.codeDescription
HAVING
(((Count(DimensionLevel.LevelCode))<>1));
GO
GRANT DELETE ON [dbo].[AUDITDimensionLevel] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[AUDITDimensionLevel] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[AUDITDimensionLevel] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[AUDITDimensionLevel] TO [public] AS [dbo]
GO

