SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AUDITDimensionSchoolSurvey]
AS
SELECT SchoolSurvey.ssID, SchoolSurvey.svyYear, SchoolSurvey.schNo,
Count(DimensionSchoolSurvey.[Survey ID]) AS [CountOfSurvey ID]
FROM
dbo.SchoolSurvey LEFT JOIN dbo.DimensionSchoolSurvey
ON dbo.SchoolSurvey.ssID = dbo.DimensionSchoolSurvey.[Survey ID]
GROUP BY
SchoolSurvey.ssID, SchoolSurvey.svyYear, SchoolSurvey.schNo
HAVING (((Count(DimensionSchoolSurvey.[Survey ID]))<>1));
GO
GRANT DELETE ON [dbo].[AUDITDimensionSchoolSurvey] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[AUDITDimensionSchoolSurvey] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[AUDITDimensionSchoolSurvey] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[AUDITDimensionSchoolSurvey] TO [public] AS [dbo]
GO

