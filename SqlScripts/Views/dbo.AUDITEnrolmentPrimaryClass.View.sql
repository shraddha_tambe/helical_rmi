SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AUDITEnrolmentPrimaryClass]
AS
SELECT Schools.schNo, Schools.schName, SchoolSurvey.svyYear,
qauditsubEnrollmentTotals.ssID, qauditsubEnrollmentTotals.enLevel,
qauditsubEnrollmentTotals.EnTotal, qauditsubPrimaryClassTotals.PCTotal
FROM
Schools INNER JOIN
((qauditsubEnrollmentTotals INNER JOIN qauditsubPrimaryClassTotals
ON (qauditsubEnrollmentTotals.enLevel = qauditsubPrimaryClassTotals.pclLevel)
AND (qauditsubEnrollmentTotals.ssID = qauditsubPrimaryClassTotals.ssID))
INNER JOIN dbo.SchoolSurvey ON qauditsubEnrollmentTotals.ssID =
SchoolSurvey.ssID) ON Schools.schNo = SchoolSurvey.schNo
WHERE (((qauditsubPrimaryClassTotals.PCTotal)<>[EnTotal])
AND ((Schools.schType)='P'));
GO
GRANT DELETE ON [dbo].[AUDITEnrolmentPrimaryClass] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[AUDITEnrolmentPrimaryClass] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[AUDITEnrolmentPrimaryClass] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[AUDITEnrolmentPrimaryClass] TO [public] AS [dbo]
GO

