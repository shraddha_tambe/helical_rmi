SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionAge]
AS
SELECT
dbo.Survey.svyYear,
Ages.num AS Age,
lvlYear AS yearofEd,

Case
	When lvlYear = Ages.num - dbo.Survey.[svyPSAge]+1 Then 1
	Else 0
End AS AtLevelAge,

Case
	When Ages.num - dbo.Survey.[svyPSAge]+1 Between edLevel.edlMinYear
         And edLevel.edlMaxYear Then 1
	Else 0
End AS AtedLevelAge,

Case
	When Ages.num - dbo.Survey.[svyPSAge]+1 Between edLevelAlt.edlMinYear
         And edLevelAlt.edlMaxYear Then 1
	Else 0
End AS AtedLevelAltAge,

Case
	When Ages.num - dbo.Survey.[svyPSAge]+1 Between edLevelAlt2.edlMinYear
         And edLevelAlt2.edlMaxYear Then 1
	Else 0
End AS AtedLevelAlt2Age,
dbo.lkpLevels.codeCode AS LevelCode,
edLevel.codeCode AS edLevelCode,
edLevelAlt.codeCode AS edLevelAltCode,
edLevelAlt2.codeCode AS edLevelAlt2Code

FROM
dbo.metaNumbers AS Ages,
dbo.lkpEducationLevels AS edLevel,
dbo.lkpEducationLevelsAlt AS edLevelAlt,
dbo.lkpEducationLevelsAlt2 AS edLevelAlt2,
dbo.Survey,
dbo.lkpLevels
WHERE (((Ages.num) Between 0 And 50)
AND ((dbo.lkpLevels.lvlYear) Between [edLevel].[edlMinYear]
And [edLevel].[edlMaxYear] And (dbo.lkpLevels.lvlYear)
Between [edLevelAlt].[edlMinYear]
And [edLevelAlt].[edlMaxYear] And (dbo.lkpLevels.lvlYear)
Between [edLevelAlt2].[edlMinYear] And [edLevelAlt2].[edlMaxYear]))
GO

