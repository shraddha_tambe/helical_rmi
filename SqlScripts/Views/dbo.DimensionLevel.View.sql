SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2007
-- Description:	Dimension Level
--
-- Dimension based on class level, allows aggregations to use
-- any characteristics of the class level, including ISCED classifications hierarchy
-- and any EducationLevel hierarchy.
-- rewritten Sep 2018 gracefully handle missing values in lookups
-- =============================================
CREATE VIEW [dbo].[DimensionLevel]
AS
SELECT
	TRLevels.codeCode AS LevelCode
	, TRLevels.codeDescription AS [Level]
	, TRLevels.lvlYear as [Year of Education]
	, TRLevels.lvlGV AS LevelGV
	, EL.codeCode AS edLevelCode
	, EL.codeDescription AS [Education Level]
	, ELAlt.codeCode AS edLevelAltCode
	, ELAlt.codeDescription AS [Education Level Alt]
	, ELAlt2.codeCode AS edLevelAlt2Code
	, ELAlt2.codeDescription AS [Education Level Alt2]
	, TRLevels.secCode AS SectorCode
	, secDesc AS Sector
	, EL.edlMinYear AS edLevelMin
	, EL.edlMaxYear AS edLevelMax
	, ELAlt.edlMinYear AS edLevelAltMin
	, ELAlt.edlMaxYear AS edLevelAltMax
	, ELAlt2.edlMinYear AS edLevelAlt2Min
	, ELAlt2.edlMaxYear AS edLevelAlt2Max
	, ISCEDLevelSub.ilsCode AS [ISCED SubClass]
	, ISCEDLevel.ilCode AS [ISCED Level]
	, ISCEDLevel.ilName AS [ISCED Level Name]

FROM TRLevels
	LEFT JOIN ISCEDLevelSub
		ON TRLevels.ilsCode = ISCEDLevelSub.ilsCode
	LEFT JOIN ISCEDLevel
		ON ISCEDLevelSub.ilCode = ISCEDLevel.ilCode
	LEFT JOIN TREducationLevels EL
		ON TRLevels.lvlYear BETWEEN EL.edlMinYear AND EL.edlMaxYear
	LEFT JOIN TREducationLevelsAlt ELAlt
		ON TRLevels.lvlYear BETWEEN ELAlt.edlMinYear AND ELAlt.edlMaxYear
	LEFT JOIN TREducationLevelsAlt2 ELAlt2
		ON TRLevels.lvlYear BETWEEN ELAlt2.edlMinYear AND ELAlt2.edlMaxYear
	LEFT JOIN TREducationSectors SEC
		ON TRLevels.secCode = SEC.secCode
GO
GRANT SELECT ON [dbo].[DimensionLevel] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[DimensionLevel] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[DimensionLevel] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[DimensionLevel] TO [pSchoolWrite] AS [dbo]
GO

