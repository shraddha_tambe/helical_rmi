SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionRank]
AS


SELECT
S.svyYear,
S.schNo,
S.rankEnrol AS [School Enrol],
S.rankDistrictSchoolType AS [District Rank],
S.rankDistrictSchoolTypeDec AS [District Decile],
S.rankDistrictSchoolTypeQtl AS [District Quartile],
S.rankSchoolType AS [Rank],
S.rankSchoolTypeDec AS Decile,
S.rankSchoolTypeQtl AS Quartile
FROM dbo.SurveyYearRank S
GO

