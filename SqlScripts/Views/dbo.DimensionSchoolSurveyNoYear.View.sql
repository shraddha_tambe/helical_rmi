SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DimensionSchoolSurveyNoYear]
AS
SELECT
DSS.ssID AS [Survey ID],
DSS.svyYear AS [Survey Data Year],
DSS.schNo AS [School No],
DSS.schName AS [School Name],
DSS.SchNo + ': ' + DSS.SchName as [SchoolID_Name],
DSS.SchNo + ': ' + DSS.SchName + ' (' + DSS.ssSchType + ')' as [SchoolID_Name_Type],
DSS.SchNo + ': ' + DSS.SchName + ' (' + Districts.dShort + ')' as [SchoolID_Name_District],

DSS.schReg AS [Registration No],
DSS.schXNo AS [School External ID],
Islands.iGroup AS [District Code],
Districts.dName AS District,
Districts.dShort as [District Short],
Islands.iName AS Island,
Islands.iOuter AS Region,
dbo.qsubEnrolmentPartition.ptName AS [Size],
lkpElectorateL.codeDescription AS [Local Electorate],
DSS.ssElectL AS [Local Electorate No],
lkpElectorateN.codeDescription AS [National Electorate],
DSS.ssElectN AS [National Electorate No],
Auth.*,
Lang.*,
DSS.ssISClass AS InfrastructureClass,
DSS.ssISSize AS InfrastructureSizeClass,
SchoolTypes.stDescription AS [Current School Type],
DSS.schType AS CurrentSchoolTypeCode,
DSS.ssSchType AS SchoolTypeCode,
DSS.schClass AS [School Class],
DSS.ssAccess AS AccessCode,
dbo.TRAccess.codeDescription AS Access,
Case
	When DSS.ssParentCommittee = 0 Then 'N'
	Else 'Y'
End AS  HasParentCommitteeYN,

PC.codeDescription AS ParentCommitteeSupport,
DSS.schEst AS [Year Established],
DSS.schClosed AS [Year Closed],
SchoolTypes_1.stDescription AS SchoolType,
1 AS NumberOfSchools,
dbo.lkpElectorateL.elgisID,
dbo.lkpElectorateN.engisID,
dbo.Islands.igisID,
dbo.Districts.dgisID,
ListXTab.*,
DSS.ssParent AS ParentSchool,
CASE
	When ssExt = 0 Then 'N'
	Else 'Y'
End As HasParentSchool

FROM
	(
		SELECT
			Schools.schNo,
			Schools.schName,
			ss.ssID,
			SS.svyYear,
			Schools.iCode,
			Schools.schType,
			Schools.schReg,
			Schools.schXNo,
			Schools.schEst,
			Schools.schClosed,
			Schools.schClass,
			IsNull(ss.ssSchType,[schType]) AS ssSchType,
			IsNull(ss.ssAuth,[schAuth]) AS ssAuth,
			IsNull(ss.ssLang,[schLang]) AS ssLang,
			IsNull(ss.ssElectL,[schElectL]) AS ssElectL,
			IsNull(ss.ssElectN,[schElectN]) AS ssElectN,
			SS.ssISClass,
			SS.ssISSize,
			SS.ssParent,
			SS.ssAccess,
			SS.ssExt,
			SS.ssParentCommittee
			FROM
			dbo.Schools LEFT JOIN dbo.SchoolSurvey AS ss ON Schools.schNo = ss.schNo
	) DSS
	LEFT JOIN TRSchoolTypes AS SchoolTypes
		ON DSS.schType = SchoolTypes.stCode
	LEFT JOIN TRSchoolTypes AS SchoolTypes_1
		ON DSS.ssSchType = SchoolTypes_1.stCode
	LEFT JOIN Islands
		ON DSS.iCode = Islands.iCode
		LEFT JOIN Districts
			ON Islands.iGroup = Districts.dID
	LEFT JOIN lkpElectorateN
		ON DSS.ssElectN = dbo.lkpElectorateN.codeCode
	LEFT JOIN lkpElectorateL
		ON DSS.ssElectL = dbo.lkpElectorateL.codeCode
	LEFT JOIN dbo.ListXTab
		ON DSS.schNo = ListXtab.schNo
	LEFT JOIN dbo.qsubEnrolmentPartition
		ON DSS.ssID = dbo.qsubEnrolmentPartition.ssID
	LEFT JOIN dbo.DimensionAuthority Auth
		ON DSS.ssAuth = Auth.AuthorityCode
	LEFT JOIN dbo.DimensionLanguage Lang
		ON DSS.ssLang = Lang.LanguageCode
	LEFT JOIN dbo.PIVParentCommitteeSub PC
		ON DSS.ssID = PC.ssID
	LEFT JOIN dbo.TRAccess
		ON DSS.ssAccess = dbo.TRAccess.codeCode
GO
GRANT SELECT ON [dbo].[DimensionSchoolSurveyNoYear] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[DimensionSchoolSurveyNoYear] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[DimensionSchoolSurveyNoYear] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[DimensionSchoolSurveyNoYear] TO [pSchoolWrite] AS [dbo]
GO

