SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[EducationLevelAgeRange]
AS
-- This version will go 20 years past the last survey, and uses the PSStartAge for those years from the last survey year

SELECT     YEARS.num populationYear, Survey.svyYear as LatestSurveyYear, Survey.svyPSAGE as OfficialStartAge, dbo.metaNumbers.num AS Age, dbo.lkpEducationLevels.codeCode AS AgeEdLevelCode,
                      dbo.lkpEducationLevels.codeDescription AS [Age Expected Education Level], dbo.lkpEducationLevelsAlt.codeCode AS AgeEdLevelAltCode,
                      dbo.lkpEducationLevelsAlt.codeDescription AS [Age Expected Education Level Alt], dbo.lkpEducationLevelsAlt2.codeCode AS AgeEdLevelAlt2Code,
                      dbo.lkpEducationLevelsAlt2.codeDescription AS [Age Expected Education Level Alt2]

FROM         (Select  num + (svyYear) as num,Max(svyYear) as LatestSurveyYear from metaNumbers N
					cross Join Survey S WHERE num between 0 and 20 group by num+svyYear) YEARS
							INNER JOIN
					 dbo.Survey
						ON Years.LatestSurveyYear = Survey.svyYear
							CROSS JOIN
                      dbo.metaNumbers CROSS JOIN
                      dbo.lkpEducationLevels CROSS JOIN
                      dbo.lkpEducationLevelsAlt CROSS JOIN
                      dbo.lkpEducationLevelsAlt2
WHERE
				(dbo.lkpEducationLevels.edlMinYear + dbo.Survey.svyPSAge - 1 <= dbo.metaNumbers.num) AND
                      (dbo.lkpEducationLevels.edlMaxYear + dbo.Survey.svyPSAge - 1 >= dbo.metaNumbers.num) AND
                      (dbo.lkpEducationLevelsAlt.edlMinYear + dbo.Survey.svyPSAge - 1 <= dbo.metaNumbers.num) AND
                      (dbo.lkpEducationLevelsAlt.edlMaxYear + dbo.Survey.svyPSAge - 1 >= dbo.metaNumbers.num) AND
                      (dbo.lkpEducationLevelsAlt2.edlMinYear + dbo.Survey.svyPSAge - 1 <= dbo.metaNumbers.num) AND
                      (dbo.lkpEducationLevelsAlt2.edlMaxYear + dbo.Survey.svyPSAge - 1 >= dbo.metaNumbers.num)
GO

