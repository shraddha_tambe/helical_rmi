SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsPupilTablesDistanceTransport]
AS
Select
ssId,
ptCode Category,
ptRow Code,
genderCode,
Gender,
DT.codeDescription Type,
ptCol Mode,
case genderCode when 'M' then ptM when 'F' then ptF end NumChildren
From PupilTables PT
	CROSS JOIN DimensionGender
LEFT JOIN TRDistanceCodes DT
	ON PT.ptRow = DT.codeCode
	AND ptCode = 'TRANSPORT'

WHERE ptCode = 'TRANSPORT'
GO

