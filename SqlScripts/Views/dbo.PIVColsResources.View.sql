SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------
--- this view is the default reource format, in Somalia that has functioning
---
----------------------------------------------------
CREATE VIEW [dbo].[PIVColsResources]
AS
SELECT
Resources.resID ID,
Resources.ssID,
REsources.resName Category,
Resources.resSplit AS [Code],
coalesce(resSplit, resSplit) [Type],
case Resources.resAvail when -1 then 'Y' when 0 then 'N' else null end Available,
Resources.resNumber Number,
--Resources.resQty AS Qty,  -- qty i not needed?
----resFunctioning Functioning,
----case
----	when resFunctioning = 1 then 'Y'
----	when resFunctioning = 0 then 'N'
----	else null
----end FunctioningYN,
case
	when resCondition = 1 then 'Good'
	when resCondition = 2 then 'Fair'
	when resCondition = 3 then 'Poor'
end Condition
FROM
	Resources
	LEFT JOIN lkpWaterSupplyTypes WST
		ON Resources.resSplit = WST.wsType
		AND Resources.resName='Water Supply'
GO

