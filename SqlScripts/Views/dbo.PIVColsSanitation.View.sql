SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsSanitation]
AS
SELECT
	ssID,
	toiNum Number,

	toiCondition Condition,

	lkpToiletTypes.ttypName AS [Type],
	Toilets.toiUse AS UsedBy
	-- yn is not included
FROM Toilets
INNER JOIN lkpToiletTypes
	ON lkpToiletTypes.ttypName = Toilets.toiType
GO

