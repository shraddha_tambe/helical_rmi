SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 5 11 2013
-- Description:	Report on teacher hurs from classes table
-- Note the SOMAI format from 2013 onward staores only the total hours taught by the teacher
-- hence the treatment of max vs sum
-- see PIVSurvey_SubjectsOffered
-- =============================================
CREATE VIEW [dbo].[PIVColsTeacherHours]
AS

Select ssID
, tSex Gender
, count(tID) TeachersWithClasses
, sum(HoursTaught) HoursTaught
FROM
(
Select C.ssID
, CT.tID
, tSex
, sum(pcHrsWeek)  HoursTaught
FROM Classes C
INNER JOIN ClassTeacher CT
	ON c.pcID = CT.pcID
INNER JOIN SchoolSurvey SS
	ON C.ssId = ss.ssID
LEFT JOIN TeacherIdentity TI
	ON CT.tId = TI.tID
GROUP BY C.ssID, CT.tID, svyYear, tSex
) SUB
GROUP BY ssID, tSex
GO

