SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PIVColsToilets]
AS
SELECT
	ssID,
	toiNum Number,

	case [toiCondition]
		when '1' then 'A'
		when '2' then 'B'
		when '3' then 'C'
		else toiCondition
	end AS Condition,
	lkpToiletTypes.ttypName AS [Type],
	Toilets.toiUse AS UsedBy
	-- yn is not included
FROM Toilets
INNER JOIN lkpToiletTypes
	ON lkpToiletTypes.ttypName = Toilets.toiType
GO

