SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Partition_TertiaryAge]
AS
-- these views are to control specific permissions on different lookups
Select * from [Partitions]
WHERE ptSet = 'TertiaryAge'
GO
GRANT DELETE ON [dbo].[Partition_TertiaryAge] TO [pEnrolmentAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[Partition_TertiaryAge] TO [pEnrolmentAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[Partition_TertiaryAge] TO [pEnrolmentAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[Partition_TertiaryAge] TO [public] AS [dbo]
GO

