SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[RecursiveRoleMembershipUserEx]
AS
/* this view shows only the roles to which the current_user beliongs */
WITH RoleMembers( MemberName, RoleName,Depth) AS
(
	select cast('USER' as sysname) as MemberName, user_name() as RoleName , 0 as Depth
	UNION ALL
	select 'GROUP' as MemberName, P.name as RoleName , 0 as Depth
	from sys.database_principals P
	WHERE type = 'G'
	and is_member(P.name)=1

	UNION ALL
	SELECT CTE.RoleName , R.Name, Depth+1
	from sys.sysusers R
	INNER JOIN sys.database_role_members RM
	ON RM.role_principal_ID = R.uid
	INNER JOIN sys.sysusers M
	ON RM.member_principal_ID = M.uid
	INNER JOIN RoleMembers CTE
		On CTE.RoleName = M.name


)
SELECT DISTINCT roleName
	,cast(Puser.value as nvarchar(40)) PineapplesRole
FROM RoleMembers
	Inner JOIN
	(Select * from sys.database_principals WHERE [type] = 'R') AllRoles
	ON RoleMembers.roleName = AllRoles.[name]
	LEFT JOIN
	(Select major_id, [value] from sys.extended_properties WHERE Class=4 and [name]='PineapplesUser') Puser
	ON Puser.major_ID = AllRoles.principal_id
GO

