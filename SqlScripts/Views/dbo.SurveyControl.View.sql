SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[SurveyControl]
AS
SELECT [syID]
      ,[syYear]			saYear
      ,[schNo]
      ,[saSent]
      ,[saCollected]
      ,[saReceived]
      ,[saCompletedby]
      ,[saEntered]
      ,[saEnteredBy]
      ,[saAudit]
      ,[saAuditDate]
      ,[saAuditBy]
      ,[saAuditResult]
      ,[saAuditNote]
      ,[saComment]
      ,[saReceivedTarget]
      ,[saReceivedD]
      ,[saReceivedDTarget]
      ,[saFileLocn]
      ,[syDormant]		saDormant

  FROM dbo.[SchoolYearHistory]
GO
GRANT SELECT ON [dbo].[SurveyControl] TO [pSurveyRead] AS [dbo]
GO

