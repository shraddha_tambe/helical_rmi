SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRCostCentres]
AS
SELECT     ccCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN ccDescription
	WHEN 1 THEN ccDescriptionL1
	WHEN 2 THEN ccDescriptionL2

END,ccDescription) AS codeDescription
FROM         dbo.CostCentres
GO
GRANT SELECT ON [dbo].[TRCostCentres] TO [public] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[TRCostCentres] TO [public] AS [dbo]
GO

