SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TREducationLevelsAlt2]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codeDescription
	WHEN 1 THEN codeDescriptionL1
	WHEN 2 THEN codeDescriptionL2

END,codeDescription) AS codeDescription,
edlMinYear,
edlMaxYear

FROM         dbo.lkpEducationLevelsAlt2
GO
GRANT SELECT ON [dbo].[TREducationLevelsAlt2] TO [public] AS [dbo]
GO

