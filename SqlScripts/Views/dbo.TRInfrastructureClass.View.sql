SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRInfrastructureClass]
AS
SELECT     isCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN isDesc
	WHEN 1 THEN isDescL1
	WHEN 2 THEN isDescL2

END,isDesc) AS isDesc

FROM         dbo.lkpInfrastructureClass
GO
GRANT SELECT ON [dbo].[TRInfrastructureClass] TO [public] AS [dbo]
GO

