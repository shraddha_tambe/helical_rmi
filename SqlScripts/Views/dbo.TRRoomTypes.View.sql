SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRRoomTypes]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codedescription
	WHEN 1 THEN codedescriptionL1
	WHEN 2 THEN codedescriptionL2

END,codedescription) AS codedescription,codeGroup,codeSort,rmresCat,rfcnCode

FROM         dbo.lkpRoomTypes
GO
GRANT SELECT ON [dbo].[TRRoomTypes] TO [public] AS [dbo]
GO

