SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRTeacherRole]
AS
SELECT     codeCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN codeDescription
	WHEN 1 THEN codeDescriptionL1
	WHEN 2 THEN codeDescriptionL2

END,codeDescription) AS codeDescription,codeSort,trsalLevelMin,trsalLevelMax,trSalaryPointMin
	,trSalaryPointMax
	,trSalaryPointMedian
	,[secCode]
     ,[trReportsTo]
FROM         dbo.lkpTeacherRole
GO
GRANT SELECT ON [dbo].[TRTeacherRole] TO [public] AS [dbo]
GO

