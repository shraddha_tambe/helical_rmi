SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TRTeacherStatus]
AS
SELECT     statusCode,
isnull(CASE dbo.userLanguage()
	WHEN 0 THEN statusDesc
	WHEN 1 THEN statusDescL1
	WHEN 2 THEN statusDescL2

END,statusDesc) AS statusDesc

FROM         dbo.lkpTeacherStatus
GO
GRANT SELECT ON [dbo].[TRTeacherStatus] TO [public] AS [dbo]
GO

