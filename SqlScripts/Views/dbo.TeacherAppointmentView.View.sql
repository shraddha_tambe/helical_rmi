SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[TeacherAppointmentView]
AS
SELECT     TA.taID, TA.tID, TA.taDate, TA.taType, TA.SchNo, TA.taRole, TA.spCode, TA.taSalary, TA.estrID
	, S.schName
	, TI.tSurname, TI.tGiven
FROM         dbo.TeacherAppointment AS TA
				INNER JOIN  dbo.Schools AS S ON TA.SchNo = S.schNo
				INNER JOIN TEacherIdentity TI on TA.tID = tI.tID
GO

