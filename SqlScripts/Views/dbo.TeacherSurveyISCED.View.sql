SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	busness rules for determining the sector and isced classification of a teacher
--
-- =============================================
-- Uses the following fields:
--- from teacher survey,the sector recorded, the min and max class levels taught
---- the school type from the school survey
--- LOGIC:
-- 1) If every class level taught in the school type has the same ISCED use that ISCED
-- 2) if the teacher teachers only a single level, use the ISCED for that level
-- 3) if no teacher level specified, but a teacher sector,
----   a) if sector specified, use the isced of the minimum level matching the school type and sector
----   b) if no sector, use the isced of the minimum level from the school type
-- 4) if the teacher's max and min levels both specified
-----  a) if max and min have same isced, use that ISCED
-----  b) if sector specified, use the isced of the minimum level matching that sector,
-----     in the range taught by the teacher
-----  c) if no sector, use the isced of the minimum level taught by the teacher
CREATE VIEW [dbo].[TeacherSurveyISCED]
AS
SELECT
tchsID,
svyYear,
schNo,
ssSchType,
tchSector,
tchClass,
tchClassMax,
case
	when not (stILSCode is null) then stILSCode
	when not (LevelILSCode is null) then LevelILSCode
	when not (ilsCode is null) then ilsCode
	when not (stSecILSCode is null) then stSecILSCode
	else stSecMinILSCode
end TSISCED,

case
	when not (stGV is null) then stGV
	when not (LevelGV is null) then LevelGV
	when not (GV is null) then GV
	when not (stSecGV is null) then stSecGV
	else stSecMinGV
end TSGV
FROM
(
SELECT TeacherSurvey.tchsID,
SchoolSurvey.svyYear,
SchoolSurvey.schNo,
SchoolSurvey.ssSchType,
TeacherSurvey.tchSector,
TeacherSurvey.tchClass,
TeacherSurvey.tchClassMax,
lkpLevels.ilsCode ilsCode,
lkpLevels.lvlGV GV,

case
	when lkpLevels.ilsCode is null then null
	when lkpLevels_1.ilsCode is null then lkpLevels.ilsCode	-- only the min level is defined
	when lkpLevels_1.ilsCode = lkpLevels.ilsCode then lkpLevels.ilsCode -- both the same
	else null	-- 2 values differrent - cannot infer ils uniquely from the class levels taught
end LevelILSCode,

case
	when lkpLevels.lvlGV is null then null
	when lkpLevels_1.lvlGV is null then lkpLevels.lvlGV	-- only the min level is defined
	when lkpLevels_1.lvlGV = lkpLevels.lvlGV then lkpLevels.lvlGV -- both the same
	else null	-- 2 values differrent - cannot infer ils uniquely from the class levels taught
end LevelGV,


SchoolTypeSectorILSCode.UniqueILSCode stSecILSCode,
SchoolTypeSectorILSCode.MinILSCode stSecMinILSCode,
SchoolTypeILSCode.UniqueILSCode stILSCode,
SchoolTypeSectorILSCode.UniqueGV stSecGV,
SchoolTypeSectorILSCode.MinGV stSecMInGV,
SchoolTypeILSCode.UniqueGV stGV

FROM (SchoolSurvey
INNER JOIN SchoolTypeILSCode ON SchoolSurvey.ssSchType = SchoolTypeILSCode.stCode)
INNER JOIN (((TeacherSurvey
	LEFT JOIN lkpLevels ON TeacherSurvey.tchClass = lkpLevels.codeCode)
	LEFT JOIN lkpLevels AS lkpLevels_1 ON TeacherSurvey.tchClassMax = lkpLevels_1.codeCode)
	INNER JOIN SchoolTypeSectorILSCode ON TeacherSurvey.tchSector = SchoolTypeSectorILSCode.secCode) ON (SchoolSurvey.ssSchType = SchoolTypeSectorILSCode.stCode) AND (SchoolSurvey.ssID = TeacherSurvey.ssID)
) subQ
GO

