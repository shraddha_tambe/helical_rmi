SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ddAuthority]
AS
SELECT     TOP 100 PERCENT Authority, SortOrder
FROM         (SELECT     '(all)' AS Authority, '' AS SortOrder
                       FROM          dbo.metaNumbers
                       WHERE      (num = 0)
                       UNION
                       SELECT     authName, authName AS SortOrder
                       FROM         dbo.TRAuthorities AS Authorities) AS L
ORDER BY SortOrder
GO
GRANT DELETE ON [dbo].[ddAuthority] TO [pAdminAdmin] AS [dbo]
GO
GRANT INSERT ON [dbo].[ddAuthority] TO [pAdminAdmin] AS [dbo]
GO
GRANT UPDATE ON [dbo].[ddAuthority] TO [pAdminAdmin] AS [dbo]
GO
GRANT SELECT ON [dbo].[ddAuthority] TO [public] AS [dbo]
GO

