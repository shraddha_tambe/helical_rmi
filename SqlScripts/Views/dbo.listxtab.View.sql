SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[listxtab]

			AS
			SELECT schNo FROM listschools 
GO
GRANT SELECT ON [dbo].[listxtab] TO [pSchoolRead] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[listxtab] TO [public] AS [dbo]
GO

