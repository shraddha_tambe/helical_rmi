SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paAssessment]
WITH VIEW_METADATA
AS
Select A.*
, S.schName
, TI.tFullName
, TI.tSurname
, TI.tGiven
, PS.paScore
from paAssessment_ A
INNER JOIN TeacherIdentity TI
	ON A.tID = TI.tID
INNER JOIN Schools S
	ON A.paSchNo = S.schNo
LEFT JOIN paAssessmentScore PS
	ON A.paID = PS.paID
GO

