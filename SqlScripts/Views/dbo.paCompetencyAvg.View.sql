SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paCompetencyAvg]
WITH VIEW_METADATA
AS
Select paID
, comFullID
, avg(RawElmAvg) RawComAvg
, convert(decimal(4,1), avg(RawElmAvg))ComAvg
FROM
paElementAvg

GROUP BY paID
, comFullID
GO

