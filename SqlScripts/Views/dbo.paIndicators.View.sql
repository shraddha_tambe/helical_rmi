SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[paIndicators]
WITH VIEW_METADATA
AS
Select I.*
, C.pacomCode + '.' + E.paelmCode + '.' + I.paindCode indID
, C.pafrmCode + '.' + C.pacomCode + '.' + E.paelmCode  + '.' + I.paindCode indFullID
, C.pacomCode + '.' + E.paelmCode elmID
, C.pafrmCode + '.' + C.pacomCode + '.' + E.paelmCode   elmFullID
, E.paelmDescription elmDescription
, C.pacomCode  comID
, C.pafrmCode + '.' + C.pacomCode    comFullID
, C.pacomDescription comDescription

, C.pafrmCode
, F.pafrmDescription
from paIndicators_ I
INNER JOIN paElements_ E
	ON I.paelmID = E.paelmID
INNER JOIN paCompetencies_ C
	ON E.pacomID = C.pacomID
INNER JOIN paFrameworks_ F
	ON C.pafrmCode = F.pafrmCode
GO

