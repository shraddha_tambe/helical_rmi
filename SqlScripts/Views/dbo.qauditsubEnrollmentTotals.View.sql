SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qauditsubEnrollmentTotals]
AS
SELECT
Enrollments.ssID,
Enrollments.enLevel,
Sum( [enM]+[enF]) AS EnTotal
FROM
Enrollments
GROUP BY Enrollments.ssID, Enrollments.enLevel
GO

