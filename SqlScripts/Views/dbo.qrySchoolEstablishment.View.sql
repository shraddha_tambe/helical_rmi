SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 1 12 2007
-- Description:	school establishment - used by EstablishmentCheck
-- =============================================
CREATE VIEW [dbo].[qrySchoolEstablishment]
as
SELECT SchoolEstablishment.*,
SchoolEstablishmentRoles.estrID,
SchoolEstablishmentRoles.estrRoleGrade,
SchoolEstablishmentRoles.estrCount,
SchoolEstablishmentRoles.spCode,
TRRoleGrades.rgDescription AS rgDescription
FROM SchoolEstablishment
	INNER JOIN SchoolEstablishmentRoles
		ON SchoolEstablishment.estID = SchoolEstablishmentRoles.estID
	LEFT JOIN lkpSalaryPoints
		ON lkpSalaryPoints.spCode = SchoolEstablishmentRoles.spCode
	LEFT  JOIN TRRoleGrades
		ON TRRoleGrades.rgCode = SchoolEstablishmentRoles.estrRoleGrade
GO

