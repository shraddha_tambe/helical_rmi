SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qryTeacherList]
AS
SELECT     dbo.TeacherSurvey.tchsID, dbo.TeacherSurvey.ssID, dbo.TeacherSurvey.tchSort, dbo.TeacherSurvey.tchEmplNo, dbo.TeacherSurvey.tchRegister,
                      dbo.TeacherSurvey.tchProvident, dbo.TeacherSurvey.tchUnion, dbo.TeacherSurvey.tchFirstName, dbo.TeacherSurvey.tchFamilyName,
                      dbo.TeacherSurvey.tchSalaryScale, dbo.TeacherSurvey.tchSalary, dbo.TeacherSurvey.tchDOB, dbo.TeacherSurvey.tchGender, dbo.TeacherSurvey.tchCitizenship,
                      dbo.TeacherSurvey.tchIsland, dbo.TeacherSurvey.tchDistrict, dbo.TeacherSurvey.tchSponsor, dbo.TeacherSurvey.tchFTE, dbo.TeacherSurvey.tchFullPart,
                      dbo.TeacherSurvey.tchYears, dbo.TeacherSurvey.tchYearsSchool, dbo.TeacherSurvey.tchTrained, dbo.TeacherSurvey.tchEdQual, dbo.TeacherSurvey.tchQual,
                      dbo.TeacherSurvey.tchSubjectTrained, dbo.TeacherSurvey.tchCivilStatus, dbo.TeacherSurvey.tchNumDep, dbo.TeacherSurvey.tchHouse,
                      dbo.TeacherSurvey.tchSubjectMajor, dbo.TeacherSurvey.tchSubjectMinor, dbo.TeacherSurvey.tchSubjectMinor2, dbo.TeacherSurvey.tchClass,
                      dbo.TeacherSurvey.tchClassMax, dbo.TeacherSurvey.tchJoint, dbo.TeacherSurvey.tchComposite, dbo.TeacherSurvey.tchStatus, dbo.TeacherSurvey.tchRole,
                      dbo.TeacherSurvey.tchTAM, dbo.TeacherSurvey.tID, dbo.TeacherSurvey.tchECE, dbo.TeacherSurvey.tchInserviceYear, dbo.TeacherSurvey.tchInservice,
                      dbo.TeacherSurvey.tchSector, dbo.TeacherSurvey.tchYearStarted, dbo.TeacherSurvey.tchSpouseFirstName, dbo.TeacherSurvey.tchSpouseFamilyName,
                      dbo.TeacherSurvey.tchSpouseOccupation, dbo.TeacherSurvey.tchSpouseEmployer, dbo.TeacherSurvey.tchSpousetID, dbo.TeacherSurvey.tchLangMajor,
                      dbo.TeacherSurvey.tchLangMinor, dbo.lkpTeacherStatus.statusDesc, dbo.lkpTeacherRole.codeDescription AS RoleDesc
FROM         dbo.lkpTeacherStatus RIGHT OUTER JOIN
                      dbo.TeacherSurvey LEFT OUTER JOIN
                      dbo.lkpTeacherQual ON dbo.TeacherSurvey.tchQual = dbo.lkpTeacherQual.codeCode LEFT OUTER JOIN
                      dbo.lkpTeacherRole ON dbo.TeacherSurvey.tchRole = dbo.lkpTeacherRole.codeCode ON dbo.lkpTeacherStatus.statusCode = dbo.TeacherSurvey.tchStatus
GO

