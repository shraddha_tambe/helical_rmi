SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[qsumSchoolEnrollments]
AS
SELECT   dbo.Enrollments.ssID, Sum(dbo.Enrollments.enM) AS Male,
Sum(dbo.Enrollments.enF) AS Female,
sum(isnull(enM,0)+isnull(enf,0)) as Total
FROM dbo.Enrollments
GROUP BY dbo.Enrollments.ssID;
GO

