SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 26 11 2007
-- Description:	dormitory summary by survey
-- =============================================
CREATE VIEW [dbo].[ssIDDormitories]
AS
SELECT
DORM.ssID,
Count(DORM.rmType) AS NumRooms,
Sum(DORM.rmSize) AS TotSize,
Min(DORM.rmSize) AS minSize,
Max(DORM.rmSize) AS MaxSize,
Sum(case when [rmSize]>=[stdValue] then 1 else 0 end) AS sizeOK,
Sum(case IsNull([rmSize],0) when 0 then 0 else 1 end ) AS SizeSupplied,
Sum(DORM.rmCapM) AS capM,
Sum(DORM.rmCapF) AS capF,
Sum(DORM.rmShowersM) AS showersM,
Sum(DORM.rmShowersF) AS ShowersF,
Sum(DORM.toiletsM) AS toiletsM,
Sum(DORM.toiletsF) AS toiletsF,
STD.stdValue
FROM vtblDormitories AS DORM
	INNER JOIN SchoolSurvey SS
		ON DORM.ssID = SS.ssID
	INNER JOIN schoolyearApplicableStandards AS STD
		ON SS.schNo = STD.schNo and SS.svyYear = STD.LifeYear
-- note that this view means that the following record MUST be defined in
-- SurveyYearStandards
WHERE (((STD.stdName)='ROOM_SIZE') AND ((STD.stdItem)='DORM'))
GROUP BY DORM.ssID, STD.stdValue
GO

