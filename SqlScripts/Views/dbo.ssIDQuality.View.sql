SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ssIDQuality]
AS
SELECT     ssID,
Count(SchoolSurveyQuality.ssqID) AS NumIssues,

Sum(CASE ssqLevel WHEN 1 THEN 1 else 0 end) AS NumAlerts,
Sum(CASE ssqLevel WHEN 2 THEN 1 else 0 end) AS NumBlocks,
Max(SchoolSurveyQuality.ssqLevel) AS MaxLevel,
Sum(CASE ssqLevel WHEN 1 THEN 1 WHEN 2 THEN 1 else 0 end) AS NumOS

FROM dbo.SchoolSurveyQuality
GROUP BY ssID
GO

