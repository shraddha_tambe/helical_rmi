SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblPowerSupplyEst]
AS
SELECT
ESTIMATE.LifeYear AS [Survey Year],
ESTIMATE.schNo,
ESTIMATE.bestYear AS [Year Of Data],
ESTIMATE.Estimate,
ESTIMATE.Offset AS [Age Of Data],
ESTIMATE.ActualssID,
ESTIMATE.SurveyDimensionssID,
ESTIMATE.bestssID,
ESTIMATE.bestssqLevel AS [Data Quality Level],
ESTIMATE.ActualssqLevel AS [Survey Year Quality Level],
vtblPowerSupply.*
FROM
ESTIMATE_BestSurveyResourceCategory AS ESTIMATE INNER JOIN vtblPowerSupply ON ESTIMATE.bestssID = vtblPowerSupply.ssID
WHERE (((ESTIMATE.resName)= 'Power Supply'));
GO
GRANT SELECT ON [dbo].[vtblPowerSupplyEst] TO [pSchoolRead] AS [dbo]
GO
GRANT DELETE ON [dbo].[vtblPowerSupplyEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT INSERT ON [dbo].[vtblPowerSupplyEst] TO [pSchoolWrite] AS [dbo]
GO
GRANT UPDATE ON [dbo].[vtblPowerSupplyEst] TO [pSchoolWrite] AS [dbo]
GO

