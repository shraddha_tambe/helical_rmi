SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblPreschoolAttendees]
AS
SELECT
PupilTables.ptID,
PupilTables.ssID,
PupilTables.ptCode,
PupilTables.ptAge,
PupilTables.ptRow AS KinderName,
PupilTables.ptM,
PupilTables.ptF
FROM dbo.PupilTables
WHERE (((PupilTables.ptCode)='KINDER'))
GO

