SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblSTA]
AS
SELECT
EST.stID,
EST.exID,
ex.exCode,
EX.exYear,
EST.schNo,
EST.stGender,
EST.stSubject,
EST.stComponent,

[num] AS Lvl,
'LVL' + convert(char(1), num) as LvlNo,
Case [num]
	When 0 then st0
	When 1 Then st1
	When 2 Then st2
	When 3 Then st3
	When 4 Then st4
	When 5 Then st5
    When 6 Then st6
	When 7 Then st7
	When 8 Then st8
	When 9 Then st9
End as PupilResults,

case [num]
	When 0 then st0
	When 1 Then st1
	else null
end as AtRisk,

case
	when num <= 1 then 'AtRisk'
	else 'OK'
end AS RiskGroup

FROM
	(dbo.ExamStandardTest EST
		INNER JOIN Exams EX
			ON EST.exID = EX.exID)
	CROSS JOIN dbo.metaNumbers
		WHERE num between 0 and 9
GO

