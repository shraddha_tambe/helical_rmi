SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblTeacherHousing]
AS
SELECT
	Resources.resID,
	Resources.ssID,
	case when resName like 'Staff Housing%' then
			right([resName],len(resName) - Len('Staff Housing'))   --23/3/2010
		when resName like 'Teacher Housing%' then
			right([resName],len(resName) - Len('Teacher Housing'))
	end
	as Materials,
    Resources.resSplit AS HousingType,
	Resources.resAvail,
	Resources.resNumber,
	REsources.resName,
	Case
	When resCondition = 0 Then 'Null'
	Else Substring('CBA',[resCondition],1)			-- fixed from ABC 15 1 2010
End AS HousingCondition
FROM dbo.Resources

WHERE resName like 'Staff Housing%'
or resName like 'Teacher Housing%'
GO

