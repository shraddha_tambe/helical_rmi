SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblTeacherSurvey]
AS

Select TS.*
, case
	when TQE.ytqCertified = 1 then 1
	when TQNE.ytqCertified = 1 then 1
	else 0
end Certified
,  case
	when TQE.ytqQualified = 1 then 1
	when TQNE.ytqQualified = 1 then 1
	else 0
end Qualified
, TI.tDOB
, TI.tSex
from TeacherSurvey TS
LEFT JOIN TeacherIdentity TI
	ON TS.tID = TI.tID
LEFT JOIN SchoolSurvey SS
	ON TS.ssID = SS.ssID
LEFT JOIN lkpTeacherRole TR
	ON TS.tchRole = TR.codeCode
LEFT JOIN SurveyYearTeacherQual TQE
	ON TQE.svyYEar = SS.svyYear
	AND TQE.ytqSector = TR.secCode
	AND TQE.ytqQual = TS.tchEdQual
LEFT JOIN SurveyYearTeacherQual TQNE
	ON TQNE.svyYEar = SS.svyYear
	AND TQNE.ytqSector = TR.secCode
	AND TQNE.ytqQual = TS.tchQual
GO

