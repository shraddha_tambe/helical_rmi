SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vtblWaterSupply]
AS
SELECT
Resources.resID,
Resources.ssID,
Resources.resSplit AS WaterSupplyType,
Resources.resAvail,
Resources.resNumber,
Resources.resCondition,
Resources.resQty AS TankCapacity,
Case
	When [resCondition]= 1 Then 'Good'
	When [resCondition]= 2 Then 'Fair'
	When [resCondition]= 3 Then 'Poor'
	Else 'Null'
End AS Condition,
case
	when isnull([wsCleanSafe],0) = 0 then 0
	when resCondition > wsCleanSafe then 0
	else 1
end CleanSafe
FROM
dbo.Resources LEFT JOIN dbo.lkpWaterSupplyTypes
ON dbo.Resources.resSplit = dbo.lkpWaterSupplyTypes.wsType
WHERE ((dbo.Resources.resName='Water Supply'))
GO

