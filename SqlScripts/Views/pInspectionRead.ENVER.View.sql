SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	soomon islands specific Inspection type
-- this is an example of an inspection type that
-- stores its specific data as an Xml blob on the inspection record
-- This is loaded from a Kobo form
-- =============================================
CREATE VIEW [pInspectionRead].[ENVER]
WITH VIEW_METADATA
AS
Select S.*
, S.extraData.value('(/row/@Index)[1]','int') rowIndex

, S.extraData.value('(/row/@M0)[1]','int') M0
, S.extraData.value('(/row/@F0)[1]','int') F0
, S.extraData.value('(/row/@C0)[1]','int') C0

, S.extraData.value('(/row/@M1)[1]','int') M1
, S.extraData.value('(/row/@F1)[1]','int') F1
, S.extraData.value('(/row/@C1)[1]','int') C1

, S.extraData.value('(/row/@M2)[1]','int') M2
, S.extraData.value('(/row/@F2)[1]','int') F2
, S.extraData.value('(/row/@C2)[1]','int') C2

, S.extraData.value('(/row/@M3)[1]','int') M3
, S.extraData.value('(/row/@F3)[1]','int') F3
, S.extraData.value('(/row/@C3)[1]','int') C3

, S.extraData.value('(/row/@M4)[1]','int') M4
, S.extraData.value('(/row/@F4)[1]','int') F4
, S.extraData.value('(/row/@C4)[1]','int') C4

, S.extraData.value('(/row/@M5)[1]','int') M5
, S.extraData.value('(/row/@F5)[1]','int') F5
, S.extraData.value('(/row/@C5)[1]','int') C5

, S.extraData.value('(/row/@M6)[1]','int') M6
, S.extraData.value('(/row/@F6)[1]','int') F6
, S.extraData.value('(/row/@C6)[1]','int') C6

, S.extraData.value('(/row/@M7)[1]','int') M7
, S.extraData.value('(/row/@F7)[1]','int') F7
, S.extraData.value('(/row/@C7)[1]','int') C7

, S.extraData.value('(/row/@M8)[1]','int') M8
, S.extraData.value('(/row/@F8)[1]','int') F8
, S.extraData.value('(/row/@C8)[1]','int') C8

, S.extraData.value('(/row/@M9)[1]','int') M9
, S.extraData.value('(/row/@F9)[1]','int') F9
, S.extraData.value('(/row/@C9)[1]','int') C9

, S.extraData.value('(/row/@M10)[1]','int') M10
, S.extraData.value('(/row/@F10)[1]','int') F10
, S.extraData.value('(/row/@C10)[1]','int') C10

, S.extraData.value('(/row/@M10)[1]','int') M11
, S.extraData.value('(/row/@F10)[1]','int') F11
, S.extraData.value('(/row/@C10)[1]','int') C11

, S.extraData.value('(/row/@M10)[1]','int') M12
, S.extraData.value('(/row/@F10)[1]','int') F12
, S.extraData.value('(/row/@C10)[1]','int') C12

, S.extraData.value('(/row/@M10)[1]','int') M13
, S.extraData.value('(/row/@F10)[1]','int') F13
, S.extraData.value('(/row/@C10)[1]','int') C13

, S.extraData.value('(/row/@M10)[1]','int') M14
, S.extraData.value('(/row/@F10)[1]','int') F14
, S.extraData.value('(/row/@C10)[1]','int') C14

, S.extraData.value('(/row/@Photo0_1)[1]','int') Photo0_1
, S.extraData.value('(/row/@Photo0_2)[1]','int') Photo0_2
, S.extraData.value('(/row/@Photo0_3)[1]','int') Photo0_3

, S.extraData.value('(/row/@Photo1_1)[1]','int') Photo1_1
, S.extraData.value('(/row/@Photo1_2)[1]','int') Photo1_2
, S.extraData.value('(/row/@Photo1_3)[1]','int') Photo1_3

, S.extraData.value('(/row/@Photo2_1)[1]','int') Photo2_1
, S.extraData.value('(/row/@Photo2_2)[1]','int') Photo2_2
, S.extraData.value('(/row/@Photo2_3)[1]','int') Photo2_3

, S.extraData.value('(/row/@Photo3_1)[1]','int') Photo3_1
, S.extraData.value('(/row/@Photo3_2)[1]','int') Photo3_2
, S.extraData.value('(/row/@Photo3_3)[1]','int') Photo3_3

, S.extraData.value('(/row/@Photo4_1)[1]','int') Photo4_1
, S.extraData.value('(/row/@Photo4_2)[1]','int') Photo4_2
, S.extraData.value('(/row/@Photo4_3)[1]','int') Photo4_3

, S.extraData.value('(/row/@Photo5_1)[1]','int') Photo5_1
, S.extraData.value('(/row/@Photo5_2)[1]','int') Photo5_2
, S.extraData.value('(/row/@Photo5_3)[1]','int') Photo5_3

, S.extraData.value('(/row/@Photo6_1)[1]','int') Photo6_1
, S.extraData.value('(/row/@Photo6_2)[1]','int') Photo6_2
, S.extraData.value('(/row/@Photo6_3)[1]','int') Photo6_3

, S.extraData.value('(/row/@Photo7_1)[1]','int') Photo7_1
, S.extraData.value('(/row/@Photo7_2)[1]','int') Photo7_2
, S.extraData.value('(/row/@Photo7_3)[1]','int') Photo7_3

, S.extraData.value('(/row/@Photo8_1)[1]','int') Photo8_1
, S.extraData.value('(/row/@Photo8_2)[1]','int') Photo8_2
, S.extraData.value('(/row/@Photo8_3)[1]','int') Photo8_3

, S.extraData.value('(/row/@Photo9_1)[1]','int') Photo9_1
, S.extraData.value('(/row/@Photo9_2)[1]','int') Photo9_2
, S.extraData.value('(/row/@Photo9_3)[1]','int') Photo9_3

, S.extraData.value('(/row/@Photo10_1)[1]','int') Photo10_1
, S.extraData.value('(/row/@Photo10_2)[1]','int') Photo10_2
, S.extraData.value('(/row/@Photo10_3)[1]','int') Photo10_3

, S.extraData.value('(/row/@Photo11_1)[1]','int') Photo11_1
, S.extraData.value('(/row/@Photo11_2)[1]','int') Photo11_2
, S.extraData.value('(/row/@Photo11_3)[1]','int') Photo11_3

, S.extraData.value('(/row/@Photo12_1)[1]','int') Photo12_1
, S.extraData.value('(/row/@Photo12_2)[1]','int') Photo12_2
, S.extraData.value('(/row/@Photo12_3)[1]','int') Photo12_3

, S.extraData.value('(/row/@Photo13_1)[1]','int') Photo13_1
, S.extraData.value('(/row/@Photo13_2)[1]','int') Photo13_2
, S.extraData.value('(/row/@Photo13_3)[1]','int') Photo13_3

, S.extraData.value('(/row/@Photo14_1)[1]','int') Photo14_1
, S.extraData.value('(/row/@Photo14_2)[1]','int') Photo14_2
, S.extraData.value('(/row/@Photo14_3)[1]','int') Photo14_3

FROM pInspectionRead.SchoolInspections S
WHERE S.InspTypeCode = 'ENVER'
GO

