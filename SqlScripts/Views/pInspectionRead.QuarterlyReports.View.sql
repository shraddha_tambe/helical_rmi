SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pInspectionRead].[QuarterlyReports]
WITH VIEW_METADATA
AS
SELECT SI.inspID
	, QR.qrID
	, SI.schNo
	, S.schName
	, SI.inspStart AS StartDate
	, SI.inspEnd AS EndDate
	, SI.inspNote AS Note
	, SI.inspBy AS InspectedBy
	, QR.qrNumDaysOpen AS NumDaysOpen
	, QR.qrAggDaysAttM AS AggDaysAttM
	, QR.qrAggDaysAttF AS AggDaysAttF
	, QR.qrAggDaysAtt AS AggDaysAtt
	, QR.qrAggDaysAbsM AS AggDaysAbsM
	, QR.qrAggDaysAbsF AS AggDaysAbsF
	, QR.qrAggDaysAbs AS AggDaysAbs
	, QR.qrAggDaysMemM As AggDaysMemM
	, QR.qrAggDaysMemF As AggDaysMemF
	, QR.qrAggDaysMem As AggDaysMem
	, QR.qrAvgDailyAttM AS AvgDailyAttM
	, QR.qrAvgDailyAttF AS AvgDailyAttF
	, QR.qrAvgDailyAtt AS AvgDailyAtt
	, QR.qrAvgDailyMemM AS AvgDailyMemM
	, QR.qrAvgDailyMemF AS AvgDailyMemF
	, QR.qrAvgDailyMem AS AvgDailyMem
	, QR.qrTotEnrolToDateM AS TotEnrolToDateM
	, QR.qrTotEnrolToDateF AS TotEnrolToDateF
	, QR.qrTotEnrolToDate AS TotEnrolToDate
	, QR.qrDropoutM AS DropoutM
	, QR.qrDropoutF AS DropoutF
	, QR.qrDropout AS Dropout
	, QR.qrTrinM AS TrinM
	, QR.qrTrinF AS TrinF
	, QR.qrTrin AS Trin
	, QR.qrTroutM AS TroutM
	, QR.qrTroutF AS TroutF
	, QR.qrTrout AS Trout
	, QR.qrGrad8M AS Grad8M
	, QR.qrGrad8F AS Grad8F
	, QR.qrGrad8 AS Grad8
	, QR.qrGrad12M AS Grad12M
	, QR.qrGrad12F AS Grad12F
	, QR.qrGrad12 AS Grad12
	, QR.qrPupilEnrolLastDay AS PupilEnrolLastDay
	, ISET.inspsetName AS InspQuarterlyReport
	, ISET.inspsetType
	-- adding the changetracking fields to lookup tables means they need to be unambiguously specified with table identifier
	, QR.pEditUser
	, QR.pEditDateTime
	, QR.pCreateUser
	, QR.pCreateDateTime
	, QR.pRowversion
	, QR.pCreateTag
-- parse the quarterly report id?
FROM dbo.SchoolInspection AS SI
    LEFT OUTER JOIN
    dbo.InspectionSet AS ISET ON SI.inspsetID = ISET.inspsetID
    LEFT OUTER JOIN
    dbo.QuarterlyReport AS QR ON SI.inspID = QR.qrID
    LEFT OUTER JOIN
    dbo.lkpInspectionTypes AS IT ON ISET.inspsetType = IT.intyCode
	INNER JOIN dbo.Schools AS S ON SI.schNo = S.schNo
WHERE ISET.inspsetType = 'QUARTER';
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:  Brian Lewis
-- Create date: 12 10 2016
-- Description: Trigger on QuerterlyReportView
-- =============================================
CREATE TRIGGER [pInspectionRead].[QuarterlyReportsUpdate]
   ON  [pInspectionRead].[QuarterlyReports]
   INSTEAD OF INSERT, UPDATE, DELETE
AS 
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

 -- INSERT ------- 
 

 -- can we assume that the inspection set already exists?
 -- for sake of generality assume not for now, 
 -- but may be better data management o fail if the user supplies an invalid inspection set

 INSERT INTO InspectionSet
 (
  inspsetName
  , inspsetType
  , inspsetYear
 )
 SELECT DISTINCT INSERTED.inspQuarterlyReport
 , 'QUARTER'
 , convert(int, left(inspQuarterlyReport,4))
 From INSERTED
 LEFT JOIN InspectionSet INSPSET
  ON INSERTED.inspQuarterlyReport = INSPSET.inspsetName
 WHERE INSPSET.inspsetID is null

 -- create the school inspection record

 INSERT INTO SchoolInspection
  ( schNo
	 ,inspsetID
     ,inspStart
     ,inspEnd
     ,inspNote
     ,inspBy
	 ,pCreateUser
     ,pCreateDateTime
     ,pEditUser
     ,pEditDateTime
  )
  SELECT 
  INSERTED.schNo
  ,INSPSET.inspsetID
  ,INSERTED.StartDate
  ,INSERTED.EndDate
  ,INSERTED.Note
  ,INSERTED.InspectedBy
  ,INSERTED.pCreateUser		
  ,INSERTED.pCreateDateTime
  ,INSERTED.pCreateUser		-- edit = create on INSERT
  ,INSERTED.pCreateDateTime

 From INSERTED
  INNER JOIN InspectionSet INSPSET
  ON INSERTED.inspQuarterlyReport = INSPSET.inspsetName
  LEFT JOIN SchoolInspection SI
   ON INSPSET.inspsetID = SI.inspsetID
   AND INSERTED.schNo = SI.schNo

 WHERE INSERTED.qrID is null
  AND SI.inspID is null   -- don't create a dup here

 -- now we have all the inspIDs -- we can insert into QuarterlyReport
 INSERT INTO QuarterlyReport
 (
     qrID
        ,qrNumDaysOpen
        ,qrAggDaysAttM
        ,qrAggDaysAttF
        ,qrAggDaysAtt

        ,qrAggDaysAbsM
        ,qrAggDaysAbsF
        ,qrAggDaysAbs

        ,qrAvgDailyMemM
        ,qrAvgDailyMemF
        ,qrAvgDailyMem

        ,qrTotEnrolToDateM
        ,qrTotEnrolToDateF
        ,qrTotEnrolToDate

        ,qrDropoutM
        ,qrDropoutF
        ,qrDropout

        ,qrTrinM
        ,qrTrinF
        ,qrTrin

        ,qrTroutM
        ,qrTroutF
        ,qrTrout

        ,qrGrad8M
        ,qrGrad8F
        ,qrGrad8

        ,qrGrad12M
        ,qrGrad12F
        ,qrGrad12

        ,qrPupilEnrolLastDay

        ,pCreateUser
        ,pCreateDateTime
        ,pEditUser
        ,pEditDateTime
        ,pCreateTag
  )
 SELECT 
  SI.inspID
      ,NumDaysOpen

      ,AggDaysAttM
      ,AggDaysAttF
      ,AggDaysAtt

      ,AggDaysAbsM
      ,AggDaysAbsF
      ,AggDaysAbs

      ,AvgDailyMemM
      ,AvgDailyMemF
      ,AvgDailyMem

      ,TotEnrolToDateM
      ,TotEnrolToDateF
      ,TotEnrolToDate

      ,DropoutM
      ,DropoutF
      ,Dropout

      ,TrinM
      ,TrinF
      ,Trin

      ,TroutM
      ,TroutF
      ,Trout
      
   ,Grad8M
      ,Grad8F
      ,Grad8

      ,Grad12M
      ,Grad12F
      ,Grad12

      ,PupilEnrolLastDay
  
      ,INSERTED.pCreateUser
      ,INSERTED.pCreateDateTime
      ,INSERTED.pCreateUser		-- edit = create on insert
      ,INSERTED.pCreateDateTime
      ,INSERTED.pCreateTag
  FROM INSERTED    -- Insert statements for trigger here
 INNER JOIN InspectionSet INSPSET
  ON INSERTED.inspQuarterlyReport = INSPSET.inspsetName
 INNER JOIN SchoolInspection SI
  ON Si.inspsetID = INSPSET.inspsetID
  AND SI.schNo = INSERTED.schNo
 WHERE INSERTED.qrID is null

 -- end insert

 -- update
 -- the only thing we can update on SchoolInspection is the inspsetID - if we change the QR to a different quarter ( a correction)
 UPDATE SchoolInspection
  SET inspsetID = INSPSET.inspsetID
  ,inspStart = INSERTED.StartDate
  ,inspEnd = INSERTED.EndDate
  ,inspNote = INSERTED.Note
  ,inspBy = INSERTED.InspectedBy
  ,pEditUser = INSERTED.pEditUser
  ,pEditDateTime = INSERTED.pEditDateTime
 FROM SchoolInspection
 
 INNER JOIN INSERTED
  ON SchoolInspection.inspID = INSERTED.inspID
 INNER JOIN DELETED 
  ON INSERTED.inspID = DELETED.inspID
 INNER JOIN InspectionSet INSPSET
  ON INSERTED.inspQuarterlyReport = INSPSET.inspsetName


 -- update QuarterlyReport
 
 UPDATE QuarterlyReport
    SET
      qrNumDaysOpen = I.NumDaysOpen

      ,qrPupilEnrolLastDay = I.PupilEnrolLastDay

      ,qrAggDaysAttM = I.AggDaysAttM
      ,qrAggDaysAttF = I.AggDaysAttF
      ,qrAggDaysAtt = I.AggDaysAtt
      
   ,qrAggDaysAbsM = I.AggDaysAbsM
      ,qrAggDaysAbsF = I.AggDaysAbsF
      ,qrAggDaysAbs = I.AggDaysAbs
      
   ,qrAvgDailyMemM = I.AvgDailyMemM
      ,qrAvgDailyMemF = I.AvgDailyMemF
      ,qrAvgDailyMem = I.AvgDailyMem

   ,qrTotEnrolToDateM = I.TotEnrolToDateM
      ,qrTotEnrolToDateF = I.TotEnrolToDateF
      ,qrTotEnrolToDate = I.TotEnrolToDate
      
   ,qrDropoutM = I.DropoutM
      ,qrDropoutF = I.DropoutF
      ,qrDropout = I.Dropout
      
   ,qrTrinM = I.TrinM
      ,qrTrinF = I.TrinF
      ,qrTrin = I.Trin
      
   ,qrTroutM = I.TroutM
      ,qrTroutF = I.TroutF
      ,qrTrout = I.Trout
      
   ,qrGrad8M = I.Grad8M
      ,qrGrad8F = I.Grad8F
      ,qrGrad8 = I.Grad8
      
   ,qrGrad12M = I.Grad12M
      ,qrGrad12F = I.Grad12F
      ,qrGrad12 = I.Grad12
      
      ,pEditUser = I.pEditUser
      ,pEditDateTime = I.pEditDateTime

 FROM QuarterlyReport
 INNER JOIN INSERTED I
  ON QuarterlyReport.qrID = I.qrID
 INNER JOIN DELETED D
  ON I.qrID = D.qrID

 -- end update

 -- delete

 DELETE FROM QuarterlyReport
 FROM QuarterlyReport
 INNER JOIN DELETED D
  ON QuarterlyReport.qrID = D.qrID
 LEFT JOIN INSERTED I
  ON D.qrID = I.qrID
 WHERE I.qrID is null

 DELETE FROM SchoolInspection
 FROM SchoolInspection
 INNER JOIN DELETED D
  ON SchoolInspection.inspID = D.qrID
 LEFT JOIN INSERTED I
  ON D.qrID = I.qrID
 WHERE I.qrID is null

 -- end delete

END
GO

