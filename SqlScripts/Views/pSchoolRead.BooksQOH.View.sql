SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pSchoolRead].[BooksQOH]
WITH VIEW_METADATA
AS

Select
bkCode
, sum(bkcntQty) QOH
, sum(bkcntReplace) QOHReplace
FROM
(
Select
ROW_NUMBER() OVER(PARTITION BY bkcntLocType, bkcntLoc, bkCode ORDER BY bkcntDate DESC) row
, bkcntLocType, bkcntLoc, bkCode, bkcntQty, bkcntReplace
FRom BookCount BC
) SUB
WHERE sub.row = 1
GROUP BY bkCode
GO

