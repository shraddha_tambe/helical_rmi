SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pSchoolRead].[PIVParentCommitteeSub]
AS
SELECT     ssID, ssParentCommittee,ssPCMeet,ssPCSupport,codeDescription

FROM     dbo.SchoolSurvey SS

		INNER JOIN dbo.lkpPCSupport PC
		ON PC.codeNum = SS.ssPCSupport
GO

