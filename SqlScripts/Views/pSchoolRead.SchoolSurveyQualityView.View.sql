SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pSchoolRead].[SchoolSurveyQualityView]
AS
SELECT
QA.*
, SS.svyYear
, SS.schNo
, S.schName
, S.iCode
, I.iGroup dID
, SYH.systCode schType
, SYH.syAuth ssAuth
FROM SchoolSurveyquality QA
INNER JOIN SchoolSurvey SS
	ON QA.ssID = SS.ssId
INNER JOIN Schools S
	ON SS.schNo = S.schNo
INNER JOIN Islands I
	ON S.iCode = I.iCode
INNER JOIN SchoolYearHistory SYH
	ON SS.svyYear = SYH.syYear
		AND SS.schNo = SYH.schNo
GO

