SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pSurveyRead].[SchoolTypeTemplates]
AS
Select stCode SchoolType, ytEForm Template, svyYear SurveyYear
FROM SurveyYEarSchoolTypes
WHERE svyYEar = (Select max(svyYEar) from Survey)
GO

