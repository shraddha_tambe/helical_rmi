SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [pTeacherRead].[TeacherLinks]
WITH VIEW_METADATA
AS
SELECT lnkID
      ,TL.tID
      ,TL.docID
      ,lnkFunction
      ,lnkHidden
      ,pCreateTag
      ,docTitle
      ,docDescription
      ,docSource
      ,docDate
      ,docRotate
      ,docTags
      ,docType
      ,D.pCreateUser
      ,D.pCreateDateTime
      ,D.pEditUser
      ,D.pEditDateTime
      ,D.pRowversion
	  , TI.tPhoto
	  , case when TI.tPhoto = D.docID then 1 else 0 end isCurrentPhoto
from DocumentLinks_ TL
	INNER JOIN Documents_ D
		ON TL.docID = D.docID
	INNER JOIN TeacherIdentity TI
		ON TI.tID = TL.tID
	WHERE TL.tID is not null
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [pTeacherRead].[TeacherLinksUpdate]
   ON  [pTeacherRead].[TeacherLinks]
   INSTEAD OF INSERT, UPDATE, DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	-- INSERT
	-- if there is no docID on a record, create it

	declare @datestamp datetime 
	Select @datestamp = getdate()

		-- insert any new docIDs
	INSERT INTO [dbo].[Documents_]
			   ([docID]
			   ,[docTitle]
			   ,[docDescription]
			   ,[docSource]
			   ,[docDate]
			   ,[docRotate]
			   ,[docTags]
			   , pCreateUser
			   , pCreateDateTime
			   , pEditUser
			   , pEditDateTime
			   )
	SELECT         
				I.docID
			   ,I.[docTitle]
			   ,I.[docDescription]
			   ,I.[docSource]
			   ,I.[docDate]
			   ,isnull(I.[docRotate],0)
			   ,I.[docTags]
			   , isnull(I.pCreateUser, original_login())
			   , isnull(I.pCreateDateTime, @datestamp)
			   , isnull(I.pEditUser, original_login())
			   , isnull(I.pEditDateTime, @datestamp)
		FROM INSERTED I
		LEFT JOIN Documents_ DOC
			ON I.docID = DOC.docID
		WHERE DOC.docID is null
		
		-- update exisitng document records
		UPDATE Documents_
		SET docTitle = I.docTitle
		, docDescription = I.docDescription
		, docSource = I.docSource
		, docRotate = isnull(I.docRotate, Documents_.docRotate)
		, pCreateUser = coalesce(I.pCreateUser, Documents_.pCreateUser, original_login())
		, pCreateDateTime = coalesce(I.pCreateDateTime, Documents_.pCreateDateTime, @datestamp)
		, pEditUser = case when update (pEditUser) then isnull(I.pEditUser, original_login()) else original_login() end
		, pEditDateTime = case when update(pEditDateTime) then isnull(I.pEditDateTime, @datestamp) else @datestamp end

		FROM Documents_
			INNER JOIN INSERTED I
				ON Documents_.docId = I.docID

		if update (pEditDateTime) 
			print 'pEditDateTime TRUE'
		else
			print 'pEditDateTime FALSE'

	-- insert to TeacherLinks_
	INSERT INTO DocumentLinks_
	(
		tID
		, docID
		, lnkFunction
		, pCreateTag
	)
	SELECT tID
	, docID
	, lnkFunction
	, pCreateTag
	FROM INSERTED
		WHERE lnkID is null

	-- UPDATE to TeacherLinks_

	UPDATE DocumentLinks_
	SET tID = I.tID
	, docID = I.docID
	, lnkFunction = I.lnkFunction
	FROM INSERTED I
	WHERE I.lnkID = DocumentLinks_.lnkID

	-- DELETE FROM  TeacherLinks
	DELETE
	FRom DocumentLinks_
	FROM DocumentLinks_
	INNER JOIN DELETED
		ON DocumentLinks_.lnkId = DELETED.lnkID
	WHERE DocumentLinks_.lnkID not in 
	(Select lnkID from INSERTED)

	-- update the current photo ID on the teacher record if required
	-- 
	UPDATE TeacherIdentity
		SET tPhoto = docID
	FROM TeacherIdentity
	INNER JOIN INSERTED 
		ON TeacherIdentity.tID = INSERTED.tID
	WHERE (
	INSERTED.isCurrentPhoto = 1 
	-- always update if this is a photo, and there is non currenently recorded
	OR ( INSERTED.lnkFunction = 'PORTRAIT' and TeacherIdentity.tPhoto is null)
	)
END

GO

