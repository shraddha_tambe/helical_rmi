SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - District Ed Level Age
--
-- District enrolments divideded by class level, showing whether the pupil is at, over or under
-- the EdLevel OfficialAge
-- The year of ed of the class level determines the EdLevel here, and the official age range
-- is determined in the usual way (from Offical Start Age and Range of years of Ed for the Ed Level)
-- Suggested Use: High Level analysis of over / under age enrolments.
-- Note this view uses only the principal education levels defined in lkpEducationLevels
-- It does not use the Alt or Alt2 groupings (to do?)
-- see also:
-- warehouse.NationEdLevelAge
-- =============================================
CREATE VIEW [warehouse].[DistrictEdLevelAge]
AS
Select surveyYear, ClassLevel, districtCode, yearOfEd, EdLevel, GenderCode
, sum(case when EdLevelOfficialAge = 'UNDER' then Enrol else null end ) UnderAge
, sum(case when EdLevelOfficialAge = '=' then Enrol else null end ) OfficialAge
, sum(case when EdLevelOfficialAge = 'OVER' then Enrol else null end ) OverAge
, sum(Enrol) Enrol
, sum(case when Estimate = 1 then Enrol else null end ) EstimatedEnrol
From Warehouse.enrolmentRatios
GROUP BY
surveyYear, ClassLevel, districtCode, yearOfEd, EdLevel, GenderCode
GO

