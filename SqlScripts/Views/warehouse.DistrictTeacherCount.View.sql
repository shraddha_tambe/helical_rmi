SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Teacher Counts
--
-- Teacher counts aggregated by district and sector.
-- Derived from warehouse.schoolTeacherCount table.
-- =============================================
CREATE VIEW [warehouse].[DistrictTeacherCount]
AS
Select SurveyYear
, DistrictCode
, GenderCode
, AgeGroup
, Sector
-- this logic assumes that the column 'Support' is a text field in SchoolTeacherCount
-- and that records are disaggregated between 'Support' and (null) values - ie teaching staff
-- warehouse.schoolTeacherCount shoould be changed so that Support is the NUMBER of support staff in the group, and NumTeachers
-- Certified, Qualified Certqual do NOT include non-teaching staff
-- When this change is made, these sums become once again simple aggregates of the underlying table
-- see Issue #518

, sum(case when Support is not null then NumTeachers else null end) Support
, sum(case when Support is null then NumTeachers else null end) NumTeachers
, sum(case when Support is null then Certified else null end) Certified
, sum(case when Support is null then Qualified else null end) Qualified
, sum(case when Support is null then CertQual else null end) CertQual
from warehouse.schoolTeacherCount
GROUP BY SurveyYear,  DistrictCode, GenderCode, Sector, AgeGroup
GO

