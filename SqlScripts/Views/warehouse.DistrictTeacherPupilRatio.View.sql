SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2018
-- Description:	Warehouse - Teacher Pupil Ratio
--
-- Calculating Pupil Teacher Ratios requires having the Enrolment values on the same row as the
-- teacher numbers.
-- this view splits by sector and aggregates up to District

-- =============================================
CREATE VIEW [warehouse].[DistrictTeacherPupilRatio]
WITH VIEW_METADATA
AS

Select SurveyYear
, DistrictCode
, dName District
, Sector
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(certQual) CertQual
, sum(Enrol) Enrol
from warehouse.SchoolTeacherPupilRatio TPR
	LEFT JOIN Districts D
	ON DistrictCode = D.dID
GROUP BY SurveyYear
, DistrictCode
, dName
, Sector
GO

