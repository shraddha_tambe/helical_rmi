SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Over and Under Age reporting
--
-- Using enrolment data and definitions of age ranges for education levels,
-- Calculate the numbers:
-- OVER Age (ie > ed level age range)
-- UNDER age (< ed level age range)
-- AT Age (at offical age range)
-- AT age is the same as nEnrol on Enrolment Ratio views.
-- Note that OVERS are over the education level range; a pupil may be OVERAGE for their class level,
-- but still within the age range for the education level
--
-- =============================================
CREATE VIEW [warehouse].[EdLevelAge]
AS
Select surveyYear
	, ClassLevel
	, districtCode
	, authorityCode
	, SchoolTypecode
	, yearOfEd
	, EdLevel
	, GenderCode
	, sum(case when EdLevelOfficialAge = 'UNDER' then Enrol else null end ) UnderAge
	, sum(case when EdLevelOfficialAge = '=' then Enrol else null end ) OfficialAge
	, sum(case when EdLevelOfficialAge = 'OVER' then Enrol else null end ) OverAge
	, sum(Enrol) Enrol
	, sum(case when Estimate = 1 then Enrol else null end ) EstimatedEnrol
	From Warehouse.enrolmentRatios
	GROUP BY
	surveyYear, ClassLevel, districtCode, authorityCode, SchoolTypecode, yearOfEd, EdLevel, GenderCode
GO

