SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Exam Results
--
-- Exam results are based on the "standard-benchmark-achievement level" hierarchy.
-- Variants of presentation are
-- consolidation: national district (state) or school levelreporting by benchmark or consoilidated to standard
-- normalised or "crosstabbed"
-- In the cross tabbed versions up to 10 achievement levels are presented on a single record.
-- This is more amenable to some chart presentations.
--
-- The datasets are:
-- ExamNationResults - national consolidation
-- ExamNationResultsX - national consolidation crosstabbed
-- ExamNationStandards - national consolidation by standard
-- ExamNationStandardsX - national consolidation by standard - crosstabbed
-- ExamStateResults - district consolidation (table)
-- ExamStateResultsX - district consolidation crosstabbed
-- ExamStateStandards - district consolidation by standard
-- ExamStateStandardsX - district consolidation by standard - crosstabbed
-- ExamSchoolResults - school level reporting (table)
-- ExamSchoolResultsX - school level reporting crosstabbed
-- ExamSchooltandards - school level reporting by standard
-- ExamSchoolStandardsX - school level reporting by standard - crosstabbed
--
-- All views are built from the 2 tables ExamStateREsults and ExamSchoolResults
-- These are created by the stored proc warehouse.buildExamResults
-- Note that this SP is NOT called from the warehouse.buildWarehouse function,
-- but instead is involed as part of the upload of each exam file.
-- =============================================
CREATE VIEW [warehouse].[ExamNationStandards]
AS
Select examID
, examCode
, examYear
, examName
, Gender
, standardID
, standardCode
, standardDesc
, achievementLevel
, achievementDesc
, sum(Candidates) Candidates
from warehouse.ExamStateResults
GROUP BY examID
, examCode
, examYear
, examName
, Gender
, standardID
, standardCode
, standardDesc
, achievementLevel
, achievementDesc
GO

