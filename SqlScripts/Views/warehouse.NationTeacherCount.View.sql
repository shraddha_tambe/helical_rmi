SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2017
-- Description:	Warehouse - Teacher Counts
-- Teacher counts aggregated to national level, split by sector age group and gender
-- USefuk for pivot table analysis or simple presentation of teacher demographics e.g.
-- Teacher supply and demand
-- 2 10 2018 now based on DistrictTeacherCount to isloate potential change to SchoolTeacherCount table
-- see Issue #518
-- =============================================
CREATE VIEW [warehouse].[NationTeacherCount]
AS
Select SurveyYear
, GenderCode
, AgeGroup
, Sector
, sum(Support) Support
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual
from warehouse.DistrictTeacherCount
GROUP BY SurveyYear,  GenderCode, Sector, AgeGroup
GO

