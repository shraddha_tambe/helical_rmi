SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2018
-- Description:	Warehouse - Teacher Pupil Ratio
--
-- Calculating Pupil Teacher Ratios requires having the Enrolment values on the same row as the
-- teacher numbers.
-- this view splits by sector and aggregates up to Nation
-- =============================================
CREATE VIEW [warehouse].[NationTeacherPupilRatio]
WITH VIEW_METADATA
AS

Select SurveyYear
, Sector
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(certQual) CertQual
, sum(Enrol) Enrol
from warehouse.SchoolTeacherPupilRatio TPR
GROUP BY SurveyYear
, Sector
GO

