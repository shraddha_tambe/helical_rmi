SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date:
-- Description:	School Flow
--
-- Wrapper around warehouse.cohort table to include flow calculations at the school level
-- ie PromoteRate, RepeatRate, SurvivalRate (single year survival aka "transition rate")
-- This version is intended to be used in reports, use nationCohort for "cubes"
-- (pivot tables, tableau) where ratios should be calculated based on current aggregations.
-- TRANSFER versions:
-- field named xxxxTR calculate the same ratios but attempt to take into account internal transfers
-- These should be used with caution as internal transfer data may not be very reliable
-- See related views:
-- warehouse.districtCohort
-- warehouse.districtFlow
-- warehouse.nationCohort
-- warehouse.nationFlow
-- warehouse.cohort (table)
-- At the school level, data may prove too variable to get consistnet result for flow indicators.
-- =============================================
CREATE VIEW [warehouse].[SchoolFlow]
WITH VIEW_METADATA
AS

Select S.*
, 1 - RepeatRate - PromoteRate DropoutRate
, 1 - RepeatRateTR - PromoteRateTR DropoutRateTR
, case when RepeatRate = 1 then null else PromoteRate / (1 - RepeatRate) end SurvivalRate
, case when RepeatRateTR = 1 then null else PromoteRateTR / (1 - RepeatRateTR) end SurvivalRateTR
FROM
(
	Select C.*
	, case when Enrol is null then null
		else convert(float,RepNY) /Enrol end RepeatRate
	, case when ( Enrol - isnull(TroutNY,0)) is null then null
		else convert(float,RepNY) / Enrol - isnull(TroutNY,0) end RepeatRateTR
	, case when Enrol is null then null
		else ( convert(float,EnrolNYNextLevel) - RepNYNextLevel)  / Enrol end PromoteRate
	, case when ( Enrol - isnull(TroutNY,0)) is null then null
			else ( convert(float,EnrolNYNextLevel) - RepNYNextLevel - - isnull(TrinNYNextLevel,0))  / ( Enrol - isnull(TroutNY,0)) end PromoteRateTR
	from warehouse.SchoolCohort C

) S
GO

