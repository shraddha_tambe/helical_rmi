SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2018
-- Description:	Warehouse - Teacher Pupil Ratio
--
-- Calculating Pupil Teacher Ratios requires having the Enrolment values on the smae row as the
-- teacher numbers.
-- this view splits by sector within school where applicable,
-- and allow aggregations up from school
-- =============================================
CREATE VIEW [warehouse].[SchoolTeacherPupilRatio]
WITH VIEW_METADATA
AS

Select schNo
, SurveyYear
, DistrictCode
, AuthorityCode
, SchoolTypeCode
, Sector
, sum(numTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual
, sum(Enrol) Enrol
FROM
(
Select schNo
, SurveyYear
, DistrictCode
, AuthorityCode
, SchoolTypeCode
, Sector
, NumTeachers
, Certified
, Qualified
, CertQual
, convert(int, null) Enrol
From warehouse.schoolTeacherCount

--- IMPORTANT now using Support is null to identify 'academic' staff
--- so be careful cross checking the teacher numbers in here against other warehouse queries
--- that may include Support staff
WHERE Support is null

UNION ALL
	Select E.schNo, E.surveyYear
	, DSS.[District Code] DistrictCode
	, DSS.AuthorityCode
	, DSS.SchoolTypeCode
	, L.secCode Sector
	, null NumTeachers
	, null Certified
	, null Qualified
	, null CertQual
	, Enrol
	from warehouse.enrol E
		INNER JOIN lkpLevels L
			ON E.ClassLevel = L.codeCode
		INNER JOIN Warehouse.bestSurvey S
			ON S.schNo = E.schNo
			AND S.SurveyYear = E.surveyYear
		LEFT JOIN Warehouse.dimensionSchoolSurvey DSS
			ON Dss.[Survey ID] = S.surveyDimensionID
) U
GROUP BY
schNo
, SurveyYear
, DistrictCode
, AuthorityCode
, SchoolTypeCode
, Sector
GO

