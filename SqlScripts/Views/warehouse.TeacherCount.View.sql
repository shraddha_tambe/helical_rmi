SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brian Lewis
-- Create date: 2018
-- Description:	Warehouse - Teacher Count
--
-- Top level reporting view for teacher numbers:
-- Shows total number of teachers, number certified, number qualified, number both certified and qualified
-- This is a wrapper around warehouse.schoolTeacherCount, which has the same data by school
--
-- Aggregated up to school type, authority, district and sector
-- as well as teacher age bracket and gender
-- Includes names as well as codes for agregation fields,
-- and includes all Authority dimension; ie including Govt / Non-Govt (Public /Private)
--
-- When used in a cube, this allows aggregation on any of these dimensions.
-- =============================================
CREATE VIEW [warehouse].[TeacherCount]
AS
Select W.*
, A.Authority
, A.AuthorityTypeCode
, A.AuthorityType
, A.AuthorityGroupCode
, A.AuthorityGroup
, T.stDescription SchoolType
, D.dName District
FROM
(
Select SurveyYear
, DistrictCode
, SchoolTypeCode
, AuthorityCode
, GenderCode
, AgeGroup
, Sector
, Support
, sum(NumTeachers) NumTeachers
, sum(Certified) Certified
, sum(Qualified) Qualified
, sum(CertQual) CertQual
from warehouse.schoolTeacherCount
GROUP BY SurveyYear,  DistrictCode, SchoolTypeCode, AuthorityCode, GenderCode, Sector, Support, AgeGroup
) W
LEFT JOIN Districts D
	ON W.DistrictCode = D.dID
LEFT JOIN SchoolTypes T
	ON W.SchoolTypeCode = T.stCode
LEFT JOIN DimensionAuthority A
	ON W.AuthorityCode = A.authorityCode
GO

